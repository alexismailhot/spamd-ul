package ca.ulaval.glo4003.projet.base.ws.access.api.response;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;

public class AccessCodeResponseAssembler {

    public AccessCodeResponse toResponse(AccessCode accessCode) {
        return new AccessCodeResponse(accessCode.getCodeNumber());
    }
}
