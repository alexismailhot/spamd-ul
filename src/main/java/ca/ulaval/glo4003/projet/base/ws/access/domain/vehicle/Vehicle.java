package ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle;

import java.util.Objects;

public class Vehicle {

    private final String model;
    private final String brand;
    private final Integer modelYear;
    private final String plateNumber;
    private final VehicleType vehicleType;

    public Vehicle(String model, String brand, Integer modelYear, String plateNumber, VehicleType vehicleType) {
        this.model = model;
        this.brand = brand;
        this.modelYear = modelYear;
        this.plateNumber = plateNumber;
        this.vehicleType = vehicleType;
    }

    public boolean isModelYearBefore(Integer otherVehicleYear) {
        return modelYear < otherVehicleYear;
    }

    public boolean isModelYearAfter(Integer otherVehicleYear) {
        return modelYear > otherVehicleYear;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return (
            Objects.equals(model, vehicle.model) &&
            Objects.equals(brand, vehicle.brand) &&
            Objects.equals(modelYear, vehicle.modelYear) &&
            Objects.equals(plateNumber, vehicle.plateNumber) &&
            vehicleType == vehicle.vehicleType
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, brand, modelYear, plateNumber, vehicleType);
    }
}
