package ca.ulaval.glo4003.projet.base.ws.initiative.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InitiativeFundingRequest {

    private final double amount;

    @JsonCreator
    public InitiativeFundingRequest(@JsonProperty(value = "amount", required = true) double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }
}
