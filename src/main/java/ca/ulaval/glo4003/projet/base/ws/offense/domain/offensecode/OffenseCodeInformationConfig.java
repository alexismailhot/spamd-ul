package ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OffenseCodeInformationConfig {

    private final Map<OffenseCode, OffenseCodeInformation> offenseCodeInformationByCode = new HashMap<>();

    public OffenseCodeInformationConfig(List<OffenseCodeInformation> offenseCodeInformations) {
        offenseCodeInformations.forEach(
            offenseCodeInformation -> this.offenseCodeInformationByCode.put(offenseCodeInformation.getOffenseCode(), offenseCodeInformation)
        );
    }

    public OffenseCodeInformation getOffenseCodeInformation(OffenseCode offenseCode) {
        return this.offenseCodeInformationByCode.get(offenseCode);
    }
}
