package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.util.Objects;

public class SessionParkingPassInformation extends ParkingPassInformation {

    private final Session session;

    public SessionParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod, String email, Session session) {
        super(parkingZone, deliveryMethod, email);
        this.session = session;
    }

    public SessionParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod, PostalAddress postalAddress, Session session) {
        super(parkingZone, deliveryMethod, postalAddress);
        this.session = session;
    }

    public SessionParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod, Session session) {
        super(parkingZone, deliveryMethod);
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SessionParkingPassInformation that = (SessionParkingPassInformation) o;
        return Objects.equals(session, that.session);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), session);
    }
}
