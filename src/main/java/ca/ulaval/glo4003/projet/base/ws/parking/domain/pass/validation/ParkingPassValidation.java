package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

public class ParkingPassValidation {

    private final ParkingCode parkingCode;
    private final LocalDateTime timeIssued;
    private final ParkingZone parkingZone;

    public ParkingPassValidation(ParkingCode parkingCode, LocalDateTime timeIssued, ParkingZone parkingZone) {
        this.parkingCode = parkingCode;
        this.timeIssued = timeIssued;
        this.parkingZone = parkingZone;
    }

    public Optional<ParkingCode> getParkingCode() {
        return Optional.ofNullable(parkingCode);
    }

    public LocalDateTime getTimeIssued() {
        return timeIssued;
    }

    public ParkingZone getParkingZone() {
        return parkingZone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingPassValidation that = (ParkingPassValidation) o;
        return Objects.equals(parkingCode, that.parkingCode) && Objects.equals(timeIssued, that.timeIssued) && parkingZone == that.parkingZone;
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingCode, timeIssued, parkingZone);
    }
}
