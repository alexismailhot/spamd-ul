package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;

public interface ParkingPassRatesConfig {
    Price getParkingPassRate(ParkingZone parkingZone, ParkingPassType parkingPassType);
}
