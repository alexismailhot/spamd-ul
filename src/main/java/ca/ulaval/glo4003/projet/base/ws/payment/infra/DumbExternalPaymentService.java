package ca.ulaval.glo4003.projet.base.ws.payment.infra;

import ca.ulaval.glo4003.projet.base.ws.money.api.CapitalFormatter;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;

public class DumbExternalPaymentService implements PaymentService {

    @Override
    public void processPayment(CreditCard creditCard, Price price) {
        System.out.printf(
            "[SPAMD-UL] Payment processed (%.2f$) for the following credit card: %s%n",
            CapitalFormatter.fromBigDecimal(price.getAmount()),
            creditCard.toString()
        );
    }
}
