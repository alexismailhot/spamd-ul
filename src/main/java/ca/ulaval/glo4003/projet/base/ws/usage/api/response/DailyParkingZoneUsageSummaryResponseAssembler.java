package ca.ulaval.glo4003.projet.base.ws.usage.api.response;

import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;

public class DailyParkingZoneUsageSummaryResponseAssembler {

    public DailyParkingZoneUsageSummaryResponse toResponse(ParkingZoneUsageSummary parkingZoneUsageSummary) {
        return new DailyParkingZoneUsageSummaryResponse((int) parkingZoneUsageSummary.getAverageUsage(), parkingZoneUsageSummary.getZone());
    }
}
