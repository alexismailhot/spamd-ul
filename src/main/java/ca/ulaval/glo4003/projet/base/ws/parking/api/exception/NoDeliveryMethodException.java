package ca.ulaval.glo4003.projet.base.ws.parking.api.exception;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;

public class NoDeliveryMethodException extends RuntimeException {
    private static final String NO_DELIVERY_METHOD = "NO_DELIVERY_METHOD";

    private final DeliveryMethod deliveryMethod;

    public NoDeliveryMethodException(DeliveryMethod deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getError() {
        return NO_DELIVERY_METHOD;
    }

    public String getDescription() {
        return String.format("The information for chosen delivery method : %s was not provided", deliveryMethod.toString());
    }
}
