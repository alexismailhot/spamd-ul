package ca.ulaval.glo4003.projet.base.ws.offense.api;

import ca.ulaval.glo4003.projet.base.ws.offense.api.request.OffenseIdRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.PayOffenseRequest;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationRequest;
import ca.ulaval.glo4003.projet.base.ws.offense.api.response.OffenseResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.service.OffenseService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import javax.ws.rs.core.Response;

public class OffenseResourceImpl implements OffenseResource {

    private static final Response.ResponseBuilder CREATED_RESPONSE_BUILDER = Response.status(Response.Status.CREATED);

    private final OffenseService offenseService;
    private final OffenseResponseAssembler offenseResponseAssembler;
    private final CreditCardAssembler creditCardAssembler;
    private final ParkingPassValidationAssembler parkingPassValidationAssembler;
    private final OffenseIdRequestAssembler offenseIdRequestAssembler;

    public OffenseResourceImpl(
        OffenseService offenseService,
        OffenseResponseAssembler offenseResponseAssembler,
        CreditCardAssembler creditCardAssembler,
        ParkingPassValidationAssembler parkingPassValidationAssembler,
        OffenseIdRequestAssembler offenseIdRequestAssembler
    ) {
        this.offenseService = offenseService;
        this.offenseResponseAssembler = offenseResponseAssembler;
        this.creditCardAssembler = creditCardAssembler;
        this.parkingPassValidationAssembler = parkingPassValidationAssembler;
        this.offenseIdRequestAssembler = offenseIdRequestAssembler;
    }

    @Override
    public Response createParkingOffenseIfOffenseWasCommitted(ParkingPassValidationRequest parkingPassValidationRequest) {
        return offenseService
            .createOffenseIfParkingPassOffenseWasCommitted(parkingPassValidationAssembler.toDomain(parkingPassValidationRequest))
            .map(offenseResponseAssembler::toResponse)
            .map(CREATED_RESPONSE_BUILDER::entity)
            .orElse(Response.ok())
            .build();
    }

    @Override
    public Response payOffense(PayOffenseRequest payOffenseRequest) {
        offenseService.payOffense(
            creditCardAssembler.toDomain(payOffenseRequest.getCreditCardRequest()),
            offenseIdRequestAssembler.toDomain(payOffenseRequest.getOffenseId())
        );
        return Response.ok().build();
    }
}
