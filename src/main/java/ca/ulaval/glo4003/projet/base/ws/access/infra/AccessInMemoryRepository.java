package ca.ulaval.glo4003.projet.base.ws.access.infra;

import ca.ulaval.glo4003.projet.base.ws.access.domain.Access;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.access.domain.AccessRepository;

import java.util.HashMap;
import java.util.Optional;

public class AccessInMemoryRepository implements AccessRepository {

    private final HashMap<AccessCode, Access> accesses = new HashMap<>();

    @Override
    public void save(Access access) {
        accesses.put(access.getAccessCode(), access);
    }

    @Override
    public int getCount() {
        return accesses.size();
    }

    @Override
    public Optional<Access> findByAccessCode(AccessCode accessCode) {
        return Optional.ofNullable(accesses.get(accessCode));
    }
}
