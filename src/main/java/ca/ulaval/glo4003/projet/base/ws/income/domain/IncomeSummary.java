package ca.ulaval.glo4003.projet.base.ws.income.domain;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class IncomeSummary {

    private final long sales;
    private final Price totalIncome;

    public IncomeSummary(long sales, Price totalIncome) {
        this.sales = sales;
        this.totalIncome = totalIncome;
    }

    public long getSales() {
        return sales;
    }

    public Price getTotalIncome() {
        return totalIncome;
    }
}
