package ca.ulaval.glo4003.projet.base.ws.usage.api;

import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/usage")
public interface UsageResource {
    @GET
    @Path("parking")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get parking usage statistics for all parking zones usage between from and until. If no" +
        "from or until provided, then default value is today's date.",
        responses = { @ApiResponse(responseCode = "200"), @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info") }
    )
    Response getAllZonesPeriodUsage(@QueryParam("from") LocalDate fromDate, @QueryParam("until") LocalDate untilDate);

    @GET
    @Path("parking/monthly")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get parking usage statistics for all parking zones for a given month of a given year. If no month or year" +
        "provided, then month is current month and year is current year.",
        responses = { @ApiResponse(responseCode = "200"), @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info") }
    )
    Response getAllZonesMonthlyUsage(@QueryParam("month") Month month, @QueryParam("year") Year year);

    @GET
    @Path("parking/daily")
    @Produces(MediaType.APPLICATION_JSON)
    Response getAllZonesDailyUsage(@QueryParam("date") LocalDate date);

    @GET
    @Path("parking/zone/{zone}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get parking usage statistics for a specific parking zone between from and until. If no" +
        "from or until provided, then default value is today's date.",
        responses = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info"),
            @ApiResponse(responseCode = "404", description = "Invalid Parking Zone"),
        }
    )
    Response getPeriodUsage(@PathParam("zone") String zone, @QueryParam("from") LocalDate from, @QueryParam("until") LocalDate until);

    @POST
    @Path("parking")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Add usage for parking zone",
        responses = {
            @ApiResponse(responseCode = "201", description = "Parking entry was created"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info"),
            @ApiResponse(responseCode = "404", description = "Invalid Parking Zone"),
        }
    )
    Response addUsage(AddUsageRequest request);

    @GET
    @Path("parking/zone/{zone}/daily")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get daily usage statistics for a parking zone of a given date. If no date provided then date is" + "today's date",
        responses = {
            @ApiResponse(responseCode = "201", description = "Parking entry was created"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info"),
            @ApiResponse(responseCode = "404", description = "Invalid Parking Zone"),
        }
    )
    Response getDailyUsage(@PathParam("zone") String zone, @QueryParam("date") LocalDate date);

    @GET
    @Path("parking/zone/{zone}/monthly")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get parking usage statistics for specific zone for a given month of a given year. If no month or year" +
        "provided, then month is current month and year is current year.",
        responses = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info"),
            @ApiResponse(responseCode = "404", description = "Invalid Parking Zone"),
        }
    )
    Response getMonthlyUsage(@PathParam("zone") String zone, @QueryParam("month") Month month, @QueryParam("year") Year year);
}
