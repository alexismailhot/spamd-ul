package ca.ulaval.glo4003.projet.base.ws.access.domain.income;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class AccessIncomeFactory {

    private final Clock clock;

    public AccessIncomeFactory(Clock clock) {
        this.clock = clock;
    }

    public AccessIncome create(Price price, VehicleType vehicleType) {
        LocalDateTime created = clock.instant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return new AccessIncome(price, vehicleType, created);
    }
}
