package ca.ulaval.glo4003.projet.base.ws.parking.domain;

public enum DeliveryMethod {
    EMAIL,
    POSTAL,
    SSP,
}
