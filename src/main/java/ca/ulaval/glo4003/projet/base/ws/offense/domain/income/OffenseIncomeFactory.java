package ca.ulaval.glo4003.projet.base.ws.offense.domain.income;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class OffenseIncomeFactory {

    private final Clock clock;

    public OffenseIncomeFactory(Clock clock) {
        this.clock = clock;
    }

    public OffenseIncome create(Price price) {
        LocalDateTime created = clock.instant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return new OffenseIncome(price, created);
    }
}
