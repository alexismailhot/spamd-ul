package ca.ulaval.glo4003.projet.base.ws.income.api.response.assembler;

import ca.ulaval.glo4003.projet.base.ws.income.api.response.IncomeSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummary;
import ca.ulaval.glo4003.projet.base.ws.money.api.CapitalFormatter;

public class IncomeSummaryResponseAssembler {

    public IncomeSummaryResponse toResponse(IncomeSummary incomeSummary) {
        return new IncomeSummaryResponse(CapitalFormatter.fromBigDecimal(incomeSummary.getTotalIncome().getAmount()), incomeSummary.getSales());
    }
}
