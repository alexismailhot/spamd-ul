package ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidationBuilder;

public class ParkingPassValidationAssembler {

    public ParkingPassValidation toDomain(ParkingPassValidationRequest parkingPassValidationRequest) {
        ParkingPassValidationBuilder builder = ParkingPassValidationBuilder.newBuilder(
            parkingPassValidationRequest.getTimeIssued(),
            parkingPassValidationRequest.getParkingZone()
        );
        parkingPassValidationRequest.getParkingCode().ifPresent(code -> assembleParkingCode(code, builder));

        return builder.build();
    }

    private void assembleParkingCode(Integer code, ParkingPassValidationBuilder builder) {
        builder.withParkingCode(new ParkingCode(code));
    }
}
