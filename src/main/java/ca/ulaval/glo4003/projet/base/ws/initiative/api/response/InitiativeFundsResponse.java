package ca.ulaval.glo4003.projet.base.ws.initiative.api.response;

import java.math.BigDecimal;

public class InitiativeFundsResponse {

    private final BigDecimal funds;

    public InitiativeFundsResponse(BigDecimal funds) {
        this.funds = funds;
    }

    public BigDecimal getFunds() {
        return funds;
    }
}
