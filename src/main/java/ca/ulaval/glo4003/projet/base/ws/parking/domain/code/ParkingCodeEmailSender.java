package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import ca.ulaval.glo4003.projet.base.ws.email.domain.EmailSender;

public class ParkingCodeEmailSender implements ParkingCodeSender {

    private static final String PARKING_CODE_EMAIL_SUBJECT = "Votre code de vignette";
    private static final String PARKING_CODE_EMAIL_BODY_TEMPLATE = "Votre code de vignette est: %s";

    private final EmailSender emailSender;
    private final ParkingCode parkingCode;
    private final String email;

    public ParkingCodeEmailSender(EmailSender emailSender, ParkingCode parkingCode, String email) {
        this.emailSender = emailSender;
        this.parkingCode = parkingCode;
        this.email = email;
    }

    @Override
    public void send() {
        emailSender.send(PARKING_CODE_EMAIL_SUBJECT, String.format(PARKING_CODE_EMAIL_BODY_TEMPLATE, parkingCode.getCode()), email);
    }
}
