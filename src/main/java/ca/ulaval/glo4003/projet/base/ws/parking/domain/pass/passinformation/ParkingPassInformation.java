package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;
import java.util.Objects;
import java.util.Optional;

public abstract class ParkingPassInformation {

    private final ParkingZone parkingZone;
    private final DeliveryMethod deliveryMethod;
    private final String email;
    private final PostalAddress postalAddress;

    public ParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod, String email) {
        this.parkingZone = parkingZone;
        this.deliveryMethod = deliveryMethod;
        this.email = email;
        this.postalAddress = null;
    }

    public ParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod, PostalAddress postalAddress) {
        this.parkingZone = parkingZone;
        this.deliveryMethod = deliveryMethod;
        this.postalAddress = postalAddress;
        this.email = null;
    }

    public ParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod) {
        this.parkingZone = parkingZone;
        this.deliveryMethod = deliveryMethod;
        this.email = null;
        this.postalAddress = null;
    }

    public ParkingZone getParkingZone() {
        return parkingZone;
    }

    public DeliveryMethod getDeliveryMethod() {
        return deliveryMethod;
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }

    public Optional<PostalAddress> getPostalAddress() {
        return Optional.ofNullable(postalAddress);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingPassInformation that = (ParkingPassInformation) o;
        return (
            parkingZone == that.parkingZone &&
            deliveryMethod == that.deliveryMethod &&
            Objects.equals(email, that.email) &&
            Objects.equals(postalAddress, that.postalAddress)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingZone, deliveryMethod, email, postalAddress);
    }
}
