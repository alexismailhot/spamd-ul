package ca.ulaval.glo4003.projet.base.ws.access.infra.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.DriverConfig;
import java.time.LocalDate;

public class DriverInMemoryConfig implements DriverConfig {

    private static final Integer MINIMUM_DRIVER_AGE = 16;
    private static final Integer MAXIMUM_DRIVER_AGE = 130;
    private static final LocalDate MAXIMUM_BIRTHDATE = LocalDate.now().minusYears(MAXIMUM_DRIVER_AGE);
    private static final LocalDate MINIMUM_BIRTHDATE = LocalDate.now().minusYears(MINIMUM_DRIVER_AGE);

    @Override
    public Integer getMinimumDriverAge() {
        return MINIMUM_DRIVER_AGE;
    }

    @Override
    public Integer getMaximumDriverAge() {
        return MAXIMUM_DRIVER_AGE;
    }

    @Override
    public LocalDate getMinimumBirthDate() {
        return MINIMUM_BIRTHDATE;
    }

    @Override
    public LocalDate getMaximumBirthDate() {
        return MAXIMUM_BIRTHDATE;
    }
}
