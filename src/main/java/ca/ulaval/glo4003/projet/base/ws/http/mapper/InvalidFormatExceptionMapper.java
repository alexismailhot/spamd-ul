package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidFormatExceptionMapper implements ExceptionMapper<InvalidFormatException> {

    private static final String ERROR_MESSAGE = "INVALID_FORMAT";
    private static final String ERROR_DESCRIPTION = "invalid format";

    private final ErrorResponseAssembler errorResponseAssembler;

    public InvalidFormatExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InvalidFormatException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(ERROR_MESSAGE, ERROR_DESCRIPTION);
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
