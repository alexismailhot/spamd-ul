package ca.ulaval.glo4003.projet.base.ws.initiative.api;

import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeCreationRequest;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeFundingRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/initiative")
public interface InitiativeResource {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Create an initiative",
        description = "Send initiative name and amount to fund, creates the initiative with this information",
        responses = {
            @ApiResponse(responseCode = "201", description = "Valid information, initiative created"),
            @ApiResponse(responseCode = "403", description = "Requested amount to fund initiative exceeds available funds"),
        }
    )
    Response createInitiative(InitiativeCreationRequest initiativeCreationRequest);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a summary of all initiatives", responses = { @ApiResponse(responseCode = "200") })
    Response getInitiatives();

    @GET
    @Path("/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get details about a specific initiative",
        responses = {
            @ApiResponse(responseCode = "200"), @ApiResponse(responseCode = "404", description = "Initiative was not found for given initiative code"),
        }
    )
    Response getInitiative(@PathParam("code") String initiativeCode);

    @PUT
    @Path("/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Fund an existing initiative",
        description = "Send the initiative code and amount to fund",
        responses = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "403", description = "Requested amount to fund initiative exceeds available funds"),
            @ApiResponse(responseCode = "404", description = "Initiative was not found for given initiative code"),
        }
    )
    Response fundInitiative(@PathParam("code") String initiativeCode, InitiativeFundingRequest initiativeFundingRequest);

    @GET
    @Path("funds/offense")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get initiative funds from offense income", responses = { @ApiResponse(responseCode = "200") })
    Response getAllInitiativeFundsFromOffenseIncome();

    @GET
    @Path("funds/offense/yearly")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get initiative funds from offense income for current university year", responses = { @ApiResponse(responseCode = "200") })
    Response getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear();
}
