package ca.ulaval.glo4003.projet.base.ws.income.api;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.income.api.response.IncomeSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.income.api.response.assembler.IncomeSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummary;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import javax.ws.rs.core.Response;

public class IncomeResourceImpl implements IncomeResource {

    private final IncomeService incomeService;
    private final IncomeSummaryResponseAssembler incomeSummaryResponseAssembler;

    public IncomeResourceImpl(IncomeService incomeService, IncomeSummaryResponseAssembler incomeSummaryResponseAssembler) {
        this.incomeService = incomeService;
        this.incomeSummaryResponseAssembler = incomeSummaryResponseAssembler;
    }

    @Override
    public Response getAccessIncomeSummaryForCurrentUniversityYear() {
        IncomeSummary accessIncomeSummary = incomeService.getAccessIncomeSummaryForCurrentUniversityYear();
        IncomeSummaryResponse incomesResponse = incomeSummaryResponseAssembler.toResponse(accessIncomeSummary);
        return Response.ok().entity(incomesResponse).build();
    }

    @Override
    public Response getAllAccessIncomeForVehicleType(VehicleType vehicleType) {
        IncomeSummary incomeSummary = incomeService.getAccessIncomeSummaryFor(vehicleType);
        IncomeSummaryResponse incomeSummaryResponse = incomeSummaryResponseAssembler.toResponse(incomeSummary);
        return Response.ok().entity(incomeSummaryResponse).build();
    }

    @Override
    public Response getAccessIncomeForCurrentUniversityYearForVehicleType(String vehicleTypeValue) {
        VehicleType vehicleType = VehicleType.fromString(vehicleTypeValue);
        IncomeSummary incomeSummary = incomeService.getAccessIncomeSummaryForCurrentUniversityYearFor(vehicleType);
        IncomeSummaryResponse vehicleTypeAccessIncomesSummaryResponse = incomeSummaryResponseAssembler.toResponse(incomeSummary);
        return Response.ok().entity(vehicleTypeAccessIncomesSummaryResponse).build();
    }

    @Override
    public Response getParkingPassIncomeSummaryForCurrentUniversityYear() {
        IncomeSummary summary = incomeService.getParkingIncomeSummaryForCurrentUniversityYear();
        IncomeSummaryResponse incomesResponse = incomeSummaryResponseAssembler.toResponse(summary);
        return Response.ok().entity(incomesResponse).build();
    }
}
