package ca.ulaval.glo4003.projet.base.ws.universityyear.api.request;

import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Year;

public class SessionRequest {

    private final SessionType sessionType;
    private final Year year;

    @JsonCreator
    public SessionRequest(
        @JsonProperty(value = "sessionType", required = true) SessionType sessionType,
        @JsonProperty(value = "year", required = true) Year year
    ) {
        this.sessionType = sessionType;
        this.year = year;
    }

    public SessionType getSessionType() {
        return sessionType;
    }

    public Year getYear() {
        return year;
    }
}
