package ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler;

import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.InitiativeFundsResponse;
import ca.ulaval.glo4003.projet.base.ws.money.api.CapitalFormatter;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class InitiativeFundsResponseAssembler {

    public InitiativeFundsResponse toResponse(Price funds) {
        return new InitiativeFundsResponse(CapitalFormatter.fromBigDecimal(funds.getAmount()));
    }
}
