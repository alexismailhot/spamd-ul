package ca.ulaval.glo4003.projet.base.ws.access.infra.rates;

import ca.ulaval.glo4003.projet.base.ws.access.domain.AccessRatesConfig;
import ca.ulaval.glo4003.projet.base.ws.access.domain.AccessTimeType;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

import java.util.Map;

public class AccessRatesULavalConfig implements AccessRatesConfig {
    private final Map<VehicleType, Map<AccessTimeType, Price>> accessRates;

    public AccessRatesULavalConfig(Map<VehicleType, Map<AccessTimeType, Price>> accessRates) {
        this.accessRates = accessRates;
    }

    @Override
    public Map<VehicleType, Map<AccessTimeType, Price>> getAccessRates() {
        return accessRates;
    }
}
