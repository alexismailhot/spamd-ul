package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.parking.api.exception.NoDeliveryMethodException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class NoDeliveryMethodExceptionMapper implements ExceptionMapper<NoDeliveryMethodException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public NoDeliveryMethodExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(NoDeliveryMethodException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
