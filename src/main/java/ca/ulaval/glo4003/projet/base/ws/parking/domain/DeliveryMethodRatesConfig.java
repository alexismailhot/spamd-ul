package ca.ulaval.glo4003.projet.base.ws.parking.domain;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public interface DeliveryMethodRatesConfig {
    Price getDeliveryMethodRate(DeliveryMethod deliveryMethod);
}
