package ca.ulaval.glo4003.projet.base.ws.carboncredit.api;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.service.CarbonCreditService;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import javax.ws.rs.core.Response;

public class CarbonCreditResourceImpl implements CarbonCreditResource {

    private final CarbonCreditService carbonCreditService;
    private final InitiativeService initiativeService;

    public CarbonCreditResourceImpl(CarbonCreditService carbonCreditService, InitiativeService initiativeService) {
        this.carbonCreditService = carbonCreditService;
        this.initiativeService = initiativeService;
    }

    @Override
    public Response activateAutomaticCarbonCreditSpending() {
        carbonCreditService.activateAutomaticCarbonCreditSpending();
        return Response.ok().build();
    }

    @Override
    public Response deactivateAutomaticCarbonCreditSpending() {
        carbonCreditService.deactivateAutomaticCarbonCreditSpending();
        return Response.ok().build();
    }

    @Override
    public Response getNumberOfCarbonCreditsBought() {
        double numberOfCarbonCreditsBought = initiativeService.getNumberOfCarbonCreditsBought();
        return Response.ok().entity(numberOfCarbonCreditsBought).build();
    }
}
