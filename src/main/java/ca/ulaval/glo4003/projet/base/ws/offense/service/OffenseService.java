package ca.ulaval.glo4003.projet.base.ws.offense.service;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.Offense;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.exception.OffenseAlreadyPaidException;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.exception.OffenseNotFoundException;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncome;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformation;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformationConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.InvalidParkingPassException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import ca.ulaval.glo4003.projet.base.ws.parking.service.ParkingService;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import java.util.Map;
import java.util.Optional;

public class OffenseService {

    private final Map<Class<? extends InvalidParkingPassException>, OffenseCode> offenses;
    private final OffenseCodeInformationConfig offenseCodeInformationConfig;
    private final ParkingService parkingService;
    private final PaymentService paymentService;
    private final OffenseRepository offenseRepository;
    private final OffenseFactory offenseFactory;
    private final OffenseIncomeFactory offenseIncomeFactory;
    private final OffenseIncomeRepository offenseIncomeRepository;

    public OffenseService(
        Map<Class<? extends InvalidParkingPassException>, OffenseCode> offenses,
        OffenseCodeInformationConfig offenseCodeInformationConfig,
        ParkingService parkingService,
        PaymentService paymentService,
        OffenseRepository offenseRepository,
        OffenseFactory offenseFactory,
        OffenseIncomeFactory offenseIncomeFactory,
        OffenseIncomeRepository offenseIncomeRepository
    ) {
        this.offenses = offenses;
        this.offenseCodeInformationConfig = offenseCodeInformationConfig;
        this.parkingService = parkingService;
        this.paymentService = paymentService;
        this.offenseRepository = offenseRepository;
        this.offenseFactory = offenseFactory;
        this.offenseIncomeFactory = offenseIncomeFactory;
        this.offenseIncomeRepository = offenseIncomeRepository;
    }

    public Optional<Offense> createOffenseIfParkingPassOffenseWasCommitted(ParkingPassValidation parkingPassValidation) {
        try {
            parkingService.validateParkingPass(parkingPassValidation);
        } catch (InvalidParkingPassException e) {
            OffenseCode offenseCode = offenses.get(e.getClass());
            OffenseCodeInformation offenseCodeInformation = offenseCodeInformationConfig.getOffenseCodeInformation(offenseCode);
            return Optional.of(registerNewOffense(offenseCode, offenseCodeInformation.getOffense(), offenseCodeInformation.getAmount()));
        }
        return Optional.empty();
    }

    public void payOffense(CreditCard creditCard, OffenseId offenseId) {
        Optional<Offense> optionalOffense = offenseRepository.findByOffenseId(offenseId);
        Offense offense = optionalOffense.orElseThrow(() -> new OffenseNotFoundException(offenseId));
        if (offense.isPaid()) {
            throw new OffenseAlreadyPaidException(offenseId);
        }
        offense.pay();
        offenseRepository.save(offense);
        paymentService.processPayment(creditCard, offense.getAmount());

        OffenseIncome offenseIncome = offenseIncomeFactory.create(offense.getAmount());
        offenseIncomeRepository.save(offenseIncome);
    }

    private Offense registerNewOffense(OffenseCode offenseCode, String description, Price amount) {
        Offense offense = offenseFactory.create(offenseCode, description, amount);
        offenseRepository.save(offense);
        return offense;
    }
}
