package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;

public class ParkingPassNotFoundForParkingCodeException extends InvalidParkingPassException {

    private final ParkingCode parkingCode;

    public ParkingPassNotFoundForParkingCodeException(ParkingCode parkingCode) {
        this.parkingCode = parkingCode;
    }

    @Override
    public String getDescription() {
        return String.format("No Parking pass found for Parking code %s.", parkingCode.getCode());
    }
}
