package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.WeeklyAccessInformation;

public class WeeklyAccessFactory {

    private final AccessRepository accessRepository;

    public WeeklyAccessFactory(AccessRepository accessRepository) {
        this.accessRepository = accessRepository;
    }

    public WeeklyAccess create(WeeklyAccessInformation weeklyAccessInformation) {
        return new WeeklyAccess(generateAccessCode(), weeklyAccessInformation.getAccessDay(), weeklyAccessInformation.getSession());
    }

    private AccessCode generateAccessCode() {
        return new AccessCode(accessRepository.getCount());
    }
}
