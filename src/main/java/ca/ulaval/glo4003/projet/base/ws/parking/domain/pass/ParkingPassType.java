package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass;

public enum ParkingPassType {
    ONE_DAY_A_WEEK_FOR_A_SEMESTER,
    MONTHLY,
    ONE_SEMESTER,
    TWO_SEMESTERS,
    THREE_SEMESTERS,
}
