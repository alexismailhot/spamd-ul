package ca.ulaval.glo4003.projet.base.ws.offense.infra.offensecode;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OffenseCodeInformationDto {

    private final String infraction;
    private final OffenseCode code;
    private final double montant;

    @JsonCreator
    public OffenseCodeInformationDto(
        @JsonProperty(value = "infraction", required = true) String infraction,
        @JsonProperty(value = "code", required = true) OffenseCode code,
        @JsonProperty(value = "montant", required = true) double montant
    ) {
        this.infraction = infraction;
        this.code = code;
        this.montant = montant;
    }

    public String getInfraction() {
        return infraction;
    }

    public OffenseCode getCode() {
        return code;
    }

    public double getMontant() {
        return montant;
    }
}
