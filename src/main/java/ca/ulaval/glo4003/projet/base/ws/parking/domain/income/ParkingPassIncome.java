package ca.ulaval.glo4003.projet.base.ws.parking.domain.income;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.time.LocalDateTime;

public class ParkingPassIncome {

    private final Price price;
    private final LocalDateTime created;

    public ParkingPassIncome(Price price, LocalDateTime created) {
        this.price = price;
        this.created = created;
    }

    public Price getPrice() {
        return price;
    }

    public boolean isIn(UniversityYear universityYear) {
        return universityYear.includes(created.toLocalDate());
    }
}
