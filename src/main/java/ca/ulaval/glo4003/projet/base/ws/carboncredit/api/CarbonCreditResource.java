package ca.ulaval.glo4003.projet.base.ws.carboncredit.api;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/carbonCredit")
public interface CarbonCreditResource {
    @POST
    @Path("/activateAutoBuy")
    Response activateAutomaticCarbonCreditSpending();

    @POST
    @Path("/deactivateAutoBuy")
    Response deactivateAutomaticCarbonCreditSpending();

    @GET
    @Path("/quantity")
    Response getNumberOfCarbonCreditsBought();
}
