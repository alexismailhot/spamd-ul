package ca.ulaval.glo4003.projet.base.ws.parking.infra.rates;

import java.util.Map;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethodRatesConfig;

public class DeliveryMethodULavalRatesConfig implements DeliveryMethodRatesConfig {

    private final Map<DeliveryMethod, Price> deliveryMethodRates;

    public DeliveryMethodULavalRatesConfig(Map<DeliveryMethod, Price> deliveryMethodRates) {
        this.deliveryMethodRates = deliveryMethodRates;
    }

    public Price getDeliveryMethodRate(DeliveryMethod deliveryMethod) {
        return deliveryMethodRates.get(deliveryMethod);
    }
}
