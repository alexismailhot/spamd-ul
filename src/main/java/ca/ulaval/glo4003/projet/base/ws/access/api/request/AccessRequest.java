package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class AccessRequest {

    private final DriverRequest driver;
    private final VehicleRequest vehicle;
    private final SessionRequest session;
    private final CreditCardRequest creditCard;

    public AccessRequest(
        @JsonProperty(value = "driver", required = true) DriverRequest driverRequest,
        @JsonProperty(value = "vehicle", required = true) VehicleRequest vehicleRequest,
        @JsonProperty(value = "session", required = true) SessionRequest session,
        @JsonProperty(value = "creditCard", required = true) CreditCardRequest creditCard
    ) {
        this.driver = driverRequest;
        this.vehicle = vehicleRequest;
        this.session = session;
        this.creditCard = creditCard;
    }

    public DriverRequest getDriver() {
        return driver;
    }

    public VehicleRequest getVehicle() {
        return vehicle;
    }

    public SessionRequest getSession() {
        return session;
    }

    public CreditCardRequest getCreditCard() {
        return creditCard;
    }
}
