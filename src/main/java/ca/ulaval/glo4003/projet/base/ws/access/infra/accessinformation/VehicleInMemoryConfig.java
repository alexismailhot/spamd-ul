package ca.ulaval.glo4003.projet.base.ws.access.infra.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleConfig;
import java.time.LocalDate;

public class VehicleInMemoryConfig implements VehicleConfig {

    private static final Integer MINIMUM_VEHICLE_YEAR = 1900;
    private static final Integer MAXIMUM_VEHICLE_YEAR = LocalDate.now().getYear() + 1;

    @Override
    public Integer getMaximumVehicleYear() {
        return MAXIMUM_VEHICLE_YEAR;
    }

    @Override
    public Integer getMinimumVehicleYear() {
        return MINIMUM_VEHICLE_YEAR;
    }
}
