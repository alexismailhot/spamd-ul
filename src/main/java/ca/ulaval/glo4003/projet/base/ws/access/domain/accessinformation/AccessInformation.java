package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import java.time.LocalDate;
import java.util.Objects;

public abstract class AccessInformation {

    private final Driver driver;
    private final Vehicle vehicle;

    protected AccessInformation(Driver driver, Vehicle vehicle) {
        this.driver = driver;
        this.vehicle = vehicle;
    }

    public boolean driverHasBirthDateBefore(LocalDate otherBirthdate) {
        return driver.hasBirthDateBefore(otherBirthdate);
    }

    public boolean driverHasBirthDateAfter(LocalDate otherBirthdate) {
        return driver.hasBirthDateAfter(otherBirthdate);
    }

    public boolean isVehicleYearBefore(Integer otherVehicleYear) {
        return vehicle.isModelYearBefore(otherVehicleYear);
    }

    public boolean isVehicleYearAfter(Integer otherVehicleYear) {
        return vehicle.isModelYearAfter(otherVehicleYear);
    }

    public VehicleType getVehicleType() {
        return vehicle.getVehicleType();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccessInformation that = (AccessInformation) o;
        return Objects.equals(driver, that.driver) && Objects.equals(vehicle, that.vehicle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(driver, vehicle);
    }
}
