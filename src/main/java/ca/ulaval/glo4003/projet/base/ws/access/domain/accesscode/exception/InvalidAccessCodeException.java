package ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception;

public abstract class InvalidAccessCodeException extends RuntimeException {
    private static final String INVALID_ACCESS_CODE = "INVALID_ACCESS_CODE";

    public String getError() {
        return INVALID_ACCESS_CODE;
    }

    public abstract String getDescription();
}
