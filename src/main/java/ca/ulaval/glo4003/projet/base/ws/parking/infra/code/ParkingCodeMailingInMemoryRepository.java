package ca.ulaval.glo4003.projet.base.ws.parking.infra.code;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeMailing;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeMailingRepository;
import java.util.ArrayList;
import java.util.List;

public class ParkingCodeMailingInMemoryRepository implements ParkingCodeMailingRepository {

    private final List<ParkingCodeMailing> parkingCodeMailings = new ArrayList<>();

    @Override
    public void save(ParkingCodeMailing parkingCodeMailing) {
        parkingCodeMailings.add(parkingCodeMailing);
    }
}
