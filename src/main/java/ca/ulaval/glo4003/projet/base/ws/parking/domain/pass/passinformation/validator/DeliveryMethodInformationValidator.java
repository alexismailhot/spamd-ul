package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;

public interface DeliveryMethodInformationValidator {
    void validate(ParkingPassInformation parkingPassInformation);
}
