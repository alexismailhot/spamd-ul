package ca.ulaval.glo4003.projet.base.ws.usage.domain;

import java.time.LocalDate;
import java.util.List;

public class DatesWithSameUsage {

    private final List<LocalDate> dates;
    private final int usage;

    public DatesWithSameUsage(List<LocalDate> dates, int usage) {
        this.dates = dates;
        this.usage = usage;
    }

    public List<LocalDate> getDates() {
        return dates;
    }

    public int getUsage() {
        return usage;
    }
}
