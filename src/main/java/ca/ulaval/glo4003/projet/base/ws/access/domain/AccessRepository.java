package ca.ulaval.glo4003.projet.base.ws.access.domain;

import java.util.Optional;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;

public interface AccessRepository {
    void save(Access access);

    int getCount();

    Optional<Access> findByAccessCode(AccessCode accessCode);
}
