package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;

public class UnknownParkingCodeException extends RuntimeException {
    private static final String ERROR = "UNKNOWN_PARKING_CODE";
    private static final String DESCRIPTION = "unknown parking code : %d";

    private final ParkingCode parkingCode;

    public UnknownParkingCodeException(ParkingCode parkingCode) {
        this.parkingCode = parkingCode;
    }

    public String getError() {
        return ERROR;
    }

    public String getDescription() {
        return String.format(DESCRIPTION, parkingCode.getCode());
    }
}
