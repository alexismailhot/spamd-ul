package ca.ulaval.glo4003.projet.base.ws.offense.domain.income;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.Objects;

public class OffenseIncome {

    private final Price price;
    private final LocalDateTime created;

    public OffenseIncome(Price price, LocalDateTime created) {
        this.price = price;
        this.created = created;
    }

    public Price getPrice() {
        return price;
    }

    public boolean isCreatedIn(Year year) {
        return Year.of(created.getYear()).equals(year);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OffenseIncome that = (OffenseIncome) o;
        return Objects.equals(price, that.price) && Objects.equals(created, that.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, created);
    }

    public boolean isIn(UniversityYear universityYear) {
        LocalDate createdDate = created.toLocalDate();
        return universityYear.includes(createdDate);
    }
}
