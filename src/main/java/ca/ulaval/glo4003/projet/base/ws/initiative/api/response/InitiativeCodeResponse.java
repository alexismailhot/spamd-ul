package ca.ulaval.glo4003.projet.base.ws.initiative.api.response;

public class InitiativeCodeResponse {

    private final String initiativeCode;

    public InitiativeCodeResponse(String initiativeCode) {
        this.initiativeCode = initiativeCode;
    }

    public String getInitiativeCode() {
        return initiativeCode;
    }
}
