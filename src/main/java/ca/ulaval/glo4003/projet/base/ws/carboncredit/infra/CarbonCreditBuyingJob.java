package ca.ulaval.glo4003.projet.base.ws.carboncredit.infra;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.exception.CarbonCreditBuyingJobFailedException;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import org.quartz.*;

public class CarbonCreditBuyingJob implements Job {

    @Override
    public void execute(JobExecutionContext context) {
        try {
            SchedulerContext schedulerContext = context.getScheduler().getContext();
            InitiativeService initiativeService = (InitiativeService) schedulerContext.get("InitiativeService");
            initiativeService.spendRemainingFundsOnCarbonCreditInitiatives();
        } catch (SchedulerException e) {
            throw new CarbonCreditBuyingJobFailedException(e);
        }
    }
}
