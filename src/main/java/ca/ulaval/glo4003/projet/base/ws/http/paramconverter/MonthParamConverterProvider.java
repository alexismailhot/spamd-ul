package ca.ulaval.glo4003.projet.base.ws.http.paramconverter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.Month;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;

public class MonthParamConverterProvider implements ParamConverterProvider {

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if (Month.class == rawType) {
            return new ParamConverter<T>() {
                @Override
                public T fromString(String string) {
                    try {
                        if (string == null) {
                            LocalDate currentDate = LocalDate.now();
                            return rawType.cast(currentDate.getMonth());
                        }
                        Month month = Month.of(Integer.parseInt(string));
                        return rawType.cast(month);
                    } catch (Exception ex) {
                        throw new BadRequestException(ex);
                    }
                }

                @Override
                public String toString(T t) {
                    Month month = (Month) t;
                    return month.toString();
                }
            };
        }

        return null;
    }
}
