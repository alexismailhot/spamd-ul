package ca.ulaval.glo4003.projet.base.ws.parking.domain;

public class PostalAddress {

    private final String address;
    private final String apartmentNumber;
    private final String state;
    private final String country;
    private final String city;
    private final String postalCode;

    public PostalAddress(String address, String apartmentNumber, String state, String country, String city, String postalCode) {
        this.address = address;
        this.apartmentNumber = apartmentNumber;
        this.state = state;
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    @Override
    public String toString() {
        return (
            "PostalAddress{" +
            "address='" +
            address +
            '\'' +
            ", apartmentNumber='" +
            apartmentNumber +
            '\'' +
            ", state='" +
            state +
            '\'' +
            ", country='" +
            country +
            '\'' +
            ", city='" +
            city +
            '\'' +
            ", postalCode='" +
            postalCode +
            '\'' +
            '}'
        );
    }
}
