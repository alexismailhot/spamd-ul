package ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.Optional;

public class ParkingPassValidationRequest {

    private final Integer parkingCode;
    private final LocalDateTime timeIssued;
    private final ParkingZone parkingZone;

    @JsonCreator
    public ParkingPassValidationRequest(
        @JsonProperty(value = "parkingCode") Integer parkingCode,
        @JsonProperty(value = "timeIssued", required = true) LocalDateTime timeIssued,
        @JsonProperty(value = "parkingZone", required = true) ParkingZone parkingZone
    ) {
        this.parkingCode = parkingCode;
        this.timeIssued = timeIssued;
        this.parkingZone = parkingZone;
    }

    public Optional<Integer> getParkingCode() {
        return Optional.ofNullable(parkingCode);
    }

    public LocalDateTime getTimeIssued() {
        return timeIssued;
    }

    public ParkingZone getParkingZone() {
        return parkingZone;
    }
}
