package ca.ulaval.glo4003.projet.base.ws.initiative.infra.funding;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingConfig;

public class InitiativeFundingULavalConfig implements InitiativeFundingConfig {

    private static final double PERCENTAGE = 0.4;

    @Override
    public double getPercentageOfFundsAllocated() {
        return PERCENTAGE;
    }
}
