package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SessionAccessRequest extends AccessRequest {

    @JsonCreator
    public SessionAccessRequest(
        @JsonProperty(value = "driver", required = true) DriverRequest driverRequest,
        @JsonProperty(value = "vehicle", required = true) VehicleRequest vehicleRequest,
        @JsonProperty(value = "session", required = true) SessionRequest session,
        @JsonProperty(value = "creditCard", required = true) CreditCardRequest creditCard
    ) {
        super(driverRequest, vehicleRequest, session, creditCard);
    }
}
