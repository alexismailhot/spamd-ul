package ca.ulaval.glo4003.projet.base.ws.access.domain.income;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.util.List;

public interface AccessIncomeRepository {
    void save(AccessIncome accessIncome);

    List<AccessIncome> findByVehicleType(VehicleType vehicleType);

    List<AccessIncome> findByUniversityYear(UniversityYear universityYear);

    List<AccessIncome> getAll();

    List<AccessIncome> findByVehicleTypeAndUniversityYear(VehicleType vehicleType, UniversityYear universityYear);
}
