package ca.ulaval.glo4003.projet.base.ws.universityyear.domain;

import java.time.MonthDay;

public enum SessionType {
    FALL(MonthDay.of(9, 1)),
    WINTER(MonthDay.of(1, 1)),
    SUMMER(MonthDay.of(5, 1));

    private final MonthDay monthDay;

    SessionType(MonthDay monthDay) {
        this.monthDay = monthDay;
    }

    public MonthDay getMonthDay() {
        return monthDay;
    }
}
