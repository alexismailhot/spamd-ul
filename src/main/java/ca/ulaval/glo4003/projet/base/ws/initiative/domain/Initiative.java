package ca.ulaval.glo4003.projet.base.ws.initiative.domain;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class Initiative {

    private final String name;
    private final InitiativeCode initiativeCode;
    private Price attributedFunds;

    public Initiative(String name, Price attributedFunds, InitiativeCode initiativeCode) {
        this.name = name;
        this.attributedFunds = attributedFunds;
        this.initiativeCode = initiativeCode;
    }

    public InitiativeCode getInitiativeCode() {
        return initiativeCode;
    }

    public String getName() {
        return name;
    }

    public Price getAttributedFunds() {
        return attributedFunds;
    }

    public void fund(Price price) {
        attributedFunds = attributedFunds.add(price);
    }
}
