package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.InvalidParkingPassException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidParkingCodeExceptionMapper implements ExceptionMapper<InvalidParkingPassException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public InvalidParkingCodeExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InvalidParkingPassException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.FORBIDDEN).entity(errorResponse).build();
    }
}
