package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.exception;

public abstract class InvalidAccessInformationException extends RuntimeException {

    public abstract String getError();

    public abstract String getDescription();
}
