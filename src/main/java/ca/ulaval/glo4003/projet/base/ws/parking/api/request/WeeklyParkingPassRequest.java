package ca.ulaval.glo4003.projet.base.ws.parking.api.request;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.DayOfWeek;
import java.util.Optional;

public class WeeklyParkingPassRequest {

    private final ParkingZone parkingZone;
    private final DeliveryMethod deliveryMethod;
    private final String email;
    private final DayOfWeek parkingDay;
    private final SessionRequest session;
    private final CreditCardRequest creditCard;
    private final PostalAddressRequest postalAddress;

    @JsonCreator
    public WeeklyParkingPassRequest(
        @JsonProperty(value = "parkingZone", required = true) ParkingZone parkingZone,
        @JsonProperty(value = "deliveryMethod", required = true) DeliveryMethod deliveryMethod,
        @JsonProperty(value = "email") String email,
        @JsonProperty(value = "parkingDay", required = true) DayOfWeek parkingDay,
        @JsonProperty(value = "session", required = true) SessionRequest session,
        @JsonProperty(value = "creditCard", required = true) CreditCardRequest creditCard,
        @JsonProperty(value = "postalAddress") PostalAddressRequest postalAddress
    ) {
        this.parkingZone = parkingZone;
        this.deliveryMethod = deliveryMethod;
        this.email = email;
        this.parkingDay = parkingDay;
        this.session = session;
        this.creditCard = creditCard;
        this.postalAddress = postalAddress;
    }

    public ParkingZone getParkingZone() {
        return parkingZone;
    }

    public DeliveryMethod getDeliveryMethod() {
        return deliveryMethod;
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }

    public DayOfWeek getParkingDay() {
        return parkingDay;
    }

    public CreditCardRequest getCreditCard() {
        return creditCard;
    }

    public Optional<PostalAddressRequest> getPostalAddress() {
        return Optional.ofNullable(postalAddress);
    }

    public SessionRequest getSession() {
        return session;
    }
}
