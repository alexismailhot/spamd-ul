package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.time.DayOfWeek;
import java.time.LocalDateTime;

public class WeeklyAccess extends Access {

    private final DayOfWeek accessDay;
    private final Session session;

    public WeeklyAccess(AccessCode accessCode, DayOfWeek accessDay, Session session) {
        super(accessCode);
        this.accessDay = accessDay;
        this.session = session;
    }

    @Override
    public boolean isValidOn(LocalDateTime time) {
        return time.getDayOfWeek().equals(accessDay) && session.includes(time.toLocalDate());
    }
}
