package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session.SessionParkingPass;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class WeeklyParkingPass extends SessionParkingPass {

    private final DayOfWeek parkingDay;

    public WeeklyParkingPass(ParkingCode parkingCode, ParkingZone parkingZone, DayOfWeek parkingDay, Session session) {
        super(parkingCode, parkingZone, session);
        this.parkingDay = parkingDay;
    }

    @Override
    public boolean isValidOn(LocalDate date) {
        return date.getDayOfWeek().equals(parkingDay) && super.isValidOn(date);
    }
}
