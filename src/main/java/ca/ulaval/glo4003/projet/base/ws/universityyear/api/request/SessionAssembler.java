package ca.ulaval.glo4003.projet.base.ws.universityyear.api.request;

import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;

public class SessionAssembler {

    public Session toDomain(SessionRequest sessionRequest) {
        return new Session(sessionRequest.getSessionType(), sessionRequest.getYear());
    }
}
