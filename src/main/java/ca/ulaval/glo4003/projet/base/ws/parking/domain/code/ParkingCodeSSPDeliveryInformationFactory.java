package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

public class ParkingCodeSSPDeliveryInformationFactory {

    ParkingCodeSSPDeliveryInformation create(ParkingCode parkingCode) {
        return new ParkingCodeSSPDeliveryInformation(parkingCode);
    }
}
