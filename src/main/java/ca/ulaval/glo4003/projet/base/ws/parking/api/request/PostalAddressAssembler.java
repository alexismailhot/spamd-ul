package ca.ulaval.glo4003.projet.base.ws.parking.api.request;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;

public class PostalAddressAssembler {

    public PostalAddress toDomain(PostalAddressRequest postalAddressRequest) {
        return new PostalAddress(
            postalAddressRequest.getAddress(),
            postalAddressRequest.getApartmentNumber(),
            postalAddressRequest.getState(),
            postalAddressRequest.getCountry(),
            postalAddressRequest.getCity(),
            postalAddressRequest.getPostalCode()
        );
    }
}
