package ca.ulaval.glo4003.projet.base.ws.universityyear.domain;

import java.time.LocalDate;
import java.time.Year;

public class UniversityYearProvider {

    public UniversityYear provide(LocalDate date) {
        Year startYear = Year.of(date.getYear());
        if (dateMonthIsBeforeUniversityStartMonth(date)) {
            startYear = startYear.minusYears(1);
        }

        Session fallSession = new Session(SessionType.FALL, startYear);
        Session winterSession = new Session(SessionType.WINTER, startYear.plusYears(1));
        Session summerSession = new Session(SessionType.SUMMER, startYear.plusYears(1));

        return new UniversityYear(fallSession, winterSession, summerSession);
    }

    private boolean dateMonthIsBeforeUniversityStartMonth(LocalDate date) {
        return date.getMonthValue() < SessionType.FALL.getMonthDay().getMonthValue();
    }
}
