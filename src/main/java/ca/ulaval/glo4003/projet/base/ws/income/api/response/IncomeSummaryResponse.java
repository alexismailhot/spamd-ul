package ca.ulaval.glo4003.projet.base.ws.income.api.response;

import java.math.BigDecimal;

public class IncomeSummaryResponse {

    private final BigDecimal totalIncome;
    private final long numberOfSales;

    public IncomeSummaryResponse(BigDecimal totalIncome, long numberOfSales) {
        this.totalIncome = totalIncome;
        this.numberOfSales = numberOfSales;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public long getNumberOfSales() {
        return numberOfSales;
    }
}
