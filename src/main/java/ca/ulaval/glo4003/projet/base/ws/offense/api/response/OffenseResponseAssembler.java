package ca.ulaval.glo4003.projet.base.ws.offense.api.response;

import ca.ulaval.glo4003.projet.base.ws.money.api.CapitalFormatter;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.Offense;

public class OffenseResponseAssembler {

    public OffenseResponse toResponse(Offense offense) {
        return new OffenseResponse(
            offense.getOffenseId().getId(),
            offense.getOffenseCode(),
            offense.getDescription(),
            CapitalFormatter.fromBigDecimal(offense.getAmount().getAmount())
        );
    }
}
