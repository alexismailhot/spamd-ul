package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import java.time.LocalDateTime;

public abstract class Access {

    private final AccessCode accessCode;

    protected Access(AccessCode accessCode) {
        this.accessCode = accessCode;
    }

    public abstract boolean isValidOn(LocalDateTime time);

    public AccessCode getAccessCode() {
        return accessCode;
    }
}
