package ca.ulaval.glo4003.projet.base.ws.parking.api.response;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;

public class ParkingPassResponseAssembler {

    public ParkingPassResponse toResponse(ParkingCode parkingCode) {
        return new ParkingPassResponse(parkingCode.getCode());
    }
}
