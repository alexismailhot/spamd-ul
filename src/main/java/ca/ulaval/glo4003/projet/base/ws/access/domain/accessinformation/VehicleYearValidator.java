package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.exception.InvalidVehicleYearException;

public class VehicleYearValidator extends AccessInformationValidator {

    private final Integer minimumVehicleYear;
    private final Integer maximumVehicleYear;

    public VehicleYearValidator(AccessInformationValidator accessInformationValidator, Integer minimumVehicleYear, Integer maximumVehicleYear) {
        super(accessInformationValidator);
        this.minimumVehicleYear = minimumVehicleYear;
        this.maximumVehicleYear = maximumVehicleYear;
    }

    @Override
    public void validate(AccessInformation accessInformation) {
        if (accessInformation.isVehicleYearBefore(minimumVehicleYear) || accessInformation.isVehicleYearAfter(maximumVehicleYear)) {
            throw new InvalidVehicleYearException(maximumVehicleYear, minimumVehicleYear);
        }
        super.validate(accessInformation);
    }
}
