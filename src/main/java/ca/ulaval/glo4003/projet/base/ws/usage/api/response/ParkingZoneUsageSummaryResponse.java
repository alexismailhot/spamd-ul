package ca.ulaval.glo4003.projet.base.ws.usage.api.response;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;

public class ParkingZoneUsageSummaryResponse {

    private final ParkingZone zone;
    private final DatesActivitySummaryResponse mostActiveDay;
    private final DatesActivitySummaryResponse leastActiveDay;
    private final float averageDailyActivity;

    public ParkingZoneUsageSummaryResponse(
        DatesActivitySummaryResponse mostActiveDay,
        DatesActivitySummaryResponse leastActiveDay,
        float averageDailyActivity,
        ParkingZone zone
    ) {
        this.mostActiveDay = mostActiveDay;
        this.leastActiveDay = leastActiveDay;
        this.averageDailyActivity = averageDailyActivity;
        this.zone = zone;
    }

    public ParkingZone getZone() {
        return zone;
    }

    public DatesActivitySummaryResponse getMostActiveDay() {
        return mostActiveDay;
    }

    public DatesActivitySummaryResponse getLeastActiveDay() {
        return leastActiveDay;
    }

    public float getAverageDailyActivity() {
        return averageDailyActivity;
    }
}
