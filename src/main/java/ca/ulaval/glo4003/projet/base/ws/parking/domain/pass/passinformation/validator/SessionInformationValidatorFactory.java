package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;

import java.time.Clock;

public class SessionInformationValidatorFactory {
    private final Clock clock;

    public SessionInformationValidatorFactory(Clock clock) {
        this.clock = clock;
    }

    public SessionInformationValidator create(ParkingPassInformation parkingPassInformation) {
        if (parkingPassInformation instanceof SessionParkingPassInformation) {
            return new SessionValidator(clock);
        }
        return new SessionNoOpValidator();
    }
}
