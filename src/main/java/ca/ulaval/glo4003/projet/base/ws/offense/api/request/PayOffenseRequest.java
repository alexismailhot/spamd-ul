package ca.ulaval.glo4003.projet.base.ws.offense.api.request;

import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PayOffenseRequest {

    private final OffenseIdRequest offenseId;
    private final CreditCardRequest creditCard;

    @JsonCreator
    public PayOffenseRequest(
        @JsonProperty(value = "offenseId", required = true) OffenseIdRequest offenseId,
        @JsonProperty(value = "creditCard", required = true) CreditCardRequest creditCardRequest
    ) {
        this.offenseId = offenseId;
        this.creditCard = creditCardRequest;
    }

    public OffenseIdRequest getOffenseId() {
        return offenseId;
    }

    public CreditCardRequest getCreditCardRequest() {
        return creditCard;
    }
}
