package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception.InitiativeNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InitiativeNotFoundExceptionMapper implements ExceptionMapper<InitiativeNotFoundException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public InitiativeNotFoundExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InitiativeNotFoundException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.NOT_FOUND).entity(errorResponse).build();
    }
}
