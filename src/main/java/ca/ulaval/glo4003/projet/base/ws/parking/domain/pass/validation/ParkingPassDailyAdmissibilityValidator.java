package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotValidOnDateException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import java.time.LocalDate;

public class ParkingPassDailyAdmissibilityValidator extends ParkingPassValidator {

    public ParkingPassDailyAdmissibilityValidator(ParkingPassValidator parkingPassValidator) {
        super(parkingPassValidator);
    }

    @Override
    public void validate(ParkingPassValidation parkingPassValidation, ParkingPass parkingPass) {
        LocalDate parkingDate = parkingPassValidation.getTimeIssued().toLocalDate();
        if (!parkingPass.isValidOn(parkingDate)) {
            throw new ParkingPassNotValidOnDateException(parkingPass.getParkingCode(), parkingDate);
        }
        super.validate(parkingPassValidation, parkingPass);
    }
}
