package ca.ulaval.glo4003.projet.base.ws.access.api;

import ca.ulaval.glo4003.projet.base.ws.access.api.request.SessionAccessRequest;
import ca.ulaval.glo4003.projet.base.ws.access.api.request.WeeklyAccessRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/access")
public interface AccessResource {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/weekly")
    @Operation(
        summary = "Buy an Access Code to enter campus one day a week for a session",
        description = "Send driver, vehicle and Credit Card info to get Access Code, charges the given Credit Card according to rates",
        responses = {
            @ApiResponse(responseCode = "201", description = "Valid infos, Access Code created"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see Response message for info"),
        }
    )
    Response createWeeklyAccess(WeeklyAccessRequest weeklyAccessRequest);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/session")
    @Operation(
        summary = "Buy an Access Code to enter campus every day of a session",
        description = "Send driver, vehicle and Credit Card info to get Access Code, charges the given Credit Card according to rates",
        responses = {
            @ApiResponse(responseCode = "201", description = "Valid infos, Access Code created"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see Response message for info"),
        }
    )
    Response createSessionAccess(SessionAccessRequest sessionAccessRequest);

    @GET
    @Path("/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Check if access Code is valid for Day of the week",
        responses = {
            @ApiResponse(responseCode = "200", description = "Access Granted, Access Code is valid for the day"),
            @ApiResponse(responseCode = "403", description = "Access Forbidden, Access Code was not found or is invalid for given day"),
        }
    )
    Response getAccess(@PathParam("code") int accessCode);
}
