package ca.ulaval.glo4003.projet.base.ws.income.service;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummary;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummaryFactory;
import ca.ulaval.glo4003.projet.base.ws.money.domain.*;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncome;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYearProvider;
import java.time.Clock;
import java.time.ZoneId;
import java.util.List;

public class IncomeService {

    private final AccessIncomeRepository accessIncomeRepository;
    private final ParkingPassIncomeRepository parkingPassIncomeRepository;
    private final OffenseIncomeRepository offenseIncomeRepository;
    private final IncomeSummaryFactory incomeSummaryFactory;
    private final UniversityYearProvider universityYearProvider;
    private final Clock clock;

    public IncomeService(
        AccessIncomeRepository accessIncomeRepository,
        ParkingPassIncomeRepository parkingPassIncomeRepository,
        OffenseIncomeRepository offenseIncomeRepository,
        IncomeSummaryFactory incomeSummaryFactory,
        UniversityYearProvider universityYearProvider,
        Clock clock
    ) {
        this.accessIncomeRepository = accessIncomeRepository;
        this.parkingPassIncomeRepository = parkingPassIncomeRepository;
        this.offenseIncomeRepository = offenseIncomeRepository;
        this.incomeSummaryFactory = incomeSummaryFactory;
        this.universityYearProvider = universityYearProvider;
        this.clock = clock;
    }

    public IncomeSummary getAccessIncomeSummaryForCurrentUniversityYear() {
        List<AccessIncome> accessIncomes = accessIncomeRepository.findByUniversityYear(getCurrentUniversityYear());
        return incomeSummaryFactory.createFromAccessIncome(accessIncomes);
    }

    public IncomeSummary getParkingIncomeSummaryForCurrentUniversityYear() {
        List<ParkingPassIncome> parkingPassIncomes = parkingPassIncomeRepository.findByUniversityYear(getCurrentUniversityYear());
        return incomeSummaryFactory.createFromParkingIncome(parkingPassIncomes);
    }

    public IncomeSummary getAccessIncomeSummaryFor(VehicleType vehicleType) {
        List<AccessIncome> accessIncomes = accessIncomeRepository.findByVehicleType(vehicleType);
        return incomeSummaryFactory.createFromAccessIncome(accessIncomes);
    }

    public IncomeSummary getAccessIncomeSummaryForCurrentUniversityYearFor(VehicleType vehicleType) {
        List<AccessIncome> accessIncomes = accessIncomeRepository.findByVehicleTypeAndUniversityYear(vehicleType, getCurrentUniversityYear());
        return incomeSummaryFactory.createFromAccessIncome(accessIncomes);
    }

    public Price getTotalOffenseIncome() {
        List<OffenseIncome> offenseIncomes = offenseIncomeRepository.getAll();
        return calculateOffensesTotalIncome(offenseIncomes);
    }

    public Price getTotalOffenseIncomeForCurrentUniversityYear() {
        List<OffenseIncome> offenseIncomes = offenseIncomeRepository.findByUniversityYear(getCurrentUniversityYear());
        return calculateOffensesTotalIncome(offenseIncomes);
    }

    public Price getTotalIncome() {
        List<ParkingPassIncome> parkingPassIncomes = parkingPassIncomeRepository.getAll();
        List<AccessIncome> accessIncomes = accessIncomeRepository.getAll();
        List<OffenseIncome> offenseIncomes = offenseIncomeRepository.getAll();

        Price parkingPassIncomesAmount = parkingPassIncomes.stream().map(ParkingPassIncome::getPrice).reduce(Price.zero(), Price::add);
        Price accessIncomesAmount = accessIncomes.stream().map(AccessIncome::getPrice).reduce(Price.zero(), Price::add);
        Price offenseIncomesAmount = calculateOffensesTotalIncome(offenseIncomes);

        return parkingPassIncomesAmount.add(accessIncomesAmount).add(offenseIncomesAmount);
    }

    private UniversityYear getCurrentUniversityYear() {
        return universityYearProvider.provide(clock.instant().atZone(ZoneId.systemDefault()).toLocalDate());
    }

    private Price calculateOffensesTotalIncome(List<OffenseIncome> offenseIncomes) {
        return offenseIncomes.stream().map(OffenseIncome::getPrice).reduce(Price.zero(), Price::add);
    }
}
