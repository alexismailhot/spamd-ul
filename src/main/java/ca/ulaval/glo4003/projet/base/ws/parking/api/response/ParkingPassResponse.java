package ca.ulaval.glo4003.projet.base.ws.parking.api.response;

public class ParkingPassResponse {

    private final int code;

    public ParkingPassResponse(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
