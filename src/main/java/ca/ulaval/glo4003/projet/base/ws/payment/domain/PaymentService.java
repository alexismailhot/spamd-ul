package ca.ulaval.glo4003.projet.base.ws.payment.domain;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public interface PaymentService {
    void processPayment(CreditCard creditCard, Price price);
}
