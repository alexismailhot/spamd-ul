package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.exception.OffenseAlreadyPaidException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class OffenseAlreadyPaidExceptionMapper implements ExceptionMapper<OffenseAlreadyPaidException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public OffenseAlreadyPaidExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(OffenseAlreadyPaidException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
