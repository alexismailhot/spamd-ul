package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.WeeklyAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionAssembler;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;

public class WeeklyAccessInformationAssembler {

    private final VehicleAssembler vehicleAssembler;
    private final DriverAssembler driverAssembler;
    private final SessionAssembler sessionAssembler;

    public WeeklyAccessInformationAssembler(VehicleAssembler vehicleAssembler, DriverAssembler driverAssembler, SessionAssembler sessionAssembler) {
        this.vehicleAssembler = vehicleAssembler;
        this.driverAssembler = driverAssembler;
        this.sessionAssembler = sessionAssembler;
    }

    public WeeklyAccessInformation toDomain(WeeklyAccessRequest weeklyAccessRequest) {
        Driver driver = driverAssembler.toDomain(weeklyAccessRequest.getDriver());
        Vehicle vehicle = vehicleAssembler.toDomain(weeklyAccessRequest.getVehicle());
        Session session = sessionAssembler.toDomain(weeklyAccessRequest.getSession());
        return new WeeklyAccessInformation(driver, vehicle, weeklyAccessRequest.getAccessDay(), session);
    }
}
