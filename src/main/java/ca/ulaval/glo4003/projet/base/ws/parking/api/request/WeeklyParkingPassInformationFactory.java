package ca.ulaval.glo4003.projet.base.ws.parking.api.request;

import ca.ulaval.glo4003.projet.base.ws.parking.api.exception.NoDeliveryMethodException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import org.apache.commons.lang3.NotImplementedException;

public class WeeklyParkingPassInformationFactory {

    private final PostalAddressAssembler postalAddressAssembler;

    public WeeklyParkingPassInformationFactory(PostalAddressAssembler postalAddressAssembler) {
        this.postalAddressAssembler = postalAddressAssembler;
    }

    public WeeklyParkingPassInformation create(WeeklyParkingPassRequest weeklyParkingPassRequest) {
        DeliveryMethod deliveryMethod = weeklyParkingPassRequest.getDeliveryMethod();
        ParkingZone parkingZone = weeklyParkingPassRequest.getParkingZone();
        DayOfWeek parkingDay = weeklyParkingPassRequest.getParkingDay();

        SessionRequest sessionRequest = weeklyParkingPassRequest.getSession();
        Year year = sessionRequest.getYear();
        SessionType sessionType = sessionRequest.getSessionType();

        switch (deliveryMethod) {
            case EMAIL:
                String email = weeklyParkingPassRequest.getEmail().orElseThrow(() -> new NoDeliveryMethodException(deliveryMethod));
                return new WeeklyParkingPassInformation(parkingZone, deliveryMethod, email, parkingDay, new Session(sessionType, year));
            case POSTAL:
                PostalAddressRequest postalAddress = weeklyParkingPassRequest
                    .getPostalAddress()
                    .orElseThrow(() -> new NoDeliveryMethodException(deliveryMethod));

                return new WeeklyParkingPassInformation(
                    parkingZone,
                    deliveryMethod,
                    parkingDay,
                    postalAddressAssembler.toDomain(postalAddress),
                    new Session(sessionType, year)
                );
            case SSP:
                return new WeeklyParkingPassInformation(parkingZone, deliveryMethod, parkingDay, new Session(sessionType, year));
            default:
                throw new NotImplementedException(String.format("Delivery method %s is not supported", deliveryMethod.toString()));
        }
    }
}
