package ca.ulaval.glo4003.projet.base.ws.parking.api.request;

import ca.ulaval.glo4003.projet.base.ws.parking.api.exception.NoDeliveryMethodException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.Year;
import org.apache.commons.lang3.NotImplementedException;

public class SessionParkingPassInformationFactory {

    private final PostalAddressAssembler postalAddressAssembler;

    public SessionParkingPassInformationFactory(PostalAddressAssembler postalAddressAssembler) {
        this.postalAddressAssembler = postalAddressAssembler;
    }

    public SessionParkingPassInformation create(SessionParkingPassRequest sessionParkingPassRequest) {
        DeliveryMethod deliveryMethod = sessionParkingPassRequest.getDeliveryMethod();
        ParkingZone parkingZone = sessionParkingPassRequest.getParkingZone();

        SessionRequest sessionRequest = sessionParkingPassRequest.getSession();
        Year year = sessionRequest.getYear();
        SessionType sessionType = sessionRequest.getSessionType();

        switch (deliveryMethod) {
            case EMAIL:
                String email = sessionParkingPassRequest.getEmail().orElseThrow(() -> new NoDeliveryMethodException(deliveryMethod));
                return new SessionParkingPassInformation(parkingZone, deliveryMethod, email, new Session(sessionType, year));
            case POSTAL:
                PostalAddressRequest postalAddress = sessionParkingPassRequest
                    .getPostalAddress()
                    .orElseThrow(() -> new NoDeliveryMethodException(deliveryMethod));

                return new SessionParkingPassInformation(
                    parkingZone,
                    deliveryMethod,
                    postalAddressAssembler.toDomain(postalAddress),
                    new Session(sessionType, year)
                );
            case SSP:
                return new SessionParkingPassInformation(parkingZone, deliveryMethod, new Session(sessionType, year));
            default:
                throw new NotImplementedException(String.format("Delivery method %s is not supported", deliveryMethod.toString()));
        }
    }
}
