package ca.ulaval.glo4003.projet.base.ws.offense.domain;

import java.util.Objects;

public class OffenseId {

    private final int id;

    public OffenseId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OffenseId offenseId = (OffenseId) o;
        return id == offenseId.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
