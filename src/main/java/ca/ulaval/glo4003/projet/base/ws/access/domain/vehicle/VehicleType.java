package ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle;

public enum VehicleType {
    GREEDY("Gourmande"),
    ECONOMIC("Économique"),
    ZERO_POLLUTION("0 pollution"),
    ECONOMIC_HYBRID("Hybride Économique"),
    SUPER_ECONOMIC("Super économique");

    private final String name;

    VehicleType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static VehicleType fromString(String vehicleTypeValue) {
        try {
            return VehicleType.valueOf(vehicleTypeValue.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidVehicleTypeException(vehicleTypeValue);
        }
    }
}
