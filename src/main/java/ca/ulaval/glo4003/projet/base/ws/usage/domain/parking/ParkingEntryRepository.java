package ca.ulaval.glo4003.projet.base.ws.usage.domain.parking;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import java.time.LocalDate;
import java.util.List;

public interface ParkingEntryRepository {
    void save(ParkingEntry parkingEntry);

    List<ParkingEntry> getByZoneInPeriod(ParkingZone zone, LocalDate from, LocalDate until);
}
