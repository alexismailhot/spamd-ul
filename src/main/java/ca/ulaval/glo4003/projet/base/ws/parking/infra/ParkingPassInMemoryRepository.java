package ca.ulaval.glo4003.projet.base.ws.parking.infra;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRepository;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ParkingPassInMemoryRepository implements ParkingPassRepository {
    private final Map<ParkingCode, ParkingPass> parkingPasses = new HashMap<>();

    @Override
    public void save(ParkingPass parkingPass) {
        parkingPasses.put(parkingPass.getParkingCode(), parkingPass);
    }

    @Override
    public int getCount() {
        return parkingPasses.size();
    }

    @Override
    public Optional<ParkingPass> findByParkingCode(ParkingCode code) {
      return Optional.ofNullable(parkingPasses.get(code));
    }
}
