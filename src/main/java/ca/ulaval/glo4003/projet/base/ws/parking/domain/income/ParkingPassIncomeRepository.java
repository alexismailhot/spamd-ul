package ca.ulaval.glo4003.projet.base.ws.parking.domain.income;

import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.util.List;

public interface ParkingPassIncomeRepository {
    void save(ParkingPassIncome parkingPassIncome);

    List<ParkingPassIncome> findByUniversityYear(UniversityYear universityYear);

    List<ParkingPassIncome> getAll();
}
