package ca.ulaval.glo4003.projet.base.ws.access.domain.driver;

import java.time.LocalDate;
import java.util.Objects;

public class Driver {

    private final String name;
    private final LocalDate birthdate;
    private final Sex sex;

    public Driver(String name, LocalDate birthdate, Sex sex) {
        this.name = name;
        this.birthdate = birthdate;
        this.sex = sex;
    }

    public boolean hasBirthDateBefore(LocalDate otherBirthdate) {
        return birthdate.isBefore(otherBirthdate);
    }

    public boolean hasBirthDateAfter(LocalDate otherBirthdate) {
        return birthdate.isAfter(otherBirthdate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Driver driver = (Driver) o;
        return Objects.equals(name, driver.name) && Objects.equals(birthdate, driver.birthdate) && sex == driver.sex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthdate, sex);
    }
}
