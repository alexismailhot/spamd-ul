package ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle;

public interface VehicleConfig {
    Integer getMaximumVehicleYear();

    Integer getMinimumVehicleYear();
}
