package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

public interface ParkingCodeSender {
    void send();
}
