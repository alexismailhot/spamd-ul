package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;

public class VehicleAssembler {

    public Vehicle toDomain(VehicleRequest vehicleRequest) {
        return new Vehicle(
            vehicleRequest.getModel(),
            vehicleRequest.getBrand(),
            vehicleRequest.getModelYear(),
            vehicleRequest.getPlate(),
            vehicleRequest.getType()
        );
    }
}
