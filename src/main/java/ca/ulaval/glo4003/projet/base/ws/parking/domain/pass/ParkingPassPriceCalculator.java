package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethodRatesConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;

public class ParkingPassPriceCalculator {

    private final ParkingPassRatesConfig parkingPassRatesConfig;
    private final DeliveryMethodRatesConfig deliveryMethodRatesConfig;

    public ParkingPassPriceCalculator(ParkingPassRatesConfig parkingPassRatesConfig, DeliveryMethodRatesConfig deliveryMethodRatesConfig) {
        this.parkingPassRatesConfig = parkingPassRatesConfig;
        this.deliveryMethodRatesConfig = deliveryMethodRatesConfig;
    }

    public Price calculatePrice(ParkingZone parkingZone, ParkingPassType parkingPassType, DeliveryMethod deliveryMethod) {
        Price parkingPrice = parkingPassRatesConfig.getParkingPassRate(parkingZone, parkingPassType);
        Price deliveryPrice = deliveryMethodRatesConfig.getDeliveryMethodRate(deliveryMethod);
        return parkingPrice.add(deliveryPrice);
    }
}
