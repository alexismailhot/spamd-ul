package ca.ulaval.glo4003.projet.base.ws.parking.infra.code;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSSPDeliveryInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSSPDeliveryInformationRepository;
import java.util.ArrayList;
import java.util.List;

public class ParkingCodeSSPDeliveryInformationInMemoryRepository implements ParkingCodeSSPDeliveryInformationRepository {

    private final List<ParkingCodeSSPDeliveryInformation> parkingCodeSSPDeliveryInformations = new ArrayList<>();

    @Override
    public void save(ParkingCodeSSPDeliveryInformation parkingCodeSSPDeliveryInformation) {
        this.parkingCodeSSPDeliveryInformations.add(parkingCodeSSPDeliveryInformation);
    }
}
