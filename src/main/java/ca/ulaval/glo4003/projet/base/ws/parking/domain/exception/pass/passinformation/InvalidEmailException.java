package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation;

public class InvalidEmailException extends InvalidParkingPassInformationException {

    private final String email;
    private static final String INVALID_EMAIL_ERROR = "INVALID_EMAIL";

    public InvalidEmailException(String email) {
        this.email = email;
    }

    @Override
    public String getError() {
        return INVALID_EMAIL_ERROR;
    }

    @Override
    public String getDescription() {
        return String.format("The following email is invalid: %s", email);
    }
}
