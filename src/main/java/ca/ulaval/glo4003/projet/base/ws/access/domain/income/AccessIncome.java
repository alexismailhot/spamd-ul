package ca.ulaval.glo4003.projet.base.ws.access.domain.income;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class AccessIncome {

    private final Price price;
    private final VehicleType vehicleType;
    private final LocalDateTime created;

    public AccessIncome(Price price, VehicleType vehicleType, LocalDateTime created) {
        this.price = price;
        this.vehicleType = vehicleType;
        this.created = created;
    }

    public Price getPrice() {
        return price;
    }

    public boolean isVehicleType(VehicleType vehicleType) {
        return this.vehicleType == vehicleType;
    }

    public boolean isIn(UniversityYear universityYear) {
        LocalDate createdDate = created.toLocalDate();
        return universityYear.includes(createdDate);
    }
}
