package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;

public class NoOpDeliveryMethodInformationValidator implements DeliveryMethodInformationValidator {

    @Override
    public void validate(ParkingPassInformation parkingPassInformation) {
        // noop
    }
}
