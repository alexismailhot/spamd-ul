package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass;

public abstract class InvalidParkingPassException extends RuntimeException {

    private static final String INVALID_PARKING_CODE = "INVALID_PARKING_CODE";

    public String getError() {
        return INVALID_PARKING_CODE;
    }

    public abstract String getDescription();
}
