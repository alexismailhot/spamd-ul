package ca.ulaval.glo4003.projet.base.ws.payment.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreditCardRequest {

    private final String number;
    private final String name;
    private final String expiryDate;
    private final String ccv;

    @JsonCreator
    public CreditCardRequest(
        @JsonProperty(value = "number", required = true) String number,
        @JsonProperty(value = "name", required = true) String name,
        @JsonProperty(value = "expiryDate", required = true) String expiryDate,
        @JsonProperty(value = "ccv", required = true) String ccv
    ) {
        this.number = number;
        this.name = name;
        this.expiryDate = expiryDate;
        this.ccv = ccv;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getCcv() {
        return ccv;
    }
}
