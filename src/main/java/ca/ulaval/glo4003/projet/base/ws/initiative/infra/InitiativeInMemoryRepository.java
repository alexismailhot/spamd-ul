package ca.ulaval.glo4003.projet.base.ws.initiative.infra;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class InitiativeInMemoryRepository implements InitiativeRepository {

    private final HashMap<InitiativeCode, Initiative> initiatives = new HashMap<>();

    @Override
    public void save(Initiative initiative) {
        initiatives.put(initiative.getInitiativeCode(), initiative);
    }

    @Override
    public List<Initiative> getAll() {
        return new ArrayList<>(initiatives.values());
    }

    @Override
    public Optional<Initiative> findByInitiativeCode(InitiativeCode initiativeCode) {
        return Optional.ofNullable(initiatives.get(initiativeCode));
    }
}
