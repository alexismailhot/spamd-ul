package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

public class ParkingCodeSSPDeliveryInformation {

    private final ParkingCode parkingCode;

    public ParkingCodeSSPDeliveryInformation(ParkingCode parkingCode) {
        this.parkingCode = parkingCode;
    }

    public ParkingCode getParkingCode() {
        return parkingCode;
    }
}
