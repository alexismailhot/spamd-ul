package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import java.util.Objects;

public class ParkingCode {
    private final int code;

    public ParkingCode(int code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParkingCode that = (ParkingCode) o;

        return code == (that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    public int getCode() {
        return code;
    }
}
