package ca.ulaval.glo4003.projet.base.ws.usage.service;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntry;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntryRepository;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UsageService {

    private final ParkingEntryRepository parkingEntryRepository;

    public UsageService(ParkingEntryRepository parkingEntryRepository) {
        this.parkingEntryRepository = parkingEntryRepository;
    }

    public ParkingZoneUsageSummary generateZoneUsageSummary(ParkingZone zone, LocalDate from, LocalDate until) {
        List<ParkingEntry> entries = parkingEntryRepository.getByZoneInPeriod(zone, from, until);
        return new ParkingZoneUsageSummary(entries, zone, from, until);
    }

    public List<ParkingZoneUsageSummary> generateAllZonesUsageSummary(LocalDate from, LocalDate until) {
        List<ParkingZoneUsageSummary> summariesByZone = new ArrayList<>();
        for (ParkingZone zone : ParkingZone.values()) {
            summariesByZone.add(generateZoneUsageSummary(zone, from, until));
        }
        return summariesByZone;
    }

    public void recordUsage(ParkingEntry parkingEntry) {
        parkingEntryRepository.save(parkingEntry);
    }
}
