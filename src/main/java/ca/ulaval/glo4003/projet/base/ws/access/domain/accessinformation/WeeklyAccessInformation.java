package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.time.DayOfWeek;
import java.util.Objects;

public class WeeklyAccessInformation extends SessionAccessInformation {

    private final DayOfWeek accessDay;

    public WeeklyAccessInformation(Driver driver, Vehicle vehicle, DayOfWeek accessDay, Session session) {
        super(driver, vehicle, session);
        this.accessDay = accessDay;
    }

    public DayOfWeek getAccessDay() {
        return accessDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WeeklyAccessInformation that = (WeeklyAccessInformation) o;
        return accessDay == that.accessDay;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), accessDay);
    }
}
