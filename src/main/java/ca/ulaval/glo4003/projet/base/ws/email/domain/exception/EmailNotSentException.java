package ca.ulaval.glo4003.projet.base.ws.email.domain.exception;

public class EmailNotSentException extends RuntimeException {

    private final String subject;
    private final String to;

    private static final String EMAIL_NOT_SENT_ERROR = "EMAIL_NOT_SENT";

    public EmailNotSentException(String subject, String to) {
        this.subject = subject;
        this.to = to;
    }

    public String getError() {
        return EMAIL_NOT_SENT_ERROR;
    }

    public String getDescription() {
        return String.format("Email with subject %s could not be sent to %s", subject, to);
    }
}
