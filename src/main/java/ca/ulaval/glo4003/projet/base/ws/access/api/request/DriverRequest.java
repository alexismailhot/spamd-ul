package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Sex;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;

public class DriverRequest {

    private final String name;
    private final LocalDate birthdate;
    private final Sex sex;

    @JsonCreator
    public DriverRequest(
        @JsonProperty(value = "name", required = true) String name,
        @JsonProperty(value = "birthdate", required = true) LocalDate birthdate,
        @JsonProperty(value = "sex", required = true) Sex sex
    ) {
        this.name = name;
        this.birthdate = birthdate;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public Sex getSex() {
        return sex;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }
}
