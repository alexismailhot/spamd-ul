package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;

public class SessionParkingPassFactory {

    private final ParkingPassRepository parkingPassRepository;

    public SessionParkingPassFactory(ParkingPassRepository parkingPassRepository) {
        this.parkingPassRepository = parkingPassRepository;
    }

    public SessionParkingPass create(ParkingZone parkingZone, Session session) {
        int count = parkingPassRepository.getCount();
        ParkingCode parkingCode = new ParkingCode(count);
        return new SessionParkingPass(parkingCode, parkingZone, session);
    }
}
