package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass;

import java.util.Optional;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;

public interface ParkingPassRepository {
    void save(ParkingPass parkingPass);

    int getCount();

    Optional<ParkingPass> findByParkingCode(ParkingCode code);
}
