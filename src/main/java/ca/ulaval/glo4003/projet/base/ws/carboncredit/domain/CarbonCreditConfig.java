package ca.ulaval.glo4003.projet.base.ws.carboncredit.domain;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public interface CarbonCreditConfig {
    Price getUnitPrice();
    InitiativeCode getCarbonCreditInitiativeCode();
    String getInitiativeName();
}
