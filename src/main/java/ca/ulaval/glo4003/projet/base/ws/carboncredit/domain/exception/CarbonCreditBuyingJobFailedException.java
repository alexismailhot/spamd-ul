package ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.exception;

import org.quartz.SchedulerException;

public class CarbonCreditBuyingJobFailedException extends RuntimeException {

    private static final String ERROR_NAME = "CARBON_CREDIT_BUYING_JOB_FAILED_EXCEPTION";
    private final SchedulerException exception;

    public CarbonCreditBuyingJobFailedException(SchedulerException exception) {
        this.exception = exception;
    }

    public String getError() {
        return ERROR_NAME;
    }

    public String getDescription() {
        return String.format("Carbon credit buying job failed. Error : %s", exception.getMessage());
    }
}
