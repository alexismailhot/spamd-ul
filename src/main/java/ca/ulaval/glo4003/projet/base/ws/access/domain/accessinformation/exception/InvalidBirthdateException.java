package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.exception;

import java.time.LocalDate;

public class InvalidBirthdateException extends InvalidAccessInformationException {

    private static final String INVALID_BIRTHDATE_ERROR = "INVALID_BIRTHDATE";

    private final LocalDate minimumBirthdate;
    private final LocalDate maximumBirthdate;

    public InvalidBirthdateException(LocalDate minimumBirthdate, LocalDate maximumBirthdate) {
        this.minimumBirthdate = minimumBirthdate;
        this.maximumBirthdate = maximumBirthdate;
    }

    @Override
    public String getError() {
        return INVALID_BIRTHDATE_ERROR;
    }

    @Override
    public String getDescription() {
        return String.format("Driver birthdate can't be before %s or after %s", maximumBirthdate.toString(), minimumBirthdate.toString());
    }
}
