package ca.ulaval.glo4003.projet.base.ws.carboncredit.infra;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.CarbonCreditConfig;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class CarbonCreditULavalConfig implements CarbonCreditConfig {

    private static final Price UNIT_PRICE = Price.of(21.81);
    private static final InitiativeCode INITIATIVE_CODE = new InitiativeCode("MCARB");
    private static final String CARBON_CREDIT_INITIATIVE_NAME = "Crédits carbone";

    @Override
    public Price getUnitPrice() {
        return UNIT_PRICE;
    }

    @Override
    public InitiativeCode getCarbonCreditInitiativeCode() {
        return INITIATIVE_CODE;
    }

    @Override
    public String getInitiativeName() {
        return CARBON_CREDIT_INITIATIVE_NAME;
    }
}
