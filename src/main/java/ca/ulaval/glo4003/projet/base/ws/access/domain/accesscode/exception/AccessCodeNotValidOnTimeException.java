package ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import java.time.LocalDateTime;

public class AccessCodeNotValidOnTimeException extends InvalidAccessCodeException {

    private final AccessCode accessCode;
    private final LocalDateTime timeOfAttemptedAccess;

    public AccessCodeNotValidOnTimeException(AccessCode accessCode, LocalDateTime timeOfAttemptedAccess) {
        this.accessCode = accessCode;
        this.timeOfAttemptedAccess = timeOfAttemptedAccess;
    }

    @Override
    public String getDescription() {
        return String.format("Access code %s is not valid on time of attempted access %s.", accessCode.getCodeNumber(), timeOfAttemptedAccess.toString());
    }
}
