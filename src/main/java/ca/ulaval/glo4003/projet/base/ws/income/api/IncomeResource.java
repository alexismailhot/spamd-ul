package ca.ulaval.glo4003.projet.base.ws.income.api;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/income")
public interface IncomeResource {
    @GET
    @Path("access/yearly")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get total access income for current university year", responses = { @ApiResponse(responseCode = "200") })
    Response getAccessIncomeSummaryForCurrentUniversityYear();

    @GET
    @Path("access/{vehicleType}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get total access sales for given type of vehicle",
        responses = { @ApiResponse(responseCode = "200"), @ApiResponse(responseCode = "400", description = "Invalid vehicle type") }
    )
    Response getAllAccessIncomeForVehicleType(@PathParam("vehicleType") VehicleType vehicleType);

    @GET
    @Path("access/yearly/{vehicleType}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Get total access sales for given type of vehicle for current university year",
        responses = { @ApiResponse(responseCode = "200"), @ApiResponse(responseCode = "400", description = "Invalid vehicle type") }
    )
    Response getAccessIncomeForCurrentUniversityYearForVehicleType(@PathParam("vehicleType") String vehicleType);

    @GET
    @Path("parking/yearly")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get total access income from parking for current university year", responses = { @ApiResponse(responseCode = "200") })
    Response getParkingPassIncomeSummaryForCurrentUniversityYear();
}
