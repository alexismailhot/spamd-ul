package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception.InvalidAccessCodeException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidAccessCodeExceptionMapper implements ExceptionMapper<InvalidAccessCodeException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public InvalidAccessCodeExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InvalidAccessCodeException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.FORBIDDEN).entity(errorResponse).build();
    }
}
