package ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode;

public enum OffenseCode {
    ZONE_01,
    VIG_01,
    TEMPS_01,
    ZONE_02,
    VIG_02,
    VIG_03,
    VIG_04,
    ZONE_03,
}
