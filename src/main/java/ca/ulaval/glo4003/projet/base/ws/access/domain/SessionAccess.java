package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.time.LocalDateTime;

public class SessionAccess extends Access {

    private final Session session;

    public SessionAccess(AccessCode accessCode, Session session) {
        super(accessCode);
        this.session = session;
    }

    @Override
    public boolean isValidOn(LocalDateTime time) {
        return session.includes(time.toLocalDate());
    }
}
