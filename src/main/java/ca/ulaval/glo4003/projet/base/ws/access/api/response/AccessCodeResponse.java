package ca.ulaval.glo4003.projet.base.ws.access.api.response;

public class AccessCodeResponse {

    private final int accessCode;

    public AccessCodeResponse(int accessCode) {
        this.accessCode = accessCode;
    }

    public int getAccessCode() {
        return accessCode;
    }
}
