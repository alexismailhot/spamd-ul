package ca.ulaval.glo4003.projet.base.ws.access.infra.rates;

import ca.ulaval.glo4003.projet.base.ws.access.domain.AccessTimeType;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class AccessRatesCSVReader {

    private final Map<String, VehicleType> stringToVehicleType = new HashMap<>();
    private final String accessRatesCsvPath;

    public AccessRatesCSVReader(String accessRatesCsvPath) {
        this.accessRatesCsvPath = accessRatesCsvPath;
        Arrays.stream(VehicleType.values()).forEach(type -> stringToVehicleType.put(type.getName(), type));
    }

    public Map<VehicleType, Map<AccessTimeType, Price>> read() throws FileNotFoundException {
        final Map<VehicleType, Map<AccessTimeType, Price>> accessRates = new HashMap<>();
        try (Scanner scanner = new Scanner(new File(accessRatesCsvPath))) {
            scanner.nextLine();
            while (scanner.hasNextLine()) {
                getRatesFromLine(scanner.nextLine(), accessRates);
            }
        }
        return accessRates;
    }

    private void getRatesFromLine(String line, Map<VehicleType, Map<AccessTimeType, Price>> accessRates) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        VehicleType vehicleType = stringToVehicleType.get(values.get(0));
        Map<AccessTimeType, Price> accessTimeTypeRates = new HashMap<>();
        accessTimeTypeRates.put(AccessTimeType.ONE_HOUR, Price.of(Double.parseDouble(values.get(1))));
        accessTimeTypeRates.put(AccessTimeType.ONE_DAY, Price.of(Double.parseDouble(values.get(2))));
        accessTimeTypeRates.put(AccessTimeType.ONE_DAY_A_WEEK_FOR_ONE_SEMESTER, Price.of(Double.parseDouble(values.get(3))));
        accessTimeTypeRates.put(AccessTimeType.ONE_SEMESTER, Price.of(Double.parseDouble(values.get(4))));
        accessTimeTypeRates.put(AccessTimeType.TWO_SEMESTERS, Price.of(Double.parseDouble(values.get(5))));
        accessTimeTypeRates.put(AccessTimeType.THREE_SEMESTERS, Price.of(Double.parseDouble(values.get(6))));
        accessRates.put(vehicleType, accessTimeTypeRates);
    }
}
