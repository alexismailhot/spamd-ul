package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class AccessPriceCalculator {

    private final AccessRatesConfig accessRatesConfig;

    public AccessPriceCalculator(AccessRatesConfig accessRatesConfig) {
        this.accessRatesConfig = accessRatesConfig;
    }

    public Price calculatePrice(VehicleType vehicleType, AccessTimeType accessTimeType) {
        return accessRatesConfig.getAccessRates().get(vehicleType).get(accessTimeType);
    }
}
