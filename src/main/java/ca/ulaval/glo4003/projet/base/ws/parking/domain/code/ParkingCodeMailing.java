package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;

public class ParkingCodeMailing {

    private final ParkingCode parkingCode;
    private final PostalAddress postalAddress;

    public ParkingCodeMailing(ParkingCode parkingCode, PostalAddress postalAddress) {
        this.parkingCode = parkingCode;
        this.postalAddress = postalAddress;
    }

    public ParkingCode getParkingCode() {
        return parkingCode;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }
}
