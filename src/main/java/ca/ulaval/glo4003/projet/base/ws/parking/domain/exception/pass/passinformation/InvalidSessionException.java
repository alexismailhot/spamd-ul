package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation;

import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;

public class InvalidSessionException extends InvalidParkingPassInformationException {
    private static final String INVALID_SESSION_CODE_ERROR = "INVALID_SESSION";

    private final Session session;

    public InvalidSessionException(Session session) {
        this.session = session;
    }

    @Override
    public String getError() {
        return INVALID_SESSION_CODE_ERROR;
    }

    @Override
    public String getDescription() {
        return String.format("The following session is unavailable for buying at this time : %s", session.toString());
    }
}
