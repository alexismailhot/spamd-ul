package ca.ulaval.glo4003.projet.base.ws.usage.api.response;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;

public class DailyParkingZoneUsageSummaryResponse {

    private final int activity;
    private final ParkingZone zone;

    public DailyParkingZoneUsageSummaryResponse(int activity, ParkingZone zone) {
        this.activity = activity;
        this.zone = zone;
    }

    public int getActivity() {
        return activity;
    }

    public ParkingZone getZone() {
        return zone;
    }
}
