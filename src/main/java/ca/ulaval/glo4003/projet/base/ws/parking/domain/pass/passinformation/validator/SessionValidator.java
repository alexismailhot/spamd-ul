package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidSessionException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;

public class SessionValidator implements SessionInformationValidator {
    private final Clock clock;

    public SessionValidator(Clock clock) {
        this.clock = clock;
    }

    @Override
    public void validate(ParkingPassInformation parkingPassInformation) {
        SessionParkingPassInformation sessionParkingPassInformation = (SessionParkingPassInformation) parkingPassInformation;
        LocalDate now = clock.instant().atZone(ZoneId.systemDefault()).toLocalDate();
        Session session = sessionParkingPassInformation.getSession();

        if (session.isBefore(now)) {
            throw new InvalidSessionException(session);
        }

        UniversityYear currentUniversityYear = UniversityYear.of(now);
        UniversityYear nextUniversityYear = currentUniversityYear.next();
        UniversityYear sessionUniversityYear = session.getUniversityYear();

        if (!(currentUniversityYear.equals(sessionUniversityYear) || nextUniversityYear.equals(sessionUniversityYear))) {
            throw new InvalidSessionException(session);
        }
    }
}
