package ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception;

import ca.ulaval.glo4003.projet.base.ws.money.api.CapitalFormatter;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class InsufficientInitiativeFundsException extends RuntimeException {

    private static final String INSUFFICIENT_INITIATIVE_FUNDS = "INSUFFICIENT_INITIATIVE_FUNDS";
    private final Price amount;

    public InsufficientInitiativeFundsException(Price amount) {
        this.amount = amount;
    }

    public String getError() {
        return INSUFFICIENT_INITIATIVE_FUNDS;
    }

    public String getDescription() {
        return String.format("The amount of %s$ exceeds available funds", CapitalFormatter.fromBigDecimal(amount.getAmount()));
    }
}
