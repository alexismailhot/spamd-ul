package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

public class ParkingCodePostalSender implements ParkingCodeSender {

    private final ParkingCodeMailingRepository parkingCodeMailingRepository;
    private final ParkingCodeMailing parkingCodeMailing;

    public ParkingCodePostalSender(ParkingCodeMailingRepository parkingCodeMailingRepository, ParkingCodeMailing parkingCodeMailing) {
        this.parkingCodeMailingRepository = parkingCodeMailingRepository;
        this.parkingCodeMailing = parkingCodeMailing;
    }

    @Override
    public void send() {
        parkingCodeMailingRepository.save(parkingCodeMailing);
        System.out.printf(
            "[SPAMD-UL] Parking code %d needs to be sent manually to %s",
            parkingCodeMailing.getParkingCode().getCode(),
            parkingCodeMailing.getPostalAddress().toString()
        );
    }
}
