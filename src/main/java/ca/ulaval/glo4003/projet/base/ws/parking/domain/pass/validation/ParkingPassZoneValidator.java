package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotValidInZoneException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;

public class ParkingPassZoneValidator extends ParkingPassValidator {

    public ParkingPassZoneValidator(ParkingPassValidator parkingPassValidator) {
        super(parkingPassValidator);
    }

    @Override
    public void validate(ParkingPassValidation parkingPassValidation, ParkingPass parkingPass) {
        ParkingZone zoneUsed = parkingPassValidation.getParkingZone();
        if (!parkingPass.isValidIn(zoneUsed)) {
            throw new ParkingPassNotValidInZoneException(parkingPass.getParkingCode(), zoneUsed);
        }
        super.validate(parkingPassValidation, parkingPass);
    }
}
