package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception.InsufficientInitiativeFundsException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InsufficientInitiativeFundsExceptionMapper implements ExceptionMapper<InsufficientInitiativeFundsException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public InsufficientInitiativeFundsExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InsufficientInitiativeFundsException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.FORBIDDEN).entity(errorResponse).build();
    }
}
