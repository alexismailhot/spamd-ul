package ca.ulaval.glo4003.projet.base.ws.parking.infra.rates;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassType;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ParkingRatesCSVReader {

    private final Map<String, ParkingZone> stringToParkingZone = new HashMap<>();
    private final String parkingRatesCSVPath;

    public ParkingRatesCSVReader(String parkingRatesCSVPath) {
        this.parkingRatesCSVPath = parkingRatesCSVPath;
        Arrays.stream(ParkingZone.values()).forEach(zone -> stringToParkingZone.put(zone.getName(), zone));
    }

    public Map<ParkingZone, Map<ParkingPassType, Price>> read() throws FileNotFoundException {
        final Map<ParkingZone, Map<ParkingPassType, Price>> parkingRates = new HashMap<>();
        try (Scanner scanner = new Scanner(new File(this.parkingRatesCSVPath))) {
            scanner.nextLine();
            while (scanner.hasNextLine()) {
                getRatesFromLine(scanner.nextLine(), parkingRates);
            }
        }
        return parkingRates;
    }

    private void getRatesFromLine(String line, Map<ParkingZone, Map<ParkingPassType, Price>> parkingRates) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        ParkingZone parkingZone = stringToParkingZone.get(values.get(0));
        Map<ParkingPassType, Price> parkingPassTypeRates = new HashMap<>();
        parkingPassTypeRates.put(ParkingPassType.ONE_DAY_A_WEEK_FOR_A_SEMESTER, Price.of(Long.parseLong(values.get(1))));
        parkingPassTypeRates.put(ParkingPassType.MONTHLY, Price.of(Long.parseLong(values.get(2))));
        parkingPassTypeRates.put(ParkingPassType.ONE_SEMESTER, Price.of(Long.parseLong(values.get(3))));
        parkingPassTypeRates.put(ParkingPassType.TWO_SEMESTERS, Price.of(Long.parseLong(values.get(4))));
        parkingPassTypeRates.put(ParkingPassType.THREE_SEMESTERS, Price.of(Long.parseLong(values.get(5))));
        parkingRates.put(parkingZone, parkingPassTypeRates);
    }
}
