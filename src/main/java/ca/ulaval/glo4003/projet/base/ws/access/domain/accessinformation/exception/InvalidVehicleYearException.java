package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.exception;

public class InvalidVehicleYearException extends InvalidAccessInformationException {

    private static final String INVALID_VEHICLE_YEAR_ERROR = "INVALID_VEHICLE_YEAR";

    private final Integer maximumVehicleYear;
    private final Integer minimumVehicleYear;

    public InvalidVehicleYearException(Integer maximumVehicleYear, Integer minimumVehicleYear) {
        super();
        this.maximumVehicleYear = maximumVehicleYear;
        this.minimumVehicleYear = minimumVehicleYear;
    }

    @Override
    public String getError() {
        return INVALID_VEHICLE_YEAR_ERROR;
    }

    @Override
    public String getDescription() {
        return String.format("Vehicle's model year must be between %s and %s", minimumVehicleYear, maximumVehicleYear);
    }
}
