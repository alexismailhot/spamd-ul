package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import java.util.Optional;

public abstract class ParkingPassValidator {

    private final ParkingPassValidator parkingPassValidator;

    public ParkingPassValidator(ParkingPassValidator parkingPassValidator) {
        this.parkingPassValidator = parkingPassValidator;
    }

    public void validate(ParkingPassValidation parkingPassValidation, ParkingPass parkingPass) {
        Optional.ofNullable(parkingPassValidator).ifPresent(validator -> validator.validate(parkingPassValidation, parkingPass));
    }
}
