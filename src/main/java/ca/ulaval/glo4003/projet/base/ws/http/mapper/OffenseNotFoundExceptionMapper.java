package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.exception.OffenseNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class OffenseNotFoundExceptionMapper implements ExceptionMapper<OffenseNotFoundException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public OffenseNotFoundExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(OffenseNotFoundException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
