package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.InvalidParkingZoneException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidParkingZoneExceptionMapper implements ExceptionMapper<InvalidParkingZoneException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public InvalidParkingZoneExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InvalidParkingZoneException exception) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(exception.getError(), exception.getDescription());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
