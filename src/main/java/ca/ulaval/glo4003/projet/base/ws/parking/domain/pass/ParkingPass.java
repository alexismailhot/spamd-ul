package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass;

import java.time.LocalDate;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;

public abstract class ParkingPass {
    private final ParkingCode parkingCode;
    private final ParkingZone parkingZone;

    public ParkingPass(ParkingCode parkingCode, ParkingZone parkingZone) {
        this.parkingCode = parkingCode;
        this.parkingZone = parkingZone;
    }

    public abstract boolean isValidOn(LocalDate date);

    public boolean isValidIn(ParkingZone zone) {
        return parkingZone.equals(zone);
    }

    public ParkingCode getParkingCode() {
        return parkingCode;
    }

    public ParkingZone getParkingZone() {
        return parkingZone;
    }
}
