package ca.ulaval.glo4003.projet.base.ws.carboncredit.service;

public class CarbonCreditService {

    private final AutomaticJob automaticJob;

    public CarbonCreditService(AutomaticJob automaticJob) {
        this.automaticJob = automaticJob;
    }

    public void activateAutomaticCarbonCreditSpending() {
        automaticJob.activate();
    }

    public void deactivateAutomaticCarbonCreditSpending() {
        automaticJob.deactivate();
    }
}
