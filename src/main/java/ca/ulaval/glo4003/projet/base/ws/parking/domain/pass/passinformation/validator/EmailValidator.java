package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidEmailException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class EmailValidator implements DeliveryMethodInformationValidator {

    @Override
    public void validate(ParkingPassInformation parkingPassInformation) {
        String email = parkingPassInformation.getEmail().orElseThrow(IllegalStateException::new);
        try {
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
        } catch (AddressException exception) {
            throw new InvalidEmailException(email);
        }
    }
}
