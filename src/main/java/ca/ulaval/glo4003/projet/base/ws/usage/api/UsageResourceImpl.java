package ca.ulaval.glo4003.projet.base.ws.usage.api;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequest;
import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.DailyParkingZoneUsageSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.DailyParkingZoneUsageSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.ParkingZoneUsageSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.ParkingZoneUsageSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;
import ca.ulaval.glo4003.projet.base.ws.usage.service.UsageService;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.core.Response;

public class UsageResourceImpl implements UsageResource {

    private final UsageService usageService;
    private final ParkingZoneUsageSummaryResponseAssembler parkingZoneUsageSummaryResponseAssembler;
    private final Clock clock;
    private final AddUsageRequestAssembler addUsageRequestAssembler;
    private final DailyParkingZoneUsageSummaryResponseAssembler dailyParkingZoneUsageSummaryResponseAssembler;

    public UsageResourceImpl(
        UsageService usageService,
        ParkingZoneUsageSummaryResponseAssembler parkingZoneUsageSummaryResponseAssembler,
        Clock clock,
        AddUsageRequestAssembler addUsageRequestAssembler,
        DailyParkingZoneUsageSummaryResponseAssembler dailyParkingZoneUsageSummaryResponseAssembler
    ) {
        this.usageService = usageService;
        this.parkingZoneUsageSummaryResponseAssembler = parkingZoneUsageSummaryResponseAssembler;
        this.clock = clock;
        this.addUsageRequestAssembler = addUsageRequestAssembler;
        this.dailyParkingZoneUsageSummaryResponseAssembler = dailyParkingZoneUsageSummaryResponseAssembler;
    }

    @Override
    public Response getPeriodUsage(String zone, LocalDate from, LocalDate until) {
        ParkingZone parkingZone = ParkingZone.fromString(zone);
        return Response.ok().entity(getUsage(parkingZone, from, until)).build();
    }

    @Override
    public Response getDailyUsage(String zone, LocalDate date) {
        ParkingZone parkingZone = ParkingZone.fromString(zone);
        return Response.ok().entity(getDailyUsageForZone(parkingZone, date, date)).build();
    }

    @Override
    public Response getMonthlyUsage(String zone, Month month, Year year) {
        LocalDate now = clock.instant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate firstDateOfMonth = LocalDate.of(year.getValue(), month.getValue(), 1);
        ParkingZone parkingZone = ParkingZone.fromString(zone);

        if (isUsageOfCurrentMonth(month, year, now)) {
            return Response.ok().entity(getUsage(parkingZone, firstDateOfMonth, now)).build();
        }

        LocalDate lastDateOfMonth = getLastDateOfMonth(month.getValue(), year.getValue());
        return Response.ok().entity(getUsage(parkingZone, firstDateOfMonth, lastDateOfMonth)).build();
    }

    @Override
    public Response getAllZonesPeriodUsage(LocalDate fromDate, LocalDate untilDate) {
        return Response.ok().entity(getAllZonesUsage(fromDate, untilDate)).build();
    }

    @Override
    public Response getAllZonesDailyUsage(LocalDate date) {
        List<ParkingZoneUsageSummary> parkingZoneUsages = usageService.generateAllZonesUsageSummary(date, date);
        List<DailyParkingZoneUsageSummaryResponse> dailyParkingZoneActivitySummaryResponses = parkingZoneUsages
            .stream()
            .map(dailyParkingZoneUsageSummaryResponseAssembler::toResponse)
            .collect(Collectors.toList());
        return Response.ok().entity(dailyParkingZoneActivitySummaryResponses).build();
    }

    @Override
    public Response getAllZonesMonthlyUsage(Month month, Year year) {
        LocalDate now = clock.instant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate firstDateOfMonth = LocalDate.of(year.getValue(), month.getValue(), 1);

        if (isUsageOfCurrentMonth(month, year, now)) {
            return Response.ok().entity(getAllZonesUsage(firstDateOfMonth, now)).build();
        }

        LocalDate lastDateOfMonth = getLastDateOfMonth(month.getValue(), year.getValue());
        return Response.ok().entity(getAllZonesUsage(firstDateOfMonth, lastDateOfMonth)).build();
    }

    private boolean isUsageOfCurrentMonth(Month month, Year year, LocalDate now) {
        return month == now.getMonth() && year.getValue() == now.getYear();
    }

    @Override
    public Response addUsage(AddUsageRequest request) {
        usageService.recordUsage(addUsageRequestAssembler.toDomain(request));
        return Response.status(Response.Status.CREATED).build();
    }

    private DailyParkingZoneUsageSummaryResponse getDailyUsageForZone(ParkingZone zone, LocalDate from, LocalDate until) {
        ParkingZoneUsageSummary parkingZoneUsageSummary = usageService.generateZoneUsageSummary(zone, from, until);

        return dailyParkingZoneUsageSummaryResponseAssembler.toResponse(parkingZoneUsageSummary);
    }

    private ParkingZoneUsageSummaryResponse getUsage(ParkingZone zone, LocalDate from, LocalDate until) {
        ParkingZoneUsageSummary parkingZoneUsageSummary = usageService.generateZoneUsageSummary(zone, from, until);
        return parkingZoneUsageSummaryResponseAssembler.toResponse(parkingZoneUsageSummary);
    }

    private List<ParkingZoneUsageSummaryResponse> getAllZonesUsage(LocalDate from, LocalDate until) {
        List<ParkingZoneUsageSummary> parkingZoneUsages = usageService.generateAllZonesUsageSummary(from, until);
        return parkingZoneUsages.stream().map(parkingZoneUsageSummaryResponseAssembler::toResponse).collect(Collectors.toList());
    }

    private LocalDate getLastDateOfMonth(int month, int year) {
        LocalDate firstOfMonth = LocalDate.of(year, month, 1);
        return firstOfMonth.withDayOfMonth(firstOfMonth.getMonth().length(firstOfMonth.isLeapYear()));
    }
}
