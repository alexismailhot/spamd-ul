package ca.ulaval.glo4003.projet.base.ws.offense.api;

import ca.ulaval.glo4003.projet.base.ws.offense.api.request.PayOffenseRequest;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/offense")
public interface OffenseResource {
    @POST
    @Path("parking")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Validate parking pass and create offense if invalid",
        description = "Send parkingCode, parkingZone and timeIssued. Then, validate if parking pass associated is valid in zone and in date. " +
        "If invalid, give offense according to reason of invalidity.",
        responses = {
            @ApiResponse(responseCode = "200", description = "Parking pass associated with parking code is valid."),
            @ApiResponse(responseCode = "201", description = "Parking pass is invalid and an offense was created"),
        }
    )
    Response createParkingOffenseIfOffenseWasCommitted(ParkingPassValidationRequest request);

    @POST
    @Path("payment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Pay an offense",
        description = "Send offense id and credit card info. Then, payment is made for amount of offense by credit card.",
        responses = {
            @ApiResponse(responseCode = "201", description = "Offense associated with offense id is paid"),
            @ApiResponse(responseCode = "404", description = "Offense id provided was not associated to any known offense."),
        }
    )
    Response payOffense(PayOffenseRequest payOffenseRequest);
}
