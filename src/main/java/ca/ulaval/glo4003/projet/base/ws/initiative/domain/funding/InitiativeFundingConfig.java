package ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding;

public interface InitiativeFundingConfig {
    double getPercentageOfFundsAllocated();
}
