package ca.ulaval.glo4003.projet.base.ws.offense.domain;

import java.util.Optional;

public interface OffenseRepository {
    void save(Offense offense);

    int getCount();

    Optional<Offense> findByOffenseId(OffenseId offenseId);
}
