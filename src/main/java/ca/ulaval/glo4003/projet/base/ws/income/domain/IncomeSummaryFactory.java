package ca.ulaval.glo4003.projet.base.ws.income.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import java.util.List;

public class IncomeSummaryFactory {

    public IncomeSummary createFromAccessIncome(List<AccessIncome> accessIncomes) {
        long sales = accessIncomes.size();
        Price totalIncome = accessIncomes.stream().map(AccessIncome::getPrice).reduce(Price.zero(), Price::add);
        return new IncomeSummary(sales, totalIncome);
    }

    public IncomeSummary createFromParkingIncome(List<ParkingPassIncome> parkingPassIncomes) {
        long sales = parkingPassIncomes.size();
        Price totalIncome = parkingPassIncomes.stream().map(ParkingPassIncome::getPrice).reduce(Price.zero(), Price::add);
        return new IncomeSummary(sales, totalIncome);
    }
}
