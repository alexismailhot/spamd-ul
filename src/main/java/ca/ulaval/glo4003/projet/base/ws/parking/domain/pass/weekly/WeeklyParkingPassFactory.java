package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.time.DayOfWeek;

public class WeeklyParkingPassFactory {

    private final ParkingPassRepository parkingPassRepository;

    public WeeklyParkingPassFactory(ParkingPassRepository parkingPassRepository) {
        this.parkingPassRepository = parkingPassRepository;
    }

    public WeeklyParkingPass create(ParkingZone parkingZone, DayOfWeek parkingDay, Session session) {
        int count = parkingPassRepository.getCount();
        ParkingCode parkingCode = new ParkingCode(count);
        return new WeeklyParkingPass(parkingCode, parkingZone, parkingDay, session);
    }
}
