package ca.ulaval.glo4003.projet.base.ws.usage.api.response;

import ca.ulaval.glo4003.projet.base.ws.usage.domain.DatesWithSameUsage;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;

public class ParkingZoneUsageSummaryResponseAssembler {

    public ParkingZoneUsageSummaryResponse toResponse(ParkingZoneUsageSummary parkingZoneUsageSummary) {
        DatesWithSameUsage maxDatesWithSameUsage = parkingZoneUsageSummary.getDatesWithMaximumUsage();
        DatesWithSameUsage minDatesWithSameUsage = parkingZoneUsageSummary.getDatesWithMinimumUsage();
        return new ParkingZoneUsageSummaryResponse(
            new DatesActivitySummaryResponse(maxDatesWithSameUsage.getDates(), maxDatesWithSameUsage.getUsage()),
            new DatesActivitySummaryResponse(minDatesWithSameUsage.getDates(), minDatesWithSameUsage.getUsage()),
            (float) parkingZoneUsageSummary.getAverageUsage(),
            parkingZoneUsageSummary.getZone()
        );
    }
}
