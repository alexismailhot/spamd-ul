package ca.ulaval.glo4003.projet.base.ws.parking.api;

import ca.ulaval.glo4003.projet.base.ws.parking.api.request.SessionParkingPassRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/parking")
public interface ParkingResource {
    @POST
    @Path("weekly")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Buy a weekly parking pass",
        description = "Returns parking code associated with bought weekly parking pass",
        responses = {
            @ApiResponse(responseCode = "200", description = "Weekly parking pass was bought, returns associated parking code"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info"),
        }
    )
    Response buyWeeklyParkingPass(WeeklyParkingPassRequest weeklyParkingPassRequest);

    @POST
    @Path("session")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(
        summary = "Buy a session parking pass",
        description = "Returns parking code associated with bought weekly parking pass",
        responses = {
            @ApiResponse(responseCode = "200", description = "Session parking pass was bought, returns associated parking code"),
            @ApiResponse(responseCode = "400", description = "Invalid request, see response message for info"),
        }
    )
    Response buySessionParkingPass(SessionParkingPassRequest sessionParkingPassRequest);
}
