package ca.ulaval.glo4003.projet.base.ws.email.infra;

import ca.ulaval.glo4003.projet.base.ws.email.domain.EmailSender;
import ca.ulaval.glo4003.projet.base.ws.email.domain.exception.EmailNotSentException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class ULavalEmailSender implements EmailSender {

    private static final String ADDRESS_FROM = "SPAMD-UL@ulaval.ca";
    private static final String SMTP_HOST_SERVER = "smtp.ulaval.ca";
    private static final String SENDER_NAME = "SPAMD-UL";
    private static final String SMTP_HOST_PROPERTY_KEY = "mail.smtp.host";

    @Override
    public void send(String subject, String body, String to) {
        try {
            Session session = createSession();
            MimeMessage message = createMimeMessage(subject, body, to, session);
            Transport.send(message);
        } catch (Exception e) {
            throw new EmailNotSentException(subject, to);
        }
    }

    private MimeMessage createMimeMessage(String subject, String body, String to, Session session) throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(ADDRESS_FROM, SENDER_NAME));
        message.setSubject(subject);
        message.setText(body);
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        return message;
    }

    private Session createSession() {
        Properties props = System.getProperties();
        props.put(SMTP_HOST_PROPERTY_KEY, SMTP_HOST_SERVER);
        return Session.getInstance(props);
    }
}
