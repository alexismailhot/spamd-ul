package ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class OffenseCodeInformation {

    private final OffenseCode offenseCode;
    private final String offense;
    private final Price amount;

    public OffenseCodeInformation(OffenseCode offenseCode, String offense, Price amount) {
        this.offenseCode = offenseCode;
        this.offense = offense;
        this.amount = amount;
    }

    public String getOffense() {
        return offense;
    }

    public Price getAmount() {
        return amount;
    }

    public OffenseCode getOffenseCode() {
        return offenseCode;
    }
}
