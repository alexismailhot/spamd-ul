package ca.ulaval.glo4003.projet.base.ws.http.mapper;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidParkingPassInformationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidWeeklyParkingPassInformationExceptionMapper implements ExceptionMapper<InvalidParkingPassInformationException> {

    private final ErrorResponseAssembler errorResponseAssembler;

    public InvalidWeeklyParkingPassInformationExceptionMapper(ErrorResponseAssembler errorResponseAssembler) {
        this.errorResponseAssembler = errorResponseAssembler;
    }

    @Override
    public Response toResponse(InvalidParkingPassInformationException e) {
        ErrorResponse errorResponse = errorResponseAssembler.assemble(e.getError(), e.getDescription());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
