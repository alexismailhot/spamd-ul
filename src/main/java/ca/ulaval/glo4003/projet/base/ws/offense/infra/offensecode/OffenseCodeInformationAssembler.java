package ca.ulaval.glo4003.projet.base.ws.offense.infra.offensecode;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformation;

public class OffenseCodeInformationAssembler {

    public OffenseCodeInformation toDomain(OffenseCodeInformationDto offenseCodeInformationDto) {
        return new OffenseCodeInformation(
            offenseCodeInformationDto.getCode(),
            offenseCodeInformationDto.getInfraction(),
            Price.of(offenseCodeInformationDto.getMontant())
        );
    }
}
