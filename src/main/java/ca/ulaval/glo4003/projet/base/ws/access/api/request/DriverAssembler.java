package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;

public class DriverAssembler {

    public Driver toDomain(DriverRequest driverRequest) {
        return new Driver(driverRequest.getName(), driverRequest.getBirthdate(), driverRequest.getSex());
    }
}
