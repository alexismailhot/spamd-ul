package ca.ulaval.glo4003.projet.base.ws.universityyear.domain;

import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.exception.InvalidSessionTypeException;

import java.time.LocalDate;
import java.time.Year;
import java.util.Objects;

public class Session {

    private final SessionType sessionType;
    private final Year year;

    public Session(SessionType sessionType, Year year) {
        this.sessionType = sessionType;
        this.year = year;
    }

    public SessionType getType() {
        return sessionType;
    }

    public Year getYear() {
        return this.year;
    }

    public static Session of(LocalDate date) {
        LocalDate fallStartDate = SessionType.FALL.getMonthDay().atYear(date.getYear());
        LocalDate fallEndDate = fallStartDate.plusMonths(4).minusDays(1);

        LocalDate winterStartDate = SessionType.WINTER.getMonthDay().atYear(date.getYear());
        LocalDate winterEndDate = winterStartDate.plusMonths(4).minusDays(1);

        LocalDate summerStartDate = SessionType.SUMMER.getMonthDay().atYear(date.getYear());
        LocalDate summerEndDate = summerStartDate.plusMonths(4).minusDays(1);

        if (!(date.isBefore(fallStartDate) || date.isAfter(fallEndDate))) {
            return new Session(SessionType.FALL, Year.of(date.getYear()));
        } else if (!(date.isBefore(winterStartDate) || date.isAfter(winterEndDate))) {
            return new Session(SessionType.WINTER, Year.of(date.getYear()));
        } else if (!(date.isBefore(summerStartDate) || date.isAfter(summerEndDate))) {
            return new Session(SessionType.SUMMER, Year.of(date.getYear()));
        }
        throw new InvalidSessionTypeException();
    }

    public boolean includes(LocalDate date) {
        LocalDate sessionStartDate = getStartDate();
        return !(date.isBefore(sessionStartDate) || date.isAfter(sessionStartDate.plusMonths(4).minusDays(1)));
    }

    public boolean isBefore(LocalDate date) {
        LocalDate sessionEndDate = sessionType.getMonthDay().atYear(year.getValue()).plusMonths(4).minusDays(1);
        return sessionEndDate.isBefore(date);
    }

    public Session next() {
        switch (sessionType) {
            case FALL:
                return new Session(SessionType.WINTER, year.plusYears(1));
            case WINTER:
                return new Session(SessionType.SUMMER, year);
            case SUMMER:
                return new Session(SessionType.FALL, year);
            default:
                throw new InvalidSessionTypeException();
        }
    }

    @Override
    public String toString() {
        return String.format("%s-%d", sessionType.name(), year.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return sessionType == session.sessionType && Objects.equals(year, session.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionType, year);
    }

    private LocalDate getStartDate() {
        return sessionType.getMonthDay().atYear(year.getValue());
    }

    public UniversityYear getUniversityYear() {
        switch (sessionType) {
            case FALL:
                return new UniversityYear(this, this.next(), this.next().next());
            case WINTER:
                return new UniversityYear(this.previous(), this, this.next());
            case SUMMER:
                return new UniversityYear(this.previous().previous(), this.previous(), this);
            default:
                throw new InvalidSessionTypeException();
        }
    }

    private Session previous() {
        switch (sessionType) {
            case FALL:
                return new Session(SessionType.SUMMER, year);
            case WINTER:
                return new Session(SessionType.FALL, year.minusYears(1));
            case SUMMER:
                return new Session(SessionType.WINTER, year);
            default:
                throw new InvalidSessionTypeException();
        }
    }
}
