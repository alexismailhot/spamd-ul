package ca.ulaval.glo4003.projet.base.ws.access.domain.driver;

import java.time.LocalDate;

public interface DriverConfig {
    Integer getMinimumDriverAge();

    Integer getMaximumDriverAge();

    LocalDate getMinimumBirthDate();

    LocalDate getMaximumBirthDate();
}
