package ca.ulaval.glo4003.projet.base.ws.universityyear.domain;

import java.time.LocalDate;
import java.util.Objects;

public class UniversityYear {

    private final Session fallSession;
    private final Session winterSession;
    private final Session summerSession;

    public UniversityYear(Session fallSession, Session winterSession, Session summerSession) {
        this.fallSession = fallSession;
        this.winterSession = winterSession;
        this.summerSession = summerSession;
    }

    public static UniversityYear of(LocalDate now) {
        Session sessionFromDate = Session.of(now);
        return sessionFromDate.getUniversityYear();
    }

    public boolean includes(LocalDate date) {
        return fallSession.includes(date) || winterSession.includes(date) || summerSession.includes(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UniversityYear that = (UniversityYear) o;
        return (
            Objects.equals(fallSession, that.fallSession) &&
            Objects.equals(winterSession, that.winterSession) &&
            Objects.equals(summerSession, that.summerSession)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(fallSession, winterSession, summerSession);
    }

    public UniversityYear next() {
        return new UniversityYear(summerSession.next(), summerSession.next().next(), summerSession.next().next().next());
    }
}
