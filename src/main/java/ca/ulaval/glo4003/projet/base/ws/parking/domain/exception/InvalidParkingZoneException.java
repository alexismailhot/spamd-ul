package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception;

public class InvalidParkingZoneException extends RuntimeException {

    private static final String ERROR = "INVALID_PARKING_ZONE";
    private static final String DESCRIPTION = "invalid parking zone : %s";

    private final String parkingZone;

    public InvalidParkingZoneException(String parkingCode) {
        this.parkingZone = parkingCode;
    }

    public String getError() {
        return ERROR;
    }

    public String getDescription() {
        return String.format(DESCRIPTION, parkingZone);
    }
}
