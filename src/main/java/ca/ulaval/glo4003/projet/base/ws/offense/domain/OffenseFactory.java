package ca.ulaval.glo4003.projet.base.ws.offense.domain;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;

public class OffenseFactory {

    private final OffenseRepository offenseRepository;

    public OffenseFactory(OffenseRepository offenseRepository) {
        this.offenseRepository = offenseRepository;
    }

    public Offense create(OffenseCode offenseCode, String description, Price amount) {
        OffenseId offenseId = new OffenseId(offenseRepository.getCount());
        return new Offense(offenseId, offenseCode, description, amount);
    }
}
