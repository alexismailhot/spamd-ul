package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

public interface ParkingCodeSSPDeliveryInformationRepository {
    void save(ParkingCodeSSPDeliveryInformation parkingCodeSSPDeliveryInformation);
}
