package ca.ulaval.glo4003.projet.base.ws.http.mapper;

public class ErrorResponseAssembler {

    public ErrorResponse assemble(String error, String description) {
        return new ErrorResponse(error, description);
    }
}
