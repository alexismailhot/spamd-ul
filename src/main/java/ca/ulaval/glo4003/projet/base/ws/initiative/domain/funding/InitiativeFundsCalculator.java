package ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.util.List;

public class InitiativeFundsCalculator {

    private final InitiativeFundingConfig initiativeFundingConfig;

    public InitiativeFundsCalculator(InitiativeFundingConfig initiativeFundingConfig) {
        this.initiativeFundingConfig = initiativeFundingConfig;
    }

    public Price calculateFundsAvailable(List<Initiative> initiativesAlreadyFunded, Price income) {
        Price fundedInitiativesAmount = initiativesAlreadyFunded.stream().map(Initiative::getAttributedFunds).reduce(Price.zero(), Price::add);
        Price attributedAmountForAllInitiatives = calculateFundsAllocatedToAllInitiatives(income);
        return attributedAmountForAllInitiatives.subtractBy(fundedInitiativesAmount);
    }

    public Price calculateFundsAllocatedToAllInitiatives(Price income) {
        return income.multiplyBy(initiativeFundingConfig.getPercentageOfFundsAllocated());
    }
}
