package ca.ulaval.glo4003.projet.base;

import ca.ulaval.glo4003.projet.base.ws.access.api.AccessResource;
import ca.ulaval.glo4003.projet.base.ws.access.api.AccessResourceImpl;
import ca.ulaval.glo4003.projet.base.ws.access.api.request.*;
import ca.ulaval.glo4003.projet.base.ws.access.api.request.DriverAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.api.request.VehicleAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.api.response.AccessCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.domain.*;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.AccessInformationValidator;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.BirthdateValidator;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.SessionValidator;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.VehicleYearValidator;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.DriverConfig;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleConfig;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.access.infra.AccessInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.access.infra.AccessIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.access.infra.accessinformation.DriverInMemoryConfig;
import ca.ulaval.glo4003.projet.base.ws.access.infra.accessinformation.VehicleInMemoryConfig;
import ca.ulaval.glo4003.projet.base.ws.access.infra.rates.AccessRatesCSVReader;
import ca.ulaval.glo4003.projet.base.ws.access.infra.rates.AccessRatesULavalConfig;
import ca.ulaval.glo4003.projet.base.ws.access.service.AccessService;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.api.CarbonCreditResource;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.api.CarbonCreditResourceImpl;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.CarbonCreditConfig;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.CarbonCreditBuyingJob;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.CarbonCreditULavalConfig;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.QuartzAutomaticJob;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.service.AutomaticJob;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.service.CarbonCreditService;
import ca.ulaval.glo4003.projet.base.ws.email.domain.EmailSender;
import ca.ulaval.glo4003.projet.base.ws.email.infra.ULavalEmailSender;
import ca.ulaval.glo4003.projet.base.ws.http.CORSResponseFilter;
import ca.ulaval.glo4003.projet.base.ws.http.ObjectMapperContextResolver;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.EmailNotSentExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.ErrorResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InitiativeNotFoundExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InsufficientInitiativeFundsExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InvalidAccessCodeExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InvalidAccessInformationExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InvalidFormatExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InvalidParkingCodeExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InvalidParkingZoneExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.InvalidWeeklyParkingPassInformationExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.JsonMappingExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.NoDeliveryMethodExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.OffenseAlreadyPaidExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.OffenseNotFoundExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.mapper.UnknownParkingCodeExceptionMapper;
import ca.ulaval.glo4003.projet.base.ws.http.paramconverter.LocalDateParamConverterProvider;
import ca.ulaval.glo4003.projet.base.ws.http.paramconverter.MonthParamConverterProvider;
import ca.ulaval.glo4003.projet.base.ws.http.paramconverter.YearParamConverterProvider;
import ca.ulaval.glo4003.projet.base.ws.income.api.IncomeResource;
import ca.ulaval.glo4003.projet.base.ws.income.api.IncomeResourceImpl;
import ca.ulaval.glo4003.projet.base.ws.income.api.response.assembler.IncomeSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummaryFactory;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.InitiativeResource;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.InitiativeResourceImpl;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.BriefInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.DetailedInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeFundsResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeFactory;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingAmountValidator;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingConfig;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundsCalculator;
import ca.ulaval.glo4003.projet.base.ws.initiative.infra.InitiativeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.infra.funding.InitiativeFundingULavalConfig;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.api.OffenseResource;
import ca.ulaval.glo4003.projet.base.ws.offense.api.OffenseResourceImpl;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.OffenseIdRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingCodeRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.response.OffenseResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformationConfig;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.OffenseInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.income.OffenseIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.offensecode.OffenseCodeInformationAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.offensecode.OffenseCodeInformationJsonReader;
import ca.ulaval.glo4003.projet.base.ws.offense.service.OffenseService;
import ca.ulaval.glo4003.projet.base.ws.parking.api.ParkingResource;
import ca.ulaval.glo4003.projet.base.ws.parking.api.ParkingResourceImpl;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.PostalAddressAssembler;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.SessionParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.response.ParkingPassResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeMailingFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSSPDeliveryInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSSPDeliveryInformationRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSenderFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.InvalidParkingPassException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.NoParkingPassException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotFoundForParkingCodeException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotValidInZoneException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotValidOnDateException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassPriceCalculator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRatesConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassType;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.DeliveryMethodInformationValidatorFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.ParkingPassInformationValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.SessionInformationValidatorFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session.SessionParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassDailyAdmissibilityValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassZoneValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.ParkingPassInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.code.ParkingCodeMailingInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.code.ParkingCodeSSPDeliveryInformationInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.income.ParkingPassIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.rates.DeliveryMethodULavalRatesConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.rates.ParkingPassRatesULavalConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.rates.ParkingRatesCSVReader;
import ca.ulaval.glo4003.projet.base.ws.parking.service.ParkingService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import ca.ulaval.glo4003.projet.base.ws.payment.infra.DumbExternalPaymentService;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionAssembler;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYearProvider;
import ca.ulaval.glo4003.projet.base.ws.usage.api.UsageResource;
import ca.ulaval.glo4003.projet.base.ws.usage.api.UsageResourceImpl;
import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.DailyParkingZoneUsageSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.ParkingZoneUsageSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntryRepository;
import ca.ulaval.glo4003.projet.base.ws.usage.infra.parking.ParkingEntryInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.usage.service.UsageService;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.ws.rs.core.Application;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Clock;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

@OpenAPIDefinition(
    info = @Info(
        title = "SAMPD UL",
        version = "1.0",
        description = "API for handling of access and parking at Laval University",
        license = @License(name = "Apache 2.0", url = "http://localhost:8080/api/")
    )
)
@SuppressWarnings("all")
public class Main {

    public static final String AUTOMATIC_BUYING_RECURRENCE = "59 59 23 L * ?";
    private static final String PARKING_FEES_FILE_PATH = "src/main/java/ca/ulaval/glo4003/projet/base/ws/parking/infra/rates/frais-zone.csv";
    private static final String ACCESS_FEES_FILE_PATH = "src/main/java/ca/ulaval/glo4003/projet/base/ws/access/infra/rates/frais-acces.csv";
    private static final String OFFENSE_CODES_FILE_PATH = "src/main/java/ca/ulaval/glo4003/projet/base/ws/offense/infra/offensecode/infraction.json";

    public static void main(String[] args) throws Exception {
        // Setup resources (API)
        Clock clock = Clock.systemDefaultZone();
        DumbExternalPaymentService dumbExternalPaymentService = new DumbExternalPaymentService();
        CreditCardAssembler creditCardAssembler = new CreditCardAssembler();
        ErrorResponseAssembler errorResponseAssembler = new ErrorResponseAssembler();
        AccessIncomeRepository accessIncomeRepository = new AccessIncomeInMemoryRepository();
        ParkingPassIncomeRepository parkingPassIncomeRepository = new ParkingPassIncomeInMemoryRepository();
        OffenseIncomeRepository offenseIncomeRepository = new OffenseIncomeInMemoryRepository();
        InitiativeFundingConfig initiativeFundingConfig = new InitiativeFundingULavalConfig();
        InitiativeFundsCalculator initiativeFundCalculator = new InitiativeFundsCalculator(initiativeFundingConfig);
        IncomeService incomeService = createIncomeService(accessIncomeRepository, parkingPassIncomeRepository, offenseIncomeRepository, clock);

        InitiativeRepository initiativeRepository = new InitiativeInMemoryRepository();
        CarbonCreditConfig carbonCreditConfig = new CarbonCreditULavalConfig();
        initiativeRepository.save(new Initiative(carbonCreditConfig.getInitiativeName(), Price.zero(), carbonCreditConfig.getCarbonCreditInitiativeCode()));
        InitiativeService initiativeService = createInitiativeService(initiativeFundCalculator, incomeService, initiativeRepository, carbonCreditConfig);
        InitiativeResource initiativeResource = createInitiativeResource(
            clock,
            initiativeFundCalculator,
            initiativeFundingConfig,
            incomeService,
            initiativeService
        );

        AccessResource accessResource = createAccessResource(dumbExternalPaymentService, creditCardAssembler, clock, accessIncomeRepository);
        ParkingService parkingService = createParkingService(dumbExternalPaymentService, parkingPassIncomeRepository, clock);
        ParkingResource parkingResource = createParkingResource(creditCardAssembler, parkingService);
        IncomeResource incomeResource = createIncomeResource(incomeService);
        UsageResource usageResource = createUsageResource(clock);
        OffenseResource offenseResource = createOffenseResource(
            parkingService,
            dumbExternalPaymentService,
            creditCardAssembler,
            offenseIncomeRepository,
            clock
        );

        JobDetail carbonCreditBuyingJob = JobBuilder.newJob(CarbonCreditBuyingJob.class).withIdentity("CarbonCreditBuyingJob").build();
        Scheduler scheduler = setupScheduler(initiativeService, carbonCreditBuyingJob);
        QuartzAutomaticJob carbonCreditBuyingQuartzAutomaticJob = new QuartzAutomaticJob(scheduler, carbonCreditBuyingJob.getKey());

        CarbonCreditService carbonCreditService = createCarbonCreditService(carbonCreditBuyingQuartzAutomaticJob);
        CarbonCreditResource carbonCreditResource = new CarbonCreditResourceImpl(carbonCreditService, initiativeService);

        // Setup Swagger integration
        OpenApiResource openApiResource = new OpenApiResource();

        // Setup API context (JERSEY + JETTY)
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/api/");
        ResourceConfig resourceConfig = ResourceConfig.forApplication(
            new Application() {
                @Override
                public Set<Object> getSingletons() {
                    HashSet<Object> resources = new HashSet<>();
                    // Add resources to context
                    resources.add(accessResource);
                    resources.add(parkingResource);
                    resources.add(offenseResource);
                    resources.add(usageResource);
                    resources.add(initiativeResource);
                    resources.add(incomeResource);
                    resources.add(carbonCreditResource);
                    return resources;
                }
            }
        );
        resourceConfig.register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class);
        resourceConfig.register(LocalDateParamConverterProvider.class);
        resourceConfig.register(YearParamConverterProvider.class);
        resourceConfig.register(MonthParamConverterProvider.class);
        resourceConfig.register(CORSResponseFilter.class);
        resourceConfig.register(
            new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, 10000)
        );
        resourceConfig.register(new ObjectMapperContextResolver());
        resourceConfig.register(openApiResource);

        resourceConfig.register(new InvalidFormatExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new InvalidAccessInformationExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new InvalidAccessCodeExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new EmailNotSentExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new InvalidWeeklyParkingPassInformationExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new InvalidParkingCodeExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new JsonMappingExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new InitiativeNotFoundExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new InsufficientInitiativeFundsExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new UnknownParkingCodeExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new InvalidParkingZoneExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new NoDeliveryMethodExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new OffenseNotFoundExceptionMapper(errorResponseAssembler));
        resourceConfig.register(new OffenseAlreadyPaidExceptionMapper(errorResponseAssembler));

        ServletContainer servletContainer = new ServletContainer(resourceConfig);
        ServletHolder servletHolder = new ServletHolder(servletContainer);
        context.addServlet(servletHolder, "/*");

        // Setup http server
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] { context });
        Server server = new Server(8080);
        server.setHandler(contexts);

        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }

    private static Scheduler setupScheduler(InitiativeService initiativeService, JobDetail job) throws SchedulerException {
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();

        Scheduler scheduler = schedulerFactory.getScheduler();
        scheduler.getContext().put("InitiativeService", initiativeService);
        Trigger trigger = TriggerBuilder
            .newTrigger()
            .withIdentity("CarbonCreditBuyingJob_TRIGGER")
            .withSchedule(CronScheduleBuilder.cronSchedule(AUTOMATIC_BUYING_RECURRENCE))
            .startNow()
            .build();

        scheduler.scheduleJob(job, trigger);
        scheduler.start();
        return scheduler;
    }

    private static ParkingResource createParkingResource(CreditCardAssembler creditCardAssembler, ParkingService parkingService) throws FileNotFoundException {
        PostalAddressAssembler postalAddressAssembler = new PostalAddressAssembler();
        WeeklyParkingPassInformationFactory weeklyParkingPassInformationFactory = new WeeklyParkingPassInformationFactory(postalAddressAssembler);
        ParkingPassResponseAssembler parkingPassResponseAssembler = new ParkingPassResponseAssembler();

        return new ParkingResourceImpl(
            parkingService,
            weeklyParkingPassInformationFactory,
            new SessionParkingPassInformationFactory(postalAddressAssembler),
            parkingPassResponseAssembler,
            creditCardAssembler
        );
    }

    private static DeliveryMethodULavalRatesConfig createDeliveryMethodRatesConfig() {
        Map<DeliveryMethod, Price> deliveryMethodRates = new HashMap<>();
        deliveryMethodRates.put(DeliveryMethod.EMAIL, Price.zero());
        deliveryMethodRates.put(DeliveryMethod.POSTAL, Price.of(5));
        deliveryMethodRates.put(DeliveryMethod.SSP, Price.zero());
        return new DeliveryMethodULavalRatesConfig(deliveryMethodRates);
    }

    private static UsageResource createUsageResource(Clock clock) {
        ParkingZoneUsageSummaryResponseAssembler parkingZoneUsageSummaryResponseAssembler = new ParkingZoneUsageSummaryResponseAssembler();
        ParkingEntryRepository parkingEntryRepository = new ParkingEntryInMemoryRepository();
        UsageService usageService = new UsageService(parkingEntryRepository);
        AddUsageRequestAssembler addUsageRequestAssembler = new AddUsageRequestAssembler();
        DailyParkingZoneUsageSummaryResponseAssembler dailyParkingZoneUsageSummaryResponseAssembler = new DailyParkingZoneUsageSummaryResponseAssembler();
        return new UsageResourceImpl(
            usageService,
            parkingZoneUsageSummaryResponseAssembler,
            clock,
            addUsageRequestAssembler,
            dailyParkingZoneUsageSummaryResponseAssembler
        );
    }

    private static AccessResource createAccessResource(
        PaymentService paymentService,
        CreditCardAssembler creditCardAssembler,
        Clock clock,
        AccessIncomeRepository accessIncomeRepository
    )
        throws FileNotFoundException {
        AccessRepository accessRepository = new AccessInMemoryRepository();
        DriverAssembler driverAssembler = new DriverAssembler();
        VehicleAssembler vehicleAssembler = new VehicleAssembler();
        SessionAssembler sessionAssembler = new SessionAssembler();

        AccessRatesCSVReader accessRatesCSVReader = new AccessRatesCSVReader(ACCESS_FEES_FILE_PATH);
        Map<VehicleType, Map<AccessTimeType, Price>> accessRates = accessRatesCSVReader.read();
        AccessRatesConfig accessRatesUlavalConfig = new AccessRatesULavalConfig(accessRates);

        WeeklyAccessInformationAssembler weeklyAccessInformationAssembler = new WeeklyAccessInformationAssembler(
            vehicleAssembler,
            driverAssembler,
            sessionAssembler
        );
        SessionAccessInformationAssembler sessionAccessInformationAssembler = new SessionAccessInformationAssembler(
            vehicleAssembler,
            driverAssembler,
            sessionAssembler
        );
        WeeklyAccessFactory weeklyAccessFactory = new WeeklyAccessFactory(accessRepository);
        SessionAccessFactory sessionAccessFactory = new SessionAccessFactory(accessRepository);

        DriverConfig driverConfig = new DriverInMemoryConfig();
        VehicleConfig vehicleConfig = new VehicleInMemoryConfig();

        VehicleYearValidator vehicleYearValidator = new VehicleYearValidator(
            null,
            vehicleConfig.getMinimumVehicleYear(),
            vehicleConfig.getMaximumVehicleYear()
        );
        BirthdateValidator birthdateValidator = new BirthdateValidator(
            vehicleYearValidator,
            driverConfig.getMinimumBirthDate(),
            driverConfig.getMaximumBirthDate()
        );
        SessionValidator sessionValidator = new SessionValidator(
            birthdateValidator,
            clock
        );

        AccessInformationValidator accessInformationValidator = sessionValidator;
        AccessIncomeFactory accessIncomeFactory = new AccessIncomeFactory(Clock.systemDefaultZone());
        AccessService accessService = new AccessService(
            weeklyAccessFactory,
            sessionAccessFactory,
            accessIncomeFactory,
            accessIncomeRepository,
            accessInformationValidator,
            accessRepository,
            new AccessGrantingValidator(),
            paymentService,
            new AccessPriceCalculator(accessRatesUlavalConfig),
            clock
        );

        return new AccessResourceImpl(
            accessService,
            new AccessCodeResponseAssembler(),
            sessionAccessInformationAssembler,
            weeklyAccessInformationAssembler,
            creditCardAssembler
        );
    }

    private static InitiativeResource createInitiativeResource(
        Clock clock,
        InitiativeFundsCalculator initiativeFundCalculator,
        InitiativeFundingConfig initiativeFundingConfig,
        IncomeService incomeService,
        InitiativeService initiativeService
    ) {
        InitiativeRepository initiativeRepository = new InitiativeInMemoryRepository();
        InitiativeFactory initiativeFactory = new InitiativeFactory();
        InitiativeFundingAmountValidator initiativeFundingAmountValidator = new InitiativeFundingAmountValidator();
        InitiativeCodeResponseAssembler initiativeCodeResponseAssembler = new InitiativeCodeResponseAssembler();
        BriefInitiativeResponseAssembler briefInitiativeResponseAssembler = new BriefInitiativeResponseAssembler();
        DetailedInitiativeResponseAssembler detailedInitiativeResponseAssembler = new DetailedInitiativeResponseAssembler();
        InitiativeFundsResponseAssembler initiativeFundsResponseAssembler = new InitiativeFundsResponseAssembler();

        InitiativeResource initiativeResource = new InitiativeResourceImpl(
            initiativeService,
            initiativeCodeResponseAssembler,
            briefInitiativeResponseAssembler,
            detailedInitiativeResponseAssembler,
            initiativeFundsResponseAssembler
        );

        return initiativeResource;
    }

    private static InitiativeService createInitiativeService(
        InitiativeFundsCalculator initiativeFundCalculator,
        IncomeService incomeService,
        InitiativeRepository initiativeRepository,
        CarbonCreditConfig carbonCreditConfig
    ) {
        InitiativeFundingAmountValidator initiativeFundingAmountValidator = new InitiativeFundingAmountValidator();
        InitiativeFactory initiativeFactory = new InitiativeFactory();
        InitiativeService initiativeService = new InitiativeService(
            initiativeFactory,
            initiativeFundingAmountValidator,
            initiativeRepository,
            initiativeFundCalculator,
            incomeService,
            carbonCreditConfig
        );
        return initiativeService;
    }

    private static IncomeService createIncomeService(
        AccessIncomeRepository accessIncomeRepository,
        ParkingPassIncomeRepository parkingPassIncomeRepository,
        OffenseIncomeRepository offenseIncomeRepository,
        Clock clock
    ) {
        IncomeSummaryFactory incomeSummaryFactory = new IncomeSummaryFactory();
        return new IncomeService(
            accessIncomeRepository,
            parkingPassIncomeRepository,
            offenseIncomeRepository,
            incomeSummaryFactory,
            new UniversityYearProvider(),
            clock
        );
    }

    private static IncomeResource createIncomeResource(IncomeService incomeService) {
        IncomeSummaryResponseAssembler accessIncomesForVehicleTypeResponseSummaryAssembler = new IncomeSummaryResponseAssembler();
        return new IncomeResourceImpl(incomeService, accessIncomesForVehicleTypeResponseSummaryAssembler);
    }

    private static OffenseResource createOffenseResource(
        ParkingService parkingService,
        PaymentService paymentService,
        CreditCardAssembler creditCardAssembler,
        OffenseIncomeRepository offenseIncomeRepository,
        Clock clock
    )
        throws IOException {
        OffenseCodeInformationAssembler offenseCodeInformationAssembler = new OffenseCodeInformationAssembler();
        OffenseCodeInformationJsonReader jsonReader = new OffenseCodeInformationJsonReader(OFFENSE_CODES_FILE_PATH, offenseCodeInformationAssembler);
        OffenseCodeInformationConfig offenseCodeInformationConfig = new OffenseCodeInformationConfig(jsonReader.read());
        OffenseRepository offenseRepository = new OffenseInMemoryRepository();
        OffenseFactory offenseFactory = new OffenseFactory(offenseRepository);
        OffenseIncomeFactory offenseIncomeFactory = new OffenseIncomeFactory(clock);
        OffenseService offenseService = new OffenseService(
            createOffenseCodeByParkingPassExceptions(),
            offenseCodeInformationConfig,
            parkingService,
            paymentService,
            offenseRepository,
            offenseFactory,
            offenseIncomeFactory,
            offenseIncomeRepository
        );
        OffenseResponseAssembler offenseResponseAssembler = new OffenseResponseAssembler();
        ParkingCodeRequestAssembler parkingCodeRequestAssembler = new ParkingCodeRequestAssembler();
        OffenseIdRequestAssembler offenseIdRequestAssembler = new OffenseIdRequestAssembler();
        ParkingPassValidationAssembler parkingPassValidationAssembler = new ParkingPassValidationAssembler();

        return new OffenseResourceImpl(
            offenseService,
            offenseResponseAssembler,
            creditCardAssembler,
            parkingPassValidationAssembler,
            offenseIdRequestAssembler
        );
    }

    private static CarbonCreditService createCarbonCreditService(AutomaticJob automaticJob) {
        CarbonCreditService carbonCreditService = new CarbonCreditService(automaticJob);
        return carbonCreditService;
    }

    private static Map<Class<? extends InvalidParkingPassException>, OffenseCode> createOffenseCodeByParkingPassExceptions() {
        Map<Class<? extends InvalidParkingPassException>, OffenseCode> offenseCodeByParkingPassExceptions = new HashMap<>();
        offenseCodeByParkingPassExceptions.put(ParkingPassNotFoundForParkingCodeException.class, OffenseCode.VIG_02);
        offenseCodeByParkingPassExceptions.put(NoParkingPassException.class, OffenseCode.VIG_03);
        offenseCodeByParkingPassExceptions.put(ParkingPassNotValidInZoneException.class, OffenseCode.ZONE_01);
        offenseCodeByParkingPassExceptions.put(ParkingPassNotValidOnDateException.class, OffenseCode.VIG_01);
        return offenseCodeByParkingPassExceptions;
    }

    private static ParkingService createParkingService(PaymentService paymentService, ParkingPassIncomeRepository parkingPassIncomeRepository, Clock clock)
        throws FileNotFoundException {
        ParkingPassRepository parkingPassRepository = new ParkingPassInMemoryRepository();
        ParkingRatesCSVReader parkingRatesCSVReader = new ParkingRatesCSVReader(PARKING_FEES_FILE_PATH);
        Map<ParkingZone, Map<ParkingPassType, Price>> parkingRates = parkingRatesCSVReader.read();
        ParkingPassRatesConfig parkingRatesULavalConfig = new ParkingPassRatesULavalConfig(parkingRates);
        DeliveryMethodULavalRatesConfig deliveryMethodRatesConfig = createDeliveryMethodRatesConfig();
        EmailSender emailSender = new ULavalEmailSender();
        ParkingCodeMailingFactory parkingCodeMailingFactory = new ParkingCodeMailingFactory();
        ParkingCodeSSPDeliveryInformationFactory parkingCodeSSPDeliveryInformationFactory = new ParkingCodeSSPDeliveryInformationFactory();
        ParkingCodeSSPDeliveryInformationRepository parkingCodeSSPDeliveryInformationRepository = new ParkingCodeSSPDeliveryInformationInMemoryRepository();
        WeeklyParkingPassFactory weeklyParkingPassFactory = new WeeklyParkingPassFactory(parkingPassRepository);
        ParkingCodeSenderFactory parkingCodeSenderFactory = new ParkingCodeSenderFactory(
            emailSender,
            new ParkingCodeMailingInMemoryRepository(),
            parkingCodeMailingFactory,
            parkingCodeSSPDeliveryInformationRepository,
            parkingCodeSSPDeliveryInformationFactory
        );
        SessionInformationValidatorFactory sessionInformationValidatorFactory = new SessionInformationValidatorFactory(clock);
        ParkingPassInformationValidator parkingPassInformationValidator = new ParkingPassInformationValidator(new DeliveryMethodInformationValidatorFactory(), sessionInformationValidatorFactory);
        ParkingPassPriceCalculator parkingPassPriceCalculator = new ParkingPassPriceCalculator(parkingRatesULavalConfig, deliveryMethodRatesConfig);
        ParkingPassValidator parkingPassValidator = new ParkingPassDailyAdmissibilityValidator(new ParkingPassZoneValidator(null));

        ParkingPassIncomeFactory parkingPassIncomeFactory = new ParkingPassIncomeFactory(clock);

        return new ParkingService(
            parkingPassRepository,
            weeklyParkingPassFactory,
            new SessionParkingPassFactory(parkingPassRepository),
            paymentService,
            parkingCodeSenderFactory,
            parkingPassIncomeFactory,
            parkingPassIncomeRepository,
            parkingPassInformationValidator,
            parkingPassPriceCalculator,
            parkingPassValidator
        );
    }
}
