package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;

public class ParkingPassNotValidInZoneException extends InvalidParkingPassException {

    private final ParkingCode parkingCode;
    private final ParkingZone parkingZone;

    public ParkingPassNotValidInZoneException(ParkingCode parkingCode, ParkingZone parkingZone) {
        this.parkingCode = parkingCode;
        this.parkingZone = parkingZone;
    }

    @Override
    public String getDescription() {
        return String.format("Parking pass %s cannot be used in zone %s", parkingCode.getCode(), parkingZone);
    }
}
