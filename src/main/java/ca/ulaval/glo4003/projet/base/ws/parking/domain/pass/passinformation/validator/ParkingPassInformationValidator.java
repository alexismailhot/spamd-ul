package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;

public class ParkingPassInformationValidator {

    private final DeliveryMethodInformationValidatorFactory deliveryMethodInformationValidatorFactory;
    private final SessionInformationValidatorFactory sessionInformationValidatorFactory;

    public ParkingPassInformationValidator(DeliveryMethodInformationValidatorFactory deliveryMethodInformationValidatorFactory, SessionInformationValidatorFactory sessionInformationValidatorFactory) {
        this.deliveryMethodInformationValidatorFactory = deliveryMethodInformationValidatorFactory;
        this.sessionInformationValidatorFactory = sessionInformationValidatorFactory;
    }

    public void validate(ParkingPassInformation parkingPassInformation) {
        deliveryMethodInformationValidatorFactory.create(parkingPassInformation.getDeliveryMethod()).validate(parkingPassInformation);
        sessionInformationValidatorFactory.create(parkingPassInformation).validate(parkingPassInformation);
    }
}
