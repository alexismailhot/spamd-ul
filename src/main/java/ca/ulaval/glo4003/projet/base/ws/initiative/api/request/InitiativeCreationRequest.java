package ca.ulaval.glo4003.projet.base.ws.initiative.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InitiativeCreationRequest {

    private final String name;
    private final double amount;

    @JsonCreator
    public InitiativeCreationRequest(
        @JsonProperty(value = "name", required = true) String name,
        @JsonProperty(value = "amount", required = true) double amount
    ) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }
}
