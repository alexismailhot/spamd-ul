package ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler;

import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.BriefInitiativeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;

public class BriefInitiativeResponseAssembler {

    public BriefInitiativeResponse toResponse(Initiative initiative) {
        return new BriefInitiativeResponse(initiative.getInitiativeCode().getCode(), initiative.getName());
    }
}
