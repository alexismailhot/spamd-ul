package ca.ulaval.glo4003.projet.base.ws.offense.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OffenseIdRequest {

    private final int id;

    @JsonCreator
    public OffenseIdRequest(@JsonProperty(value = "id", required = true) int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
