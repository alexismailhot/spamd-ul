package ca.ulaval.glo4003.projet.base.ws.access.infra;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccessIncomeInMemoryRepository implements AccessIncomeRepository {

    private final List<AccessIncome> incomes = new ArrayList<>();

    @Override
    public void save(AccessIncome accessIncome) {
        incomes.add(accessIncome);
    }

    @Override
    public List<AccessIncome> findByVehicleType(VehicleType vehicleType) {
        return incomes.stream().filter(income -> income.isVehicleType(vehicleType)).collect(Collectors.toList());
    }

    @Override
    public List<AccessIncome> findByVehicleTypeAndUniversityYear(VehicleType vehicleType, UniversityYear universityYear) {
        return incomes.stream().filter(income -> income.isVehicleType(vehicleType) && income.isIn(universityYear)).collect(Collectors.toList());
    }

    @Override
    public List<AccessIncome> findByUniversityYear(UniversityYear universityYear) {
        return incomes.stream().filter(income -> income.isIn(universityYear)).collect(Collectors.toList());
    }

    @Override
    public List<AccessIncome> getAll() {
        return incomes;
    }
}
