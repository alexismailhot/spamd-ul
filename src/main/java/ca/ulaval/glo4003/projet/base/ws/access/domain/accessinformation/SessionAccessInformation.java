package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.util.Objects;

public class SessionAccessInformation extends AccessInformation {

    private final Session session;

    public SessionAccessInformation(Driver driver, Vehicle vehicle, Session session) {
        super(driver, vehicle);
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SessionAccessInformation that = (SessionAccessInformation) o;
        return Objects.equals(session, that.session);
    }
}
