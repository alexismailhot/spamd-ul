package ca.ulaval.glo4003.projet.base.ws.offense.infra.income;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncome;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OffenseIncomeInMemoryRepository implements OffenseIncomeRepository {

    private final List<OffenseIncome> incomes = new ArrayList<>();

    @Override
    public void save(OffenseIncome offenseIncome) {
        incomes.add(offenseIncome);
    }

    @Override
    public List<OffenseIncome> getAll() {
        return incomes;
    }

    @Override
    public List<OffenseIncome> findByUniversityYear(UniversityYear universityYear) {
        return incomes.stream().filter(income -> income.isIn(universityYear)).collect(Collectors.toList());
    }
}
