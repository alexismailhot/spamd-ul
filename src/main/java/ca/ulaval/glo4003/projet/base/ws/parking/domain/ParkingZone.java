package ca.ulaval.glo4003.projet.base.ws.parking.domain;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.InvalidParkingZoneException;

public enum ParkingZone {
    ZONE_1("Zone1"),
    ZONE_2("Zone2"),
    ZONE_3("Zone3"),
    ZONE_R("ZoneR");

    private final String name;

    ParkingZone(String name) {
        this.name = name;
    }

    public static ParkingZone fromString(String zone) {
        try {
            return ParkingZone.valueOf(zone.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidParkingZoneException(zone);
        }
    }

    public String getName() {
        return name;
    }
}
