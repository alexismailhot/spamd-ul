package ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle;

public class InvalidVehicleTypeException extends RuntimeException {

    private static final String ERROR = "INVALID_VEHICLE_TYPE";

    private final String vehicleType;

    public InvalidVehicleTypeException(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getError() {
        return ERROR;
    }

    public String getDescription() {
        return String.format("invalid vehicule zone : %s", vehicleType);
    }
}
