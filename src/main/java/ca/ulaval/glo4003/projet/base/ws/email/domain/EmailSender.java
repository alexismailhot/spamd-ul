package ca.ulaval.glo4003.projet.base.ws.email.domain;

public interface EmailSender {
    void send(String subject, String body, String to);
}
