package ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;

public class InitiativeNotFoundException extends RuntimeException {

    private static final String INITIATIVE_NOT_FOUND = "INITIATIVE_NOT_FOUND";
    private final InitiativeCode initiativeCode;

    public InitiativeNotFoundException(InitiativeCode initiativeCode) {
        this.initiativeCode = initiativeCode;
    }

    public String getError() {
        return INITIATIVE_NOT_FOUND;
    }

    public String getDescription() {
        return String.format("No Initiative found for Initiative code %s", initiativeCode.getCode());
    }
}
