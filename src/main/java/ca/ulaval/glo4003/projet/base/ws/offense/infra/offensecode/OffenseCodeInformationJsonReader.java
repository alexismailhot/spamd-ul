package ca.ulaval.glo4003.projet.base.ws.offense.infra.offensecode;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformation;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class OffenseCodeInformationJsonReader {

    private final String filePath;
    private final OffenseCodeInformationAssembler offenseCodeInformationAssembler;

    public OffenseCodeInformationJsonReader(String filePath, OffenseCodeInformationAssembler offenseCodeInformationAssembler) {
        this.filePath = filePath;
        this.offenseCodeInformationAssembler = offenseCodeInformationAssembler;
    }

    public List<OffenseCodeInformation> read() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);

        List<OffenseCodeInformationDto> offenseCodeInformationDtos = objectMapper.readValue(
            new File(filePath),
            new TypeReference<List<OffenseCodeInformationDto>>() {}
        );
        return offenseCodeInformationDtos.stream().map(offenseCodeInformationAssembler::toDomain).collect(Collectors.toList());
    }
}
