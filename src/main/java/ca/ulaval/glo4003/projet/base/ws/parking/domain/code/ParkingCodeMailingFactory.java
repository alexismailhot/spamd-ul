package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;

public class ParkingCodeMailingFactory {

    public ParkingCodeMailing create(ParkingCode parkingCode, PostalAddress postalAddress) {
        return new ParkingCodeMailing(parkingCode, postalAddress);
    }
}
