package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidPostalCodeException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;

public class PostalCodeValidator implements DeliveryMethodInformationValidator {

    private static final int POSTAL_CODE_LENGTH = 6;

    @Override
    public void validate(ParkingPassInformation parkingPassInformation) {
        PostalAddress postalAddress = parkingPassInformation.getPostalAddress().orElseThrow(IllegalStateException::new);
        String postalCode = postalAddress.getPostalCode();
        postalCode = postalCode.replaceAll(" ", "");
        if (postalCode.length() != POSTAL_CODE_LENGTH) {
            throw new InvalidPostalCodeException(postalCode);
        }
        for (int i = 0; i < POSTAL_CODE_LENGTH; i += 1) {
            boolean isAlphabetic = Character.isAlphabetic(postalCode.charAt(i));
            boolean isDigit = Character.isDigit(postalCode.charAt(i));
            boolean isEven = i % 2 == 0;
            if ((isEven && !isAlphabetic) || (!isEven && !isDigit)) {
                throw new InvalidPostalCodeException(postalCode);
            }
        }
    }
}
