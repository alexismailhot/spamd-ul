package ca.ulaval.glo4003.projet.base.ws.money.api;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CapitalFormatter {

    public static BigDecimal fromBigDecimal(BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_UP);
    }
}
