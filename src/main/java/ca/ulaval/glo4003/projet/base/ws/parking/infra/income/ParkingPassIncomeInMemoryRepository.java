package ca.ulaval.glo4003.projet.base.ws.parking.infra.income;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingPassIncomeInMemoryRepository implements ParkingPassIncomeRepository {

    private final List<ParkingPassIncome> incomes = new ArrayList<>();

    @Override
    public void save(ParkingPassIncome parkingPassIncome) {
        incomes.add(parkingPassIncome);
    }

    @Override
    public List<ParkingPassIncome> findByUniversityYear(UniversityYear universityYear) {
        return incomes.stream().filter(income -> income.isIn(universityYear)).collect(Collectors.toList());
    }

    @Override
    public List<ParkingPassIncome> getAll() {
        return incomes;
    }
}
