package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.time.DayOfWeek;
import java.util.Objects;

public class WeeklyParkingPassInformation extends SessionParkingPassInformation {

    private final DayOfWeek parkingDay;

    public WeeklyParkingPassInformation(
        ParkingZone parkingZone,
        DeliveryMethod deliveryMethod,
        DayOfWeek parkingDay,
        PostalAddress postalAddress,
        Session session
    ) {
        super(parkingZone, deliveryMethod, postalAddress, session);
        this.parkingDay = parkingDay;
    }

    public WeeklyParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod, String email, DayOfWeek parkingDay, Session session) {
        super(parkingZone, deliveryMethod, email, session);
        this.parkingDay = parkingDay;
    }

    public WeeklyParkingPassInformation(ParkingZone parkingZone, DeliveryMethod deliveryMethod, DayOfWeek parkingDay, Session session) {
        super(parkingZone, deliveryMethod, session);
        this.parkingDay = parkingDay;
    }

    public DayOfWeek getParkingDay() {
        return parkingDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WeeklyParkingPassInformation that = (WeeklyParkingPassInformation) o;
        return parkingDay == that.parkingDay;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), parkingDay);
    }
}
