package ca.ulaval.glo4003.projet.base.ws.usage.api.request;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;

public class AddUsageRequest {

    private final ParkingZone parkingZone;
    private final LocalDate date;

    @JsonCreator
    public AddUsageRequest(
        @JsonProperty(value = "parkingZone", required = true) final ParkingZone parkingZone,
        @JsonProperty(value = "date", required = true) final LocalDate date
    ) {
        this.parkingZone = parkingZone;
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    public ParkingZone getParkingZone() {
        return parkingZone;
    }
}
