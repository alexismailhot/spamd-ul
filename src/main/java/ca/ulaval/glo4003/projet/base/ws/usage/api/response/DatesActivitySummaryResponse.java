package ca.ulaval.glo4003.projet.base.ws.usage.api.response;

import java.time.LocalDate;
import java.util.List;

public class DatesActivitySummaryResponse {

    private final List<LocalDate> days;
    private final int numberOfEntries;

    public DatesActivitySummaryResponse(List<LocalDate> days, int numberOfEntries) {
        this.days = days;
        this.numberOfEntries = numberOfEntries;
    }

    public List<LocalDate> getDays() {
        return days;
    }

    public int getNumberOfEntries() {
        return numberOfEntries;
    }
}
