package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VehicleRequest {

    private final String brand;
    private final String model;
    private final Integer modelYear;
    private final String plate;
    private final VehicleType type;

    @JsonCreator
    public VehicleRequest(
        @JsonProperty(value = "brand", required = true) String brand,
        @JsonProperty(value = "model", required = true) String model,
        @JsonProperty(value = "modelYear", required = true) Integer modelYear,
        @JsonProperty(value = "plate", required = true) String plate,
        @JsonProperty(value = "type", required = true) VehicleType type
    ) {
        this.brand = brand;
        this.model = model;
        this.modelYear = modelYear;
        this.plate = plate;
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public Integer getModelYear() {
        return modelYear;
    }

    public String getPlate() {
        return plate;
    }

    public VehicleType getType() {
        return type;
    }
}
