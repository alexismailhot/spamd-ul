package ca.ulaval.glo4003.projet.base.ws.carboncredit.service;

public interface AutomaticJob {
    void activate();
    void deactivate();
}
