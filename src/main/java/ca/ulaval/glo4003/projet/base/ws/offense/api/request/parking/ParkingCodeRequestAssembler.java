package ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;

public class ParkingCodeRequestAssembler {

    public ParkingCode toDomain(ParkingCodeRequest parkingCodeRequest) {
        return new ParkingCode(parkingCodeRequest.getParkingCode());
    }
}
