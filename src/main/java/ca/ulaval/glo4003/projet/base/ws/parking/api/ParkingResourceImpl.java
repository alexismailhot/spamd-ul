package ca.ulaval.glo4003.projet.base.ws.parking.api;

import ca.ulaval.glo4003.projet.base.ws.parking.api.request.SessionParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.SessionParkingPassRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.response.ParkingPassResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.service.ParkingService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import javax.ws.rs.core.Response;

public class ParkingResourceImpl implements ParkingResource {

    private final ParkingService parkingService;
    private final ParkingPassResponseAssembler parkingPassResponseAssembler;
    private final WeeklyParkingPassInformationFactory weeklyParkingPassInformationFactory;
    private final SessionParkingPassInformationFactory sessionParkingPassInformationFactory;
    private final CreditCardAssembler creditCardAssembler;

    public ParkingResourceImpl(
        ParkingService parkingService,
        WeeklyParkingPassInformationFactory weeklyParkingPassInformationFactory,
        SessionParkingPassInformationFactory sessionParkingPassInformationFactory,
        ParkingPassResponseAssembler parkingPassResponseAssembler,
        CreditCardAssembler creditCardAssembler
    ) {
        this.parkingService = parkingService;
        this.parkingPassResponseAssembler = parkingPassResponseAssembler;
        this.weeklyParkingPassInformationFactory = weeklyParkingPassInformationFactory;
        this.sessionParkingPassInformationFactory = sessionParkingPassInformationFactory;
        this.creditCardAssembler = creditCardAssembler;
    }

    @Override
    public Response buyWeeklyParkingPass(WeeklyParkingPassRequest weeklyParkingPassRequest) {
        ParkingCode parkingCode = parkingService.buyWeeklyParkingPass(
            weeklyParkingPassInformationFactory.create(weeklyParkingPassRequest),
            creditCardAssembler.toDomain(weeklyParkingPassRequest.getCreditCard())
        );
        return Response.status(Response.Status.CREATED).entity(parkingPassResponseAssembler.toResponse(parkingCode)).build();
    }

    @Override
    public Response buySessionParkingPass(SessionParkingPassRequest sessionParkingPassRequest) {
        ParkingCode parkingCode = parkingService.buySessionParkingPass(
            sessionParkingPassInformationFactory.create(sessionParkingPassRequest),
            creditCardAssembler.toDomain(sessionParkingPassRequest.getCreditCard())
        );
        return Response.status(Response.Status.CREATED).entity(parkingPassResponseAssembler.toResponse(parkingCode)).build();
    }
}
