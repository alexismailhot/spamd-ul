package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception.AccessCodeNotValidOnTimeException;
import java.time.LocalDateTime;

public class AccessGrantingValidator {

    public void validate(Access access, LocalDateTime time) {
        if (!access.isValidOn(time)) {
            throw new AccessCodeNotValidOnTimeException(access.getAccessCode(), time);
        }
    }
}
