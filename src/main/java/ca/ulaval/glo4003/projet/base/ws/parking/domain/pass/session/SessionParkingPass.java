package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import java.time.LocalDate;

public class SessionParkingPass extends ParkingPass {

    private final Session session;

    public SessionParkingPass(ParkingCode parkingCode, ParkingZone parkingZone, Session session) {
        super(parkingCode, parkingZone);
        this.session = session;
    }

    @Override
    public boolean isValidOn(LocalDate date) {
        return session.includes(date);
    }
}
