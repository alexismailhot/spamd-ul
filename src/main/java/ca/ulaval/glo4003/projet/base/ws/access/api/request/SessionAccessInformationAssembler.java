package ca.ulaval.glo4003.projet.base.ws.access.api.request;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.SessionAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionAssembler;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;

public class SessionAccessInformationAssembler {

    private final VehicleAssembler vehicleAssembler;
    private final DriverAssembler driverAssembler;
    private final SessionAssembler sessionAssembler;

    public SessionAccessInformationAssembler(VehicleAssembler vehicleAssembler, DriverAssembler driverAssembler, SessionAssembler sessionAssembler) {
        this.vehicleAssembler = vehicleAssembler;
        this.driverAssembler = driverAssembler;
        this.sessionAssembler = sessionAssembler;
    }

    public SessionAccessInformation toDomain(SessionAccessRequest sessionAccessRequest) {
        Driver driver = driverAssembler.toDomain(sessionAccessRequest.getDriver());
        Vehicle vehicle = vehicleAssembler.toDomain(sessionAccessRequest.getVehicle());
        Session session = sessionAssembler.toDomain(sessionAccessRequest.getSession());
        return new SessionAccessInformation(driver, vehicle, session);
    }
}
