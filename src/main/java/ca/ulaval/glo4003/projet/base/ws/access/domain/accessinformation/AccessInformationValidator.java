package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import java.util.Optional;

public abstract class AccessInformationValidator {

    private final AccessInformationValidator accessInformationValidator;

    public AccessInformationValidator(AccessInformationValidator accessInformationValidator) {
        this.accessInformationValidator = accessInformationValidator;
    }

    public void validate(AccessInformation accessInformation) {
        Optional.ofNullable(accessInformationValidator).ifPresent(validator -> validator.validate(accessInformation));
    }
}
