package ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler;

import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.DetailedInitiativeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.money.api.CapitalFormatter;

public class DetailedInitiativeResponseAssembler {

    public DetailedInitiativeResponse toResponse(Initiative initiative) {
        return new DetailedInitiativeResponse(initiative.getName(), CapitalFormatter.fromBigDecimal(initiative.getAttributedFunds().getAmount()));
    }
}
