package ca.ulaval.glo4003.projet.base.ws.initiative.domain;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class InitiativeFactory {

    public Initiative create(String name, Price attributedFunds) {
        return new Initiative(name, attributedFunds, generateInitiativeCode());
    }

    private InitiativeCode generateInitiativeCode() {
        return new InitiativeCode();
    }
}
