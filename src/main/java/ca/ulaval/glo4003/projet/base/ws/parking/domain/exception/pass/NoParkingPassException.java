package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass;

public class NoParkingPassException extends InvalidParkingPassException {

    @Override
    public String getDescription() {
        return "No parking pass";
    }
}
