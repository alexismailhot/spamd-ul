package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

import java.util.Map;

public interface AccessRatesConfig {
    Map<VehicleType, Map<AccessTimeType, Price>> getAccessRates();
}
