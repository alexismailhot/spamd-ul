package ca.ulaval.glo4003.projet.base.ws.initiative.domain;

import java.util.List;
import java.util.Optional;

public interface InitiativeRepository {
    void save(Initiative initiative);

    List<Initiative> getAll();

    Optional<Initiative> findByInitiativeCode(InitiativeCode initiativeCode);
}
