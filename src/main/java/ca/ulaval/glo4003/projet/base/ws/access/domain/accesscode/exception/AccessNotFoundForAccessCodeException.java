package ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;

public class AccessNotFoundForAccessCodeException extends InvalidAccessCodeException {

    private final AccessCode accessCode;

    public AccessNotFoundForAccessCodeException(AccessCode accessCode) {
        this.accessCode = accessCode;
    }

    @Override
    public String getDescription() {
        return String.format("No Access found for Access code %s.", accessCode.getCodeNumber());
    }
}
