package ca.ulaval.glo4003.projet.base.ws.offense.api.request;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;

public class OffenseIdRequestAssembler {

    public OffenseId toDomain(OffenseIdRequest offenseIdRequest) {
        return new OffenseId(offenseIdRequest.getId());
    }
}
