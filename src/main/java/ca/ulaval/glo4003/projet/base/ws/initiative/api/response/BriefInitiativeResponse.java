package ca.ulaval.glo4003.projet.base.ws.initiative.api.response;

public class BriefInitiativeResponse {

    private final String code;
    private final String name;

    public BriefInitiativeResponse(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
