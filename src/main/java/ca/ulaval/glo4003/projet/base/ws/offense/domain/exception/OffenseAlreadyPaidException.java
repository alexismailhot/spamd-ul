package ca.ulaval.glo4003.projet.base.ws.offense.domain.exception;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;

public class OffenseAlreadyPaidException extends RuntimeException {

    private static final String OFFENSE_NOT_FOUND_ERROR = "OFFENSE_ALREADY_PAID";

    private final OffenseId offenseId;

    public OffenseAlreadyPaidException(OffenseId offenseId) {
        this.offenseId = offenseId;
    }

    public String getError() {
        return OFFENSE_NOT_FOUND_ERROR;
    }

    public String getDescription() {
        return String.format("Offense [offenseId = %s] is already paid", offenseId.getId());
    }
}
