package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import java.time.LocalDate;

public class ParkingPassNotValidOnDateException extends InvalidParkingPassException {

    private final ParkingCode parkingCode;
    private final LocalDate date;

    public ParkingPassNotValidOnDateException(ParkingCode parkingCode, LocalDate date) {
        this.parkingCode = parkingCode;
        this.date = date;
    }

    @Override
    public String getDescription() {
        return String.format("Parking pass %s is not valid on %s.", parkingCode.getCode(), date.toString().toLowerCase());
    }
}
