package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidSessionException;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;

public class SessionValidator extends AccessInformationValidator {

    private final Clock clock;

    public SessionValidator(AccessInformationValidator accessInformationValidator, Clock clock) {
        super(accessInformationValidator);
        this.clock = clock;
    }

    @Override
    public void validate(AccessInformation accessInformation) {
        if (accessInformation instanceof SessionAccessInformation) {
           validateSession(((SessionAccessInformation) accessInformation).getSession());
        }
        super.validate(accessInformation);
    }

    private void validateSession(Session session) {
        LocalDate now = clock.instant().atZone(ZoneId.systemDefault()).toLocalDate();

        if (session.isBefore(now)) {
            throw new InvalidSessionException(session);
        }

        UniversityYear currentUniversityYear = UniversityYear.of(now);
        UniversityYear nextUniversityYear = currentUniversityYear.next();
        UniversityYear sessionUniversityYear = session.getUniversityYear();

        if (!(currentUniversityYear.equals(sessionUniversityYear) || nextUniversityYear.equals(sessionUniversityYear))) {
            throw new InvalidSessionException(session);
        }
    }

}
