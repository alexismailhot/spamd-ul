package ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler;

import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.InitiativeCodeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;

public class InitiativeCodeResponseAssembler {

    public InitiativeCodeResponse toResponse(InitiativeCode initiativeCode) {
        return new InitiativeCodeResponse(initiativeCode.getCode());
    }
}
