package ca.ulaval.glo4003.projet.base.ws.offense.domain;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;

public class Offense {

    private final OffenseId offenseId;
    private final OffenseCode offenseCode;
    private final String description;
    private final Price amount;
    private boolean isPaid;

    public Offense(OffenseId offenseId, OffenseCode offenseCode, String description, Price amount) {
        this.offenseId = offenseId;
        this.offenseCode = offenseCode;
        this.description = description;
        this.amount = amount;
        this.isPaid = false;
    }

    public void pay() {
        this.isPaid = true;
    }

    public Price getAmount() {
        return amount;
    }

    public OffenseId getOffenseId() {
        return offenseId;
    }

    public OffenseCode getOffenseCode() {
        return offenseCode;
    }

    public String getDescription() {
        return description;
    }

    public boolean isPaid() {
        return isPaid;
    }
}
