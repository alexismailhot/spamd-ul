package ca.ulaval.glo4003.projet.base.ws.payment.domain;

import java.util.Objects;

public class CreditCard {

    private final String number;
    private final String name;
    private final String expiryDate;
    private final String ccv;

    public CreditCard(String number, String name, String expiryDate, String ccv) {
        this.number = number;
        this.name = name;
        this.expiryDate = expiryDate;
        this.ccv = ccv;
    }

    @Override
    public String toString() {
        return "CreditCard{" + "number='" + number + '\'' + ", name='" + name + '\'' + ", expiryDate='" + expiryDate + '\'' + ", ccv='" + ccv + '\'' + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditCard that = (CreditCard) o;
        return (
            Objects.equals(number, that.number) &&
            Objects.equals(name, that.name) &&
            Objects.equals(expiryDate, that.expiryDate) &&
            Objects.equals(ccv, that.ccv)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name, expiryDate, ccv);
    }
}
