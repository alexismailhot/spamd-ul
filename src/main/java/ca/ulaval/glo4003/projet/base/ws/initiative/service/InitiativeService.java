package ca.ulaval.glo4003.projet.base.ws.initiative.service;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.CarbonCreditConfig;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeFactory;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception.InitiativeNotFoundException;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingAmountValidator;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundsCalculator;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.util.List;
import java.util.Optional;

public class InitiativeService {

    private final InitiativeFactory initiativeFactory;
    private final InitiativeFundingAmountValidator initiativeFundingAmountValidator;
    private final InitiativeRepository initiativeRepository;
    private final InitiativeFundsCalculator initiativeFundsCalculator;
    private final IncomeService incomeService;
    private final CarbonCreditConfig carbonCreditConfig;

    public InitiativeService(
        InitiativeFactory initiativeFactory,
        InitiativeFundingAmountValidator initiativeFundingAmountValidator,
        InitiativeRepository initiativeRepository,
        InitiativeFundsCalculator initiativeFundsCalculator,
        IncomeService incomeService,
        CarbonCreditConfig carbonCreditConfig
    ) {
        this.initiativeFactory = initiativeFactory;
        this.initiativeFundingAmountValidator = initiativeFundingAmountValidator;
        this.initiativeRepository = initiativeRepository;
        this.initiativeFundsCalculator = initiativeFundsCalculator;
        this.incomeService = incomeService;
        this.carbonCreditConfig = carbonCreditConfig;
    }

    public InitiativeCode createInitiative(String name, Price requestedAmount) {
        validateRequestedAmount(requestedAmount);
        Initiative initiative = initiativeFactory.create(name, requestedAmount);
        initiativeRepository.save(initiative);
        return initiative.getInitiativeCode();
    }

    public void fundInitiative(InitiativeCode initiativeCode, Price requestedAmount) {
        validateRequestedAmount(requestedAmount);

        fundInitiativeWithValidAmount(initiativeCode, requestedAmount);
    }

    private void fundInitiativeWithValidAmount(InitiativeCode initiativeCode, Price requestedAmount) {
        Initiative initiative = getInitiative(initiativeCode);
        initiative.fund(requestedAmount);
        initiativeRepository.save(initiative);
    }

    public List<Initiative> getAllInitiatives() {
        return initiativeRepository.getAll();
    }

    public Initiative getInitiative(InitiativeCode initiativeCode) {
        Optional<Initiative> optionalInitiative = initiativeRepository.findByInitiativeCode(initiativeCode);
        return optionalInitiative.orElseThrow(() -> new InitiativeNotFoundException(initiativeCode));
    }

    private void validateRequestedAmount(Price requestedAmount) {
        Price availableAmount = getRemainingFunds();
        initiativeFundingAmountValidator.validateAmount(availableAmount, requestedAmount);
    }

    private Price getRemainingFunds() {
        List<Initiative> initiatives = initiativeRepository.getAll();
        return initiativeFundsCalculator.calculateFundsAvailable(initiatives, incomeService.getTotalIncome());
    }

    public Price getInitiativeFundsFromOffenseIncome() {
        Price totalOffenseIncome = incomeService.getTotalOffenseIncome();
        return initiativeFundsCalculator.calculateFundsAllocatedToAllInitiatives(totalOffenseIncome);
    }

    public Price getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear() {
        Price offenseIncomeForCurrentUniversityYear = incomeService.getTotalOffenseIncomeForCurrentUniversityYear();
        return initiativeFundsCalculator.calculateFundsAllocatedToAllInitiatives(offenseIncomeForCurrentUniversityYear);
    }

    public void spendRemainingFundsOnCarbonCreditInitiatives() {
        Price availableAmount = getRemainingFunds();
        fundInitiativeWithValidAmount(carbonCreditConfig.getCarbonCreditInitiativeCode(), availableAmount);
    }

    public double getNumberOfCarbonCreditsBought() {
        Initiative carbonCreditInitiative = getInitiative(carbonCreditConfig.getCarbonCreditInitiativeCode());
        return carbonCreditInitiative.getAttributedFunds().dividedBy(carbonCreditConfig.getUnitPrice());
    }
}
