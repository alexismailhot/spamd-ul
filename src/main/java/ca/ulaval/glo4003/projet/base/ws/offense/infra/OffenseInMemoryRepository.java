package ca.ulaval.glo4003.projet.base.ws.offense.infra;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.Offense;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseRepository;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OffenseInMemoryRepository implements OffenseRepository {

    private final Map<OffenseId, Offense> offenses = new HashMap<>();

    @Override
    public void save(Offense offense) {
        offenses.put(offense.getOffenseId(), offense);
    }

    @Override
    public int getCount() {
        return offenses.size();
    }

    @Override
    public Optional<Offense> findByOffenseId(OffenseId offenseId) {
        return Optional.ofNullable(offenses.get(offenseId));
    }
}
