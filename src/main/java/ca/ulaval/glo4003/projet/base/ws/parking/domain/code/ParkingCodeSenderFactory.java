package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import ca.ulaval.glo4003.projet.base.ws.email.domain.EmailSender;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;

public class ParkingCodeSenderFactory {

    private final EmailSender emailSender;
    private final ParkingCodeMailingRepository parkingCodeMailingRepository;
    private final ParkingCodeMailingFactory parkingCodeMailingFactory;
    private final ParkingCodeSSPDeliveryInformationRepository parkingCodeSSPDeliveryInformationRepository;
    private final ParkingCodeSSPDeliveryInformationFactory parkingCodeSSPDeliveryInformationFactory;

    public ParkingCodeSenderFactory(
        EmailSender emailSender,
        ParkingCodeMailingRepository parkingCodeMailingRepository,
        ParkingCodeMailingFactory parkingCodeMailingFactory,
        ParkingCodeSSPDeliveryInformationRepository parkingCodeSSPDeliveryInformationRepository,
        ParkingCodeSSPDeliveryInformationFactory parkingCodeSSPDeliveryInformationFactory
    ) {
        this.emailSender = emailSender;
        this.parkingCodeMailingRepository = parkingCodeMailingRepository;
        this.parkingCodeMailingFactory = parkingCodeMailingFactory;
        this.parkingCodeSSPDeliveryInformationRepository = parkingCodeSSPDeliveryInformationRepository;
        this.parkingCodeSSPDeliveryInformationFactory = parkingCodeSSPDeliveryInformationFactory;
    }

    public ParkingCodeSender create(ParkingPassInformation parkingPassInformation, ParkingCode parkingCode) {
        DeliveryMethod deliveryMethod = parkingPassInformation.getDeliveryMethod();
        switch (deliveryMethod) {
            case EMAIL:
                return new ParkingCodeEmailSender(emailSender, parkingCode, parkingPassInformation.getEmail().orElseThrow(IllegalStateException::new));
            case POSTAL:
                ParkingCodeMailing parkingCodeMailing = parkingCodeMailingFactory.create(
                    parkingCode,
                    parkingPassInformation.getPostalAddress().orElseThrow(IllegalStateException::new)
                );
                return new ParkingCodePostalSender(parkingCodeMailingRepository, parkingCodeMailing);
            case SSP:
                ParkingCodeSSPDeliveryInformation parkingCodeSSPDeliveryInformation = parkingCodeSSPDeliveryInformationFactory.create(parkingCode);
                return new ParkingCodeSSPSender(parkingCodeSSPDeliveryInformationRepository, parkingCodeSSPDeliveryInformation);
            default:
                throw new IllegalArgumentException(String.format("Delivery method %s is not supported", deliveryMethod.toString()));
        }
    }
}
