package ca.ulaval.glo4003.projet.base.ws.access.domain.driver;

public enum Sex {
    MALE,
    FEMALE,
    OTHER
}
