package ca.ulaval.glo4003.projet.base.ws.access.api;

import ca.ulaval.glo4003.projet.base.ws.access.api.request.SessionAccessInformationAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.api.request.SessionAccessRequest;
import ca.ulaval.glo4003.projet.base.ws.access.api.request.WeeklyAccessInformationAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.api.request.WeeklyAccessRequest;
import ca.ulaval.glo4003.projet.base.ws.access.api.response.AccessCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.SessionAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.WeeklyAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.service.AccessService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import javax.ws.rs.core.Response;

public class AccessResourceImpl implements AccessResource {

    private final AccessService accessService;
    private final AccessCodeResponseAssembler accessCodeResponseAssembler;
    private final SessionAccessInformationAssembler sessionAccessInformationAssembler;
    private final WeeklyAccessInformationAssembler weeklyAccessInformationAssembler;
    private final CreditCardAssembler creditCardAssembler;

    public AccessResourceImpl(
        AccessService accessService,
        AccessCodeResponseAssembler accessCodeResponseAssembler,
        SessionAccessInformationAssembler sessionAccessInformationAssembler,
        WeeklyAccessInformationAssembler weeklyAccessInformationAssembler,
        CreditCardAssembler creditCardAssembler
    ) {
        this.accessService = accessService;
        this.accessCodeResponseAssembler = accessCodeResponseAssembler;
        this.sessionAccessInformationAssembler = sessionAccessInformationAssembler;
        this.weeklyAccessInformationAssembler = weeklyAccessInformationAssembler;
        this.creditCardAssembler = creditCardAssembler;
    }

    @Override
    public Response createWeeklyAccess(WeeklyAccessRequest weeklyAccessRequest) {
        WeeklyAccessInformation weeklyAccessInformation = weeklyAccessInformationAssembler.toDomain(weeklyAccessRequest);
        AccessCode accessCode = accessService.createWeeklyAccess(weeklyAccessInformation, creditCardAssembler.toDomain(weeklyAccessRequest.getCreditCard()));
        return Response.status(Response.Status.CREATED).entity(accessCodeResponseAssembler.toResponse(accessCode)).build();
    }

    @Override
    public Response createSessionAccess(SessionAccessRequest sessionAccessRequest) {
        SessionAccessInformation sessionAccessInformation = sessionAccessInformationAssembler.toDomain(sessionAccessRequest);
        AccessCode accessCode = accessService.createSessionAccess(sessionAccessInformation, creditCardAssembler.toDomain(sessionAccessRequest.getCreditCard()));
        return Response.status(Response.Status.CREATED).entity(accessCodeResponseAssembler.toResponse(accessCode)).build();
    }

    @Override
    public Response getAccess(int accessCode) {
        accessService.grantAccess(new AccessCode(accessCode));
        return Response.ok().build();
    }
}
