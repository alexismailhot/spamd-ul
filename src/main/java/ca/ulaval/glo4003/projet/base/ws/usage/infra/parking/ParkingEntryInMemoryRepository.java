package ca.ulaval.glo4003.projet.base.ws.usage.infra.parking;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntry;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntryRepository;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class ParkingEntryInMemoryRepository implements ParkingEntryRepository {

    private final List<ParkingEntry> parkingEntries = new ArrayList<>();

    @Override
    public void save(ParkingEntry parkingEntry) {
        parkingEntries.add(parkingEntry);
    }

    @Override
    public List<ParkingEntry> getByZoneInPeriod(ParkingZone zone, LocalDate from, LocalDate until) {
        return parkingEntries
            .stream()
            .filter(parkingEntry -> parkingEntry.hasZone(zone))
            .filter(parkingEntry -> parkingEntry.isInPeriod(from, until))
            .collect(Collectors.toList());
    }
}
