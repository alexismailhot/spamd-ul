package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import java.time.LocalDateTime;

public class ParkingPassValidationBuilder {

    private ParkingCode parkingCode;
    private final LocalDateTime timeIssued;
    private final ParkingZone parkingZone;

    private ParkingPassValidationBuilder(LocalDateTime timeIssued, ParkingZone parkingZone) {
        this.timeIssued = timeIssued;
        this.parkingZone = parkingZone;
    }

    public static ParkingPassValidationBuilder newBuilder(LocalDateTime timeIssued, ParkingZone parkingZone) {
        return new ParkingPassValidationBuilder(timeIssued, parkingZone);
    }

    public ParkingPassValidationBuilder withParkingCode(ParkingCode parkingCode) {
        this.parkingCode = parkingCode;
        return this;
    }

    public ParkingPassValidation build() {
        return new ParkingPassValidation(parkingCode, timeIssued, parkingZone);
    }
}
