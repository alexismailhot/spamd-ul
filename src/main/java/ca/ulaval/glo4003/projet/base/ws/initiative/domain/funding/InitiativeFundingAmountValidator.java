package ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception.InsufficientInitiativeFundsException;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;

public class InitiativeFundingAmountValidator {

    public void validateAmount(Price availableAmount, Price requestedAmount) {
        if (requestedAmount.compareTo(availableAmount) > 0) {
            throw new InsufficientInitiativeFundsException(requestedAmount);
        }
    }
}
