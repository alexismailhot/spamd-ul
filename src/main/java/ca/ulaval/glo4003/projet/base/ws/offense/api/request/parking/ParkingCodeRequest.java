package ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking;

public class ParkingCodeRequest {

    private final int parkingCode;

    public ParkingCodeRequest(int parkingCode) {
        this.parkingCode = parkingCode;
    }

    public int getParkingCode() {
        return parkingCode;
    }
}
