package ca.ulaval.glo4003.projet.base.ws.parking.service;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSender;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSenderFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.NoParkingPassException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotFoundForParkingCodeException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassPriceCalculator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassType;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.ParkingPassInformationValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session.SessionParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session.SessionParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import java.util.Optional;

public class ParkingService {

    private static final ParkingPassType WEEKLY_PARKING_PASS_TYPE = ParkingPassType.ONE_DAY_A_WEEK_FOR_A_SEMESTER;
    private static final ParkingPassType ONE_SESSION_PARKING_PASS_TYPE = ParkingPassType.ONE_SEMESTER;

    private final ParkingPassRepository parkingPassRepository;
    private final WeeklyParkingPassFactory weeklyParkingPassFactory;
    private final SessionParkingPassFactory sessionParkingPassFactory;
    private final PaymentService paymentService;
    private final ParkingCodeSenderFactory parkingCodeSenderFactory;
    private final ParkingPassIncomeFactory parkingPassIncomeFactory;
    private final ParkingPassIncomeRepository parkingPassIncomeRepository;
    private final ParkingPassInformationValidator parkingPassInformationValidator;
    private final ParkingPassPriceCalculator parkingPassPriceCalculator;
    private final ParkingPassValidator parkingPassValidator;

    public ParkingService(
        ParkingPassRepository parkingPassRepository,
        WeeklyParkingPassFactory weeklyParkingPassFactory,
        SessionParkingPassFactory sessionParkingPassFactory,
        PaymentService paymentService,
        ParkingCodeSenderFactory parkingCodeSenderFactory,
        ParkingPassIncomeFactory parkingPassIncomeFactory,
        ParkingPassIncomeRepository parkingPassIncomeRepository,
        ParkingPassInformationValidator parkingPassInformationValidator,
        ParkingPassPriceCalculator parkingPassPriceCalculator,
        ParkingPassValidator parkingPassValidator
    ) {
        this.parkingPassRepository = parkingPassRepository;
        this.weeklyParkingPassFactory = weeklyParkingPassFactory;
        this.sessionParkingPassFactory = sessionParkingPassFactory;
        this.paymentService = paymentService;
        this.parkingCodeSenderFactory = parkingCodeSenderFactory;
        this.parkingPassIncomeFactory = parkingPassIncomeFactory;
        this.parkingPassIncomeRepository = parkingPassIncomeRepository;
        this.parkingPassInformationValidator = parkingPassInformationValidator;
        this.parkingPassPriceCalculator = parkingPassPriceCalculator;
        this.parkingPassValidator = parkingPassValidator;
    }

    public ParkingCode buyWeeklyParkingPass(WeeklyParkingPassInformation weeklyParkingPassInformation, CreditCard creditCard) {
        parkingPassInformationValidator.validate(weeklyParkingPassInformation);

        Price parkingPassPrice = parkingPassPriceCalculator.calculatePrice(
            weeklyParkingPassInformation.getParkingZone(),
            WEEKLY_PARKING_PASS_TYPE,
            weeklyParkingPassInformation.getDeliveryMethod()
        );
        paymentService.processPayment(creditCard, parkingPassPrice);

        ParkingPassIncome parkingPassIncome = parkingPassIncomeFactory.create(parkingPassPrice);
        parkingPassIncomeRepository.save(parkingPassIncome);

        WeeklyParkingPass weeklyParkingPass = weeklyParkingPassFactory.create(
            weeklyParkingPassInformation.getParkingZone(),
            weeklyParkingPassInformation.getParkingDay(),
            weeklyParkingPassInformation.getSession()
        );

        parkingPassRepository.save(weeklyParkingPass);
        ParkingCode parkingCode = weeklyParkingPass.getParkingCode();
        ParkingCodeSender parkingCodeSender = parkingCodeSenderFactory.create(weeklyParkingPassInformation, parkingCode);
        parkingCodeSender.send();

        return parkingCode;
    }

    public ParkingCode buySessionParkingPass(SessionParkingPassInformation sessionParkingPassInformation, CreditCard creditCard) {
        parkingPassInformationValidator.validate(sessionParkingPassInformation);

        Price parkingPassPrice = parkingPassPriceCalculator.calculatePrice(
            sessionParkingPassInformation.getParkingZone(),
            ONE_SESSION_PARKING_PASS_TYPE,
            sessionParkingPassInformation.getDeliveryMethod()
        );
        paymentService.processPayment(creditCard, parkingPassPrice);

        ParkingPassIncome parkingPassIncome = parkingPassIncomeFactory.create(parkingPassPrice);
        parkingPassIncomeRepository.save(parkingPassIncome);

        SessionParkingPass sessionParkingPass = sessionParkingPassFactory.create(
            sessionParkingPassInformation.getParkingZone(),
            sessionParkingPassInformation.getSession()
        );

        parkingPassRepository.save(sessionParkingPass);
        ParkingCode parkingCode = sessionParkingPass.getParkingCode();
        ParkingCodeSender parkingCodeSender = parkingCodeSenderFactory.create(sessionParkingPassInformation, parkingCode);
        parkingCodeSender.send();

        return parkingCode;
    }

    public void validateParkingPass(ParkingPassValidation parkingPassValidation) {
        ParkingCode parkingCode = parkingPassValidation.getParkingCode().orElseThrow(NoParkingPassException::new);
        Optional<ParkingPass> optionalParkingPass = parkingPassRepository.findByParkingCode(parkingCode);
        ParkingPass parkingPass = optionalParkingPass.orElseThrow(() -> new ParkingPassNotFoundForParkingCodeException(parkingCode));
        parkingPassValidator.validate(parkingPassValidation, parkingPass);
    }
}
