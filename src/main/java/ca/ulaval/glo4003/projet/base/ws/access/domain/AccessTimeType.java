package ca.ulaval.glo4003.projet.base.ws.access.domain;

public enum AccessTimeType {
    ONE_HOUR,
    ONE_DAY,
    ONE_DAY_A_WEEK_FOR_ONE_SEMESTER,
    ONE_SEMESTER,
    TWO_SEMESTERS,
    THREE_SEMESTERS,
}
