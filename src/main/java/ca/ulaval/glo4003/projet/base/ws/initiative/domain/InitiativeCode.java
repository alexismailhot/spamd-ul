package ca.ulaval.glo4003.projet.base.ws.initiative.domain;

import java.util.Objects;
import java.util.UUID;

public class InitiativeCode {

    private final String code;

    public InitiativeCode(String code) {
        this.code = code;
    }

    public InitiativeCode() {
        this.code = UUID.randomUUID().toString();
    }

    public String getCode() {
        return code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InitiativeCode initivativeCode = (InitiativeCode) o;
        return Objects.equals(code, initivativeCode.code);
    }
}
