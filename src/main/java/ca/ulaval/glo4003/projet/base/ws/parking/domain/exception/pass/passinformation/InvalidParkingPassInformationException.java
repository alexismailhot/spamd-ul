package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation;

public abstract class InvalidParkingPassInformationException extends RuntimeException {

    public abstract String getError();

    public abstract String getDescription();
}
