package ca.ulaval.glo4003.projet.base.ws.access.domain;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.SessionAccessInformation;

public class SessionAccessFactory {

    private final AccessRepository accessRepository;

    public SessionAccessFactory(AccessRepository accessRepository) {
        this.accessRepository = accessRepository;
    }

    public SessionAccess create(SessionAccessInformation sessionAccessInformation) {
        return new SessionAccess(generateAccessCode(), sessionAccessInformation.getSession());
    }

    private AccessCode generateAccessCode() {
        return new AccessCode(accessRepository.getCount());
    }
}
