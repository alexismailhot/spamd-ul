package ca.ulaval.glo4003.projet.base.ws.usage.domain.parking;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import java.time.LocalDate;

public class ParkingEntry {

    private final LocalDate entryDate;
    private final ParkingZone parkingZone;

    public ParkingEntry(LocalDate entryDate, ParkingZone parkingZone) {
        this.entryDate = entryDate;
        this.parkingZone = parkingZone;
    }

    public LocalDate getEntryDate() {
        return entryDate;
    }

    public boolean hasZone(ParkingZone zone) {
        return zone.equals(parkingZone);
    }

    public boolean isInPeriod(LocalDate from, LocalDate until) {
        return entryDate.isAfter(from.minusDays(1)) && entryDate.isBefore(until.plusDays(1));
    }
}
