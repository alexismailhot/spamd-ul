package ca.ulaval.glo4003.projet.base.ws.parking.infra.rates;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRatesConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassType;
import java.util.Map;

public class ParkingPassRatesULavalConfig implements ParkingPassRatesConfig {

    private final Map<ParkingZone, Map<ParkingPassType, Price>> parkingRates;

    public ParkingPassRatesULavalConfig(Map<ParkingZone, Map<ParkingPassType, Price>> parkingRates) {
        this.parkingRates = parkingRates;
    }

    @Override
    public Price getParkingPassRate(ParkingZone parkingZone, ParkingPassType parkingPassType) {
        return parkingRates.get(parkingZone).get(parkingPassType);
    }
}
