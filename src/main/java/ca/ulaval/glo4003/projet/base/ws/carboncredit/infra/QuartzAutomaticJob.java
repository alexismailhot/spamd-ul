package ca.ulaval.glo4003.projet.base.ws.carboncredit.infra;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.exception.QuartzJobManagerException;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.service.AutomaticJob;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

public class QuartzAutomaticJob implements AutomaticJob {

    private final Scheduler scheduler;
    private final JobKey jobKey;

    public QuartzAutomaticJob(Scheduler scheduler, JobKey jobKey) {
        this.scheduler = scheduler;
        this.jobKey = jobKey;
    }

    public void activate() {
        try {
            scheduler.resumeJob(jobKey);
        } catch (SchedulerException schedulerException) {
            throw new QuartzJobManagerException(schedulerException);
        }
    }

    public void deactivate() {
        try {
            scheduler.pauseJob(jobKey);
        } catch (SchedulerException schedulerException) {
            throw new QuartzJobManagerException(schedulerException);
        }
    }
}
