package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

public class ParkingCodeSSPSender implements ParkingCodeSender {

    private final ParkingCodeSSPDeliveryInformationRepository parkingCodeSSPDeliveryInformationRepository;
    private final ParkingCodeSSPDeliveryInformation parkingCodeSSPDeliveryInformation;

    public ParkingCodeSSPSender(
        ParkingCodeSSPDeliveryInformationRepository parkingCodeSSPDeliveryInformationRepository,
        ParkingCodeSSPDeliveryInformation parkingCodeSSPDeliveryInformation
    ) {
        this.parkingCodeSSPDeliveryInformationRepository = parkingCodeSSPDeliveryInformationRepository;
        this.parkingCodeSSPDeliveryInformation = parkingCodeSSPDeliveryInformation;
    }

    @Override
    public void send() {
        this.parkingCodeSSPDeliveryInformationRepository.save(parkingCodeSSPDeliveryInformation);
        System.out.printf("[SPAMD-UL] Parking code %d to be picked up at SSP by ...", parkingCodeSSPDeliveryInformation.getParkingCode().getCode());
    }
}
