package ca.ulaval.glo4003.projet.base.ws.payment.api.request;

import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;

public class CreditCardAssembler {

    public CreditCard toDomain(CreditCardRequest creditCardRequest) {
        return new CreditCard(creditCardRequest.getNumber(), creditCardRequest.getName(), creditCardRequest.getExpiryDate(), creditCardRequest.getCcv());
    }
}
