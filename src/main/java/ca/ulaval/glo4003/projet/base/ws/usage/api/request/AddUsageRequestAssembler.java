package ca.ulaval.glo4003.projet.base.ws.usage.api.request;

import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntry;

public class AddUsageRequestAssembler {

    public ParkingEntry toDomain(AddUsageRequest request) {
        return new ParkingEntry(request.getDate(), request.getParkingZone());
    }
}
