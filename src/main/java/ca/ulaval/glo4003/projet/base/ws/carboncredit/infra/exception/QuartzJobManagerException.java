package ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.exception;

import org.quartz.SchedulerException;

public class QuartzJobManagerException extends RuntimeException {

    private static final String ERROR_NAME = "CARBON_CREDIT_BUYING_JOB_FAILED_EXCEPTION";
    private SchedulerException exception;

    public QuartzJobManagerException(SchedulerException exception) {
        this.exception = exception;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public String getError() {
        return ERROR_NAME;
    }

    public String getDescription() {
        return String.format("Quartz scheduling failed. Error : %s", exception.getMessage());
    }
}
