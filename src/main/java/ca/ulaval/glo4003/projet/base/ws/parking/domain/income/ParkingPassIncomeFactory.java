package ca.ulaval.glo4003.projet.base.ws.parking.domain.income;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class ParkingPassIncomeFactory {

    private final Clock clock;

    public ParkingPassIncomeFactory(Clock clock) {
        this.clock = clock;
    }

    public ParkingPassIncome create(Price price) {
        LocalDateTime created = clock.instant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return new ParkingPassIncome(price, created);
    }
}
