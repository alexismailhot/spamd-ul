package ca.ulaval.glo4003.projet.base.ws.offense.domain.income;

import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.util.List;

public interface OffenseIncomeRepository {
    void save(OffenseIncome offenseIncome);

    List<OffenseIncome> getAll();

    List<OffenseIncome> findByUniversityYear(UniversityYear universityYear);
}
