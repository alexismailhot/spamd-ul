package ca.ulaval.glo4003.projet.base.ws.parking.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostalAddressRequest {

    private final String address;
    private final String apartmentNumber;
    private final String state;
    private final String country;
    private final String city;
    private final String postalCode;

    @JsonCreator
    public PostalAddressRequest(
        @JsonProperty(value = "address", required = true) String address,
        @JsonProperty(value = "apartmentNumber") String apartmentNumber,
        @JsonProperty(value = "state", required = true) String state,
        @JsonProperty(value = "country", required = true) String country,
        @JsonProperty(value = "city", required = true) String city,
        @JsonProperty(value = "postalCode", required = true) String postalCode
    ) {
        this.address = address;
        this.apartmentNumber = apartmentNumber;
        this.state = state;
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }
}
