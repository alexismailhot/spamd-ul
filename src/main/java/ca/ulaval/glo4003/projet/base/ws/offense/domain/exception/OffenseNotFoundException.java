package ca.ulaval.glo4003.projet.base.ws.offense.domain.exception;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;

public class OffenseNotFoundException extends RuntimeException {

    private static final String OFFENSE_NOT_FOUND_ERROR = "OFFENSE_NOT_FOUND";

    private final OffenseId offenseId;

    public OffenseNotFoundException(OffenseId offenseId) {
        this.offenseId = offenseId;
    }

    public String getError() {
        return OFFENSE_NOT_FOUND_ERROR;
    }

    public String getDescription() {
        return String.format("No offense found for offenseId %s.", offenseId.getId());
    }
}
