package ca.ulaval.glo4003.projet.base.ws.access.service;

import ca.ulaval.glo4003.projet.base.ws.access.domain.*;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception.AccessNotFoundForAccessCodeException;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.AccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.AccessInformationValidator;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.SessionAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.WeeklyAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

public class AccessService {

    private static final AccessTimeType WEEKLY_ACCESS_TYPE = AccessTimeType.ONE_DAY_A_WEEK_FOR_ONE_SEMESTER;
    private static final AccessTimeType SESSION_ACCESS_TYPE = AccessTimeType.ONE_SEMESTER;

    private final WeeklyAccessFactory weeklyAccessFactory;
    private final SessionAccessFactory sessionAccessFactory;
    private final AccessIncomeFactory accessIncomeFactory;
    private final AccessIncomeRepository accessIncomeRepository;
    private final AccessInformationValidator accessInformationValidator;
    private final AccessRepository accessRepository;
    private final AccessGrantingValidator accessGrantingValidator;
    private final PaymentService paymentService;
    private final AccessPriceCalculator accessPriceCalculator;
    private final Clock clock;

    public AccessService(
        WeeklyAccessFactory weeklyAccessFactory,
        SessionAccessFactory sessionAccessFactory,
        AccessIncomeFactory accessIncomeFactory,
        AccessIncomeRepository accessIncomeRepository,
        AccessInformationValidator accessInformationValidator,
        AccessRepository accessRepository,
        AccessGrantingValidator accessGrantingValidator,
        PaymentService paymentService,
        AccessPriceCalculator accessPriceCalculator,
        Clock clock
    ) {
        this.weeklyAccessFactory = weeklyAccessFactory;
        this.sessionAccessFactory = sessionAccessFactory;
        this.accessIncomeFactory = accessIncomeFactory;
        this.accessIncomeRepository = accessIncomeRepository;
        this.accessInformationValidator = accessInformationValidator;
        this.accessRepository = accessRepository;
        this.accessGrantingValidator = accessGrantingValidator;
        this.paymentService = paymentService;
        this.accessPriceCalculator = accessPriceCalculator;
        this.clock = clock;
    }

    public AccessCode createWeeklyAccess(WeeklyAccessInformation weeklyAccessInformation, CreditCard creditCard) {
        validateAccess(weeklyAccessInformation);

        processPayment(weeklyAccessInformation, creditCard, WEEKLY_ACCESS_TYPE);

        WeeklyAccess access = weeklyAccessFactory.create(weeklyAccessInformation);
        accessRepository.save(access);

        return access.getAccessCode();
    }

    public AccessCode createSessionAccess(SessionAccessInformation sessionAccessInformation, CreditCard creditCard) {
        validateAccess(sessionAccessInformation);

        processPayment(sessionAccessInformation, creditCard, SESSION_ACCESS_TYPE);

        SessionAccess access = sessionAccessFactory.create(sessionAccessInformation);
        accessRepository.save(access);

        return access.getAccessCode();
    }

    public void grantAccess(AccessCode accessCode) {
        Optional<Access> optionalAccess = accessRepository.findByAccessCode(accessCode);
        Access access = optionalAccess.orElseThrow(() -> new AccessNotFoundForAccessCodeException(accessCode));

        LocalDateTime currentTime = clock.instant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        accessGrantingValidator.validate(access, currentTime);
    }

    private void processPayment(AccessInformation accessInformation, CreditCard creditCard, AccessTimeType accessTimeType) {
        VehicleType vehicleType = accessInformation.getVehicleType();
        Price accessPrice = accessPriceCalculator.calculatePrice(vehicleType, accessTimeType);
        paymentService.processPayment(creditCard, accessPrice);

        AccessIncome accessIncome = accessIncomeFactory.create(accessPrice, vehicleType);
        accessIncomeRepository.save(accessIncome);
    }

    private void validateAccess(AccessInformation accessInformation) {
        accessInformationValidator.validate(accessInformation);
    }
}
