package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.exception.InvalidBirthdateException;
import java.time.LocalDate;

public class BirthdateValidator extends AccessInformationValidator {

    private final LocalDate minimumBirthDate;
    private final LocalDate maximumBirthDate;

    public BirthdateValidator(AccessInformationValidator accessInformationValidator, LocalDate minimumBirthDate, LocalDate maximumBirthDate) {
        super(accessInformationValidator);
        this.maximumBirthDate = maximumBirthDate;
        this.minimumBirthDate = minimumBirthDate;
    }

    @Override
    public void validate(AccessInformation accessInformation) {
        if (accessInformation.driverHasBirthDateBefore(maximumBirthDate) || accessInformation.driverHasBirthDateAfter(minimumBirthDate)) {
            throw new InvalidBirthdateException(minimumBirthDate, maximumBirthDate);
        }
        super.validate(accessInformation);
    }
}
