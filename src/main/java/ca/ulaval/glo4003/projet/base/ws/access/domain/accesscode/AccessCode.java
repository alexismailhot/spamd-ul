package ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode;

public class AccessCode {
    private final Integer codeNumber;

    public AccessCode(int codeNumber) {
        this.codeNumber = codeNumber;
    }

    public int getCodeNumber() {
        return codeNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessCode that = (AccessCode) o;

        return codeNumber.equals(that.codeNumber);
    }

    @Override
    public int hashCode() {
        return codeNumber.hashCode();
    }
}
