package ca.ulaval.glo4003.projet.base.ws.money.domain;

import java.math.BigDecimal;
import java.util.Objects;

public class Price implements Comparable<Price> {

    private final BigDecimal amount;

    private Price(BigDecimal amount) {
        this.amount = amount;
    }

    public static Price of(double amount) {
        return new Price(BigDecimal.valueOf(amount));
    }

    public static Price of(long amount) {
        return new Price(BigDecimal.valueOf(amount));
    }

    public static Price zero() {
        return new Price(BigDecimal.valueOf(0));
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Price add(Price price) {
        return new Price(amount.add(price.amount));
    }

    public Price multiplyBy(double factor) {
        return new Price(amount.multiply(BigDecimal.valueOf(factor)));
    }

    public Price subtractBy(Price price) {
        return new Price(amount.subtract(price.amount));
    }

    public double dividedBy(Price price) {
        return amount.doubleValue() / price.amount.doubleValue();
    }

    @Override
    public int compareTo(Price price) {
        return amount.compareTo(price.amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equals(amount, price.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }
}
