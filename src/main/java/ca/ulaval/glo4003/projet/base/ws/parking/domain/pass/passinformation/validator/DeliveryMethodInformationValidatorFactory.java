package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;

public class DeliveryMethodInformationValidatorFactory {

    public DeliveryMethodInformationValidator create(DeliveryMethod deliveryMethod) {
        switch (deliveryMethod) {
            case EMAIL:
                return new EmailValidator();
            case POSTAL:
                return new PostalCodeValidator();
            case SSP:
                return new NoOpDeliveryMethodInformationValidator();
            default:
                throw new IllegalArgumentException();
        }
    }
}
