package ca.ulaval.glo4003.projet.base.ws.usage.domain.parking;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.DatesWithSameUsage;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class ParkingZoneUsageSummary {

    private final ParkingZone zone;
    private final List<ParkingEntry> parkingEntries;
    private final LocalDate from;
    private final LocalDate until;

    public ParkingZoneUsageSummary(List<ParkingEntry> entries, ParkingZone zone, LocalDate from, LocalDate until) {
        this.parkingEntries = entries;
        this.from = from;
        this.until = until;
        this.zone = zone;
    }

    public ParkingZone getZone() {
        return zone;
    }

    public double getAverageUsage() {
        return computeNumberOfParkingEntriesByDate().values().stream().mapToInt(Integer::intValue).average().orElse(0);
    }

    public DatesWithSameUsage getDatesWithMaximumUsage() {
        if (parkingEntries.isEmpty()) {
            int noUsage = 0;
            return new DatesWithSameUsage(Collections.singletonList(from), noUsage);
        }

        int maximumUsage = Collections.max(computeNumberOfParkingEntriesByDate().entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue();
        return getDatesWithSameUsage(maximumUsage);
    }

    public DatesWithSameUsage getDatesWithMinimumUsage() {
        if (parkingEntries.isEmpty()) {
            int noUsage = 0;
            return new DatesWithSameUsage(Collections.singletonList(until), noUsage);
        }

        int minimumUsage = Collections.min(computeNumberOfParkingEntriesByDate().entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue();
        return getDatesWithSameUsage(minimumUsage);
    }

    private DatesWithSameUsage getDatesWithSameUsage(int usageToMatch) {
        List<LocalDate> dates = computeNumberOfParkingEntriesByDate()
            .entrySet()
            .stream()
            .filter(entry -> entry.getValue() == usageToMatch)
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());
        return new DatesWithSameUsage(dates, usageToMatch);
    }

    private Map<LocalDate, Integer> computeNumberOfParkingEntriesByDate() {
        Map<LocalDate, Integer> numberOfParkingEntriesByDate = new HashMap<>();
        Map<LocalDate, List<ParkingEntry>> entriesByDate = new HashMap<>();

        for (LocalDate date = from; date.isBefore(until.plusDays(1)); date = date.plusDays(1)) {
            entriesByDate.put(date, filterEntriesByDate(date));
        }

        entriesByDate.forEach((date, parkingEntriesForDate) -> numberOfParkingEntriesByDate.put(date, parkingEntriesForDate.size()));

        return numberOfParkingEntriesByDate;
    }

    private List<ParkingEntry> filterEntriesByDate(LocalDate date) {
        return parkingEntries.stream().filter(entry -> entry.getEntryDate().equals(date)).collect(Collectors.toList());
    }
}
