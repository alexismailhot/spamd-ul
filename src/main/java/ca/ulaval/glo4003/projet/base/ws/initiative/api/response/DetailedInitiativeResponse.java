package ca.ulaval.glo4003.projet.base.ws.initiative.api.response;

import java.math.BigDecimal;

public class DetailedInitiativeResponse {

    private final String name;
    private final BigDecimal attributedFunds;

    public DetailedInitiativeResponse(String name, BigDecimal attributedFunds) {
        this.name = name;
        this.attributedFunds = attributedFunds;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAttributedFunds() {
        return attributedFunds;
    }
}
