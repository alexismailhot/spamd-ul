package ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation;

public class InvalidPostalCodeException extends InvalidParkingPassInformationException {

    private static final String INVALID_POSTAL_CODE_ERROR = "INVALID_POSTAL_CODE";

    private final String postalCode;

    public InvalidPostalCodeException(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String getError() {
        return INVALID_POSTAL_CODE_ERROR;
    }

    @Override
    public String getDescription() {
        return String.format("The following postal code is invalid: %s", postalCode);
    }
}
