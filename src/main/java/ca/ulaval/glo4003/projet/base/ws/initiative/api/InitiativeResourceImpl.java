package ca.ulaval.glo4003.projet.base.ws.initiative.api;

import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeCreationRequest;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeFundingRequest;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.BriefInitiativeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.DetailedInitiativeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.InitiativeFundsResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.BriefInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.DetailedInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeFundsResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.core.Response;

public class InitiativeResourceImpl implements InitiativeResource {

    private final InitiativeService initiativeService;
    private final InitiativeCodeResponseAssembler initiativeCodeResponseAssembler;
    private final BriefInitiativeResponseAssembler briefInitiativeResponseAssembler;
    private final DetailedInitiativeResponseAssembler detailedInitiativeResponseAssembler;
    private final InitiativeFundsResponseAssembler initiativeFundsResponseAssembler;

    public InitiativeResourceImpl(
        InitiativeService initiativeService,
        InitiativeCodeResponseAssembler initiativeCodeResponseAssembler,
        BriefInitiativeResponseAssembler briefInitiativeResponseAssembler,
        DetailedInitiativeResponseAssembler detailedInitiativeResponseAssembler,
        InitiativeFundsResponseAssembler initiativeFundsResponseAssembler
    ) {
        this.initiativeService = initiativeService;
        this.initiativeCodeResponseAssembler = initiativeCodeResponseAssembler;
        this.briefInitiativeResponseAssembler = briefInitiativeResponseAssembler;
        this.detailedInitiativeResponseAssembler = detailedInitiativeResponseAssembler;
        this.initiativeFundsResponseAssembler = initiativeFundsResponseAssembler;
    }

    @Override
    public Response createInitiative(InitiativeCreationRequest initiativeCreationRequest) {
        InitiativeCode initiativeCode = initiativeService.createInitiative(
            initiativeCreationRequest.getName(),
            Price.of(initiativeCreationRequest.getAmount())
        );
        return Response.status(Response.Status.CREATED).entity(initiativeCodeResponseAssembler.toResponse(initiativeCode)).build();
    }

    @Override
    public Response getInitiatives() {
        List<Initiative> initiatives = initiativeService.getAllInitiatives();
        List<BriefInitiativeResponse> briefInitiativeResponses = initiatives
            .stream()
            .map(briefInitiativeResponseAssembler::toResponse)
            .collect(Collectors.toList());
        return Response.ok().entity(briefInitiativeResponses).build();
    }

    @Override
    public Response getInitiative(String initiativeCode) {
        Initiative initiative = initiativeService.getInitiative(new InitiativeCode(initiativeCode));
        DetailedInitiativeResponse detailedInitiativeResponse = detailedInitiativeResponseAssembler.toResponse(initiative);
        return Response.ok().entity(detailedInitiativeResponse).build();
    }

    @Override
    public Response fundInitiative(String initiativeCode, InitiativeFundingRequest initiativeFundingRequest) {
        initiativeService.fundInitiative(new InitiativeCode(initiativeCode), Price.of(initiativeFundingRequest.getAmount()));
        return Response.ok().build();
    }

    @Override
    public Response getAllInitiativeFundsFromOffenseIncome() {
        Price fundsGeneratedByOffenses = initiativeService.getInitiativeFundsFromOffenseIncome();
        InitiativeFundsResponse initiativeFundsResponse = initiativeFundsResponseAssembler.toResponse(fundsGeneratedByOffenses);
        return Response.ok().entity(initiativeFundsResponse).build();
    }

    @Override
    public Response getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear() {
        Price fundsGeneratedByOffense = initiativeService.getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear();
        InitiativeFundsResponse initiativeFundsResponse = initiativeFundsResponseAssembler.toResponse(fundsGeneratedByOffense);
        return Response.ok().entity(initiativeFundsResponse).build();
    }
}
