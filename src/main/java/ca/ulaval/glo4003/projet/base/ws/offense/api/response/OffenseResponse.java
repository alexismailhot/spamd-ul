package ca.ulaval.glo4003.projet.base.ws.offense.api.response;

import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import java.math.BigDecimal;

public class OffenseResponse {

    private final int offenseId;
    private final OffenseCode offenseCode;
    private final String description;
    private final BigDecimal amount;

    public OffenseResponse(int offenseId, OffenseCode offenseCode, String description, BigDecimal amount) {
        this.offenseId = offenseId;
        this.offenseCode = offenseCode;
        this.description = description;
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public int getOffenseId() {
        return offenseId;
    }

    public OffenseCode getOffenseCode() {
        return offenseCode;
    }
}
