package ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception.InsufficientInitiativeFundsException;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InitiativeFundingAmountValidatorTest {

    private static final Price A_BIGGER_PRICE = Price.of(2000);
    private static final Price A_SMALLER_PRICE = Price.of(1000);

    private InitiativeFundingAmountValidator initiativeFundingAmountValidator;

    @BeforeEach
    void setup() {
        initiativeFundingAmountValidator = new InitiativeFundingAmountValidator();
    }

    @Test
    void whenValidateAmountWithAvailableAmountGreaterThanRequestedAmount_thenValidationIsSuccessful() {
        // then
        Assertions.assertDoesNotThrow(
            () ->
                // when
                initiativeFundingAmountValidator.validateAmount(A_BIGGER_PRICE, A_SMALLER_PRICE)
        );
    }

    @Test
    void whenValidateAmountWithAvailableAmountSmallerThanRequestedAmount_thenThrowInsufficientInitiativeFundsException() {
        // then
        Assertions.assertThrows(
            InsufficientInitiativeFundsException.class,
            () ->
                // when
                initiativeFundingAmountValidator.validateAmount(A_SMALLER_PRICE, A_BIGGER_PRICE)
        );
    }
}
