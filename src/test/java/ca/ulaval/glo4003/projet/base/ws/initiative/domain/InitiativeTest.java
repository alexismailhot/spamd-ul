package ca.ulaval.glo4003.projet.base.ws.initiative.domain;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InitiativeTest {

    private static final Price AN_AMOUNT = Price.of(20);
    private static final String INITIATIVE_NAME = "NAME";
    private static final InitiativeCode INITIATIVE_CODE = new InitiativeCode("A_CODE");
    private Initiative initiative;

    @BeforeEach
    void setUp() {
        initiative = new Initiative(INITIATIVE_NAME, AN_AMOUNT, INITIATIVE_CODE);
    }

    @Test
    void whenAddAnAmount_thenAddAnAmount() {
        // when
        initiative.fund(AN_AMOUNT);

        // then
        assertThat(initiative.getAttributedFunds()).isEquivalentAccordingToCompareTo(AN_AMOUNT.add(AN_AMOUNT));
    }
}
