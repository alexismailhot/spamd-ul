package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidSessionException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Instant;
import java.time.Year;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class SessionValidatorTest {

    private static final Instant NOW = Instant.now();
    private static final ParkingZone A_ZONE = ParkingZone.ZONE_R;
    private static final DeliveryMethod A_DELIVERY_METHOD = DeliveryMethod.EMAIL;
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;

    @Mock
    private Clock clock;

    private SessionValidator sessionValidator;

    @BeforeEach
    void setUp() {
       sessionValidator = new SessionValidator(clock);
    }

    @Test
    void givenASessionInCurrentYear_whenValidate_thenNoExceptionThrown() {
        given(clock.instant()).willReturn(NOW);
        SessionParkingPassInformation sessionParkingPassInformation = new SessionParkingPassInformation(A_ZONE, A_DELIVERY_METHOD, givenASessionInCurrentYear());

        assertDoesNotThrow(() -> sessionValidator.validate(sessionParkingPassInformation));
    }

    @Test
    void givenAPastSession_whenValidate_thenThrowsInvalidSessionException() {
        given(clock.instant()).willReturn(NOW);
        SessionParkingPassInformation sessionParkingPassInformation = new SessionParkingPassInformation(A_ZONE, A_DELIVERY_METHOD, givenAPastSession());

        assertThrows(InvalidSessionException.class, () -> sessionValidator.validate(sessionParkingPassInformation));
    }

    @Test
    void givenASessionMoreThanAYearInTheFuture_whenValidate_thenThrowsInvalidSessionException() {
        given(clock.instant()).willReturn(NOW);
        SessionParkingPassInformation sessionParkingPassInformation = new SessionParkingPassInformation(A_ZONE, A_DELIVERY_METHOD, givenASessionMoreThanAYearInTheFuture());

        assertThrows(InvalidSessionException.class, () -> sessionValidator.validate(sessionParkingPassInformation));
    }

    private Session givenASessionMoreThanAYearInTheFuture() {
        return new Session(A_SESSION_TYPE, Year.of(NOW.atZone(ZoneId.systemDefault()).getYear()).plusYears(3));
    }

    private Session givenAPastSession() {
        return new Session(A_SESSION_TYPE, Year.of(NOW.atZone(ZoneId.systemDefault()).getYear()).minusYears(1));
    }

    private Session givenASessionInCurrentYear() {
        return new Session(A_SESSION_TYPE, Year.of(NOW.atZone(ZoneId.systemDefault()).getYear()));
    }
}