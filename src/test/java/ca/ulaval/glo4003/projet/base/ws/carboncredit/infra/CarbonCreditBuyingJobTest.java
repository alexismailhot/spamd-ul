package ca.ulaval.glo4003.projet.base.ws.carboncredit.infra;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;

@ExtendWith(MockitoExtension.class)
class CarbonCreditBuyingJobTest {

    private final String AN_INITIATIVE_SERVICE_KEY = "InitiativeService";
    private CarbonCreditBuyingJob carbonCreditBuyingJob;

    @Mock
    JobExecutionContext jobExecutionContext;

    @Mock
    InitiativeService initiativeService;

    @Mock
    Scheduler scheduler;

    @Mock
    SchedulerContext schedulerContext;

    @BeforeEach
    void setUp() {
        carbonCreditBuyingJob = new CarbonCreditBuyingJob();
    }

    @Test
    void givenJobExecutionContextWithInitiativeServiceInstance_whenExecute_thenSpendRemainingFundsOnCarbonCreditInitiatives() throws SchedulerException {
        //given
        given(jobExecutionContext.getScheduler()).willReturn(scheduler);
        given(scheduler.getContext()).willReturn(schedulerContext);
        given(schedulerContext.get(AN_INITIATIVE_SERVICE_KEY)).willReturn(initiativeService);

        //when
        carbonCreditBuyingJob.execute(jobExecutionContext);

        //then
        verify(initiativeService).spendRemainingFundsOnCarbonCreditInitiatives();
    }
}
