package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidPostalCodeException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PostalCodeValidatorTest {

    private static final String A_VALID_POSTAL_CODE = "G1V2X1";
    private static final String AN_INVALID_POSTAL_CODE = "G122X1";
    private static final String AN_ADDRESS = "123 Mackay";
    private static final String AN_APARTMENT_NUMBER = "4";
    private static final String A_STATE = "Quebec";
    private static final String A_COUNTRY = "Canada";
    private static final String A_CITY = "Quebec";
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final DeliveryMethod POSTAL_DELIVERY_METHOD = DeliveryMethod.POSTAL;
    private static final DayOfWeek A_PARKING_DAY = DayOfWeek.MONDAY;
    private static final Session AN_UNIVERSITY_SESSION = new Session(SessionType.FALL, Year.of(23));

    private PostalCodeValidator postalCodeValidator;

    @BeforeEach
    void setUp() {
        postalCodeValidator = new PostalCodeValidator();
    }

    @Test
    void givenAValidPostalCode_whenValidate_thenDoesNotThrowInvalidPostalCodeException() {
        // given
        ParkingPassInformation parkingPassInformation = givenAParkingPassInformation(A_VALID_POSTAL_CODE);

        // then
        assertDoesNotThrow(
            () ->
                // when
                postalCodeValidator.validate(parkingPassInformation)
        );
    }

    @Test
    void givenAnInvalidPostalCode_whenValidate_thenThrowsInvalidPostalCodeException() {
        // given
        ParkingPassInformation parkingPassInformation = givenAParkingPassInformation(AN_INVALID_POSTAL_CODE);

        // then
        assertThrows(
            InvalidPostalCodeException.class,
            () ->
                // when
                postalCodeValidator.validate(parkingPassInformation)
        );
    }

    private ParkingPassInformation givenAParkingPassInformation(String postalCode) {
        PostalAddress postalAddress = new PostalAddress(AN_ADDRESS, AN_APARTMENT_NUMBER, A_STATE, A_COUNTRY, A_CITY, postalCode);
        return new WeeklyParkingPassInformation(A_PARKING_ZONE, POSTAL_DELIVERY_METHOD, A_PARKING_DAY, postalAddress, AN_UNIVERSITY_SESSION);
    }
}
