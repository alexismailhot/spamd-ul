package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.exception.InvalidBirthdateException;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Sex;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BirthdateValidatorTest {

    private static final LocalDate A_VALID_BIRTHDATE = LocalDate.now().minusYears(23);
    private static final LocalDate A_TOO_OLD_BIRTHDATE = LocalDate.now().minusYears(230);
    private static final LocalDate A_TOO_YOUNG_BIRTHDATE = LocalDate.now().minusYears(3);
    private static final LocalDate A_MINIMUM_BIRTHDATE = LocalDate.now().minusYears(16);
    private static final LocalDate A_MAXIMUM_BIRTHDATE = LocalDate.now().minusYears(130);
    private static final String A_NAME = "name";
    private static final Sex A_SEX = Sex.FEMALE;
    private static final String A_VEHICLE_MODEL = "vehicle model";
    private static final String A_VEHICLE_BRAND = "vehicle brand";
    private static final DayOfWeek AN_ACCESS_DAY = DayOfWeek.FRIDAY;
    private static final String A_VEHICLE_PLATE_NUMBER = "vehicle plate number";
    private static final int A_VEHICLE_MODEL_YEAR = 7;
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.now();

    private BirthdateValidator birthdateValidator;

    @Mock
    private AccessInformationValidator nextAccessInformationValidator;

    @BeforeEach
    void setUp() {
        birthdateValidator = new BirthdateValidator(nextAccessInformationValidator, A_MINIMUM_BIRTHDATE, A_MAXIMUM_BIRTHDATE);
    }

    @Test
    void givenAnAccessInformationWithAValidBirthdate_whenValidate_thenDoesNotThrowInvalidBirthDateException() {
        //given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(A_VALID_BIRTHDATE);

        //then
        assertDoesNotThrow(
            () ->
                //when
                birthdateValidator.validate(weeklyAccessInformation)
        );
    }

    @Test
    void givenAnAccessInformationWithAValidBirthdate_whenValidate_thenNextValidatorIsCalled() {
        //given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(A_VALID_BIRTHDATE);

        // when
        birthdateValidator.validate(weeklyAccessInformation);

        //then
        verify(nextAccessInformationValidator).validate(weeklyAccessInformation);
    }

    @Test
    void givenAnAccessInformationWithATooOldBirthdate_whenValidate_thenThrowInvalidBirthDateException() {
        //given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(A_TOO_OLD_BIRTHDATE);

        //then
        assertThrows(
            InvalidBirthdateException.class,
            () ->
                //when
                birthdateValidator.validate(weeklyAccessInformation)
        );
    }

    @Test
    void givenAnAccessInformationWithATooYoungBirthdate_whenValidate_thenThrowInvalidBirthdateException() {
        //given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(A_TOO_YOUNG_BIRTHDATE);

        //then
        assertThrows(
            InvalidBirthdateException.class,
            () ->
                //when
                birthdateValidator.validate(weeklyAccessInformation)
        );
    }

    private WeeklyAccessInformation givenAnAccessInformation(LocalDate driverBirthdate) {
        return new WeeklyAccessInformation(
            new Driver(A_NAME, driverBirthdate, A_SEX),
            new Vehicle(A_VEHICLE_MODEL, A_VEHICLE_BRAND, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE),
            AN_ACCESS_DAY,
            new Session(A_SESSION_TYPE, A_YEAR)
        );
    }
}
