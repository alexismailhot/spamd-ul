package ca.ulaval.glo4003.projet.base.ws.access.domain;

import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception.AccessCodeNotValidOnTimeException;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccessGrantingValidatorTest {

    private static final LocalDateTime AN_ACCESS_DAY = LocalDateTime.of(2010, 10, 10, 1, 1);

    @Mock
    private Access access;

    private AccessGrantingValidator accessGrantingValidator;

    @BeforeEach
    void setUp() {
        this.accessGrantingValidator = new AccessGrantingValidator();
    }

    @Test
    void givenAccessIsValidOnAccessDay_whenValidate_thenDoNotThrowException() {
        // given
        given(access.isValidOn(AN_ACCESS_DAY)).willReturn(true);

        // then
        Assertions.assertDoesNotThrow(
            () ->
                // when
                accessGrantingValidator.validate(access, AN_ACCESS_DAY)
        );
    }

    @Test
    void givenAccessIsNotValidOnAccessDay_whenValidate_thenThrowAccessCodeNotValidOnTimeException() {
        // given
        given(access.isValidOn(AN_ACCESS_DAY)).willReturn(false);

        // then
        Assertions.assertThrows(
            AccessCodeNotValidOnTimeException.class,
            () ->
                // when
                accessGrantingValidator.validate(access, AN_ACCESS_DAY)
        );
    }
}
