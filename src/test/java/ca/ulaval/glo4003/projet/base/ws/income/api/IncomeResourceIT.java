package ca.ulaval.glo4003.projet.base.ws.income.api;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.access.infra.AccessIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.income.api.response.IncomeSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.income.api.response.assembler.IncomeSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummaryFactory;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.income.OffenseIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.income.ParkingPassIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYearProvider;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IncomeResourceIT {

    private static final Instant NOW = Instant.now();
    private static final LocalDateTime NOW_DATE = LocalDate.now().atStartOfDay();
    private static final Price A_PRICE = Price.of(20);
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;

    private IncomeResource incomeResource;

    private AccessIncomeRepository accessIncomeRepository;

    private ParkingPassIncomeRepository parkingPassIncomeRepository;

    @Mock
    Clock clock;

    @BeforeEach
    void setUp() {
        accessIncomeRepository = new AccessIncomeInMemoryRepository();
        parkingPassIncomeRepository = new ParkingPassIncomeInMemoryRepository();
        OffenseIncomeRepository offenseIncomeRepository = new OffenseIncomeInMemoryRepository();
        IncomeSummaryFactory incomeSummaryFactory = new IncomeSummaryFactory();
        UniversityYearProvider universityYearProvider = new UniversityYearProvider();

        IncomeService incomeService = new IncomeService(
            accessIncomeRepository,
            parkingPassIncomeRepository,
            offenseIncomeRepository,
            incomeSummaryFactory,
            universityYearProvider,
            clock
        );

        IncomeSummaryResponseAssembler incomeSummaryResponseAssembler = new IncomeSummaryResponseAssembler();
        incomeResource = new IncomeResourceImpl(incomeService, incomeSummaryResponseAssembler);
    }

    @Test
    void whenGetAccessIncomesSummaryForCurrentUniversityYear_thenReturnIncomeSummaryResponse() {
        // given
        given(clock.instant()).willReturn(NOW);
        AccessIncome accessIncome = givenAnAccessIncome();
        accessIncomeRepository.save(accessIncome);

        // when
        Response response = incomeResource.getAccessIncomeSummaryForCurrentUniversityYear();
        IncomeSummaryResponse incomeSummaryResponse = (IncomeSummaryResponse) response.getEntity();

        // then
        assertThat(incomeSummaryResponse.getNumberOfSales()).isEqualTo(1);
        assertThat(incomeSummaryResponse.getTotalIncome()).isEqualTo(formatAsCapitalAmount(A_PRICE.getAmount()));
    }

    @Test
    void givenAVehicleType_whenGetAllAccessIncomesForVehicleType_thenReturnIncomeSummaryResponse() {
        // given
        AccessIncome accessIncome = givenAnAccessIncome();
        accessIncomeRepository.save(accessIncome);

        // when
        Response response = incomeResource.getAllAccessIncomeForVehicleType(A_VEHICLE_TYPE);
        IncomeSummaryResponse incomeSummaryResponse = (IncomeSummaryResponse) response.getEntity();

        // then
        assertThat(incomeSummaryResponse.getNumberOfSales()).isEqualTo(1);
        assertThat(incomeSummaryResponse.getTotalIncome()).isEqualTo(formatAsCapitalAmount(A_PRICE.getAmount()));
    }

    @Test
    void givenAVehicleType_whenGetAccessIncomesForCurrentUniversityYearForVehicleType_thenReturnIncomeSummaryResponse() {
        // given
        given(clock.instant()).willReturn(NOW);
        AccessIncome accessIncome = givenAnAccessIncome();
        accessIncomeRepository.save(accessIncome);

        // when
        Response response = incomeResource.getAccessIncomeForCurrentUniversityYearForVehicleType(A_VEHICLE_TYPE.toString());
        IncomeSummaryResponse incomeSummaryResponse = (IncomeSummaryResponse) response.getEntity();

        // then
        assertThat(incomeSummaryResponse.getNumberOfSales()).isEqualTo(1);
        assertThat(incomeSummaryResponse.getTotalIncome()).isEqualTo(formatAsCapitalAmount(A_PRICE.getAmount()));
    }

    @Test
    void whenGetParkingPassIncomesSummaryForCurrentUniversityYear_thenReturnIncomeSummaryResponse() {
        // given
        given(clock.instant()).willReturn(NOW);
        ParkingPassIncome parkingPassIncome = givenAParkingPassIncome();
        parkingPassIncomeRepository.save(parkingPassIncome);

        // when
        Response response = incomeResource.getParkingPassIncomeSummaryForCurrentUniversityYear();
        IncomeSummaryResponse incomeSummaryResponse = (IncomeSummaryResponse) response.getEntity();

        // then
        assertThat(incomeSummaryResponse.getNumberOfSales()).isEqualTo(1);
        assertThat(incomeSummaryResponse.getTotalIncome()).isEqualTo(formatAsCapitalAmount(A_PRICE.getAmount()));
    }

    private AccessIncome givenAnAccessIncome() {
        return new AccessIncome(A_PRICE, A_VEHICLE_TYPE, NOW_DATE);
    }

    private ParkingPassIncome givenAParkingPassIncome() {
        return new ParkingPassIncome(A_PRICE, NOW_DATE);
    }

    private BigDecimal formatAsCapitalAmount(BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_UP);
    }
}
