package ca.ulaval.glo4003.projet.base.ws.access.domain;

import static com.google.common.truth.Truth.*;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WeeklyAccessTest {

    private static final DayOfWeek ACCESS_DAY = DayOfWeek.MONDAY;
    private static final AccessCode ACCESS_CODE = new AccessCode(4);
    private static final Year A_YEAR = Year.of(2021);
    private static final LocalDateTime A_TIME_ON_DAY_OF_WEEK_IN_SESSION = LocalDateTime.of(A_YEAR.getValue(), 10, 4, 2, 2);
    private static final LocalDateTime A_TIME_ANOTHER_DAY_OF_WEEK = LocalDateTime.of(A_YEAR.getValue(), 10, 5, 2, 2);
    private static final LocalDateTime A_TIME_ANOTHER_SESSION = LocalDateTime.of(A_YEAR.plusYears(1).getValue(), 10, 3, 2, 2);
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Session A_SESSION = new Session(A_SESSION_TYPE, A_YEAR);

    private WeeklyAccess weeklyAccess;

    @BeforeEach
    void setUp() {
        weeklyAccess = new WeeklyAccess(ACCESS_CODE, ACCESS_DAY, A_SESSION);
    }

    @Test
    void givenTimeHasSameDayOfWeekAsAccessAndIsInSession_whenIsValidOn_thenReturnTrue() {
        // given

        // when
        boolean isValidOn = weeklyAccess.isValidOn(A_TIME_ON_DAY_OF_WEEK_IN_SESSION);

        // then
        assertThat(isValidOn).isTrue();
    }

    @Test
    void givenTimeDoesNotHaveSameDayOfWeekAsAccess_whenIsValidOn_thenReturnFalse() {
        // given

        // when
        boolean isValidOn = weeklyAccess.isValidOn(A_TIME_ANOTHER_DAY_OF_WEEK);

        // then
        assertThat(isValidOn).isFalse();
    }

    @Test
    void givenTimeIsNotInSession_whenIsValidOn_thenReturnFalse() {
        // given

        // when
        boolean isValidOn = weeklyAccess.isValidOn(A_TIME_ANOTHER_SESSION);

        // then
        assertThat(isValidOn).isFalse();
    }
}
