package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WeeklyParkingPassTest {

    private static final ParkingCode A_PARKING_CODE = new ParkingCode(1);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final DayOfWeek A_DAY_OF_WEEK = DayOfWeek.MONDAY;
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.of(2021);
    private static final LocalDate A_DATE_ON_DAY_OF_WEEK_IN_SESSION = LocalDate.of(A_YEAR.getValue(), 10, 4);
    private static final LocalDate A_DATE_ANOTHER_DAY_OF_WEEK = LocalDate.of(A_YEAR.getValue(), 10, 5);
    private static final LocalDate A_DATE_ANOTHER_SESSION = LocalDate.of(A_YEAR.plusYears(1).getValue(), 10, 3);
    private static final Session A_SESSION = new Session(A_SESSION_TYPE, A_YEAR);

    private WeeklyParkingPass weeklyParkingPass;

    @BeforeEach
    void setUp() {
        this.weeklyParkingPass = new WeeklyParkingPass(A_PARKING_CODE, A_PARKING_ZONE, A_DAY_OF_WEEK, A_SESSION);
    }

    @Test
    void givenDateHasSameDayOfWeekAsWeeklyParkingPassAndIsInSession_whenIsValidOn_thenReturnTrue() {
        // given

        // when
        boolean isValidOn = weeklyParkingPass.isValidOn(A_DATE_ON_DAY_OF_WEEK_IN_SESSION);

        // then
        assertThat(isValidOn).isTrue();
    }

    @Test
    void givenDateDoesNotHaveSameDayOfWeekAsWeeklyParkingPass_whenIsValidOn_thenReturnFalse() {
        // given

        // when
        boolean isValidOn = weeklyParkingPass.isValidOn(A_DATE_ANOTHER_DAY_OF_WEEK);

        // then
        assertThat(isValidOn).isFalse();
    }

    @Test
    void givenDateIsNotInSession_whenIsValidOn_thenReturnFalse() {
        // given

        // when
        boolean isValidOn = weeklyParkingPass.isValidOn(A_DATE_ANOTHER_SESSION);

        // then
        assertThat(isValidOn).isFalse();
    }
}
