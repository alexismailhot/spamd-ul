package ca.ulaval.glo4003.projet.base.ws.usage.domain.parking;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;

class ParkingZoneUsageSummaryTest {

    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final LocalDate A_PARKING_DATE = LocalDate.of(2020, 1, 1);
    private static final LocalDate ANOTHER_PARKING_DATE = LocalDate.of(2020, 1, 2);
    private static final ParkingEntry A_PARKING_ENTRY = new ParkingEntry(A_PARKING_DATE, A_PARKING_ZONE);
    private static final ParkingEntry ANOTHER_PARKING_ENTRY = new ParkingEntry(ANOTHER_PARKING_DATE, A_PARKING_ZONE);
    private static final int MINIMUM_USAGE = 0;
    private static final int A_SINGLE_USAGE = 1;

    @Test
    void givenAllDaysHaveEqualUsageInPeriod_whenGetMaximumDatesWithSameUsage_thenDatesWithMostUsageEqualsAllDates() {
        // given
        List<ParkingEntry> entries = Arrays.asList(A_PARKING_ENTRY, ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        List<LocalDate> dates = summary.getDatesWithMaximumUsage().getDates();

        // then
        assertThat(dates).containsExactly(A_PARKING_DATE, ANOTHER_PARKING_DATE);
    }

    @Test
    void givenAllDaysHaveEqualUsageInPeriod_whenGetMaximumDatesWithSameUsage_thenUsageEqualsMaximumUsage() {
        // given
        List<ParkingEntry> entries = Arrays.asList(A_PARKING_ENTRY, ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        long usage = summary.getDatesWithMaximumUsage().getUsage();

        // then
        assertThat(usage).isEqualTo(1);
    }

    @Test
    void givenAllDaysHaveEqualUsageInPeriod_whenGetMinimumDatesWithSameUsage_thenDatesWithLeastUsageEqualsAllDates() {
        // given
        List<ParkingEntry> entries = Arrays.asList(A_PARKING_ENTRY, ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        List<LocalDate> dates = summary.getDatesWithMinimumUsage().getDates();

        // then
        assertThat(dates).containsExactly(A_PARKING_DATE, ANOTHER_PARKING_DATE);
    }

    @Test
    void givenAllDaysHaveEqualUsageInPeriod_whenGetMinimumDatesWithSameUsage_thenUsageIsMinimumUsage() {
        // given
        List<ParkingEntry> entries = Arrays.asList(A_PARKING_ENTRY, ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        long usage = summary.getDatesWithMinimumUsage().getUsage();

        // then
        assertThat(usage).isEqualTo(A_SINGLE_USAGE);
    }

    @Test
    void givenAllDaysHaveEqualUsageInPeriod_whenGetAverageUsage_thenAverageIsCorrect() {
        // given
        List<ParkingEntry> entries = Arrays.asList(A_PARKING_ENTRY, ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        double mean = summary.getAverageUsage();

        // then
        assertThat(mean).isEqualTo(1.0d);
    }

    @Test
    void givenDateWithUsageAndDateWithout_whenGetMaximumDatesWithSameUsage_thenMaximumUsageDatesContainsOnlyDateWithUsage() {
        // given
        List<ParkingEntry> entries = Collections.singletonList(ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        List<LocalDate> dates = summary.getDatesWithMaximumUsage().getDates();

        // then
        assertThat(dates).containsExactly(ANOTHER_PARKING_DATE);
    }

    @Test
    void givenDateWithUsageAndDateWithout_whenGetMaximumDatesWithSameUsage_thenUsageEqualsMaximumUsage() {
        // given
        List<ParkingEntry> entries = Collections.singletonList(ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        long usage = summary.getDatesWithMaximumUsage().getUsage();

        // then
        assertThat(usage).isEqualTo(A_SINGLE_USAGE);
    }

    @Test
    void givenDateWithUsageAndDateWithout_whenGetMinimumDatesWithSameUsage_thenMinimumUsageDatesContainsOnlyInactiveDate() {
        // given
        List<ParkingEntry> entries = Collections.singletonList(ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        List<LocalDate> dates = summary.getDatesWithMinimumUsage().getDates();

        // then
        assertThat(dates).containsExactly(A_PARKING_DATE);
    }

    @Test
    void givenDateWithUsageAndDateWithout_whenGetMinimumDatesWithSameUsage_thenUsageEqualsMinimumUsage() {
        // given
        List<ParkingEntry> entries = Collections.singletonList(ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        long usage = summary.getDatesWithMinimumUsage().getUsage();

        // then
        assertThat(usage).isEqualTo(MINIMUM_USAGE);
    }

    @Test
    void givenDateWithUsageAndDateWithout_whenGetAverageUsage_thenAverageIsCorrect() {
        // given
        List<ParkingEntry> entries = Collections.singletonList(ANOTHER_PARKING_ENTRY);
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        double mean = summary.getAverageUsage();

        // then
        assertThat(mean).isEqualTo(0.5d);
    }

    @Test
    void givenNoUsageInPeriod_whenGetMaximumDatesWithSameUsage_thenMaximumUsageDatesOnlyContainFirstDateOfPeriod() {
        // given
        List<ParkingEntry> entries = Collections.emptyList();
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        List<LocalDate> dates = summary.getDatesWithMaximumUsage().getDates();

        // then
        assertThat(dates).containsExactly(A_PARKING_DATE);
    }

    @Test
    void givenNoUsageInPeriod_whenGetMinimumDatesWithSameUsage_thenMinimumUsageDatesOnlyContainsLastDateOfPeriod() {
        // given
        List<ParkingEntry> entries = Collections.emptyList();
        ParkingZoneUsageSummary summary = new ParkingZoneUsageSummary(entries, A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // when
        List<LocalDate> dates = summary.getDatesWithMinimumUsage().getDates();

        // then
        assertThat(dates).containsExactly(ANOTHER_PARKING_DATE);
    }
}
