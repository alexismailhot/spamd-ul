package ca.ulaval.glo4003.projet.base.ws.offense.infra;

import static com.google.common.truth.Truth.*;
import static com.google.common.truth.Truth8.*;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.Offense;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OffenseInMemoryRepositoryTest {

    private static final Price A_PRICE = Price.of(6);
    private static final OffenseId AN_OFFENSE_ID = new OffenseId(4);
    private static final OffenseCode AN_OFFENSE_CODE = OffenseCode.VIG_01;
    private static final String AN_OFFENSE = "offense";

    private OffenseInMemoryRepository repository;

    @BeforeEach
    void setUp() {
        repository = new OffenseInMemoryRepository();
    }

    @Test
    void givenAssociatedOffenseInMemory_whenFindByOffenseId_thenReturnOffense() {
        // given
        Offense offense = givenAnOffense(AN_OFFENSE_ID);
        repository.save(offense);

        // when
        Optional<Offense> optionalOffense = repository.findByOffenseId(AN_OFFENSE_ID);

        // then
        assertThat(optionalOffense.get()).isEqualTo(offense);
    }

    @Test
    void givenNoAssociatedOffenseInMemory_whenFindByOffenseId_thenReturnEmptyOptional() {
        // given

        Optional<Offense> optionalOffense = repository.findByOffenseId(AN_OFFENSE_ID);

        // then
        assertThat(optionalOffense).isEmpty();
    }

    private Offense givenAnOffense(OffenseId offenseId) {
        return new Offense(offenseId, AN_OFFENSE_CODE, AN_OFFENSE, A_PRICE);
    }
}
