package ca.ulaval.glo4003.projet.base.ws.usage.domain.parking;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;

class ParkingEntryTest {

    private static final LocalDate A_PARKING_DATE = LocalDate.of(2020, 1, 1);
    private static final ParkingZone A_ZONE = ParkingZone.ZONE_R;
    private static final LocalDate A_DATE_BEFORE_PARKING_DATE = LocalDate.of(2019, 1, 1);
    private static final LocalDate A_DATE_AFTER_PARKING_DATE = LocalDate.of(2021, 1, 1);

    @Test
    void givenParkingEntry_whenIsInPeriodOfEnclosingDates_thenEqualsTrue() {
        // given
        ParkingEntry entry = new ParkingEntry(A_PARKING_DATE, A_ZONE);

        // when
        boolean isInPeriod = entry.isInPeriod(A_DATE_BEFORE_PARKING_DATE, A_DATE_AFTER_PARKING_DATE);

        // then
        assertThat(isInPeriod).isTrue();
    }

    @Test
    void givenParkingEntry_whenIsInPeriodOfParkingDateAndFutureDate_thenEqualsTrue() {
        // given
        ParkingEntry entry = new ParkingEntry(A_PARKING_DATE, A_ZONE);

        // when
        boolean isInPeriod = entry.isInPeriod(A_PARKING_DATE, A_DATE_AFTER_PARKING_DATE);

        // then
        assertThat(isInPeriod).isTrue();
    }

    @Test
    void givenParkingEntry_whenIsInPeriodOfPastDateAndParkingDate_thenEqualsTrue() {
        // given
        ParkingEntry entry = new ParkingEntry(A_PARKING_DATE, A_ZONE);

        // when
        boolean isInPeriod = entry.isInPeriod(A_DATE_BEFORE_PARKING_DATE, A_PARKING_DATE);

        // then
        assertThat(isInPeriod).isTrue();
    }
}
