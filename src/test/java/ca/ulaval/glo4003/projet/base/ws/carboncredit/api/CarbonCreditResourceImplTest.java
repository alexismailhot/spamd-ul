package ca.ulaval.glo4003.projet.base.ws.carboncredit.api;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.service.CarbonCreditService;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CarbonCreditResourceImplTest {

    private static final double AN_AMOUNT_OF_FUNDED_CARBON_CREDITS = 420;
    private CarbonCreditResource carbonCreditResource;

    @Mock
    private CarbonCreditService carbonCreditService;

    @Mock
    private InitiativeService initiativeService;

    @BeforeEach
    void setUp() {
        carbonCreditResource = new CarbonCreditResourceImpl(carbonCreditService, initiativeService);
    }

    @Test
    void whenActivateAutomaticCarbonCreditSpending_thenCarbonCreditServiceIsCalled() {
        // when
        Response response = carbonCreditResource.activateAutomaticCarbonCreditSpending();

        // then
        verify(carbonCreditService).activateAutomaticCarbonCreditSpending();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void whenDeactivateAutomaticCarbonCreditSpending_thenCarbonCreditServiceIsCalled() {
        // when
        Response response = carbonCreditResource.deactivateAutomaticCarbonCreditSpending();

        // then
        verify(carbonCreditService).deactivateAutomaticCarbonCreditSpending();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void whenGetNumberOfCarbonCreditsBought_thenCarbonCreditServiceIsCalled() {
        // when
        carbonCreditResource.getNumberOfCarbonCreditsBought();

        //then
        verify(initiativeService).getNumberOfCarbonCreditsBought();
    }

    @Test
    void whenGetNumberOfCarbonCreditsBought_thenResponseStatusCodeIsOK() {
        // when
        Response response = carbonCreditResource.getNumberOfCarbonCreditsBought();

        //then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void givenFundedCarbonCredits_whenGetNumberOfCarbonCreditsBought_thenNumberOfFundedCarbonCreditsIsReturned() {
        // given
        given(initiativeService.getNumberOfCarbonCreditsBought()).willReturn(AN_AMOUNT_OF_FUNDED_CARBON_CREDITS);

        // when
        Response response = carbonCreditResource.getNumberOfCarbonCreditsBought();
        double numberOfFundedCarbonCredits = (double) response.getEntity();

        //then
        assertThat(numberOfFundedCarbonCredits).isEqualTo(AN_AMOUNT_OF_FUNDED_CARBON_CREDITS);
    }
}
