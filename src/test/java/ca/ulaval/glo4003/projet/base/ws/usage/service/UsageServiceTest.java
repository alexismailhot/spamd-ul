package ca.ulaval.glo4003.projet.base.ws.usage.service;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntry;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntryRepository;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UsageServiceTest {

    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final LocalDate A_PARKING_DATE = LocalDate.of(2020, 1, 1);
    private static final LocalDate ANOTHER_PARKING_DATE = LocalDate.of(2020, 1, 2);
    private static final ParkingEntry A_PARKING_ENTRY = new ParkingEntry(A_PARKING_DATE, A_PARKING_ZONE);

    private UsageService usageService;

    @Mock
    private ParkingEntryRepository parkingEntryRepository;

    @BeforeEach
    void setUp() {
        usageService = new UsageService(parkingEntryRepository);
    }

    @Test
    void whenGenerateZoneUsageSummary_thenSummaryIsGeneratedForZone() {
        // given
        given(parkingEntryRepository.getByZoneInPeriod(A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE)).willReturn(Collections.emptyList());

        // when
        ParkingZoneUsageSummary summary = usageService.generateZoneUsageSummary(A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // then
        assertThat(summary.getZone()).isEqualTo(A_PARKING_ZONE);
    }

    @Test
    void whenGenerateAllZonesUsageSummary_thenSummaryIsGeneratedForAllZones() {
        // given
        given(parkingEntryRepository.getByZoneInPeriod(A_PARKING_ZONE, A_PARKING_DATE, ANOTHER_PARKING_DATE)).willReturn(Collections.emptyList());

        // when
        List<ParkingZoneUsageSummary> summaries = usageService.generateAllZonesUsageSummary(A_PARKING_DATE, ANOTHER_PARKING_DATE);

        // then
        for (ParkingZone zone : ParkingZone.values()) {
            assertThat(summaries.stream().filter(summary -> summary.getZone().equals(zone)).count()).isEqualTo(1);
        }
    }

    @Test
    void whenRecordUsage_thenSaveNewParkingEntry() {
        // given

        // when
        usageService.recordUsage(A_PARKING_ENTRY);

        // then
        verify(parkingEntryRepository).save(A_PARKING_ENTRY);
    }
}
