package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import static com.google.common.truth.Truth.*;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeliveryMethodInformationValidatorFactoryTest {

    private static final DeliveryMethod SSP_DELIVERY_METHOD = DeliveryMethod.SSP;
    private static final DeliveryMethod EMAIL_DELIVERY_METHOD = DeliveryMethod.EMAIL;
    private static final DeliveryMethod POSTAL_DELIVERY_METHOD = DeliveryMethod.POSTAL;

    private DeliveryMethodInformationValidatorFactory deliveryMethodInformationValidatorFactory;

    @BeforeEach
    void setUp() {
        deliveryMethodInformationValidatorFactory = new DeliveryMethodInformationValidatorFactory();
    }

    @Test
    void givenDeliveryMethodIsEmail_whenCreate_thenReturnEmailInformationValidator() {
        // given

        // when
        DeliveryMethodInformationValidator validator = deliveryMethodInformationValidatorFactory.create(EMAIL_DELIVERY_METHOD);

        // then
        assertThat(validator).isInstanceOf(EmailValidator.class);
    }

    @Test
    void givenDeliveryMethodIsPostal_whenCreate_thenReturnPostalCodeInformationValidator() {
        // given

        // when
        DeliveryMethodInformationValidator validator = deliveryMethodInformationValidatorFactory.create(POSTAL_DELIVERY_METHOD);

        // then
        assertThat(validator).isInstanceOf(PostalCodeValidator.class);
    }

    @Test
    void givenDeliveryMethodIsSSP_whenCreate_thenReturnNoopDeliveryMethodInformationValidator() {
        // given

        // when
        DeliveryMethodInformationValidator validator = deliveryMethodInformationValidatorFactory.create(SSP_DELIVERY_METHOD);

        // then
        assertThat(validator).isInstanceOf(NoOpDeliveryMethodInformationValidator.class);
    }
}
