package ca.ulaval.glo4003.projet.base.ws.carboncredit.service;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CarbonCreditServiceTest {

    private CarbonCreditService carbonCreditService;

    @Mock
    AutomaticJob automaticJob;

    @BeforeEach
    void setup() {
        carbonCreditService = new CarbonCreditService(automaticJob);
    }

    @Test
    void whenActivateAutomaticCarbonCreditSpending_thenStartCarbonCreditBuyingScheduler() {
        // when
        carbonCreditService.activateAutomaticCarbonCreditSpending();

        // then
        verify(automaticJob).activate();
    }

    @Test
    void whenDeactivateAutomaticCarbonCreditSpending_thenStopCarbonCreditBuyingScheduler() {
        // when
        carbonCreditService.deactivateAutomaticCarbonCreditSpending();

        // then
        verify(automaticJob).deactivate();
    }
}
