package ca.ulaval.glo4003.projet.base.ws.initiative.api;

import static com.google.common.truth.Truth.*;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.CarbonCreditConfig;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeCreationRequest;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeFundingRequest;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.DetailedInitiativeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.InitiativeCodeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.InitiativeFundsResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.BriefInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.DetailedInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeFundsResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeFactory;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingAmountValidator;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingConfig;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundsCalculator;
import ca.ulaval.glo4003.projet.base.ws.initiative.infra.InitiativeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InitiativeResourceIT {

    private static final Price AN_INCOME = Price.of(200);
    private static final Price SOME_ATTRIBUTED_FUNDS = Price.of(10);
    private static final BigDecimal A_PERCENTAGE_OF_FUNDS_ALLOCATED = BigDecimal.valueOf(0.40);
    private static final InitiativeCode AN_INITIATIVE_CODE = new InitiativeCode("A_CODE");
    private static final String AN_INITIATIVE_NAME = "Initiative ULAVAL";
    private static final Initiative AN_EXISTING_INITIATIVE = new Initiative(AN_INITIATIVE_NAME, SOME_ATTRIBUTED_FUNDS, AN_INITIATIVE_CODE);
    private static final double AN_INITIATIVE_REQUEST_AMOUNT = 40;

    private InitiativeResource initiativeResource;

    private InitiativeRepository initiativeRepository;

    @Mock
    private InitiativeFundingConfig initiativeFundingConfig;

    @Mock
    private IncomeService incomeService;

    @Mock
    CarbonCreditConfig carbonCreditConfig;

    @BeforeEach
    void setUp() {
        initiativeRepository = new InitiativeInMemoryRepository();
        initiativeResource =
            new InitiativeResourceImpl(
                new InitiativeService(
                    new InitiativeFactory(),
                    new InitiativeFundingAmountValidator(),
                    initiativeRepository,
                    new InitiativeFundsCalculator(initiativeFundingConfig),
                    incomeService,
                    carbonCreditConfig
                ),
                new InitiativeCodeResponseAssembler(),
                new BriefInitiativeResponseAssembler(),
                new DetailedInitiativeResponseAssembler(),
                new InitiativeFundsResponseAssembler()
            );

        given(initiativeFundingConfig.getPercentageOfFundsAllocated()).willReturn(A_PERCENTAGE_OF_FUNDS_ALLOCATED.doubleValue());
    }

    @Test
    void givenFundsAvailable_whenCreateInitiative_thenSaveInitiative() {
        //given
        givenAvailableFunds();

        //when
        Response createInitiativeResponse = initiativeResource.createInitiative(givenAnInitiativeCreationRequest());
        InitiativeCodeResponse initiativeCodeResponse = (InitiativeCodeResponse) createInitiativeResponse.getEntity();

        //then
        Response getInitiativeResponse = initiativeResource.getInitiative(initiativeCodeResponse.getInitiativeCode());
        DetailedInitiativeResponse detailedInitiativeResponse = (DetailedInitiativeResponse) getInitiativeResponse.getEntity();

        assertThat(detailedInitiativeResponse.getAttributedFunds())
            .isEquivalentAccordingToCompareTo(formatAsCapitalAmount(BigDecimal.valueOf(AN_INITIATIVE_REQUEST_AMOUNT)));
        assertThat(detailedInitiativeResponse.getName()).isEqualTo(AN_INITIATIVE_NAME);
    }

    @Test
    void givenAvailableFunds_whenFundInitiative_thenAddFundsToInitiative() {
        //given
        givenAvailableFunds();
        givenAnExistingInitiative();

        //when
        initiativeResource.fundInitiative(AN_INITIATIVE_CODE.getCode(), givenAnInitiativeFinancingRequest());

        //then
        Response getInitiativeResponse = initiativeResource.getInitiative(AN_INITIATIVE_CODE.getCode());
        DetailedInitiativeResponse detailedInitiativeResponse = (DetailedInitiativeResponse) getInitiativeResponse.getEntity();
        assertThat(detailedInitiativeResponse.getAttributedFunds())
            .isEquivalentAccordingToCompareTo(SOME_ATTRIBUTED_FUNDS.add(Price.of(AN_INITIATIVE_REQUEST_AMOUNT)).getAmount());
    }

    @Test
    void givenOffenseIncome_whenGetAllInitiativeFundsFromOffenseIncome_thenReturnAllInitiativeFundsFromOffenseIncome() {
        //given
        given(incomeService.getTotalOffenseIncome()).willReturn(AN_INCOME);

        //when
        Response getAllInitiativeFundsFromOffenseIncomeResponse = initiativeResource.getAllInitiativeFundsFromOffenseIncome();
        InitiativeFundsResponse initiativeFundsResponse = (InitiativeFundsResponse) getAllInitiativeFundsFromOffenseIncomeResponse.getEntity();

        //then
        assertThat(initiativeFundsResponse.getFunds())
            .isEqualTo(AN_INCOME.getAmount().multiply(A_PERCENTAGE_OF_FUNDS_ALLOCATED).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    void givenOffenseIncomeForCurrentUniversityYear_whenGetInitiativeFundsFromOffenseIncomeForCurrentUniversityYear_thenReturnInitiativeFundsFromOffenseIncomeForCurrentUniversityYear() {
        //given
        given(incomeService.getTotalOffenseIncomeForCurrentUniversityYear()).willReturn(AN_INCOME);

        //when
        Response getAllInitiativeFundsFromOffenseIncomeForCurrentUniversityYearResponse = initiativeResource.getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear();
        InitiativeFundsResponse initiativeFundsResponse = (InitiativeFundsResponse) getAllInitiativeFundsFromOffenseIncomeForCurrentUniversityYearResponse.getEntity();

        //then
        assertThat(initiativeFundsResponse.getFunds())
            .isEqualTo(AN_INCOME.getAmount().multiply(A_PERCENTAGE_OF_FUNDS_ALLOCATED).setScale(2, RoundingMode.HALF_UP));
    }

    private InitiativeCreationRequest givenAnInitiativeCreationRequest() {
        return new InitiativeCreationRequest(AN_INITIATIVE_NAME, AN_INITIATIVE_REQUEST_AMOUNT);
    }

    private void givenAvailableFunds() {
        given(incomeService.getTotalIncome()).willReturn(AN_INCOME);
    }

    private void givenAnExistingInitiative() {
        initiativeRepository.save(AN_EXISTING_INITIATIVE);
    }

    private InitiativeFundingRequest givenAnInitiativeFinancingRequest() {
        return new InitiativeFundingRequest(AN_INITIATIVE_REQUEST_AMOUNT);
    }

    private BigDecimal formatAsCapitalAmount(BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_UP);
    }
}
