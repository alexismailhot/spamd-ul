package ca.ulaval.glo4003.projet.base.ws.access.api;

import static com.google.common.truth.Truth.*;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.access.api.request.*;
import ca.ulaval.glo4003.projet.base.ws.access.api.response.AccessCodeResponse;
import ca.ulaval.glo4003.projet.base.ws.access.api.response.AccessCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.SessionAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.WeeklyAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Sex;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.access.service.AccessService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionAssembler;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccessResourceImplTest {

    private static final String A_NAME = "name";
    private static final Sex A_SEX = Sex.FEMALE;
    private static final String A_VEHICLE_MODEL = "vehicle model";
    private static final String A_VEHICLE_BRAND = "vehicle brand";
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final LocalDate A_BIRTHDATE = LocalDate.of(4, 5, 5);
    private static final DayOfWeek AN_ACCESS_DAY = DayOfWeek.MONDAY;
    private static final String A_VEHICLE_PLATE_NUMBER = "vehicle plate number";
    private static final int A_VEHICLE_MODEL_YEAR = 7;
    private static final int A_CODE_NUMBER = 3;
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.now();

    private AccessResource accessResource;

    @Mock
    private AccessService accessService;

    @BeforeEach
    void setUp() {
        VehicleAssembler vehicleAssembler = new VehicleAssembler();
        DriverAssembler driverAssembler = new DriverAssembler();
        SessionAssembler sessionAssembler = new SessionAssembler();

        accessResource =
            new AccessResourceImpl(
                accessService,
                new AccessCodeResponseAssembler(),
                new SessionAccessInformationAssembler(vehicleAssembler, driverAssembler, sessionAssembler),
                new WeeklyAccessInformationAssembler(vehicleAssembler, driverAssembler, sessionAssembler),
                new CreditCardAssembler()
            );
    }

    @Test
    void givenAnAccessRequest_whenCreateWeeklyAccess_thenReturnAccessCodeResponse() {
        // given
        CreditCardRequest creditCardRequest = givenACreditCardRequest();
        WeeklyAccessRequest weeklyAccessRequest = givenAWeeklyAccessRequest(creditCardRequest);
        CreditCard creditCard = givenACreditCard();
        given(accessService.createWeeklyAccess(givenAWeeklyAccessInformation(), creditCard)).willReturn(new AccessCode(A_CODE_NUMBER));

        // when
        Response response = accessResource.createWeeklyAccess(weeklyAccessRequest);

        // then
        AccessCodeResponse accessCodeResponse = (AccessCodeResponse) response.getEntity();
        assertThat(accessCodeResponse.getAccessCode()).isEqualTo(A_CODE_NUMBER);
    }

    @Test
    void whenCreateWeeklyAccess_thenReturn201Created() {
        // given
        CreditCardRequest creditCardRequest = givenACreditCardRequest();
        WeeklyAccessRequest weeklyAccessRequest = givenAWeeklyAccessRequest(creditCardRequest);
        CreditCard creditCard = givenACreditCard();
        given(accessService.createWeeklyAccess(givenAWeeklyAccessInformation(), creditCard)).willReturn(new AccessCode(A_CODE_NUMBER));

        // when
        Response response = accessResource.createWeeklyAccess(weeklyAccessRequest);

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
    }

    @Test
    void givenAnAccessRequest_whenCreateSessionAccess_thenReturnAccessCodeResponse() {
        // given
        CreditCardRequest creditCardRequest = givenACreditCardRequest();
        SessionAccessRequest sessionAccessRequest = givenASessionAccessRequest(creditCardRequest);
        CreditCard creditCard = givenACreditCard();
        given(accessService.createSessionAccess(givenASessionAccessInformation(), creditCard)).willReturn(new AccessCode(A_CODE_NUMBER));

        // when
        Response response = accessResource.createSessionAccess(sessionAccessRequest);

        // then
        AccessCodeResponse accessCodeResponse = (AccessCodeResponse) response.getEntity();
        assertThat(accessCodeResponse.getAccessCode()).isEqualTo(A_CODE_NUMBER);
    }

    @Test
    void whenCreateSessionAccess_thenReturn201Created() {
        // given
        CreditCardRequest creditCardRequest = givenACreditCardRequest();
        SessionAccessRequest sessionAccessRequest = givenASessionAccessRequest(creditCardRequest);
        CreditCard creditCard = givenACreditCard();
        given(accessService.createSessionAccess(givenASessionAccessInformation(), creditCard)).willReturn(new AccessCode(A_CODE_NUMBER));

        // when
        Response response = accessResource.createSessionAccess(sessionAccessRequest);

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
    }

    @Test
    void whenGetAccess_thenAccessServiceIsCalledToGrantAccess() {
        // given

        // when
        accessResource.getAccess(A_CODE_NUMBER);

        // then
        AccessCode expectedAccessCode = new AccessCode(A_CODE_NUMBER);
        verify(accessService).grantAccess(expectedAccessCode);
    }

    @Test
    void whenGetAccess_thenReturn200Ok() {
        // given

        // when
        Response response = accessResource.getAccess(A_CODE_NUMBER);

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    private CreditCardRequest givenACreditCardRequest() {
        return new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    }

    private WeeklyAccessRequest givenAWeeklyAccessRequest(CreditCardRequest creditCardRequest) {
        DriverRequest driverRequest = new DriverRequest(A_NAME, A_BIRTHDATE, A_SEX);
        VehicleRequest vehicleRequest = new VehicleRequest(A_VEHICLE_BRAND, A_VEHICLE_MODEL, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE);
        SessionRequest sessionRequest = new SessionRequest(A_SESSION_TYPE, A_YEAR);
        return new WeeklyAccessRequest(driverRequest, vehicleRequest, AN_ACCESS_DAY, sessionRequest, creditCardRequest);
    }

    private SessionAccessRequest givenASessionAccessRequest(CreditCardRequest creditCardRequest) {
        DriverRequest driverRequest = new DriverRequest(A_NAME, A_BIRTHDATE, A_SEX);
        VehicleRequest vehicleRequest = new VehicleRequest(A_VEHICLE_BRAND, A_VEHICLE_MODEL, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE);
        SessionRequest sessionRequest = new SessionRequest(A_SESSION_TYPE, A_YEAR);
        return new SessionAccessRequest(driverRequest, vehicleRequest, sessionRequest, creditCardRequest);
    }

    private CreditCard givenACreditCard() {
        return new CreditCard(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    }

    private WeeklyAccessInformation givenAWeeklyAccessInformation() {
        return new WeeklyAccessInformation(
            new Driver(A_NAME, A_BIRTHDATE, A_SEX),
            new Vehicle(A_VEHICLE_MODEL, A_VEHICLE_BRAND, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE),
            AN_ACCESS_DAY,
            new Session(A_SESSION_TYPE, A_YEAR)
        );
    }

    private SessionAccessInformation givenASessionAccessInformation() {
        return new SessionAccessInformation(
            new Driver(A_NAME, A_BIRTHDATE, A_SEX),
            new Vehicle(A_VEHICLE_MODEL, A_VEHICLE_BRAND, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE),
            new Session(A_SESSION_TYPE, A_YEAR)
        );
    }
}
