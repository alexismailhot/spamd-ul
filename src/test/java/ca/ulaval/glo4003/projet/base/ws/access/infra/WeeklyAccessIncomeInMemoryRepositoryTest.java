package ca.ulaval.glo4003.projet.base.ws.access.infra;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class WeeklyAccessIncomeInMemoryRepositoryTest {

    private static final Price A_PRICE = Price.of(4);
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final VehicleType ANOTHER_VEHICLE_TYPE = VehicleType.GREEDY;
    private static final LocalDateTime A_LOCAL_DATE_TIME = LocalDateTime.of(4, 4, 4, 4, 5);
    private static final LocalDateTime ANOTHER_LOCAL_DATE_TIME = LocalDateTime.of(433, 4, 4, 4, 5);

    private AccessIncomeRepository accessIncomeRepository;

    @Mock
    private UniversityYear universityYear;

    @BeforeEach
    void setUp() {
        accessIncomeRepository = new AccessIncomeInMemoryRepository();
    }

    @Test
    void givenAccessIncomes_whenFindByVehicleType_thenIncomesAssociatedWithVehicleTypeAreReturned() {
        // given
        AccessIncome anAccessIncome = new AccessIncome(A_PRICE, A_VEHICLE_TYPE, A_LOCAL_DATE_TIME);
        AccessIncome anotherAccessIncome = new AccessIncome(A_PRICE, ANOTHER_VEHICLE_TYPE, A_LOCAL_DATE_TIME);
        accessIncomeRepository.save(anAccessIncome);
        accessIncomeRepository.save(anotherAccessIncome);

        // when
        List<AccessIncome> incomes = accessIncomeRepository.findByVehicleType(VehicleType.ECONOMIC);

        // then
        assertThat(incomes).contains(anAccessIncome);
        assertThat(incomes).doesNotContain(anotherAccessIncome);
    }

    @Test
    void givenAccessIncomes_whenFindByUniversityYear_thenIncomesOfGivenUniversityYearAreReturned() {
        // given
        doReturn(true).when(universityYear).includes(A_LOCAL_DATE_TIME.toLocalDate());
        doReturn(false).when(universityYear).includes(ANOTHER_LOCAL_DATE_TIME.toLocalDate());

        AccessIncome accessIncome = new AccessIncome(A_PRICE, A_VEHICLE_TYPE, A_LOCAL_DATE_TIME);
        AccessIncome anotherAccessIncome = new AccessIncome(A_PRICE, A_VEHICLE_TYPE, ANOTHER_LOCAL_DATE_TIME);
        accessIncomeRepository.save(accessIncome);
        accessIncomeRepository.save(anotherAccessIncome);

        // when
        List<AccessIncome> incomes = accessIncomeRepository.findByUniversityYear(universityYear);

        // then
        assertThat(incomes).contains(accessIncome);
        assertThat(incomes).doesNotContain(anotherAccessIncome);
    }

    @Test
    void givenAccessIncomesSaved_whenGetAll_theReturnAllAccessIncomes() {
        // given
        AccessIncome accessIncome = new AccessIncome(Price.of(200), A_VEHICLE_TYPE, LocalDateTime.now());
        AccessIncome otherAccessIncome = new AccessIncome(Price.of(500), ANOTHER_VEHICLE_TYPE, LocalDateTime.now());
        accessIncomeRepository.save(accessIncome);
        accessIncomeRepository.save(otherAccessIncome);

        // when
        List<AccessIncome> incomes = accessIncomeRepository.getAll();

        // then
        assertThat(incomes).isEqualTo(Arrays.asList(accessIncome, otherAccessIncome));
    }
}
