package ca.ulaval.glo4003.projet.base.ws.access.domain;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccessPriceCalculatorTest {

    private final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private final AccessTimeType AN_ACCESS_TIME_TYPE = AccessTimeType.ONE_DAY_A_WEEK_FOR_ONE_SEMESTER;
    private final Price A_RATE = Price.of(10);
    private final Map<VehicleType, Map<AccessTimeType, Price>> SOME_ACCESS_RATES = new HashMap<>();

    @Mock
    private AccessRatesConfig accessRatesConfig;

    private AccessPriceCalculator accessPriceCalculator;

    @BeforeEach
    void setUp() {
        accessPriceCalculator = new AccessPriceCalculator(accessRatesConfig);
    }

    @Test
    void givenAccessRates_whenCalculatePrice_thenCalculateCorrectPrice() {
        //given
        givenAccessRates();
        given(accessRatesConfig.getAccessRates()).willReturn(SOME_ACCESS_RATES);

        //when
        Price returnedPrice = accessPriceCalculator.calculatePrice(A_VEHICLE_TYPE, AN_ACCESS_TIME_TYPE);

        //then
        assertThat(returnedPrice).isEqualTo(A_RATE);
    }

    private void givenAccessRates() {
        Map<AccessTimeType, Price> RATE_BY_ACCESS_TIME_TYPE = Collections.singletonMap(AN_ACCESS_TIME_TYPE, A_RATE);
        SOME_ACCESS_RATES.put(A_VEHICLE_TYPE, RATE_BY_ACCESS_TIME_TYPE);
    }
}
