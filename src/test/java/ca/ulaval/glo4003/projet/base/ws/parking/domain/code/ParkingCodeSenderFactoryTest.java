package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.email.domain.EmailSender;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingCodeSenderFactoryTest {

    private static final int A_PARKING_CODE_VALUE = 420;
    private static final ParkingCode A_PARKING_CODE = new ParkingCode(A_PARKING_CODE_VALUE);
    private static final String AN_ADDRESS = "123 Mackay";
    private static final String AN_APARTMENT_NUMBER = "4";
    private static final String A_STATE = "Quebec";
    private static final String A_COUNTRY = "Canada";
    private static final String A_CITY = "Quebec";
    private static final String A_POSTAL_CODE = "G1V2X1";
    private static final PostalAddress A_POSTAL_ADDRESS = new PostalAddress(AN_ADDRESS, AN_APARTMENT_NUMBER, A_STATE, A_COUNTRY, A_CITY, A_POSTAL_CODE);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_2;
    private static final DayOfWeek A_PARKING_DAY = DayOfWeek.MONDAY;
    private static final String AN_EMAIL = "an email";

    private ParkingCodeSenderFactory parkingCodeSenderFactory;

    @Mock
    private EmailSender emailSender;

    @Mock
    private ParkingCodeMailingFactory parkingCodeMailingFactory;

    @Mock
    private ParkingCodeMailingRepository parkingCodeMailingRepository;

    @Mock
    private ParkingCodeSSPDeliveryInformationRepository parkingCodeSSPDeliveryInformationRepository;

    @Mock
    private ParkingCodeSSPDeliveryInformationFactory parkingCodeSSPDeliveryInformationFactory;

    @BeforeEach
    void setUp() {
        parkingCodeSenderFactory =
            new ParkingCodeSenderFactory(
                emailSender,
                parkingCodeMailingRepository,
                parkingCodeMailingFactory,
                parkingCodeSSPDeliveryInformationRepository,
                parkingCodeSSPDeliveryInformationFactory
            );
    }

    @Test
    void givenDeliveryMethodIsEmail_whenCreate_thenReturnAParkingCodeEmailSender() {
        // given
        ParkingPassInformation parkingPassInformation = givenAParkingPassInformation(AN_EMAIL);

        // when
        ParkingCodeSender parkingCodeSender = parkingCodeSenderFactory.create(parkingPassInformation, A_PARKING_CODE);

        // then
        assertThat(parkingCodeSender).isInstanceOf(ParkingCodeEmailSender.class);
    }

    @Test
    void givenDeliveryMethodIsPostal_whenCreate_thenReturnAParkingCodePostalSender() {
        // given
        ParkingPassInformation parkingPassInformation = givenAParkingPassInformation(A_POSTAL_ADDRESS);

        // when
        ParkingCodeSender parkingCodeSender = parkingCodeSenderFactory.create(parkingPassInformation, A_PARKING_CODE);

        // then
        assertThat(parkingCodeSender).isInstanceOf(ParkingCodePostalSender.class);
    }

    @Test
    void givenDeliveryMethodIsSSP_whenCreate_thenReturnAParkingCodeSSPSender() {
        // given
        ParkingPassInformation parkingPassInformation = givenAParkingPassInformation();

        // when
        ParkingCodeSender parkingCodeSender = parkingCodeSenderFactory.create(parkingPassInformation, A_PARKING_CODE);

        // then
        assertThat(parkingCodeSender).isInstanceOf(ParkingCodeSSPSender.class);
    }

    private ParkingPassInformation givenAParkingPassInformation() {
        return new WeeklyParkingPassInformation(A_PARKING_ZONE, DeliveryMethod.SSP, A_PARKING_DAY, new Session(SessionType.FALL, Year.of(23)));
    }

    private ParkingPassInformation givenAParkingPassInformation(String email) {
        return new WeeklyParkingPassInformation(A_PARKING_ZONE, DeliveryMethod.EMAIL, email, A_PARKING_DAY, new Session(SessionType.FALL, Year.of(23)));
    }

    private ParkingPassInformation givenAParkingPassInformation(PostalAddress postalAddress) {
        return new WeeklyParkingPassInformation(
            A_PARKING_ZONE,
            DeliveryMethod.POSTAL,
            A_PARKING_DAY,
            postalAddress,
            new Session(SessionType.SUMMER, Year.of(1223))
        );
    }
}
