package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Year;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith({MockitoExtension.class})
class SessionInformationValidatorFactoryTest {

    private static final ParkingZone A_ZONE = ParkingZone.ZONE_1;
    private static final DeliveryMethod A_DELIVERY_METHOD = DeliveryMethod.EMAIL;
    private static final Session A_SESSION = new Session(SessionType.FALL, Year.of(1));
    @Mock
    private Clock clock;

    private SessionInformationValidatorFactory sessionInformationValidatorFactory;

    @BeforeEach
    void setUp() {
        sessionInformationValidatorFactory = new SessionInformationValidatorFactory(clock);
    }

    @Test
    void givenSessionParkingPassInformation_whenCreate_thenSessionTimeRangeInformationValidatorReturned() {
        SessionParkingPassInformation sessionParkingPassInformation = new SessionParkingPassInformation(A_ZONE, A_DELIVERY_METHOD, A_SESSION);

        SessionInformationValidator parkingPassInformationValidator = sessionInformationValidatorFactory.create(sessionParkingPassInformation);

        assertThat(parkingPassInformationValidator).isInstanceOf(SessionValidator.class);
    }
}