package ca.ulaval.glo4003.projet.base.ws.carboncredit.infra;

import static org.mockito.BDDMockito.doThrow;
import static org.mockito.BDDMockito.verify;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.exception.QuartzJobManagerException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

@ExtendWith(MockitoExtension.class)
class QuartzAutomaticJobTest {

    private static final JobKey A_JOB_KEY = new JobKey("NAME");

    @Mock
    private Scheduler scheduler;

    private QuartzAutomaticJob quartzAutomaticJob;

    @BeforeEach
    void setUp() {
        quartzAutomaticJob = new QuartzAutomaticJob(scheduler, A_JOB_KEY);
    }

    @Test
    void whenActivate_thenJobIsResumed() throws SchedulerException {
        // when
        quartzAutomaticJob.activate();

        // then
        verify(scheduler).resumeJob(A_JOB_KEY);
    }

    @Test
    void whenDeactivate_thenJobIsPaused() throws SchedulerException {
        // when
        quartzAutomaticJob.deactivate();

        // then
        verify(scheduler).pauseJob(A_JOB_KEY);
    }

    @Test
    void givenSchedulerThrowsException_whenActivate_thenThrowsQuartzJobManagerException() throws SchedulerException {
        // given
        doThrow(SchedulerException.class).when(scheduler).resumeJob(A_JOB_KEY);

        // then
        Assertions.assertThrows(
            QuartzJobManagerException.class,
            () ->
                // when
                quartzAutomaticJob.activate()
        );
    }

    @Test
    void givenSchedulerThrowsException_whenDeactivate_thenThrowsQuartzJobManagerException() throws SchedulerException {
        // given
        doThrow(SchedulerException.class).when(scheduler).pauseJob(A_JOB_KEY);

        // then
        Assertions.assertThrows(
            QuartzJobManagerException.class,
            () ->
                // when
                quartzAutomaticJob.deactivate()
        );
    }
}
