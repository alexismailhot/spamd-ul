package ca.ulaval.glo4003.projet.base.ws.access.api;

import static com.google.common.truth.Truth.*;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.access.api.request.*;
import ca.ulaval.glo4003.projet.base.ws.access.api.response.AccessCodeResponse;
import ca.ulaval.glo4003.projet.base.ws.access.api.response.AccessCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.access.domain.*;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.VehicleYearValidator;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Sex;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.access.infra.AccessInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.access.infra.AccessIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.access.service.AccessService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.payment.infra.DumbExternalPaymentService;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionAssembler;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.*;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccessResourceIT {

    private static final String A_NAME = "name";
    private static final Sex A_SEX = Sex.FEMALE;
    private static final String A_VEHICLE_MODEL = "vehicle model";
    private static final String A_VEHICLE_BRAND = "vehicle brand";
    private static final LocalDate A_BIRTHDATE = LocalDate.of(4, 5, 5);
    private static final Instant AN_INSTANT = Instant.ofEpochSecond(1606836629);
    private static final DayOfWeek AN_ACCESS_DAY = AN_INSTANT.atZone(ZoneId.systemDefault()).getDayOfWeek();
    private static final String A_VEHICLE_PLATE_NUMBER = "vehicle plate number";
    private static final int A_VEHICLE_MODEL_YEAR = 1400;
    private static final int MAXIMUM_VEHICLE_YEAR = 2000;
    private static final int MINIMUM_VEHICLE_YEAR = 1000;
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final AccessTimeType WEEKLY_ACCESS_TIME_TYPE = AccessTimeType.ONE_DAY_A_WEEK_FOR_ONE_SEMESTER;
    private static final AccessTimeType SESSION_ACCESS_TIME_TYPE = AccessTimeType.ONE_SEMESTER;
    private static final Price A_PRICE = Price.of(5);
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.of(2020);

    private AccessResource accessResource;

    @Mock
    private Clock clock;

    @Mock
    private AccessRatesConfig accessRatesConfig;

    @BeforeEach
    void setUp() {
        VehicleAssembler vehicleAssembler = new VehicleAssembler();
        DriverAssembler driverAssembler = new DriverAssembler();
        SessionAssembler sessionAssembler = new SessionAssembler();

        WeeklyAccessInformationAssembler weeklyAccessInformationAssembler = new WeeklyAccessInformationAssembler(
            vehicleAssembler,
            driverAssembler,
            sessionAssembler
        );
        SessionAccessInformationAssembler sessionAccessInformationAssembler = new SessionAccessInformationAssembler(
            vehicleAssembler,
            driverAssembler,
            sessionAssembler
        );
        AccessInMemoryRepository accessRepository = new AccessInMemoryRepository();
        AccessService accessService = new AccessService(
            new WeeklyAccessFactory(accessRepository),
            new SessionAccessFactory(accessRepository),
            new AccessIncomeFactory(clock),
            new AccessIncomeInMemoryRepository(),
            new VehicleYearValidator(null, MINIMUM_VEHICLE_YEAR, MAXIMUM_VEHICLE_YEAR),
            accessRepository,
            new AccessGrantingValidator(),
            new DumbExternalPaymentService(),
            new AccessPriceCalculator(accessRatesConfig),
            clock
        );
        accessResource =
            new AccessResourceImpl(
                accessService,
                new AccessCodeResponseAssembler(),
                sessionAccessInformationAssembler,
                weeklyAccessInformationAssembler,
                new CreditCardAssembler()
            );

        given(clock.instant()).willReturn(AN_INSTANT);
    }

    @Test
    void givenWeeklyAccessCreated_whenGetAccess_thenAccessIsGrantedOnAccessDay() {
        // given
        givenAccessRatesConfigReturnsAccessRates(WEEKLY_ACCESS_TIME_TYPE);
        Response createAccessResponse = accessResource.createWeeklyAccess(givenAWeeklyAccessRequest());
        AccessCodeResponse accessCodeResponse = (AccessCodeResponse) createAccessResponse.getEntity();

        // when
        Response getAccessResponse = accessResource.getAccess(accessCodeResponse.getAccessCode());

        // then
        assertThat(getAccessResponse.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void givenSessionAccessCreated_whenGetAccess_thenAccessIsGrantedInSession() {
        // given
        givenAccessRatesConfigReturnsAccessRates(SESSION_ACCESS_TIME_TYPE);
        Response createAccessResponse = accessResource.createSessionAccess(givenASessionAccessRequest());
        AccessCodeResponse accessCodeResponse = (AccessCodeResponse) createAccessResponse.getEntity();

        // when
        Response getAccessResponse = accessResource.getAccess(accessCodeResponse.getAccessCode());

        // then
        assertThat(getAccessResponse.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    private void givenAccessRatesConfigReturnsAccessRates(AccessTimeType accessTimeType) {
        Map<AccessTimeType, Price> priceByAccessTimeType = new HashMap<>();
        priceByAccessTimeType.put(accessTimeType, A_PRICE);
        Map<VehicleType, Map<AccessTimeType, Price>> accessRates = new HashMap<>();
        accessRates.put(A_VEHICLE_TYPE, priceByAccessTimeType);
        given(accessRatesConfig.getAccessRates()).willReturn(accessRates);
    }

    private WeeklyAccessRequest givenAWeeklyAccessRequest() {
        DriverRequest driverRequest = new DriverRequest(A_NAME, A_BIRTHDATE, A_SEX);
        VehicleRequest vehicleRequest = new VehicleRequest(A_VEHICLE_BRAND, A_VEHICLE_MODEL, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE);
        SessionRequest sessionRequest = new SessionRequest(A_SESSION_TYPE, A_YEAR);
        CreditCardRequest creditCardRequest = new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);

        return new WeeklyAccessRequest(driverRequest, vehicleRequest, AN_ACCESS_DAY, sessionRequest, creditCardRequest);
    }

    private SessionAccessRequest givenASessionAccessRequest() {
        DriverRequest driverRequest = new DriverRequest(A_NAME, A_BIRTHDATE, A_SEX);
        VehicleRequest vehicleRequest = new VehicleRequest(A_VEHICLE_BRAND, A_VEHICLE_MODEL, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE);
        SessionRequest sessionRequest = new SessionRequest(A_SESSION_TYPE, A_YEAR);
        CreditCardRequest creditCardRequest = new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);

        return new SessionAccessRequest(driverRequest, vehicleRequest, sessionRequest, creditCardRequest);
    }
}
