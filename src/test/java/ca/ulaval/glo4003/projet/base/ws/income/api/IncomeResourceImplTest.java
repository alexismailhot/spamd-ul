package ca.ulaval.glo4003.projet.base.ws.income.api;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.income.api.response.IncomeSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.income.api.response.assembler.IncomeSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummary;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IncomeResourceImplTest {

    private static final int A_NUMBER_OF_SALES = 420;
    private static final BigDecimal A_TOTAL_INCOME = BigDecimal.valueOf(123);
    private static final IncomeSummary AN_INCOME_SUMMARY = new IncomeSummary(A_NUMBER_OF_SALES, Price.of(A_TOTAL_INCOME.doubleValue()));
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;

    @Mock
    private IncomeService incomeService;

    private IncomeResource incomeResource;

    @BeforeEach
    void setUp() {
        incomeResource = new IncomeResourceImpl(incomeService, new IncomeSummaryResponseAssembler());
    }

    @Test
    void givenAVehicleType_whenGetAccessIncomeForCurrentUniversityYearForVehicleType_thenIncomeSummaryResponseIsReturned() {
        // given
        IncomeSummary incomeSummary = givenAnIncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        given(incomeService.getAccessIncomeSummaryForCurrentUniversityYearFor(A_VEHICLE_TYPE)).willReturn(incomeSummary);

        // when
        Response response = incomeResource.getAccessIncomeForCurrentUniversityYearForVehicleType(A_VEHICLE_TYPE.toString());

        // then
        assertThatResponseIsValid(response, A_NUMBER_OF_SALES, A_TOTAL_INCOME);
    }

    @Test
    void givenAVehicleType_whenGetAccessIncomeForCurrentUniversityYearForVehicleType_thenReturn200Ok() {
        // given
        given(incomeService.getAccessIncomeSummaryForCurrentUniversityYearFor(A_VEHICLE_TYPE)).willReturn(AN_INCOME_SUMMARY);

        // when
        Response response = incomeResource.getAccessIncomeForCurrentUniversityYearForVehicleType(A_VEHICLE_TYPE.toString());

        // then
        assertThatStatusCodeIs200Ok(response);
    }

    @Test
    void whenGetAccessIncomeSummaryForCurrentUniversityYear_thenIncomeSummaryResponseIsReturned() {
        // given
        IncomeSummary incomeSummary = givenAnIncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        given(incomeService.getAccessIncomeSummaryForCurrentUniversityYear()).willReturn(incomeSummary);

        // when
        Response response = incomeResource.getAccessIncomeSummaryForCurrentUniversityYear();

        // then
        assertThatResponseIsValid(response, A_NUMBER_OF_SALES, A_TOTAL_INCOME);
    }

    @Test
    void whenGetAccessIncomeSummaryForCurrentUniversityYear_thenReturn200Ok() {
        // given
        given(incomeService.getAccessIncomeSummaryForCurrentUniversityYear()).willReturn(AN_INCOME_SUMMARY);

        // when
        Response response = incomeResource.getAccessIncomeSummaryForCurrentUniversityYear();

        // then
        assertThatStatusCodeIs200Ok(response);
    }

    @Test
    void whenGetAllAccessIncomeForVehicleType_thenIncomeSummaryResponseIsReturned() {
        // given
        IncomeSummary incomeSummary = givenAnIncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        given(incomeService.getAccessIncomeSummaryFor(A_VEHICLE_TYPE)).willReturn(incomeSummary);

        // when
        Response response = incomeResource.getAllAccessIncomeForVehicleType(A_VEHICLE_TYPE);

        // then
        assertThatResponseIsValid(response, A_NUMBER_OF_SALES, A_TOTAL_INCOME);
    }

    @Test
    void whenGetAllAccessIncomeForVehicleType_thenReturn200Ok() {
        // given
        given(incomeService.getAccessIncomeSummaryFor(A_VEHICLE_TYPE)).willReturn(AN_INCOME_SUMMARY);

        // when
        Response response = incomeResource.getAllAccessIncomeForVehicleType(A_VEHICLE_TYPE);

        // then
        assertThatStatusCodeIs200Ok(response);
    }

    @Test
    void whenGetParkingIncomeSummaryForCurrentUniversityYear_thenIncomeSummaryResponseIsReturned() {
        // given
        IncomeSummary incomeSummary = givenAnIncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        given(incomeService.getParkingIncomeSummaryForCurrentUniversityYear()).willReturn(incomeSummary);

        // when
        Response response = incomeResource.getParkingPassIncomeSummaryForCurrentUniversityYear();

        // then
        assertThatResponseIsValid(response, A_NUMBER_OF_SALES, A_TOTAL_INCOME);
    }

    @Test
    void whenGetParkingIncomeSummaryForCurrentUniversityYear_thenReturn200Ok() {
        // given
        given(incomeService.getParkingIncomeSummaryForCurrentUniversityYear()).willReturn(AN_INCOME_SUMMARY);

        // when
        Response response = incomeResource.getParkingPassIncomeSummaryForCurrentUniversityYear();

        // then
        assertThatStatusCodeIs200Ok(response);
    }

    private void assertThatResponseIsValid(Response response, int expectedNumberOfSales, BigDecimal expectedTotalIncome) {
        IncomeSummaryResponse incomeSummaryResponse = (IncomeSummaryResponse) response.getEntity();
        assertThat(incomeSummaryResponse.getNumberOfSales()).isEqualTo(expectedNumberOfSales);
        assertThat(incomeSummaryResponse.getTotalIncome()).isEqualTo(formatAsCapitalAmount(expectedTotalIncome));
    }

    private void assertThatStatusCodeIs200Ok(Response response) {
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    private IncomeSummary givenAnIncomeSummary(int aNumberOfSales, BigDecimal totalIncome) {
        return new IncomeSummary(aNumberOfSales, Price.of(totalIncome.doubleValue()));
    }

    private BigDecimal formatAsCapitalAmount(BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_UP);
    }
}
