package ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InitiativeFundsCalculatorTest {

    private static final double PERCENTAGE_OF_FUNDS_ALLOCATED = 0.4;
    private static final String AN_INITIATIVE_NAME = "Initiative ULAVAL";
    private static final Price AN_INITIATIVE_FUNDS = Price.of(600);
    private static final InitiativeCode AN_INITIATIVE_CODE = new InitiativeCode("A_CODE");
    private static final String ANOTHER_INITIATIVE_NAME = "Another initiative";
    private static final Price ANOTHER_INITIATIVE_FUNDS = Price.of(400);
    private static final InitiativeCode ANOTHER_INITIATIVE_CODE = new InitiativeCode("ANOTHER_CODE");
    private static final Initiative AN_INITIATIVE = new Initiative(AN_INITIATIVE_NAME, AN_INITIATIVE_FUNDS, AN_INITIATIVE_CODE);
    private static final Initiative ANOTHER_INITIATIVE = new Initiative(ANOTHER_INITIATIVE_NAME, ANOTHER_INITIATIVE_FUNDS, ANOTHER_INITIATIVE_CODE);
    private static final Price AN_INCOME = Price.of(766);

    private InitiativeFundsCalculator initiativeFundCalculator;

    @Mock
    private InitiativeFundingConfig initiativeFundingConfig;

    @BeforeEach
    void setup() {
        initiativeFundCalculator = new InitiativeFundsCalculator(initiativeFundingConfig);
    }

    @Test
    void whenCalculateFundsAvailable_thenReturnedFundsAvailableAmountIsFundsAlreadyAllocatedSubtractedToAvailableFundsOfIncome() {
        // given
        given(initiativeFundingConfig.getPercentageOfFundsAllocated()).willReturn(PERCENTAGE_OF_FUNDS_ALLOCATED);

        // when
        Price availableFunds = initiativeFundCalculator.calculateFundsAvailable(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE), AN_INCOME);

        // then
        Price expectedAvailableFunds = AN_INCOME
            .multiplyBy(PERCENTAGE_OF_FUNDS_ALLOCATED)
            .subtractBy(AN_INITIATIVE.getAttributedFunds())
            .subtractBy(ANOTHER_INITIATIVE.getAttributedFunds());
        assertThat(availableFunds).isEqualTo(expectedAvailableFunds);
    }

    @Test
    public void givenAnIncome_whenCalculateFundsAllocatedToAllInitiatives_thenReturnAmountOfIncomeAllocated() {
        // given
        given(initiativeFundingConfig.getPercentageOfFundsAllocated()).willReturn(PERCENTAGE_OF_FUNDS_ALLOCATED);

        // when
        Price fundsAllocatedToInitiatives = initiativeFundCalculator.calculateFundsAllocatedToAllInitiatives(AN_INCOME);

        // then
        assertThat(fundsAllocatedToInitiatives).isEqualTo(AN_INCOME.multiplyBy(PERCENTAGE_OF_FUNDS_ALLOCATED));
    }
}
