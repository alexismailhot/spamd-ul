package ca.ulaval.glo4003.projet.base.ws.universityyear.domain;

import static com.google.common.truth.Truth.*;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UniversityYearProviderTest {

    private static final Month A_MONTH_AFTER_START_MONTH = Month.DECEMBER;
    private static final int A_YEAR = 2020;
    private static final LocalDate START_OF_A_NEW_UNIVERSITY_YEAR = LocalDate.of(A_YEAR, Month.SEPTEMBER, 1);
    private static final int A_DAY_OF_MONTH = 9;
    private static final Month A_MONTH_BEFORE_START_MONTH = Month.FEBRUARY;

    private UniversityYearProvider universityYearProvider;

    @BeforeEach
    void setUp() {
        universityYearProvider = new UniversityYearProvider();
    }

    @Test
    void givenDateMonthIsAfterUniversityStartMonth_whenProvide_thenUniversityStartYearIsCurrentYearAndEndYearIsNextYear() {
        // given

        // when
        UniversityYear universityYear = universityYearProvider.provide(LocalDate.of(A_YEAR, A_MONTH_AFTER_START_MONTH, A_DAY_OF_MONTH));

        // then
        Session expectedFallSession = new Session(SessionType.FALL, Year.of(A_YEAR));
        Session expectedWinterSession = new Session(SessionType.WINTER, Year.of(A_YEAR).plusYears(1));
        Session expectedSummerSession = new Session(SessionType.SUMMER, Year.of(A_YEAR).plusYears(1));

        UniversityYear expectedUniversityYear = new UniversityYear(expectedFallSession, expectedWinterSession, expectedSummerSession);
        assertThat(universityYear).isEqualTo(expectedUniversityYear);
    }

    @Test
    void givenDateMonthIsUniversityStartMonth_whenProvide_thenUniversityStartYearIsCurrentYearAndEndYearIsNextYear() {
        // given

        // when
        UniversityYear universityYear = universityYearProvider.provide(START_OF_A_NEW_UNIVERSITY_YEAR);

        // then
        Session expectedFallSession = new Session(SessionType.FALL, Year.of(A_YEAR));
        Session expectedWinterSession = new Session(SessionType.WINTER, Year.of(A_YEAR).plusYears(1));
        Session expectedSummerSession = new Session(SessionType.SUMMER, Year.of(A_YEAR).plusYears(1));

        UniversityYear expectedUniversityYear = new UniversityYear(expectedFallSession, expectedWinterSession, expectedSummerSession);
        assertThat(universityYear).isEqualTo(expectedUniversityYear);
    }

    @Test
    void givenDateMonthIsBeforeUniversityStartMonth_whenProvide_thenUniversityStartYearIsPreviousYearAndEndYearIsCurrentYear() {
        // given

        // when
        UniversityYear universityYear = universityYearProvider.provide(LocalDate.of(A_YEAR, A_MONTH_BEFORE_START_MONTH, A_DAY_OF_MONTH));

        // then
        Session expectedFallSession = new Session(SessionType.FALL, Year.of(A_YEAR).minusYears(1));
        Session expectedWinterSession = new Session(SessionType.WINTER, Year.of(A_YEAR));
        Session expectedSummerSession = new Session(SessionType.SUMMER, Year.of(A_YEAR));

        UniversityYear expectedUniversityYear = new UniversityYear(expectedFallSession, expectedWinterSession, expectedSummerSession);
        assertThat(universityYear).isEqualTo(expectedUniversityYear);
    }
}
