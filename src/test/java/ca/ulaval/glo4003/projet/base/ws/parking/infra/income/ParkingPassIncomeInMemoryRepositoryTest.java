package ca.ulaval.glo4003.projet.base.ws.parking.infra.income;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingPassIncomeInMemoryRepositoryTest {

    private static final LocalDateTime A_LOCAL_DATE_TIME = LocalDateTime.of(1, 1, 1, 1, 1, 1, 1);
    private static final LocalDateTime ANOTHER_LOCAL_DATE_TIME = LocalDateTime.of(100, 1, 1, 1, 1, 1, 1);

    private static final Price A_PRICE = Price.of(3);
    private static final ParkingPassIncome A_PARKING_PASS_INCOME = new ParkingPassIncome(Price.of(200), LocalDateTime.now());
    private static final ParkingPassIncome ANOTHER_PARKING_PASS_INCOME = new ParkingPassIncome(Price.of(400), LocalDateTime.now());

    private ParkingPassIncomeRepository parkingPassIncomeRepository;

    @Mock
    private UniversityYear universityYear;

    @BeforeEach
    void setUp() {
        parkingPassIncomeRepository = new ParkingPassIncomeInMemoryRepository();
    }

    @Test
    void givenParkingPassIncomes_whenFindByUniversityYear_thenIncomesOfGivenYearAreReturned() {
        // given
        doReturn(true).when(universityYear).includes(A_LOCAL_DATE_TIME.toLocalDate());
        doReturn(false).when(universityYear).includes(ANOTHER_LOCAL_DATE_TIME.toLocalDate());

        ParkingPassIncome income = new ParkingPassIncome(A_PRICE, A_LOCAL_DATE_TIME);
        ParkingPassIncome anotherIncome = new ParkingPassIncome(A_PRICE, ANOTHER_LOCAL_DATE_TIME);
        parkingPassIncomeRepository.save(income);
        parkingPassIncomeRepository.save(anotherIncome);

        // when
        List<ParkingPassIncome> incomes = parkingPassIncomeRepository.findByUniversityYear(universityYear);

        //then
        assertThat(incomes).contains(income);
        assertThat(incomes).doesNotContain(anotherIncome);
    }

    @Test
    void givenParkingPassIncomesSaved_whenGetAll_theReturnAllParkingPassIncomes() {
        // given
        parkingPassIncomeRepository.save(A_PARKING_PASS_INCOME);
        parkingPassIncomeRepository.save(ANOTHER_PARKING_PASS_INCOME);

        // when
        List<ParkingPassIncome> incomes = parkingPassIncomeRepository.getAll();

        // then
        assertThat(incomes).isEqualTo(Arrays.asList(A_PARKING_PASS_INCOME, ANOTHER_PARKING_PASS_INCOME));
    }
}
