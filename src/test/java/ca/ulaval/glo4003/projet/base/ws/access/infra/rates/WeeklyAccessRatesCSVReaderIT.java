package ca.ulaval.glo4003.projet.base.ws.access.infra.rates;

import static com.google.common.truth.Truth.*;

import ca.ulaval.glo4003.projet.base.ws.access.domain.AccessTimeType;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class WeeklyAccessRatesCSVReaderIT {

    private static final String ACCESS_FEES_CSV_PATH = "src/test/java/ca/ulaval/glo4003/projet/base/ws/access/infra/rates/frais-acces-test.csv";
    private static final String AN_INVALID_PATH = "not/a/valid/file";

    private AccessRatesCSVReader accessRatesCSVReader;

    @Test
    void givenFilePathExists_whenRead_thenReadParkingRatesFromCSV() throws FileNotFoundException {
        // given
        accessRatesCSVReader = new AccessRatesCSVReader(ACCESS_FEES_CSV_PATH);
        Map<VehicleType, Map<AccessTimeType, Price>> expectedAccessRates = createExpectedAccessRates();

        // when
        Map<VehicleType, Map<AccessTimeType, Price>> accessRates = accessRatesCSVReader.read();

        // then
        assertThat(accessRates).isEqualTo(expectedAccessRates);
    }

    @Test
    void givenFileDoesNotExist_whenRead_thenThrowFileNotFoundException() {
        // given
        accessRatesCSVReader = new AccessRatesCSVReader(AN_INVALID_PATH);

        // then
        Assertions.assertThrows(
            FileNotFoundException.class,
            () ->
                // when
                accessRatesCSVReader.read()
        );
    }

    private Map<VehicleType, Map<AccessTimeType, Price>> createExpectedAccessRates() {
        Map<VehicleType, Map<AccessTimeType, Price>> expectedAccessRates = new HashMap<>();
        Map<AccessTimeType, Price> expectedVehicleTypeGreedy = createExpectedAccessTypeRates(
            Arrays.asList(Price.of(3D), Price.of(12D), Price.of(65D), Price.of(250D), Price.of(500D), Price.of(725D))
        );
        Map<AccessTimeType, Price> expectedVehicleTypeEconomic = createExpectedAccessTypeRates(
            Arrays.asList(Price.of(2D), Price.of(8D), Price.of(30D), Price.of(120D), Price.of(240D), Price.of(300D))
        );
        Map<AccessTimeType, Price> expectedVehicleTypeHybridEconomic = createExpectedAccessTypeRates(
            Arrays.asList(Price.of(1.5D), Price.of(4D), Price.of(15D), Price.of(75D), Price.of(150D), Price.of(200D))
        );
        Map<AccessTimeType, Price> expectedVehicleTypeSuperEconomic = createExpectedAccessTypeRates(
            Arrays.asList(Price.of(1D), Price.of(2D), Price.of(5D), Price.of(20D), Price.of(40D), Price.of(50D))
        );
        Map<AccessTimeType, Price> expectedVehicleTypeZeroPollution = createExpectedAccessTypeRates(
            Arrays.asList(Price.of(0D), Price.of(0D), Price.of(0D), Price.of(0D), Price.of(0D), Price.of(0D))
        );
        expectedAccessRates.put(VehicleType.GREEDY, expectedVehicleTypeGreedy);
        expectedAccessRates.put(VehicleType.ECONOMIC, expectedVehicleTypeEconomic);
        expectedAccessRates.put(VehicleType.ECONOMIC_HYBRID, expectedVehicleTypeHybridEconomic);
        expectedAccessRates.put(VehicleType.SUPER_ECONOMIC, expectedVehicleTypeSuperEconomic);
        expectedAccessRates.put(VehicleType.ZERO_POLLUTION, expectedVehicleTypeZeroPollution);
        return expectedAccessRates;
    }

    private Map<AccessTimeType, Price> createExpectedAccessTypeRates(List<Price> rates) {
        Map<AccessTimeType, Price> accessTimeTypeRates = new HashMap<>();
        accessTimeTypeRates.put(AccessTimeType.ONE_HOUR, rates.get(0));
        accessTimeTypeRates.put(AccessTimeType.ONE_DAY, rates.get(1));
        accessTimeTypeRates.put(AccessTimeType.ONE_DAY_A_WEEK_FOR_ONE_SEMESTER, rates.get(2));
        accessTimeTypeRates.put(AccessTimeType.ONE_SEMESTER, rates.get(3));
        accessTimeTypeRates.put(AccessTimeType.TWO_SEMESTERS, rates.get(4));
        accessTimeTypeRates.put(AccessTimeType.THREE_SEMESTERS, rates.get(5));
        return accessTimeTypeRates;
    }
}
