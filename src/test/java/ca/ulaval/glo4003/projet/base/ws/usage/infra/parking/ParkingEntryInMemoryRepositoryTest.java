package ca.ulaval.glo4003.projet.base.ws.usage.infra.parking;

import static com.google.common.truth.Truth.*;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntry;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParkingEntryInMemoryRepositoryTest {

    private static final LocalDate AN_ENTRY_DATE = LocalDate.now();
    private static final LocalDate A_DATE_BEFORE_ENTRY_DATE = AN_ENTRY_DATE.minusYears(1);
    private static final LocalDate A_DATE_AFTER_ENTRY_DATE = AN_ENTRY_DATE.plusDays(4);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_2;
    private static final ParkingZone ANOTHER_PARKING_ZONE = ParkingZone.ZONE_R;

    private ParkingEntryInMemoryRepository parkingEntryRepository;

    @BeforeEach
    void setUp() {
        parkingEntryRepository = new ParkingEntryInMemoryRepository();
    }

    @Test
    void givenManyEntriesInZoneAndInPeriod_whenGetByZoneInPeriod_thenReturnAllThoseEntries() {
        // given
        ParkingEntry parkingEntry = new ParkingEntry(AN_ENTRY_DATE, A_PARKING_ZONE);
        ParkingEntry otherParkingEntry = new ParkingEntry(AN_ENTRY_DATE, A_PARKING_ZONE);
        parkingEntryRepository.save(parkingEntry);
        parkingEntryRepository.save(otherParkingEntry);

        // when
        List<ParkingEntry> parkingEntries = parkingEntryRepository.getByZoneInPeriod(A_PARKING_ZONE, A_DATE_BEFORE_ENTRY_DATE, A_DATE_AFTER_ENTRY_DATE);

        // then
        assertThat(parkingEntries).hasSize(2);
    }

    @Test
    void givenParkingEntryIsBeforePeriod_whenGetByZoneInPeriod_thenDoNotReturnEntry() {
        // given
        ParkingEntry parkingEntry = new ParkingEntry(AN_ENTRY_DATE, A_PARKING_ZONE);
        parkingEntryRepository.save(parkingEntry);

        // when
        List<ParkingEntry> parkingEntries = parkingEntryRepository.getByZoneInPeriod(A_PARKING_ZONE, A_DATE_AFTER_ENTRY_DATE, A_DATE_AFTER_ENTRY_DATE);

        // then
        assertThat(parkingEntries).isEmpty();
    }

    @Test
    void givenParkingEntryIsAfterPeriod_whenGetByZoneInPeriod_thenDoNotReturnEntry() {
        // given
        ParkingEntry parkingEntry = new ParkingEntry(AN_ENTRY_DATE, A_PARKING_ZONE);
        parkingEntryRepository.save(parkingEntry);

        // when
        List<ParkingEntry> parkingEntries = parkingEntryRepository.getByZoneInPeriod(A_PARKING_ZONE, A_DATE_BEFORE_ENTRY_DATE, A_DATE_BEFORE_ENTRY_DATE);

        // then
        assertThat(parkingEntries).isEmpty();
    }

    @Test
    void givenParkingEntryIsNotInZone_whenGetByZoneInPeriod_thenDoNotReturnEntry() {
        // given
        ParkingEntry parkingEntry = new ParkingEntry(AN_ENTRY_DATE, ANOTHER_PARKING_ZONE);
        parkingEntryRepository.save(parkingEntry);

        // when
        List<ParkingEntry> parkingEntries = parkingEntryRepository.getByZoneInPeriod(A_PARKING_ZONE, A_DATE_BEFORE_ENTRY_DATE, A_DATE_AFTER_ENTRY_DATE);

        // then
        assertThat(parkingEntries).isEmpty();
    }
}
