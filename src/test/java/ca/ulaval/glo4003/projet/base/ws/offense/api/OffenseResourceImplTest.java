package ca.ulaval.glo4003.projet.base.ws.offense.api;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.*;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationRequest;
import ca.ulaval.glo4003.projet.base.ws.offense.api.response.OffenseResponse;
import ca.ulaval.glo4003.projet.base.ws.offense.api.response.OffenseResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.Offense;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import ca.ulaval.glo4003.projet.base.ws.offense.service.OffenseService;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OffenseResourceImplTest {

    private static final ParkingCode A_PARKING_CODE = new ParkingCode(420);
    private static final LocalDateTime A_TIME_ISSUED = LocalDateTime.now();
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final OffenseId AN_OFFENSE_ID = new OffenseId(420);
    private static final OffenseCode AN_OFFENSE_CODE = OffenseCode.ZONE_01;
    private static final String AN_OFFENSE_DESCRIPTION = "A description";
    private static final Price AN_OFFENSE_PRICE = Price.of(420);
    private static final String A_CREDIT_CARD_NUMBER = "123456789";
    private static final String A_CREDIT_CARD_NAME = "a credit card name";
    private static final String AN_EXPIRY_DATE = "0320";
    private static final String A_CCV = "123";

    private OffenseResource offenseResource;

    @Mock
    private OffenseService offenseService;

    @BeforeEach
    void setUp() {
        OffenseResponseAssembler offenseResponseAssembler = new OffenseResponseAssembler();
        CreditCardAssembler creditCardAssembler = new CreditCardAssembler();
        OffenseIdRequestAssembler offenseIdRequestAssembler = new OffenseIdRequestAssembler();
        ParkingPassValidationAssembler parkingPassValidationAssembler = new ParkingPassValidationAssembler();
        offenseResource =
            new OffenseResourceImpl(offenseService, offenseResponseAssembler, creditCardAssembler, parkingPassValidationAssembler, offenseIdRequestAssembler);
    }

    @Test
    void givenNoOffenseWasCommitted_whenCreateParkingOffenseIfOffenseWasCommitted_return200Ok() {
        // given
        ParkingPassValidationRequest parkingPassValidationRequest = givenAParkingPassValidationRequest();
        given(offenseService.createOffenseIfParkingPassOffenseWasCommitted(givenAParkingPassValidation())).willReturn(Optional.empty());

        // when
        Response response = offenseResource.createParkingOffenseIfOffenseWasCommitted(parkingPassValidationRequest);

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void givenAnOffenceWasCommitted_whenCreateParkingOffenseIfOffenseWasCommitted_thenCreatedOffenseIsReturned() {
        //given
        Offense offense = givenAnOffense();
        ParkingPassValidationRequest parkingPassValidationRequest = givenAParkingPassValidationRequest();
        given(offenseService.createOffenseIfParkingPassOffenseWasCommitted(givenAParkingPassValidation())).willReturn(Optional.of(offense));

        //when
        Response response = offenseResource.createParkingOffenseIfOffenseWasCommitted(parkingPassValidationRequest);

        //then
        OffenseResponse offenseResponse = (OffenseResponse) response.getEntity();
        assertThat(offenseResponse.getOffenseId()).isEqualTo(AN_OFFENSE_ID.getId());
        assertThat(offenseResponse.getOffenseCode()).isEqualTo(AN_OFFENSE_CODE);
        assertThat(offenseResponse.getDescription()).isEqualTo(AN_OFFENSE_DESCRIPTION);
        assertThat(offenseResponse.getAmount()).isEqualTo(formatAsCapitalAmount(AN_OFFENSE_PRICE.getAmount()));
    }

    @Test
    void whenPayOffense_thenReturn200OK() {
        // given
        OffenseIdRequest offenseIdRequest = givenAnOffenseIdRequest();
        CreditCardRequest creditCardRequest = givenACreditCardRequest();
        PayOffenseRequest payOffenseRequest = new PayOffenseRequest(offenseIdRequest, creditCardRequest);

        // when
        Response response = offenseResource.payOffense(payOffenseRequest);

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    private ParkingPassValidationRequest givenAParkingPassValidationRequest() {
        return new ParkingPassValidationRequest(A_PARKING_CODE.getCode(), A_TIME_ISSUED, A_PARKING_ZONE);
    }

    private ParkingPassValidation givenAParkingPassValidation() {
        return new ParkingPassValidation(A_PARKING_CODE, A_TIME_ISSUED, A_PARKING_ZONE);
    }

    private OffenseIdRequest givenAnOffenseIdRequest() {
        return new OffenseIdRequest(AN_OFFENSE_ID.getId());
    }

    private CreditCardRequest givenACreditCardRequest() {
        return new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, AN_EXPIRY_DATE, A_CCV);
    }

    private Offense givenAnOffense() {
        return new Offense(AN_OFFENSE_ID, AN_OFFENSE_CODE, AN_OFFENSE_DESCRIPTION, AN_OFFENSE_PRICE);
    }

    private BigDecimal formatAsCapitalAmount(BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_UP);
    }
}
