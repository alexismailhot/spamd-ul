package ca.ulaval.glo4003.projet.base.ws.parking.api;

import ca.ulaval.glo4003.projet.base.ws.email.domain.EmailSender;
import ca.ulaval.glo4003.projet.base.ws.email.infra.ULavalEmailSender;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.PostalAddressAssembler;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.PostalAddressRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.SessionParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.response.ParkingPassResponse;
import ca.ulaval.glo4003.projet.base.ws.parking.api.response.ParkingPassResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethodRatesConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeMailingFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeMailingRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSSPDeliveryInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSSPDeliveryInformationRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSenderFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassPriceCalculator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRatesConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassType;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.DeliveryMethodInformationValidatorFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.ParkingPassInformationValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.SessionInformationValidatorFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session.SessionParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassDailyAdmissibilityValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassZoneValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.ParkingPassInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.code.ParkingCodeMailingInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.code.ParkingCodeSSPDeliveryInformationInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.income.ParkingPassIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.service.ParkingService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import ca.ulaval.glo4003.projet.base.ws.payment.infra.DumbExternalPaymentService;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.core.Response;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.Year;
import java.time.ZoneId;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ParkingResourceIT {

    private static final DeliveryMethod A_DELIVERY_METHOD = DeliveryMethod.POSTAL;
    private static final Price A_DELIVERY_METHOD_PRICE = Price.of(5);
    private static final ParkingZone DEFAULT_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final ParkingPassType A_WEEKLY_PARKING_PASS_TYPE = ParkingPassType.ONE_DAY_A_WEEK_FOR_A_SEMESTER;
    private static final Price A_PARKING_PASS_RATE_PRICE = Price.of(100);
    private static final String AN_EMAIL = "test@hotmail.com";
    private static final DayOfWeek A_DAY_OF_WEEK = DayOfWeek.FRIDAY;
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final String AN_ADDRESS = "123 Mackay";
    private static final String AN_APARTMENT_NUMBER = "4";
    private static final String A_STATE = "Quebec";
    private static final String A_COUNTRY = "Canada";
    private static final String A_CITY = "Quebec";
    private static final String A_POSTAL_CODE = "G1V2X1";
    private static final Instant AN_INSTANT = Instant.ofEpochSecond(1606836629);
    private static final Year A_YEAR = Year.of(AN_INSTANT.atZone(ZoneId.systemDefault()).getYear() + 1);
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;

    private ParkingResource parkingResource;

    private ParkingPassRepository parkingPassRepository;

    @Mock
    private Clock clock;

    @Mock
    private ParkingPassRatesConfig parkingPassRatesConfig;

    @Mock
    private DeliveryMethodRatesConfig deliveryMethodRatesConfig;

    @BeforeEach
    void setUp() {
        parkingPassRepository = new ParkingPassInMemoryRepository();
        WeeklyParkingPassFactory weeklyParkingPassFactory = new WeeklyParkingPassFactory(parkingPassRepository);
        PaymentService paymentService = new DumbExternalPaymentService();
        EmailSender emailSender = new ULavalEmailSender();
        ParkingCodeMailingRepository parkingCodeMailingRepository = new ParkingCodeMailingInMemoryRepository();
        ParkingCodeMailingFactory parkingCodeMailingFactory = new ParkingCodeMailingFactory();
        ParkingCodeSSPDeliveryInformationRepository parkingCodeSSPDeliveryInformationRepository = new ParkingCodeSSPDeliveryInformationInMemoryRepository();
        ParkingCodeSSPDeliveryInformationFactory parkingCodeSSPDeliveryInformationFactory = new ParkingCodeSSPDeliveryInformationFactory();
        ParkingCodeSenderFactory parkingCodeSenderFactory = new ParkingCodeSenderFactory(
            emailSender,
            parkingCodeMailingRepository,
            parkingCodeMailingFactory,
            parkingCodeSSPDeliveryInformationRepository,
            parkingCodeSSPDeliveryInformationFactory
        );
        ParkingPassIncomeFactory parkingPassIncomeFactory = new ParkingPassIncomeFactory(clock);
        ParkingPassIncomeRepository parkingPassIncomeRepository = new ParkingPassIncomeInMemoryRepository();
        DeliveryMethodInformationValidatorFactory deliveryMethodInformationValidatorFactory = new DeliveryMethodInformationValidatorFactory();
        SessionInformationValidatorFactory sessionInformationValidatorFactory = new SessionInformationValidatorFactory(clock);
        ParkingPassInformationValidator parkingPassInformationValidator = new ParkingPassInformationValidator(deliveryMethodInformationValidatorFactory, sessionInformationValidatorFactory);
        ParkingPassPriceCalculator parkingPassPriceCalculator = new ParkingPassPriceCalculator(parkingPassRatesConfig, deliveryMethodRatesConfig);
        ParkingPassValidator parkingPassValidator = new ParkingPassDailyAdmissibilityValidator(new ParkingPassZoneValidator(null));
        ParkingService parkingService = new ParkingService(
            parkingPassRepository,
            weeklyParkingPassFactory,
            new SessionParkingPassFactory(parkingPassRepository),
            paymentService,
            parkingCodeSenderFactory,
            parkingPassIncomeFactory,
            parkingPassIncomeRepository,
            parkingPassInformationValidator,
            parkingPassPriceCalculator,
            parkingPassValidator
        );
        CreditCardAssembler creditCardAssembler = new CreditCardAssembler();
        PostalAddressAssembler postalAddressAssembler = new PostalAddressAssembler();
        WeeklyParkingPassInformationFactory weeklyParkingPassInformationFactory = new WeeklyParkingPassInformationFactory(postalAddressAssembler);
        ParkingPassResponseAssembler parkingPassResponseAssembler = new ParkingPassResponseAssembler();

        SessionParkingPassInformationFactory sessionParkingPassInformationFactory = new SessionParkingPassInformationFactory(postalAddressAssembler);

        parkingResource =
            new ParkingResourceImpl(
                parkingService,
                weeklyParkingPassInformationFactory,
                sessionParkingPassInformationFactory,
                parkingPassResponseAssembler,
                creditCardAssembler
            );

        given(parkingPassRatesConfig.getParkingPassRate(DEFAULT_PARKING_ZONE, A_WEEKLY_PARKING_PASS_TYPE)).willReturn(A_PARKING_PASS_RATE_PRICE);
        given(deliveryMethodRatesConfig.getDeliveryMethodRate(A_DELIVERY_METHOD)).willReturn(A_DELIVERY_METHOD_PRICE);
    }

    @Test
    void givenWeeklyParkingPassRequest_whenBuyWeeklyParkingPass_thenCreatedWeeklyPassIsSaved() {
        // given
        given(clock.instant()).willReturn(AN_INSTANT);
        WeeklyParkingPassRequest weeklyParkingPassRequest = givenWeeklyParkingPassRequest();

        // when
        Response response = parkingResource.buyWeeklyParkingPass(weeklyParkingPassRequest);

        // then
        ParkingPassResponse parkingPassResponse = (ParkingPassResponse) response.getEntity();
        ParkingPass parkingPass = parkingPassRepository.findByParkingCode(new ParkingCode(parkingPassResponse.getCode())).get();
        assertThat(parkingPass.getParkingCode()).isEqualTo(new ParkingCode(parkingPassResponse.getCode()));
        assertThat(parkingPass.getParkingZone()).isEqualTo(DEFAULT_PARKING_ZONE);
    }

    private WeeklyParkingPassRequest givenWeeklyParkingPassRequest() {
        CreditCardRequest creditCardRequest = new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
        PostalAddressRequest postalAddressRequest = new PostalAddressRequest(AN_ADDRESS, AN_APARTMENT_NUMBER, A_STATE, A_COUNTRY, A_CITY, A_POSTAL_CODE);
        return new WeeklyParkingPassRequest(
            DEFAULT_PARKING_ZONE,
            A_DELIVERY_METHOD,
            AN_EMAIL,
            A_DAY_OF_WEEK,
            givenASessionRequest(),
            creditCardRequest,
            postalAddressRequest
        );
    }

    private SessionRequest givenASessionRequest() {
        return new SessionRequest(A_SESSION_TYPE, A_YEAR);
    }
}
