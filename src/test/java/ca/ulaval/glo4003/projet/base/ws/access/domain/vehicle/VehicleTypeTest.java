package ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class VehicleTypeTest {

    public static final String A_VALID_VEHICLE_TYPE_VALUE = "economic_hybrid";
    public static final String AN_INVALID_VEHICLE_TYPE_VALUE = "zone1";

    @Test
    void givenValidParkingZoneValue_whenFromString_shouldNotThrow() {
        // then
        Assertions.assertDoesNotThrow(
            () ->
                // when
                VehicleType.fromString(A_VALID_VEHICLE_TYPE_VALUE)
        );
    }

    @Test
    void givenInvalidParkingZoneValue_whenFromString_shouldThrow() {
        // then
        Assertions.assertThrows(
            InvalidVehicleTypeException.class,
            // when
            () -> VehicleType.fromString(AN_INVALID_VEHICLE_TYPE_VALUE)
        );
    }
}
