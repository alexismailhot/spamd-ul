package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation;

import static org.mockito.Mockito.*;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotValidInZoneException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPass;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.LocalDateTime;
import java.time.Year;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingPassZoneValidatorTest {

    private static final ParkingCode A_PARKING_CODE = new ParkingCode(7);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final LocalDateTime A_PARKING_TIME = LocalDateTime.of(1, 2, 4, 5, 6);
    private static final ParkingZone ANOTHER_PARKING_ZONE = ParkingZone.ZONE_3;
    private static final Session AN_UNIVERSITY_SESSION = new Session(SessionType.SUMMER, Year.of(4));

    private ParkingPassZoneValidator parkingPassZoneValidator;

    @Mock
    private ParkingPassValidator nextParkingPassValidator;

    @BeforeEach
    void setUp() {
        parkingPassZoneValidator = new ParkingPassZoneValidator(nextParkingPassValidator);
    }

    @Test
    void givenParkingPassIsValidInZone_whenValidate_thenDoesNotThrow() {
        // given
        ParkingPass parkingPass = givenAParkingPass(A_PARKING_ZONE);
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation(A_PARKING_ZONE);

        // when

        // then
        Assertions.assertDoesNotThrow(
            () ->
                // when
                parkingPassZoneValidator.validate(parkingPassValidation, parkingPass)
        );
    }

    @Test
    void givenParkingPassIsValidInZone_whenValidate_thenCallNextValidator() {
        // given
        ParkingPass parkingPass = givenAParkingPass(A_PARKING_ZONE);
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation(A_PARKING_ZONE);

        // when
        parkingPassZoneValidator.validate(parkingPassValidation, parkingPass);

        // then
        verify(nextParkingPassValidator).validate(parkingPassValidation, parkingPass);
    }

    @Test
    void givenParkingPassIsNotValidInZone_whenValidate_thenThrowParkingPassNotValidInZoneException() {
        // given
        ParkingPass parkingPass = givenAParkingPass(A_PARKING_ZONE);
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation(ANOTHER_PARKING_ZONE);

        // then
        Assertions.assertThrows(
            ParkingPassNotValidInZoneException.class,
            () ->
                // when
                parkingPassZoneValidator.validate(parkingPassValidation, parkingPass)
        );
    }

    private ParkingPass givenAParkingPass(ParkingZone parkingZone) {
        return new WeeklyParkingPass(A_PARKING_CODE, parkingZone, A_PARKING_TIME.getDayOfWeek(), AN_UNIVERSITY_SESSION);
    }

    private ParkingPassValidation givenAParkingPassValidation(ParkingZone parkingZone) {
        return new ParkingPassValidation(A_PARKING_CODE, A_PARKING_TIME, parkingZone);
    }
}
