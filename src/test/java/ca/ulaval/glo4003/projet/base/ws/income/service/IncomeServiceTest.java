package ca.ulaval.glo4003.projet.base.ws.income.service;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummary;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummaryFactory;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncome;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYearProvider;
import java.time.*;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IncomeServiceTest {

    private static final Instant NOW = Instant.now();
    private static final Price AN_ACCESS_PRICE = Price.of(6);
    private static final Price ANOTHER_ACCESS_PRICE = Price.of(32);
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final LocalDateTime A_TIME_CREATED = LocalDateTime.now();
    private static final Price A_PARKING_PASS_PRICE = Price.of(65);
    private static final Price ANOTHER_PARKING_PASS_PRICE = Price.of(352);
    private static final Price AN_OFFENSE_PRICE = Price.of(63);
    private static final Price ANOTHER_OFFENSE_PRICE = Price.of(2);
    private static final Session A_SESSION = new Session(SessionType.SUMMER, Year.of(1));
    private static final UniversityYear A_UNIVERSITY_YEAR = new UniversityYear(A_SESSION, A_SESSION, A_SESSION);
    private static final int A_NUMBER_OF_SALES = 420;
    private static final Price A_TOTAL_INCOME = Price.of(123);

    @Mock
    private AccessIncomeRepository accessIncomeRepository;

    @Mock
    private ParkingPassIncomeRepository parkingPassIncomeRepository;

    @Mock
    private OffenseIncomeRepository offenseIncomeRepository;

    @Mock
    private UniversityYearProvider universityYearProvider;

    @Mock
    private Clock clock;

    @Mock
    private IncomeSummaryFactory incomeSummaryFactory;

    private IncomeService incomeService;

    @BeforeEach
    void setUp() {
        incomeService =
            new IncomeService(
                accessIncomeRepository,
                parkingPassIncomeRepository,
                offenseIncomeRepository,
                incomeSummaryFactory,
                universityYearProvider,
                clock
            );
    }

    @Test
    void whenGetAccessIncomeSummaryForCurrentUniversityYear_thenReturnAccessIncomeSummaryForCurrentUniversityYear() {
        // given
        IncomeSummary expectedIncomeSummary = new IncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        given(clock.instant()).willReturn(NOW);
        given(universityYearProvider.provide(NOW.atZone(ZoneId.systemDefault()).toLocalDate())).willReturn(A_UNIVERSITY_YEAR);
        List<AccessIncome> accessIncomes = Arrays.asList(givenAnAccessIncome(AN_ACCESS_PRICE), givenAnAccessIncome(ANOTHER_ACCESS_PRICE));
        given(accessIncomeRepository.findByUniversityYear(A_UNIVERSITY_YEAR)).willReturn(accessIncomes);
        given(incomeSummaryFactory.createFromAccessIncome(accessIncomes)).willReturn(expectedIncomeSummary);

        // when
        IncomeSummary incomeSummary = incomeService.getAccessIncomeSummaryForCurrentUniversityYear();

        // then
        assertThat(incomeSummary).isEqualTo(expectedIncomeSummary);
    }

    @Test
    void whenGetParkingIncomeSummaryForCurrentUniversityYear_thenReturnParkingIncomeSummaryForCurrentUniversityYear() {
        // given
        IncomeSummary expectedIncomeSummary = new IncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        given(clock.instant()).willReturn(NOW);
        given(universityYearProvider.provide(NOW.atZone(ZoneId.systemDefault()).toLocalDate())).willReturn(A_UNIVERSITY_YEAR);
        List<ParkingPassIncome> parkingPassIncomes = Arrays.asList(
            givenAParkingPassIncome(A_PARKING_PASS_PRICE),
            givenAParkingPassIncome(ANOTHER_PARKING_PASS_PRICE)
        );
        given(parkingPassIncomeRepository.findByUniversityYear(A_UNIVERSITY_YEAR)).willReturn(parkingPassIncomes);
        given(incomeSummaryFactory.createFromParkingIncome(parkingPassIncomes)).willReturn(expectedIncomeSummary);

        // when
        IncomeSummary incomeSummary = incomeService.getParkingIncomeSummaryForCurrentUniversityYear();

        // then
        assertThat(incomeSummary).isEqualTo(expectedIncomeSummary);
    }

    @Test
    void givenAVehicleType_whenGetAccessIncomeSummaryFor_thenReturnAccessIncomeSummaryForVehicleType() {
        // given
        IncomeSummary expectedIncomeSummary = new IncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        List<AccessIncome> accessIncomes = Arrays.asList(givenAnAccessIncome(AN_ACCESS_PRICE), givenAnAccessIncome(ANOTHER_ACCESS_PRICE));
        given(accessIncomeRepository.findByVehicleType(A_VEHICLE_TYPE)).willReturn(accessIncomes);
        given(incomeSummaryFactory.createFromAccessIncome(accessIncomes)).willReturn(expectedIncomeSummary);

        // when
        IncomeSummary incomeSummary = incomeService.getAccessIncomeSummaryFor(A_VEHICLE_TYPE);

        // then
        assertThat(incomeSummary).isEqualTo(expectedIncomeSummary);
    }

    @Test
    void givenAVehicleType_whenGetAccessIncomeSummaryForCurrentUniversityYearFor_thenReturnAccessIncomeSummaryForCurrentUniversityYearForVehicleType() {
        // given
        IncomeSummary expectedIncomeSummary = new IncomeSummary(A_NUMBER_OF_SALES, A_TOTAL_INCOME);
        given(clock.instant()).willReturn(NOW);
        given(universityYearProvider.provide(NOW.atZone(ZoneId.systemDefault()).toLocalDate())).willReturn(A_UNIVERSITY_YEAR);
        List<AccessIncome> accessIncomes = Arrays.asList(givenAnAccessIncome(AN_ACCESS_PRICE), givenAnAccessIncome(ANOTHER_ACCESS_PRICE));
        given(accessIncomeRepository.findByVehicleTypeAndUniversityYear(A_VEHICLE_TYPE, A_UNIVERSITY_YEAR)).willReturn(accessIncomes);
        given(incomeSummaryFactory.createFromAccessIncome(accessIncomes)).willReturn(expectedIncomeSummary);

        // when
        IncomeSummary incomeSummary = incomeService.getAccessIncomeSummaryForCurrentUniversityYearFor(A_VEHICLE_TYPE);

        // then
        assertThat(incomeSummary).isEqualTo(expectedIncomeSummary);
    }

    @Test
    void whenGetTotalIncome_thenReturnAdditionOfAllIncomes() {
        // given
        given(accessIncomeRepository.getAll()).willReturn(Arrays.asList(givenAnAccessIncome(AN_ACCESS_PRICE), givenAnAccessIncome(ANOTHER_ACCESS_PRICE)));
        given(parkingPassIncomeRepository.getAll())
            .willReturn(Arrays.asList(givenAParkingPassIncome(A_PARKING_PASS_PRICE), givenAParkingPassIncome(ANOTHER_PARKING_PASS_PRICE)));
        given(offenseIncomeRepository.getAll()).willReturn(Arrays.asList(givenAnOffenseIncome(AN_OFFENSE_PRICE), givenAnOffenseIncome(ANOTHER_OFFENSE_PRICE)));

        // when
        Price totalIncome = incomeService.getTotalIncome();

        // then
        Price expectedTotalIncome = AN_ACCESS_PRICE
            .add(ANOTHER_ACCESS_PRICE)
            .add(A_PARKING_PASS_PRICE)
            .add(ANOTHER_PARKING_PASS_PRICE)
            .add(AN_OFFENSE_PRICE)
            .add(ANOTHER_OFFENSE_PRICE);
        assertThat(totalIncome).isEqualTo(expectedTotalIncome);
    }

    @Test
    void whenGetTotalOffenseIncome_thenReturnAdditionOfAllOffenseIncomes() {
        // given
        given(offenseIncomeRepository.getAll()).willReturn(Arrays.asList(givenAnOffenseIncome(AN_OFFENSE_PRICE), givenAnOffenseIncome(ANOTHER_OFFENSE_PRICE)));

        // when
        Price totalOffenseIncome = incomeService.getTotalOffenseIncome();

        // then
        Price expectedTotalOffenseIncome = AN_OFFENSE_PRICE.add(ANOTHER_OFFENSE_PRICE);
        assertThat(totalOffenseIncome).isEqualTo(expectedTotalOffenseIncome);
    }

    @Test
    void whenGetTotalOffenseIncomeForCurrentUniversityYear_thenReturnAdditionOfAllOffenseIncomes() {
        // given
        given(clock.instant()).willReturn(NOW);
        given(universityYearProvider.provide(NOW.atZone(ZoneId.systemDefault()).toLocalDate())).willReturn(A_UNIVERSITY_YEAR);
        given(offenseIncomeRepository.findByUniversityYear(A_UNIVERSITY_YEAR))
            .willReturn(Arrays.asList(givenAnOffenseIncome(AN_OFFENSE_PRICE), givenAnOffenseIncome(ANOTHER_OFFENSE_PRICE)));

        // when
        Price currentUniversityYearOffenseIncome = incomeService.getTotalOffenseIncomeForCurrentUniversityYear();

        // then
        Price currentUniversityYearTotalOffenseIncome = AN_OFFENSE_PRICE.add(ANOTHER_OFFENSE_PRICE);
        assertThat(currentUniversityYearOffenseIncome).isEqualTo(currentUniversityYearTotalOffenseIncome);
    }

    private AccessIncome givenAnAccessIncome(Price accessPrice) {
        return new AccessIncome(accessPrice, A_VEHICLE_TYPE, A_TIME_CREATED);
    }

    private ParkingPassIncome givenAParkingPassIncome(Price parkingPassPrice) {
        return new ParkingPassIncome(parkingPassPrice, A_TIME_CREATED);
    }

    private OffenseIncome givenAnOffenseIncome(Price offensePrice) {
        return new OffenseIncome(offensePrice, A_TIME_CREATED);
    }
}
