package ca.ulaval.glo4003.projet.base.ws.offense.api;

import static com.google.common.truth.Truth.*;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.OffenseIdRequest;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.OffenseIdRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.PayOffenseRequest;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking.ParkingPassValidationRequest;
import ca.ulaval.glo4003.projet.base.ws.offense.api.response.OffenseResponse;
import ca.ulaval.glo4003.projet.base.ws.offense.api.response.OffenseResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.Offense;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformation;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformationConfig;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.OffenseInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.income.OffenseIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.service.OffenseService;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.InvalidParkingPassException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotFoundForParkingCodeException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import ca.ulaval.glo4003.projet.base.ws.parking.service.ParkingService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.payment.infra.DumbExternalPaymentService;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OffenseResourceIT {

    private static final Class<ParkingPassNotFoundForParkingCodeException> AN_INVALID_PARKING_PASS_EXCEPTION_CLASS =
        ParkingPassNotFoundForParkingCodeException.class;
    private static final OffenseCode AN_OFFENSE_CODE = OffenseCode.VIG_03;
    private static final int A_PARKING_CODE_VALUE = 6;
    private static final ParkingCode A_PARKING_CODE = new ParkingCode(A_PARKING_CODE_VALUE);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_2;
    private static final LocalDateTime A_TIME_ISSUED = LocalDateTime.now();
    private static final Price AN_OFFENSE_AMOUNT = Price.of(5);
    private static final String AN_OFFENSE_DESCRIPTION = "an offense description";
    private static final int OFFENSE_ID_VALUE = 5;
    private static final OffenseId AN_OFFENSE_ID = new OffenseId(OFFENSE_ID_VALUE);
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String AN_EXPIRY_DATE = "11/23";
    private static final String A_CCV = "123";
    private static final Instant NOW = Instant.now();

    private OffenseResource offenseResource;
    private OffenseRepository offenseRepository;

    @Mock
    private OffenseCodeInformationConfig offenseCodeInformationConfig;

    @Mock
    private ParkingService parkingService;

    @Mock
    private Clock clock;

    @BeforeEach
    void setUp() {
        Map<Class<? extends InvalidParkingPassException>, OffenseCode> offenses = new HashMap<>();
        offenses.put(AN_INVALID_PARKING_PASS_EXCEPTION_CLASS, AN_OFFENSE_CODE);
        offenseRepository = new OffenseInMemoryRepository();
        offenseResource =
            new OffenseResourceImpl(
                new OffenseService(
                    offenses,
                    offenseCodeInformationConfig,
                    parkingService,
                    new DumbExternalPaymentService(),
                    offenseRepository,
                    new OffenseFactory(offenseRepository),
                    new OffenseIncomeFactory(clock),
                    new OffenseIncomeInMemoryRepository()
                ),
                new OffenseResponseAssembler(),
                new CreditCardAssembler(),
                new ParkingPassValidationAssembler(),
                new OffenseIdRequestAssembler()
            );
    }

    @Test
    void givenAnInvalidPass_whenCreateParkingOffenseIfOffenseWasCommitted_thenCreateNewOffense() {
        // given
        doThrow(ParkingPassNotFoundForParkingCodeException.class).when(parkingService).validateParkingPass(givenAParkingPassValidation());
        given(offenseCodeInformationConfig.getOffenseCodeInformation(AN_OFFENSE_CODE)).willReturn(givenAnOffenseCodeInformation());

        // when
        Response response = offenseResource.createParkingOffenseIfOffenseWasCommitted(givenAParkingPassValidatorRequest());

        // then
        OffenseResponse offenseResponse = (OffenseResponse) response.getEntity();
        OffenseId offenseId = new OffenseId(offenseResponse.getOffenseId());
        assertThat(offenseRepository.getCount()).isEqualTo(1);

        Offense offense = offenseRepository.findByOffenseId(offenseId).get();
        assertThat(offense.getAmount()).isEqualTo(AN_OFFENSE_AMOUNT);
        assertThat(offense.getDescription()).isEqualTo(AN_OFFENSE_DESCRIPTION);
        assertThat(offense.getOffenseCode()).isEqualTo(AN_OFFENSE_CODE);
    }

    @Test
    public void givenAnOffense_whenPayOffense_thenReturn200Ok() {
        // given
        given(clock.instant()).willReturn(NOW);
        Offense offense = givenAnOffense(AN_OFFENSE_ID);
        offenseRepository.save(offense);

        // when
        Response response = offenseResource.payOffense(givenAPayOffenseRequest());

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    private PayOffenseRequest givenAPayOffenseRequest() {
        CreditCardRequest creditCardRequest = new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, AN_EXPIRY_DATE, A_CCV);
        return new PayOffenseRequest(new OffenseIdRequest(OFFENSE_ID_VALUE), creditCardRequest);
    }

    private OffenseCodeInformation givenAnOffenseCodeInformation() {
        return new OffenseCodeInformation(AN_OFFENSE_CODE, AN_OFFENSE_DESCRIPTION, AN_OFFENSE_AMOUNT);
    }

    private ParkingPassValidationRequest givenAParkingPassValidatorRequest() {
        return new ParkingPassValidationRequest(A_PARKING_CODE_VALUE, A_TIME_ISSUED, A_PARKING_ZONE);
    }

    private ParkingPassValidation givenAParkingPassValidation() {
        return new ParkingPassValidation(A_PARKING_CODE, A_TIME_ISSUED, A_PARKING_ZONE);
    }

    private Offense givenAnOffense(OffenseId offenseId) {
        return new Offense(offenseId, AN_OFFENSE_CODE, AN_OFFENSE_DESCRIPTION, AN_OFFENSE_AMOUNT);
    }
}
