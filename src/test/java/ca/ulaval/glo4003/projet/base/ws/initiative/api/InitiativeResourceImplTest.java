package ca.ulaval.glo4003.projet.base.ws.initiative.api;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeCreationRequest;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.request.InitiativeFundingRequest;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.BriefInitiativeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.DetailedInitiativeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.InitiativeCodeResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.InitiativeFundsResponse;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.BriefInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.DetailedInitiativeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeCodeResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.api.response.assembler.InitiativeFundsResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InitiativeResourceImplTest {

    private static final String A_NAME = "My initiative";
    private static final double AN_AMOUNT = 42.0;
    private static final String AN_INITIATIVE_CODE_NUMBER = "AN_INITIATIVE_CODE_NUMBER";
    private static final InitiativeCode AN_INITIATIVE_CODE = new InitiativeCode("AN_INITIATIVE_CODE");
    private static final Price A_PRICE = Price.of(AN_AMOUNT);
    private static final Initiative AN_INITIATIVE = new Initiative(A_NAME, A_PRICE, AN_INITIATIVE_CODE);
    private static final String ANOTHER_NAME = "My other initiative";
    private static final Price ANOTHER_PRICE = Price.of(2);
    private static final InitiativeCode ANOTHER_INITIATIVE_CODE = new InitiativeCode("ANOTHER_INITIATIVE_CODE");
    private static final Initiative ANOTHER_INITIATIVE = new Initiative(ANOTHER_NAME, ANOTHER_PRICE, ANOTHER_INITIATIVE_CODE);
    private static final List<Initiative> INITIATIVE_LIST = Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE);
    private static final Price AN_INITIATIVE_FUNDS_AMOUNT = Price.of(4);

    private InitiativeResource initiativeResource;

    @Mock
    private InitiativeService initiativeService;

    @BeforeEach
    void setup() {
        initiativeResource =
            new InitiativeResourceImpl(
                initiativeService,
                new InitiativeCodeResponseAssembler(),
                new BriefInitiativeResponseAssembler(),
                new DetailedInitiativeResponseAssembler(),
                new InitiativeFundsResponseAssembler()
            );
    }

    @Test
    void givenInitiativeCreateRequest_whenCreateInitiative_thenInitiativeCodeIsReturned() {
        // given
        InitiativeCreationRequest initiativeCreationRequest = givenAnInitiativeCreationRequest();
        given(initiativeService.createInitiative(A_NAME, Price.of(AN_AMOUNT))).willReturn(AN_INITIATIVE_CODE);

        // when
        Response response = initiativeResource.createInitiative(initiativeCreationRequest);

        // then
        InitiativeCodeResponse initiativeCodeResponse = (InitiativeCodeResponse) response.getEntity();
        assertThat(initiativeCodeResponse.getInitiativeCode()).isEqualTo(AN_INITIATIVE_CODE.getCode());
    }

    @Test
    void whenGetInitiatives_thenListOfBriefInitiativesResponseIsReturned() {
        // given
        given(initiativeService.getAllInitiatives()).willReturn(INITIATIVE_LIST);

        // when
        Response response = initiativeResource.getInitiatives();

        // then
        List<?> briefInitiativeResponses = (List<?>) response.getEntity();
        for (int i = 0; i < briefInitiativeResponses.size(); ++i) {
            BriefInitiativeResponse briefInitiativeResponse = (BriefInitiativeResponse) briefInitiativeResponses.get(i);
            assertThat(briefInitiativeResponse.getCode()).isEqualTo(INITIATIVE_LIST.get(i).getInitiativeCode().getCode());
            assertThat(briefInitiativeResponse.getName()).isEqualTo(INITIATIVE_LIST.get(i).getName());
        }
    }

    @Test
    void givenAnInitiativeCodeNumber_whenGetInitiative_thenDetailedInitiativeResponseIsReturned() {
        // given
        given(initiativeService.getInitiative(new InitiativeCode(AN_INITIATIVE_CODE_NUMBER))).willReturn(AN_INITIATIVE);

        // when
        Response response = initiativeResource.getInitiative(AN_INITIATIVE_CODE_NUMBER);

        // then
        DetailedInitiativeResponse detailedInitiativeResponse = (DetailedInitiativeResponse) response.getEntity();
        assertThat(detailedInitiativeResponse.getAttributedFunds()).isEqualTo(formatAsCapitalAmount(AN_INITIATIVE.getAttributedFunds().getAmount()));
        assertThat(detailedInitiativeResponse.getName()).isEqualTo(AN_INITIATIVE.getName());
    }

    @Test
    void givenInitiativeFundingRequestAndAnInitiativeCodeNumber_whenFundInitiative_thenInitiativeServiceIsCalledToFinanceInitiative() {
        // given
        InitiativeFundingRequest initiativeFundingRequest = givenAnInitiativeFundingRequest();
        InitiativeCode expectedInitiativeCode = new InitiativeCode(AN_INITIATIVE_CODE_NUMBER);

        // when
        initiativeResource.fundInitiative(AN_INITIATIVE_CODE_NUMBER, initiativeFundingRequest);

        // then
        verify(initiativeService).fundInitiative(expectedInitiativeCode, Price.of(AN_AMOUNT));
    }

    @Test
    void givenInitiativeFundingRequestAndAnInitiativeCodeNumber_whenFundInitiative_thenReturn200Ok() {
        // given
        InitiativeFundingRequest initiativeFundingRequest = givenAnInitiativeFundingRequest();

        // when
        Response response = initiativeResource.fundInitiative(AN_INITIATIVE_CODE_NUMBER, initiativeFundingRequest);

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void whenGetAllInitiativeFundsFromOffenseIncome_thenReturn200Ok() {
        // given
        given(initiativeService.getInitiativeFundsFromOffenseIncome()).willReturn(AN_INITIATIVE_FUNDS_AMOUNT);

        // when
        Response response = initiativeResource.getAllInitiativeFundsFromOffenseIncome();

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void whenGetAllInitiativeFundsFromOffenseIncome_thenReturnTotalOffenseIncome() {
        // given
        given(initiativeService.getInitiativeFundsFromOffenseIncome()).willReturn(AN_INITIATIVE_FUNDS_AMOUNT);

        // when
        Response response = initiativeResource.getAllInitiativeFundsFromOffenseIncome();

        // then
        InitiativeFundsResponse initiativeFundsResponse = (InitiativeFundsResponse) response.getEntity();
        assertThat(initiativeFundsResponse.getFunds()).isEqualTo(formatAsCapitalAmount(AN_INITIATIVE_FUNDS_AMOUNT.getAmount()));
    }

    @Test
    void whenGetInitiativeFundsFromOffenseIncomeForCurrentUniversityYear_thenReturn200Ok() {
        // given
        given(initiativeService.getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear()).willReturn(AN_INITIATIVE_FUNDS_AMOUNT);

        // when
        Response response = initiativeResource.getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear();

        // then
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    void whenGetAllInitiativeFundsFromOffenseIncomeForCurrentUniversityYear_thenReturnTotalOffenseIncome() {
        // given
        given(initiativeService.getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear()).willReturn(AN_INITIATIVE_FUNDS_AMOUNT);

        // when
        Response response = initiativeResource.getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear();

        // then
        InitiativeFundsResponse initiativeFundsResponse = (InitiativeFundsResponse) response.getEntity();
        assertThat(initiativeFundsResponse.getFunds()).isEqualTo(formatAsCapitalAmount(AN_INITIATIVE_FUNDS_AMOUNT.getAmount()));
    }

    private InitiativeCreationRequest givenAnInitiativeCreationRequest() {
        return new InitiativeCreationRequest(A_NAME, AN_AMOUNT);
    }

    private InitiativeFundingRequest givenAnInitiativeFundingRequest() {
        return new InitiativeFundingRequest(AN_AMOUNT);
    }

    private BigDecimal formatAsCapitalAmount(BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_UP);
    }
}
