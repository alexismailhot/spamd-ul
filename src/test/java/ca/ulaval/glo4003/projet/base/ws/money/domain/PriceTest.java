package ca.ulaval.glo4003.projet.base.ws.money.domain;

import static com.google.common.truth.Truth.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PriceTest {

    private static final double A_PRICE_AMOUNT = 500;
    private static final double ANOTHER_PRICE_AMOUNT = 100;
    private static final double A_FACTOR = 0.4;

    private Price price;

    @BeforeEach
    void setUp() {
        price = Price.of(A_PRICE_AMOUNT);
    }

    @Test
    void givenAPriceAndAnotherPrice_whenAdd_thenReturnedAdditionOfTwoPrices() {
        // given
        Price otherPrice = Price.of(ANOTHER_PRICE_AMOUNT);

        // when
        Price result = price.add(otherPrice);

        // then
        assertThat(result).isEquivalentAccordingToCompareTo(Price.of(A_PRICE_AMOUNT + ANOTHER_PRICE_AMOUNT));
    }

    @Test
    void givenAPriceAndAnFactor_whenMultiplyBy_thenReturnedMultiplicationOfPriceByFactor() {
        // when
        Price result = price.multiplyBy(A_FACTOR);

        // then
        assertThat(result).isEquivalentAccordingToCompareTo(Price.of(A_PRICE_AMOUNT * A_FACTOR));
    }

    @Test
    void givenAPriceAndAnotherPrice_whenSubtractBy_thenReturnedSubtractionOfTwoPrices() {
        // given
        Price otherPrice = Price.of(ANOTHER_PRICE_AMOUNT);

        // when
        Price result = price.subtractBy(otherPrice);

        // then
        assertThat(result).isEquivalentAccordingToCompareTo(Price.of(A_PRICE_AMOUNT - ANOTHER_PRICE_AMOUNT));
    }

    @Test
    void givenAPriceAndAnotherPrice_whenDividedBy_thenReturnRatioOfTwoPrices() {
        // given
        Price price = Price.of(A_PRICE_AMOUNT);
        Price otherPrice = Price.of(ANOTHER_PRICE_AMOUNT);
        double expectedRatio = A_PRICE_AMOUNT / ANOTHER_PRICE_AMOUNT;

        // when
        double ratio = price.dividedBy(otherPrice);

        // then
        assertThat(ratio).isEqualTo(expectedRatio);
    }
}
