package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation;

import static org.mockito.Mockito.*;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotValidOnDateException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPass;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.LocalDateTime;
import java.time.Year;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingPassDailyAdmissibilityValidatorTest {

    private static final ParkingCode A_PARKING_CODE = new ParkingCode(7);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final LocalDateTime ANOTHER_DAY_OF_WEEK = LocalDateTime.of(1, 2, 5, 5, 6);
    private static final Year A_YEAR = Year.of(102);
    private static final Session A_SESSION = new Session(SessionType.WINTER, A_YEAR);
    private static final LocalDateTime A_PARKING_TIME_VALID = LocalDateTime.of(A_YEAR.getValue(), 3, 4, 5, 6);
    private static final LocalDateTime TIME_IN_ANOTHER_SESSION = LocalDateTime.of(A_YEAR.plusYears(3).getValue(), 3, 9, 2, 2);

    private ParkingPassDailyAdmissibilityValidator parkingPassDailyAdmissibilityValidator;

    @Mock
    private ParkingPassValidator nextParkingPassValidator;

    @BeforeEach
    void setUp() {
        parkingPassDailyAdmissibilityValidator = new ParkingPassDailyAdmissibilityValidator(nextParkingPassValidator);
    }

    @Test
    void givenParkingPassIsOnDayOfWeekAndInSession_whenValidate_thenDoesNotThrow() {
        // given
        ParkingPass parkingPass = givenAParkingPass();
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation(A_PARKING_TIME_VALID);

        // when

        // then
        Assertions.assertDoesNotThrow(
            () ->
                // when
                parkingPassDailyAdmissibilityValidator.validate(parkingPassValidation, parkingPass)
        );
    }

    @Test
    void givenParkingPassIsOnDayOfWeekAndInSession_whenValidate_thenCallNextValidator() {
        // given
        ParkingPass parkingPass = givenAParkingPass();
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation(A_PARKING_TIME_VALID);

        // when
        parkingPassDailyAdmissibilityValidator.validate(parkingPassValidation, parkingPass);

        // then
        verify(nextParkingPassValidator).validate(parkingPassValidation, parkingPass);
    }

    @Test
    void givenParkingPassIsNotValidOnDayOfWeek_whenValidate_thenThrowParkingPassNotValidOnDateException() {
        // given
        ParkingPass parkingPass = givenAParkingPass();
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation(ANOTHER_DAY_OF_WEEK);

        // then
        Assertions.assertThrows(
            ParkingPassNotValidOnDateException.class,
            () ->
                // when
                parkingPassDailyAdmissibilityValidator.validate(parkingPassValidation, parkingPass)
        );
    }

    @Test
    void givenParkingPassIsNotValidInSession_whenValidate_thenThrowParkingPassNotValidOnDateException() {
        // given
        ParkingPass parkingPass = givenAParkingPass();
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation(TIME_IN_ANOTHER_SESSION);

        // then
        Assertions.assertThrows(
            ParkingPassNotValidOnDateException.class,
            () ->
                // when
                parkingPassDailyAdmissibilityValidator.validate(parkingPassValidation, parkingPass)
        );
    }

    private ParkingPass givenAParkingPass() {
        return new WeeklyParkingPass(A_PARKING_CODE, A_PARKING_ZONE, A_PARKING_TIME_VALID.getDayOfWeek(), A_SESSION);
    }

    private ParkingPassValidation givenAParkingPassValidation(LocalDateTime parkingTime) {
        return new ParkingPassValidation(A_PARKING_CODE, parkingTime, A_PARKING_ZONE);
    }
}
