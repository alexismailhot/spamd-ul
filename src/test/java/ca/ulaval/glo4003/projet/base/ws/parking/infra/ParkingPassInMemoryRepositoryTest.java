package ca.ulaval.glo4003.projet.base.ws.parking.infra;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth8.assertThat;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPass;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParkingPassInMemoryRepositoryTest {

    private static final ParkingCode A_PARKING_CODE = new ParkingCode(9);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final DayOfWeek A_PARKING_DAY = DayOfWeek.TUESDAY;
    private static final Session AN_UNIVERSITY_SESSION = new Session(SessionType.SUMMER, Year.of(34));

    private ParkingPassInMemoryRepository parkingPassRepository;

    @BeforeEach
    void setUp() {
        parkingPassRepository = new ParkingPassInMemoryRepository();
    }

    @Test
    void givenAssociatedAccessWasSaved_whenFindByAccessCode_thenReturnAccess() {
        // given
        ParkingPass parkingPass = givenAParkingPass();
        parkingPassRepository.save(parkingPass);

        // when
        Optional<ParkingPass> optionalParkingPass = parkingPassRepository.findByParkingCode(A_PARKING_CODE);

        // then
        assertThat(optionalParkingPass.get()).isEqualTo(parkingPass);
    }

    @Test
    void givenNoAssociatedAccessSaved_whenFindByAccessCode_thenReturnEmptyOptional() {
        // given

        // when
        Optional<ParkingPass> parkingPass = parkingPassRepository.findByParkingCode(A_PARKING_CODE);

        // then
        assertThat(parkingPass).isEmpty();
    }

    private ParkingPass givenAParkingPass() {
        return new WeeklyParkingPass(A_PARKING_CODE, A_PARKING_ZONE, A_PARKING_DAY, AN_UNIVERSITY_SESSION);
    }
}
