package ca.ulaval.glo4003.projet.base.ws.usage.api.response;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.DatesWithSameUsage;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingZoneUsageSummaryResponseAssemblerTest {

    private static final LocalDate A_DATE = LocalDate.now();
    private static final List<LocalDate> A_LIST_OF_DATES = Arrays.asList(A_DATE, A_DATE.plusDays(4));
    private static final List<LocalDate> ANOTHER_LIST_OF_DATES = Arrays.asList(A_DATE.plusDays(8), A_DATE.plusDays(12));
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final double A_MEAN_ACTIVITY = 2;
    private static final int A_USAGE = 32;
    private static final int ANOTHER_USAGE = 132;

    @Mock
    private ParkingZoneUsageSummary parkingZoneUsageSummary;

    private ParkingZoneUsageSummaryResponseAssembler parkingZoneUsageSummaryResponseAssembler;

    @BeforeEach
    void setUp() {
        parkingZoneUsageSummaryResponseAssembler = new ParkingZoneUsageSummaryResponseAssembler();
    }

    @Test
    void whenToResponse_thenReturnDailyParkingZoneSummaryResponse() {
        // given
        DatesWithSameUsage datesWithMaximumUsage = new DatesWithSameUsage(A_LIST_OF_DATES, A_USAGE);
        DatesWithSameUsage datesWithMinimumUsage = new DatesWithSameUsage(ANOTHER_LIST_OF_DATES, ANOTHER_USAGE);
        given(parkingZoneUsageSummary.getDatesWithMaximumUsage()).willReturn(datesWithMaximumUsage);
        given(parkingZoneUsageSummary.getDatesWithMinimumUsage()).willReturn(datesWithMinimumUsage);
        given(parkingZoneUsageSummary.getAverageUsage()).willReturn(A_MEAN_ACTIVITY);
        given(parkingZoneUsageSummary.getZone()).willReturn(A_PARKING_ZONE);

        // when
        ParkingZoneUsageSummaryResponse response = parkingZoneUsageSummaryResponseAssembler.toResponse(parkingZoneUsageSummary);

        // then
        DatesActivitySummaryResponse mostActiveDay = response.getMostActiveDay();
        DatesActivitySummaryResponse leastActiveDay = response.getLeastActiveDay();
        assertThat(response.getAverageDailyActivity()).isEqualTo((float) A_MEAN_ACTIVITY);
        assertThat(response.getZone()).isEqualTo(A_PARKING_ZONE);
        assertThat(mostActiveDay.getDays()).containsExactlyElementsIn(A_LIST_OF_DATES);
        assertThat(mostActiveDay.getNumberOfEntries()).isEqualTo(A_USAGE);
        assertThat(leastActiveDay.getDays()).containsExactlyElementsIn(ANOTHER_LIST_OF_DATES);
        assertThat(leastActiveDay.getNumberOfEntries()).isEqualTo(ANOTHER_USAGE);
    }
}
