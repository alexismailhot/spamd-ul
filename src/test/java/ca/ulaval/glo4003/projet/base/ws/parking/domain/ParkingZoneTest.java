package ca.ulaval.glo4003.projet.base.ws.parking.domain;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.InvalidParkingZoneException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingZoneTest {

    public static final String A_VALID_PARKING_ZONE_VALUE = "zone_2";
    public static final String AN_INVALID_PARKING_ZONE_VALUE = "zone1";

    @Test
    void givenValidParkingZoneValue_whenFromString_shouldNotThrow() {
        // then
        Assertions.assertDoesNotThrow(
            () ->
                // when
                ParkingZone.fromString(A_VALID_PARKING_ZONE_VALUE)
        );
    }

    @Test
    void givenInvalidParkingZoneValue_whenFromString_shouldThrow() {
        // then
        Assertions.assertThrows(
            InvalidParkingZoneException.class,
            // when
            () -> ParkingZone.fromString(AN_INVALID_PARKING_ZONE_VALUE)
        );
    }
}
