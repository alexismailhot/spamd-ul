package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session;

import static com.google.common.truth.Truth.*;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.LocalDate;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SessionParkingPassTest {

    private static final ParkingCode A_PARKING_CODE = new ParkingCode(1);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.of(2021);
    private static final LocalDate A_DATE_BEFORE_SESSION_START = LocalDate.of(A_YEAR.minusYears(2).getValue(), 10, 4);
    private static final LocalDate A_DATE_AFTER_SESSION_END = LocalDate.of(A_YEAR.plusYears(2).getValue(), 10, 5);
    private static final LocalDate A_DATE_IN_SESSION = LocalDate.of(A_YEAR.getValue(), 10, 5);
    private static final Session A_SESSION = new Session(A_SESSION_TYPE, A_YEAR);

    private SessionParkingPass sessionParkingPass;

    @BeforeEach
    void setUp() {
        this.sessionParkingPass = new SessionParkingPass(A_PARKING_CODE, A_PARKING_ZONE, A_SESSION);
    }

    @Test
    void givenDateIsBeforeSessionStart_whenIsValidOn_thenReturnFalse() {
        // given

        // when
        boolean isValidOn = sessionParkingPass.isValidOn(A_DATE_BEFORE_SESSION_START);

        // then
        assertThat(isValidOn).isFalse();
    }

    @Test
    void givenDateIsAfterSessionEnd_whenIsValidOn_thenReturnFalse() {
        // given

        // when
        boolean isValidOn = sessionParkingPass.isValidOn(A_DATE_AFTER_SESSION_END);

        // then
        assertThat(isValidOn).isFalse();
    }

    @Test
    void givenDateIsInSessionStart_whenIsValidOn_thenReturnTrue() {
        // given

        // when
        boolean isValidOn = sessionParkingPass.isValidOn(A_DATE_IN_SESSION);

        // then
        assertThat(isValidOn).isTrue();
    }
}
