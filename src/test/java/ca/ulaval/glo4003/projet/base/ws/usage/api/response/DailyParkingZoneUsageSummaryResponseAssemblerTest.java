package ca.ulaval.glo4003.projet.base.ws.usage.api.response;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DailyParkingZoneUsageSummaryResponseAssemblerTest {

    private static final double A_MEAN_DAILY_ACTIVITY = 2;
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;

    @Mock
    private ParkingZoneUsageSummary parkingZoneUsageSummary;

    private DailyParkingZoneUsageSummaryResponseAssembler dailyParkingZoneUsageSummaryResponseAssembler;

    @BeforeEach
    void setUp() {
        dailyParkingZoneUsageSummaryResponseAssembler = new DailyParkingZoneUsageSummaryResponseAssembler();
    }

    @Test
    void whenToResponse_thenReturnDailyParkingZoneSummaryResponse() {
        // given
        given(parkingZoneUsageSummary.getAverageUsage()).willReturn(A_MEAN_DAILY_ACTIVITY);
        given(parkingZoneUsageSummary.getZone()).willReturn(A_PARKING_ZONE);

        // when
        DailyParkingZoneUsageSummaryResponse response = dailyParkingZoneUsageSummaryResponseAssembler.toResponse(parkingZoneUsageSummary);

        // then
        assertThat(response.getActivity()).isEqualTo((int) A_MEAN_DAILY_ACTIVITY);
        assertThat(response.getZone()).isEqualTo(A_PARKING_ZONE);
    }
}
