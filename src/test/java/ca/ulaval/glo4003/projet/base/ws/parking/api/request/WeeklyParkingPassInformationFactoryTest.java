package ca.ulaval.glo4003.projet.base.ws.parking.api.request;

import static com.google.common.truth.Truth8.*;

import ca.ulaval.glo4003.projet.base.ws.parking.api.exception.NoDeliveryMethodException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WeeklyParkingPassInformationFactoryTest {

    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final DayOfWeek A_PARKING_DAY = DayOfWeek.MONDAY;
    private static final String AN_EMAIL = "antoine.lefrancois@ift.ulaval.ca";
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final String AN_ADDRESS = "123 Mackay";
    private static final String AN_APARTMENT_NUMBER = "4";
    private static final String A_STATE = "Quebec";
    private static final String A_COUNTRY = "Canada";
    private static final String A_CITY = "Quebec";
    private static final String A_POSTAL_CODE = "G1V2X1";
    private static final SessionType A_SESSION_TYPE = SessionType.SUMMER;
    private static final Year A_YEAR = Year.of(4);

    private WeeklyParkingPassInformationFactory factory;

    @BeforeEach
    void setUp() {
        factory = new WeeklyParkingPassInformationFactory(new PostalAddressAssembler());
    }

    @Test
    void givenDeliveryMethodIsEmailAndEmailProvided_whenCreate_thenWeeklyParkingPassInformationHasEmail() {
        // given
        WeeklyParkingPassRequest request = givenAWeeklyParkingPassRequest(DeliveryMethod.EMAIL, AN_EMAIL);

        // when
        WeeklyParkingPassInformation weeklyParkingPassInformation = factory.create(request);

        // then
        assertThat(weeklyParkingPassInformation.getEmail()).isPresent();
    }

    @Test
    void givenDeliveryMethodIsPostalAndPostalAddressProvided_whenCreate_thenWeeklyParkingPassInformationHasPostalAddress() {
        // given
        WeeklyParkingPassRequest request = givenAWeeklyParkingPassRequest(DeliveryMethod.POSTAL, givenAPostalAddressRequest());

        // when
        WeeklyParkingPassInformation weeklyParkingPassInformation = factory.create(request);

        // then
        assertThat(weeklyParkingPassInformation.getPostalAddress()).isPresent();
    }

    @Test
    void givenDeliveryMethodIsEmailAndEmailNotProvided_whenCreate_thenThrowNoDeliveryMethodException() {
        // given
        WeeklyParkingPassRequest request = givenAWeeklyParkingPassRequest(DeliveryMethod.EMAIL, (String) null);

        // then
        Assertions.assertThrows(
            NoDeliveryMethodException.class,
            () ->
                // when
                factory.create(request)
        );
    }

    @Test
    void givenDeliveryMethodIsPostalAndPostalAddressNotProvided_whenCreate_thenThrowNoDeliveryMethodException() {
        // given
        WeeklyParkingPassRequest request = givenAWeeklyParkingPassRequest(DeliveryMethod.POSTAL, (PostalAddressRequest) null);

        // then
        Assertions.assertThrows(
            NoDeliveryMethodException.class,
            () ->
                // when
                factory.create(request)
        );
    }

    @Test
    void givenDeliveryMethodIsSSP_whenCreate_thenWeeklyParkingPassInformationHasNoEmailOrPostalAddress() {
        // given
        WeeklyParkingPassRequest request = givenAWeeklyParkingPassRequest(DeliveryMethod.SSP);

        // when
        WeeklyParkingPassInformation weeklyParkingPassInformation = factory.create(request);

        // then
        assertThat(weeklyParkingPassInformation.getEmail()).isEmpty();
        assertThat(weeklyParkingPassInformation.getPostalAddress()).isEmpty();
    }

    private WeeklyParkingPassRequest givenAWeeklyParkingPassRequest(DeliveryMethod deliveryMethod, String email) {
        return new WeeklyParkingPassRequest(
            A_PARKING_ZONE,
            deliveryMethod,
            email,
            A_PARKING_DAY,
            givenASessionRequest(),
            givenACreditCardRequest(),
            givenAPostalAddressRequest()
        );
    }

    private WeeklyParkingPassRequest givenAWeeklyParkingPassRequest(DeliveryMethod deliveryMethod, PostalAddressRequest postalAddressRequest) {
        return new WeeklyParkingPassRequest(
            A_PARKING_ZONE,
            deliveryMethod,
            AN_EMAIL,
            A_PARKING_DAY,
            givenASessionRequest(),
            givenACreditCardRequest(),
            postalAddressRequest
        );
    }

    private WeeklyParkingPassRequest givenAWeeklyParkingPassRequest(DeliveryMethod deliveryMethod) {
        return new WeeklyParkingPassRequest(A_PARKING_ZONE, deliveryMethod, null, A_PARKING_DAY, givenASessionRequest(), givenACreditCardRequest(), null);
    }

    private PostalAddressRequest givenAPostalAddressRequest() {
        return new PostalAddressRequest(AN_ADDRESS, AN_APARTMENT_NUMBER, A_STATE, A_COUNTRY, A_CITY, A_POSTAL_CODE);
    }

    private CreditCardRequest givenACreditCardRequest() {
        return new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    }

    private SessionRequest givenASessionRequest() {
        return new SessionRequest(A_SESSION_TYPE, A_YEAR);
    }
}
