package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.rates.DeliveryMethodULavalRatesConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingPassPriceCalculatorTest {

    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final ParkingPassType A_PARKING_PASS_TYPE = ParkingPassType.ONE_DAY_A_WEEK_FOR_A_SEMESTER;
    private static final Price A_DELIVERY_PRICE = Price.of(5L);
    private static final DeliveryMethod A_DELIVERY_METHOD = DeliveryMethod.EMAIL;
    private static final Price A_PASS_PRICE = Price.of(3);

    private ParkingPassPriceCalculator parkingPassPriceCalculator;

    @Mock
    private ParkingPassRatesConfig parkingPassRatesConfig;

    @Mock
    private DeliveryMethodULavalRatesConfig deliveryMethodRatesConfig;

    @BeforeEach
    void setUp() {
        parkingPassPriceCalculator = new ParkingPassPriceCalculator(parkingPassRatesConfig, deliveryMethodRatesConfig);
    }

    @Test
    void whenCalculatePrice_thenCalculatedPriceIsSumOfDeliveryPriceAndParkingRatesForZoneAndPassType() {
        // given
        given(parkingPassRatesConfig.getParkingPassRate(A_PARKING_ZONE, A_PARKING_PASS_TYPE)).willReturn(A_PASS_PRICE);
        given(deliveryMethodRatesConfig.getDeliveryMethodRate(A_DELIVERY_METHOD)).willReturn(A_DELIVERY_PRICE);

        // when
        Price price = parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, A_PARKING_PASS_TYPE, A_DELIVERY_METHOD);

        // then
        assertThat(price).isEqualTo(A_PASS_PRICE.add(A_DELIVERY_PRICE));
    }
}
