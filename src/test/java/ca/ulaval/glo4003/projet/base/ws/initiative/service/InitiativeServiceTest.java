package ca.ulaval.glo4003.projet.base.ws.initiative.service;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.CarbonCreditConfig;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeFactory;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.exception.InitiativeNotFoundException;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingAmountValidator;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundsCalculator;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InitiativeServiceTest {

    private static final String AN_INITIATIVE_NAME = "Initiative ULAVAL";
    private static final Price AN_INITIATIVE_AMOUNT = Price.of(420);
    private static final Price A_REQUESTED_AMOUNT = Price.of(600);
    private static final InitiativeCode AN_INITIATIVE_CODE = new InitiativeCode("A_CODE");
    private static final String ANOTHER_INITIATIVE_NAME = "Another initiative";
    private static final Price ANOTHER_INITIATIVE_AMOUNT = Price.of(100);
    private static final InitiativeCode ANOTHER_INITIATIVE_CODE = new InitiativeCode("ANOTHER_CODE");
    private static final Initiative AN_INITIATIVE = new Initiative(AN_INITIATIVE_NAME, AN_INITIATIVE_AMOUNT, AN_INITIATIVE_CODE);
    private static final Initiative ANOTHER_INITIATIVE = new Initiative(ANOTHER_INITIATIVE_NAME, ANOTHER_INITIATIVE_AMOUNT, ANOTHER_INITIATIVE_CODE);
    public static final Price AN_INITATIVE_AMOUNT_OF_ZERO = Price.of(0);
    private static final Price AN_AVAILABLE_FUNDS_AMOUNT = Price.of(500);
    private static final Price A_TOTAL_INCOME = Price.of(5433);
    private static final Price AN_OFFENSE_INCOME = Price.of(56);
    private static final Price AN_ALLOCATED_FUNDS_AMOUNT = Price.of(3211);
    private static final Price AN_INITIATIVE_PRICE = Price.of(43);

    private InitiativeService initiativeService;

    @Mock
    private InitiativeFactory initiativeFactory;

    @Mock
    private InitiativeRepository initiativeRepository;

    @Mock
    private InitiativeFundingAmountValidator initiativeFundingAmountValidator;

    @Mock
    private InitiativeFundsCalculator initiativeFundCalculator;

    @Mock
    private IncomeService incomeService;

    @Mock
    private Initiative mockedInitiative;

    @Mock
    private CarbonCreditConfig carbonCreditConfig;

    @BeforeEach
    void setup() {
        initiativeService =
            new InitiativeService(
                initiativeFactory,
                initiativeFundingAmountValidator,
                initiativeRepository,
                initiativeFundCalculator,
                incomeService,
                carbonCreditConfig
            );
    }

    @Test
    void whenCreateInitiative_thenAValidationIsDoneToMakeSureEnoughFundsAreAvailableForNewInitiative() {
        // given
        given(initiativeRepository.getAll()).willReturn(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE));
        given(initiativeFundCalculator.calculateFundsAvailable(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE), A_TOTAL_INCOME))
            .willReturn(AN_AVAILABLE_FUNDS_AMOUNT);
        given(incomeService.getTotalIncome()).willReturn(A_TOTAL_INCOME);
        given(initiativeFactory.create(AN_INITIATIVE_NAME, AN_INITIATIVE_AMOUNT)).willReturn(AN_INITIATIVE);

        // when
        initiativeService.createInitiative(AN_INITIATIVE_NAME, AN_INITIATIVE_AMOUNT);

        // then
        verify(initiativeFundingAmountValidator).validateAmount(AN_AVAILABLE_FUNDS_AMOUNT, AN_INITIATIVE_AMOUNT);
    }

    @Test
    void whenCreateInitiative_thenInitiativeRepositoryIsCalledToSaveInitiative() {
        // given
        given(initiativeFactory.create(AN_INITIATIVE_NAME, AN_INITIATIVE_AMOUNT)).willReturn(AN_INITIATIVE);

        // when
        initiativeService.createInitiative(AN_INITIATIVE_NAME, AN_INITIATIVE_AMOUNT);

        // then
        verify(initiativeRepository).save(AN_INITIATIVE);
    }

    @Test
    void whenCreateInitiative_thenCreatedInitiativeCodeIsReturned() {
        // given
        given(initiativeFactory.create(AN_INITIATIVE_NAME, AN_INITIATIVE_AMOUNT)).willReturn(AN_INITIATIVE);

        // when
        InitiativeCode initiativeCode = initiativeService.createInitiative(AN_INITIATIVE_NAME, AN_INITIATIVE_AMOUNT);

        // then
        assertThat(initiativeCode).isEqualTo(AN_INITIATIVE.getInitiativeCode());
    }

    @Test
    void givenNoInitiativeFound_whenFundInitiative_thenThrowInitiativeNotFoundForInitiativeCodeException() {
        // given
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.empty());

        // then
        assertThrows(
            InitiativeNotFoundException.class,
            () ->
                // when
                initiativeService.fundInitiative(AN_INITIATIVE_CODE, AN_INITIATIVE_AMOUNT)
        );
    }

    @Test
    void givenAnInitiativeFound_whenFundInitiative_thenInitiativeAmountValidatorIsCalledToValidateAmount() {
        // given
        given(initiativeRepository.getAll()).willReturn(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE));
        given(incomeService.getTotalIncome()).willReturn(A_TOTAL_INCOME);
        given(initiativeFundCalculator.calculateFundsAvailable(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE), A_TOTAL_INCOME))
            .willReturn(AN_AVAILABLE_FUNDS_AMOUNT);
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.of(AN_INITIATIVE));

        // when
        initiativeService.fundInitiative(AN_INITIATIVE_CODE, A_REQUESTED_AMOUNT);

        // then
        verify(initiativeFundingAmountValidator).validateAmount(AN_AVAILABLE_FUNDS_AMOUNT, A_REQUESTED_AMOUNT);
    }

    @Test
    void givenAnInitiativeFoundAndFundsAreAvailable_whenFundInitiative_thenRequestedAmountIsAddedToInitiative() {
        // given
        given(initiativeRepository.getAll()).willReturn(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE));
        given(incomeService.getTotalIncome()).willReturn(A_TOTAL_INCOME);
        given(initiativeFundCalculator.calculateFundsAvailable(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE), A_TOTAL_INCOME))
            .willReturn(AN_AVAILABLE_FUNDS_AMOUNT);
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.of(mockedInitiative));

        // when
        initiativeService.fundInitiative(AN_INITIATIVE_CODE, A_REQUESTED_AMOUNT);

        // then
        verify(mockedInitiative).fund(A_REQUESTED_AMOUNT);
    }

    @Test
    void givenAnInitiativeFoundAndFundsAreAvailable_whenFundInitiative_thenInitiativeWithAddedAmountIsSaved() {
        // given
        given(initiativeRepository.getAll()).willReturn(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE));
        given(incomeService.getTotalIncome()).willReturn(A_TOTAL_INCOME);
        given(initiativeFundCalculator.calculateFundsAvailable(Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE), A_TOTAL_INCOME))
            .willReturn(AN_AVAILABLE_FUNDS_AMOUNT);
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.of(AN_INITIATIVE));

        // when
        initiativeService.fundInitiative(AN_INITIATIVE_CODE, A_REQUESTED_AMOUNT);

        // then
        verify(initiativeRepository).save(AN_INITIATIVE);
    }

    @Test
    void givenAnInitiativeFound_whenGetInitiative_thenInitiativeIsReturned() {
        // given
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.of(AN_INITIATIVE));

        // when
        Initiative initiative = initiativeService.getInitiative(AN_INITIATIVE_CODE);

        // then
        assertThat(initiative).isEqualTo(AN_INITIATIVE);
    }

    @Test
    void givenNoInitiativeFound_whenGetInitiative_thenThrowInitiativeNotFoundException() {
        // given
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.empty());

        // then
        assertThrows(
            InitiativeNotFoundException.class,
            () ->
                // when
                initiativeService.getInitiative(AN_INITIATIVE_CODE)
        );
    }

    @Test
    void whenGetInitiativeFundsFromOffenseIncome_thenReturnAllocatedAmount() {
        // given
        given(incomeService.getTotalOffenseIncome()).willReturn(AN_OFFENSE_INCOME);
        given(initiativeFundCalculator.calculateFundsAllocatedToAllInitiatives(AN_OFFENSE_INCOME)).willReturn(AN_ALLOCATED_FUNDS_AMOUNT);

        // when
        Price initiativeFundsFromOffenseIncome = initiativeService.getInitiativeFundsFromOffenseIncome();

        //then
        assertThat(initiativeFundsFromOffenseIncome).isEqualTo(AN_ALLOCATED_FUNDS_AMOUNT);
    }

    @Test
    void whenGetAllInitiativeFundsFromOffenseIncomeForCurrentUniversityYear_thenReturnAllocatedAmount() {
        // given
        given(incomeService.getTotalOffenseIncomeForCurrentUniversityYear()).willReturn(AN_OFFENSE_INCOME);
        given(initiativeFundCalculator.calculateFundsAllocatedToAllInitiatives(AN_OFFENSE_INCOME)).willReturn(AN_ALLOCATED_FUNDS_AMOUNT);

        // when
        Price initiativeFundsFromOffenseIncome = initiativeService.getInitiativeFundsFromOffenseIncomeForCurrentUniversityYear();

        //then
        assertThat(initiativeFundsFromOffenseIncome).isEqualTo(AN_ALLOCATED_FUNDS_AMOUNT);
    }

    @Test
    void whenSpendRemainingFundsOnInitiative_thenRemainingFundsAreSpentOnInitiative() {
        // given
        List<Initiative> initiatives = Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE);
        given(initiativeRepository.getAll()).willReturn(initiatives);
        given(incomeService.getTotalIncome()).willReturn(A_TOTAL_INCOME);
        given(initiativeFundCalculator.calculateFundsAvailable(initiatives, A_TOTAL_INCOME)).willReturn(AN_AVAILABLE_FUNDS_AMOUNT);
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.of(mockedInitiative));
        given(carbonCreditConfig.getCarbonCreditInitiativeCode()).willReturn(AN_INITIATIVE_CODE);

        // when
        initiativeService.spendRemainingFundsOnCarbonCreditInitiatives();

        // then
        verify(mockedInitiative).fund(AN_AVAILABLE_FUNDS_AMOUNT);
    }

    @Test
    void whenSpendRemainingFundsOnCreditInitiative_thenInitiativeWithAddedAmountIsSaved() {
        // given
        List<Initiative> initiatives = Arrays.asList(AN_INITIATIVE, ANOTHER_INITIATIVE);
        given(carbonCreditConfig.getCarbonCreditInitiativeCode()).willReturn(AN_INITIATIVE_CODE);
        given(initiativeRepository.getAll()).willReturn(initiatives);
        given(incomeService.getTotalIncome()).willReturn(A_TOTAL_INCOME);
        given(initiativeFundCalculator.calculateFundsAvailable(initiatives, A_TOTAL_INCOME)).willReturn(AN_AVAILABLE_FUNDS_AMOUNT);
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.of(AN_INITIATIVE));

        // when
        initiativeService.spendRemainingFundsOnCarbonCreditInitiatives();

        // then
        verify(initiativeRepository).save(AN_INITIATIVE);
    }

    @Test
    void givenCarbonCreditInitiativeRemainingFunds_whenGetNumberOfCarbonCreditsBought_shouldReturnCorrespondingCarbonCreditAmount() {
        // given
        given(carbonCreditConfig.getCarbonCreditInitiativeCode()).willReturn(AN_INITIATIVE_CODE);
        given(carbonCreditConfig.getUnitPrice()).willReturn(AN_INITIATIVE_PRICE);
        given(initiativeRepository.findByInitiativeCode(AN_INITIATIVE_CODE)).willReturn(Optional.of(givenAnInitiative(AN_INITIATIVE_AMOUNT)));

        // when
        double numberOfCarbonCreditsBought = initiativeService.getNumberOfCarbonCreditsBought();

        //then
        double expectedNumberOfCarbonCreditBought = AN_INITIATIVE_AMOUNT.dividedBy(carbonCreditConfig.getUnitPrice());
        assertThat(numberOfCarbonCreditsBought).isEqualTo(expectedNumberOfCarbonCreditBought);
    }

    private Initiative givenAnInitiative(Price fundedAmount) {
        return new Initiative(AN_INITIATIVE_NAME, fundedAmount, AN_INITIATIVE_CODE);
    }
}
