package ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.exception.InvalidVehicleYearException;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Sex;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class VehicleYearValidatorTest {

    private static final Integer A_VALID_VEHICLE_YEAR = 2017;
    private static final Integer A_TOO_OLD_VEHICLE_YEAR = 1870;
    private static final Integer AN_INVALID_FUTURISTIC_VEHICLE_YEAR = LocalDate.now().getYear() + 25;
    private static final Integer A_MINIMUM_VEHICLE_YEAR = 1900;
    private static final Integer A_MAXIMUM_VEHICLE_YEAR = LocalDate.now().getYear() + 1;
    private static final String A_NAME = "name";
    private static final Sex A_SEX = Sex.FEMALE;
    private static final String A_VEHICLE_MODEL = "vehicle model";
    private static final String A_VEHICLE_BRAND = "vehicle brand";
    private static final LocalDate A_BIRTHDATE = LocalDate.of(5, 3, 4);
    private static final DayOfWeek AN_ACCESS_DAY = DayOfWeek.FRIDAY;
    private static final String A_VEHICLE_PLATE_NUMBER = "vehicle plate number";
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.now();

    private VehicleYearValidator vehicleYearValidator;

    @Mock
    private AccessInformationValidator nextAccessInformationValidator;

    @BeforeEach
    void setUp() {
        vehicleYearValidator = new VehicleYearValidator(nextAccessInformationValidator, A_MINIMUM_VEHICLE_YEAR, A_MAXIMUM_VEHICLE_YEAR);
    }

    @Test
    void givenAnAccessInformationWithAValidVehicleYear_whenValidate_thenDoesNotThrowInvalidVehicleYearException() {
        // given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(A_VALID_VEHICLE_YEAR);

        // then
        assertDoesNotThrow(
            () ->
                //when
                vehicleYearValidator.validate(weeklyAccessInformation)
        );
    }

    @Test
    void givenAnAccessInformationWithAValidVehicleYear_whenValidate_thenNextValidatorIsCalled() {
        // given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(A_VALID_VEHICLE_YEAR);

        // when
        vehicleYearValidator.validate(weeklyAccessInformation);

        // then
        verify(nextAccessInformationValidator).validate(weeklyAccessInformation);
    }

    @Test
    void givenAnAccessInformationWithATooOldVehicleYear_whenValidate_thenThrowInvalidVehicleYearException() {
        //given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(A_TOO_OLD_VEHICLE_YEAR);

        //then
        assertThrows(
            InvalidVehicleYearException.class,
            () ->
                //when
                vehicleYearValidator.validate(weeklyAccessInformation)
        );
    }

    @Test
    void givenAnAccessInformationWithAVehicleYearTooMuchInTheFuture_whenValidate_thenThrowInvalidVehicleYearException() {
        //given
        WeeklyAccessInformation weeklyAccessInformation = givenAnAccessInformation(AN_INVALID_FUTURISTIC_VEHICLE_YEAR);

        //then
        assertThrows(
            InvalidVehicleYearException.class,
            () ->
                //when
                vehicleYearValidator.validate(weeklyAccessInformation)
        );
    }

    private WeeklyAccessInformation givenAnAccessInformation(Integer vehicleYear) {
        return new WeeklyAccessInformation(
            new Driver(A_NAME, A_BIRTHDATE, A_SEX),
            new Vehicle(A_VEHICLE_MODEL, A_VEHICLE_BRAND, vehicleYear, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE),
            AN_ACCESS_DAY,
            new Session(A_SESSION_TYPE, A_YEAR)
        );
    }
}
