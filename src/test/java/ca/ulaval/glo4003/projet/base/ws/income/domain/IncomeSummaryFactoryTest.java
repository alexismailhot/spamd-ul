package ca.ulaval.glo4003.projet.base.ws.income.domain;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IncomeSummaryFactoryTest {

    private static final Price AN_ACCESS_PRICE = Price.of(6);
    private static final Price ANOTHER_ACCESS_PRICE = Price.of(32);
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final LocalDateTime A_TIME_CREATED = LocalDateTime.now();
    private static final Price A_PARKING_PASS_PRICE = Price.of(65);
    private static final Price ANOTHER_PARKING_PASS_PRICE = Price.of(352);

    private IncomeSummaryFactory incomeSummaryFactory;

    @BeforeEach
    void setUp() {
        incomeSummaryFactory = new IncomeSummaryFactory();
    }

    @Test
    void whenCreateFromAccessIncome_thenReturnIncomeSummaryWithNumberOfSalesEqualToNumberOfAccessIncomes() {
        // given
        List<AccessIncome> accessIncomes = Arrays.asList(givenAnAccessIncome(AN_ACCESS_PRICE), givenAnAccessIncome(ANOTHER_ACCESS_PRICE));

        // when
        IncomeSummary incomeSummary = incomeSummaryFactory.createFromAccessIncome(accessIncomes);

        // then
        int expectedNumberOfSales = accessIncomes.size();
        assertThat(incomeSummary.getSales()).isEqualTo(expectedNumberOfSales);
    }

    @Test
    void whenCreateFromAccessIncome_thenReturnIncomeSummaryWithTotalIncomeEqualToSumOfAccessIncomes() {
        // given
        List<AccessIncome> accessIncomes = Arrays.asList(givenAnAccessIncome(AN_ACCESS_PRICE), givenAnAccessIncome(ANOTHER_ACCESS_PRICE));

        // when
        IncomeSummary incomeSummary = incomeSummaryFactory.createFromAccessIncome(accessIncomes);

        // then
        Price expectedTotalIncome = AN_ACCESS_PRICE.add(ANOTHER_ACCESS_PRICE);
        assertThat(incomeSummary.getTotalIncome()).isEqualTo(expectedTotalIncome);
    }

    @Test
    void whenCreateFromParkingIncome_thenReturnIncomeSummaryWithNumberOfSalesEqualToNumberOfParkingIncomes() {
        // given
        List<ParkingPassIncome> parkingPassIncomes = Arrays.asList(
            givenAParkingPassIncome(A_PARKING_PASS_PRICE),
            givenAParkingPassIncome(ANOTHER_PARKING_PASS_PRICE)
        );

        // when
        IncomeSummary incomeSummary = incomeSummaryFactory.createFromParkingIncome(parkingPassIncomes);

        // then
        int expectedNumberOfSales = parkingPassIncomes.size();
        assertThat(incomeSummary.getSales()).isEqualTo(expectedNumberOfSales);
    }

    @Test
    void whenCreateFromParkingIncome_thenReturnIncomeSummaryWithTotalIncomeEqualToSumOfParkingIncomes() {
        // given
        List<ParkingPassIncome> parkingPassIncomes = Arrays.asList(
            givenAParkingPassIncome(A_PARKING_PASS_PRICE),
            givenAParkingPassIncome(ANOTHER_PARKING_PASS_PRICE)
        );

        // when
        IncomeSummary incomeSummary = incomeSummaryFactory.createFromParkingIncome(parkingPassIncomes);

        // then
        Price expectedTotalIncome = A_PARKING_PASS_PRICE.add(ANOTHER_PARKING_PASS_PRICE);
        assertThat(incomeSummary.getTotalIncome()).isEqualTo(expectedTotalIncome);
    }

    private AccessIncome givenAnAccessIncome(Price accessPrice) {
        return new AccessIncome(accessPrice, A_VEHICLE_TYPE, A_TIME_CREATED);
    }

    private ParkingPassIncome givenAParkingPassIncome(Price parkingPassPrice) {
        return new ParkingPassIncome(parkingPassPrice, A_TIME_CREATED);
    }
}
