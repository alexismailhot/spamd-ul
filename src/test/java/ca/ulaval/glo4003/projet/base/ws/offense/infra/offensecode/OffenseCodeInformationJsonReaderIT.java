package ca.ulaval.glo4003.projet.base.ws.offense.infra.offensecode;

import static com.google.common.truth.Truth.*;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformation;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class OffenseCodeInformationJsonReaderIT {

    private static final String AN_INVALID_PATH = "not/a/valid/file";
    private static final String OFFENSE_CODES_FILE_PATH = "src/test/java/ca/ulaval/glo4003/projet/base/ws/offense/infra/offensecode/infraction-test.json";

    private OffenseCodeInformationJsonReader offenseCodeInformationJsonReader;

    @Test
    void givenFileDoesNotExist_whenRead_thenThrowFileNotFoundException() {
        // given
        offenseCodeInformationJsonReader = new OffenseCodeInformationJsonReader(AN_INVALID_PATH, new OffenseCodeInformationAssembler());

        // then
        Assertions.assertThrows(
            FileNotFoundException.class,
            () ->
                // when
                offenseCodeInformationJsonReader.read()
        );
    }

    @Test
    void givenInfractionTestFile_whenRead_thenReturnCorrectOffenseCodeInformations() throws IOException {
        // given
        offenseCodeInformationJsonReader = new OffenseCodeInformationJsonReader(OFFENSE_CODES_FILE_PATH, new OffenseCodeInformationAssembler());

        // when
        List<OffenseCodeInformation> offenseCodeInformations = offenseCodeInformationJsonReader.read();

        // then
        assertThat(offenseCodeInformations).hasSize(2);

        OffenseCodeInformation firstExpected = offenseCodeInformations.get(0);
        assertThat(firstExpected.getAmount()).isEqualTo(Price.of((double) 55));
        assertThat(firstExpected.getOffense()).isEqualTo("mauvaise zone");
        assertThat(firstExpected.getOffenseCode()).isEqualTo(OffenseCode.ZONE_01);

        OffenseCodeInformation secondExpected = offenseCodeInformations.get(1);
        assertThat(secondExpected.getAmount()).isEqualTo(Price.of((double) 22));
        assertThat(secondExpected.getOffense()).isEqualTo("vignette pas admissible pour la journée");
        assertThat(secondExpected.getOffenseCode()).isEqualTo(OffenseCode.VIG_01);
    }
}
