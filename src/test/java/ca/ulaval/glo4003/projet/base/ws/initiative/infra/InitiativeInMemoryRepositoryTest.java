package ca.ulaval.glo4003.projet.base.ws.initiative.infra;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth8.assertThat;

import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeCode;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InitiativeInMemoryRepositoryTest {

    private static final InitiativeCode AN_INITIATIVE_CODE = new InitiativeCode("A_CODE");
    private static final Price A_PRICE = Price.of(1000);
    private static final String A_NAME = "A name";
    private static final Initiative AN_INITIATIVE = new Initiative(A_NAME, A_PRICE, AN_INITIATIVE_CODE);
    private static final InitiativeCode ANOTHER_INITIATIVE_CODE = new InitiativeCode("ANOTHER_CODE");
    private static final Price ANOTHER_PRICE = Price.of(2000);
    private static final String ANOTHER_NAME = "another name";
    private static final Initiative ANOTHER_INITIATIVE = new Initiative(ANOTHER_NAME, ANOTHER_PRICE, ANOTHER_INITIATIVE_CODE);

    private InitiativeInMemoryRepository initiativeInMemoryRepository;

    @BeforeEach
    void setUp() {
        initiativeInMemoryRepository = new InitiativeInMemoryRepository();
    }

    @Test
    void givenInitiativesSaved_whenGetAll_theReturnAllInitiatives() {
        // given
        initiativeInMemoryRepository.save(AN_INITIATIVE);
        initiativeInMemoryRepository.save(ANOTHER_INITIATIVE);

        // when
        List<Initiative> initiatives = initiativeInMemoryRepository.getAll();

        // then
        assertThat(initiatives).isEqualTo(Arrays.asList(ANOTHER_INITIATIVE, AN_INITIATIVE));
    }

    @Test
    void givenAssociatedInitiativeWasSaved_whenFindByInitiativeCode_thenReturnInitiative() {
        // given
        givenInitiativesInRepository();

        // when
        Optional<Initiative> initiative = initiativeInMemoryRepository.findByInitiativeCode(AN_INITIATIVE_CODE);

        // then
        assertThat(initiative.get()).isEqualTo(AN_INITIATIVE);
    }

    @Test
    void givenNoAssociatedInitiativeWasSaved_whenFindByInitiativeCode_thenReturnEmptyOptional() {
        // given

        // when
        Optional<Initiative> initiative = initiativeInMemoryRepository.findByInitiativeCode(AN_INITIATIVE_CODE);

        // then
        assertThat(initiative).isEmpty();
    }

    private void givenInitiativesInRepository() {
        initiativeInMemoryRepository.save(AN_INITIATIVE);
        initiativeInMemoryRepository.save(ANOTHER_INITIATIVE);
    }
}
