package ca.ulaval.glo4003.projet.base.ws.usage.api;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequest;
import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.DailyParkingZoneUsageSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.DailyParkingZoneUsageSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.ParkingZoneUsageSummaryResponse;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.ParkingZoneUsageSummaryResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntryRepository;
import ca.ulaval.glo4003.projet.base.ws.usage.infra.parking.ParkingEntryInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.usage.service.UsageService;
import java.time.*;
import java.util.List;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UsageResourceIT {

    private static final Instant NOW = Instant.now();
    private static final Year CURRENT_YEAR = Year.of(NOW.atZone(ZoneId.systemDefault()).getYear());
    private static final Month CURRENT_MONTH = NOW.atZone(ZoneId.systemDefault()).getMonth();
    private static final String A_PARKING_ZONE_STRING = "Zone_1";
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.fromString(A_PARKING_ZONE_STRING);
    private static final AddUsageRequest AN_ADD_USAGE_REQUEST = new AddUsageRequest(A_PARKING_ZONE, LocalDate.now());
    private static final AddUsageRequest AN_ADD_USAGE_REQUEST_OUTSIDE_REQUESTED_DATES = new AddUsageRequest(A_PARKING_ZONE, LocalDate.MIN);
    private static final int ONE_USAGE = 1;
    private static final ParkingZone ANOTHER_PARKING_ZONE = ParkingZone.ZONE_2;
    private static final AddUsageRequest ANOTHER_ADD_USAGE_REQUEST = new AddUsageRequest(ANOTHER_PARKING_ZONE, LocalDate.now());

    private UsageResource usageResource;

    @Mock
    Clock clock;

    @BeforeEach
    void setUp() {
        ParkingEntryRepository parkingEntryRepository = new ParkingEntryInMemoryRepository();
        UsageService usageService = new UsageService(parkingEntryRepository);
        ParkingZoneUsageSummaryResponseAssembler parkingZoneUsageSummaryResponseAssembler = new ParkingZoneUsageSummaryResponseAssembler();
        AddUsageRequestAssembler addUsageRequestAssembler = new AddUsageRequestAssembler();
        DailyParkingZoneUsageSummaryResponseAssembler dailyParkingZoneUsageSummaryResponseAssembler = new DailyParkingZoneUsageSummaryResponseAssembler();
        usageResource =
            new UsageResourceImpl(
                usageService,
                parkingZoneUsageSummaryResponseAssembler,
                clock,
                addUsageRequestAssembler,
                dailyParkingZoneUsageSummaryResponseAssembler
            );
    }

    @Test
    void givenAddUsage_whenGetDailyUsage_thenReturnUsageForRequestedDay() {
        // given
        givenAddUsage();
        givenAddUsageForAnotherParkingZone();
        givenAddUsageThatShouldNotBeReturned();

        // when
        Response response = usageResource.getDailyUsage(A_PARKING_ZONE_STRING, LocalDate.now());

        // then
        DailyParkingZoneUsageSummaryResponse dailyParkingZoneUsageSummaryResponse = (DailyParkingZoneUsageSummaryResponse) response.getEntity();
        assertThat(dailyParkingZoneUsageSummaryResponse.getZone()).isEqualTo(A_PARKING_ZONE);
        assertThat(dailyParkingZoneUsageSummaryResponse.getActivity()).isEqualTo(1);
    }

    @Test
    void givenAddUsage_whenGetMonthlyUsage_thenReturnUsageForRequestedMonth() {
        // given
        givenAddUsage();
        givenAddUsageForAnotherParkingZone();
        givenNowClock();

        // when
        Response response = usageResource.getMonthlyUsage(A_PARKING_ZONE_STRING, CURRENT_MONTH, CURRENT_YEAR);

        // then
        ParkingZoneUsageSummaryResponse parkingZoneUsageSummaryResponse = (ParkingZoneUsageSummaryResponse) response.getEntity();
        assertThat(parkingZoneUsageSummaryResponse.getZone()).isEqualTo(A_PARKING_ZONE);
        assertThat(parkingZoneUsageSummaryResponse.getMostActiveDay().getDays().contains(AN_ADD_USAGE_REQUEST.getDate())).isTrue();
    }

    @Test
    void givenAddUsage_whenGetPeriodUsage_thenReturnUsageForRequestedPeriod() {
        // given
        givenAddUsage();
        givenAddUsageForAnotherParkingZone();
        givenAddUsageThatShouldNotBeReturned();

        // when
        Response response = usageResource.getPeriodUsage(A_PARKING_ZONE_STRING, LocalDate.now().minusDays(1), LocalDate.now());

        // then
        ParkingZoneUsageSummaryResponse parkingZoneUsageSummaryResponse = (ParkingZoneUsageSummaryResponse) response.getEntity();
        assertThat(parkingZoneUsageSummaryResponse.getZone()).isEqualTo(A_PARKING_ZONE);
        assertThat(parkingZoneUsageSummaryResponse.getMostActiveDay().getNumberOfEntries()).isAtLeast(ONE_USAGE);
        assertThat(parkingZoneUsageSummaryResponse.getMostActiveDay().getDays().contains(AN_ADD_USAGE_REQUEST.getDate())).isTrue();
    }

    @Test
    void givenAddUsageWithDifferentParkingZones_whenGetAllDailyUsage_thenReturnUsageFormRequestDay() {
        // given
        givenAddUsage();
        givenAddUsageForAnotherParkingZone();
        givenAddUsageThatShouldNotBeReturned();

        // when
        Response response = usageResource.getAllZonesDailyUsage(LocalDate.now());

        // then
        List<DailyParkingZoneUsageSummaryResponse> dailyParkingZoneUsageSummaryResponse = (List<DailyParkingZoneUsageSummaryResponse>) response.getEntity();
        assertThat(dailyParkingZoneUsageSummaryResponse.get(0).getActivity()).isEqualTo(ONE_USAGE);
        assertThat(dailyParkingZoneUsageSummaryResponse.get(1).getActivity()).isEqualTo(ONE_USAGE);
    }

    @Test
    void givenAddUsageWithDifferentParkingZones_whenGetMonthlyUsage_thenReturnUsageForRequestMonth() {
        // given
        givenAddUsage();
        givenAddUsageForAnotherParkingZone();
        givenNowClock();

        // when
        Response response = usageResource.getAllZonesMonthlyUsage(CURRENT_MONTH, CURRENT_YEAR);

        // then
        List<ParkingZoneUsageSummaryResponse> parkingZoneUsageSummaryResponse = (List<ParkingZoneUsageSummaryResponse>) response.getEntity();
        assertThat(parkingZoneUsageSummaryResponse.get(0).getMostActiveDay().getDays().contains(AN_ADD_USAGE_REQUEST.getDate())).isTrue();
        assertThat(parkingZoneUsageSummaryResponse.get(1).getMostActiveDay().getDays().contains(ANOTHER_ADD_USAGE_REQUEST.getDate())).isTrue();
        assertThat(parkingZoneUsageSummaryResponse.get(0).getMostActiveDay().getDays().contains(AN_ADD_USAGE_REQUEST_OUTSIDE_REQUESTED_DATES.getDate()))
            .isFalse();
    }

    @Test
    void givenAddUsageWithDifferentParkingZones_whenGetPeriodUsage_thenUsageForThatPeriodShouldBeReturned() {
        // given
        givenAddUsage();
        givenAddUsageForAnotherParkingZone();
        givenAddUsageThatShouldNotBeReturned();

        // when
        Response response = usageResource.getAllZonesPeriodUsage(LocalDate.now().minusDays(1), LocalDate.now());

        // then
        List<ParkingZoneUsageSummaryResponse> parkingZoneUsageSummaryResponse = (List<ParkingZoneUsageSummaryResponse>) response.getEntity();
        assertThat(parkingZoneUsageSummaryResponse.get(0).getMostActiveDay().getDays().contains(AN_ADD_USAGE_REQUEST.getDate())).isTrue();
        assertThat(parkingZoneUsageSummaryResponse.get(1).getMostActiveDay().getDays().contains(ANOTHER_ADD_USAGE_REQUEST.getDate())).isTrue();
        assertThat(parkingZoneUsageSummaryResponse.get(0).getMostActiveDay().getDays().contains(AN_ADD_USAGE_REQUEST_OUTSIDE_REQUESTED_DATES.getDate()))
            .isFalse();
    }

    private void givenNowClock() {
        given(clock.instant()).willReturn(NOW);
    }

    private Response givenAddUsageForAnotherParkingZone() {
        return usageResource.addUsage(ANOTHER_ADD_USAGE_REQUEST);
    }

    private Response givenAddUsage() {
        return usageResource.addUsage(AN_ADD_USAGE_REQUEST);
    }

    private Response givenAddUsageThatShouldNotBeReturned() {
        return usageResource.addUsage(AN_ADD_USAGE_REQUEST_OUTSIDE_REQUESTED_DATES);
    }
}
