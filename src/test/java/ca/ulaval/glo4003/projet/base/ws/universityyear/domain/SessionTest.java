package ca.ulaval.glo4003.projet.base.ws.universityyear.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

import static com.google.common.truth.Truth.assertThat;

class SessionTest {

    private static final Year A_YEAR = Year.of(5);
    private static final Month A_MONTH = Month.SEPTEMBER;
    private static final int A_DAY = 1;
    private static final LocalDate A_DATE_AFTER_SESSION_END = LocalDate.of(A_YEAR.plusYears(1).getValue(), A_MONTH, A_DAY);
    private static final LocalDate A_DATE_BEFORE_SESSION_START_DATE = LocalDate.of(A_YEAR.minusYears(2).getValue(), A_MONTH, A_DAY);
    private static final int A_FALL_MONTH = 9;
    private static final int A_WINTER_MONTH = 4;
    private static final int A_SUMMER_MONTH = 7;

    private Session session;

    @BeforeEach
    void setUp() {
        SessionType sessionType = SessionType.FALL;
        session = new Session(sessionType, A_YEAR);
    }

    @Test
    void givenDateIsBeforeSessionStartDate_whenIncludes_thenReturnFalse() {
        // given

        // when
        boolean includes = session.includes(A_DATE_BEFORE_SESSION_START_DATE);

        // then
        assertThat(includes).isFalse();
    }

    @Test
    void givenDateIsMoreThanFourMonthsAfterSessionStartDate_whenIncludes_thenReturnFalse() {
        // given

        // when
        boolean includes = session.includes(A_DATE_AFTER_SESSION_END);

        // then
        assertThat(includes).isFalse();
    }

    @Test
    void givenDateIsFourMonthsAfterSessionStartDate_whenIncludes_thenReturnFalse() {
        // given

        // when
        boolean includes = session.includes(LocalDate.of(A_YEAR.getValue(), A_MONTH.plus(4), A_DAY));

        // then
        assertThat(includes).isFalse();
    }

    @Test
    void givenDateIsSessionStartDateOrInFourMonthsAfter_whenIncludes_thenReturnTrue() {
        // given

        // when
        boolean includes = session.includes(LocalDate.of(A_YEAR.getValue(), A_MONTH.plus(2), A_DAY));

        // then
        assertThat(includes).isTrue();
    }

    @Test
    void givenFallSession_whenNext_thenNextSessionIsWinterSessionOfNextYear() {
        // given
        Session session = new Session(SessionType.FALL, A_YEAR);

        // when
        Session nextSession = session.next();

        // then
        assertThat(nextSession.getYear()).isEqualTo(A_YEAR.plusYears(1));
        assertThat(nextSession.getType()).isEqualTo(SessionType.WINTER);
    }

    @Test
    void givenWinterSession_whenNext_thenNextSessionIsSummerSessionOfSameYear() {
        // given
        Session session = new Session(SessionType.WINTER, A_YEAR);

        // when
        Session nextSession = session.next();

        // then
        assertThat(nextSession.getYear()).isEqualTo(A_YEAR);
        assertThat(nextSession.getType()).isEqualTo(SessionType.SUMMER);
    }

    @Test
    void givenSummerSession_whenNext_thenNextSessionIsFallSessionOfSameYear() {
        // given
        Session session = new Session(SessionType.SUMMER, A_YEAR);

        // when
        Session nextSession = session.next();

        assertThat(nextSession.getYear()).isEqualTo(A_YEAR);
        assertThat(nextSession.getType()).isEqualTo(SessionType.FALL);
    }

    @Test
    void givenFallSession_whenGetUniversityYear_thenUniversityYearIsOfSameYear() {
        // given
        Session session = new Session(SessionType.FALL, A_YEAR);

        // when
        UniversityYear universityYear = session.getUniversityYear();

        assertThat(universityYear).isEqualTo(new UniversityYear(session, new Session(SessionType.WINTER, A_YEAR.plusYears(1)), new Session(SessionType.SUMMER, A_YEAR.plusYears(1))));
    }

    @Test
    void givenWinterSession_whenGetUniversityYear_thenUniversityYearIsOfSameYear() {
        // given
        Session session = new Session(SessionType.WINTER, A_YEAR);

        // when
        UniversityYear universityYear = session.getUniversityYear();

        assertThat(universityYear).isEqualTo(new UniversityYear(new Session(SessionType.FALL, A_YEAR.minusYears(1)), session, new Session(SessionType.SUMMER, A_YEAR)));
    }

    @Test
    void givenSummerSession_whenGetUniversityYear_thenUniversityYearIsOfSameYear() {
        // given
        Session session = new Session(SessionType.SUMMER, A_YEAR);

        // when
        UniversityYear universityYear = session.getUniversityYear();

        assertThat(universityYear).isEqualTo(new UniversityYear(new Session(SessionType.FALL, A_YEAR.minusYears(1)), new Session(SessionType.WINTER, A_YEAR), session));
    }

    @Test
    void givenDateInFallSession_whenOf_thenFallSessionReturned() {
        // given
        LocalDate date = LocalDate.of(A_YEAR.getValue(), A_FALL_MONTH, A_DAY);

        // when
        Session session = Session.of(date);

        assertThat(session.getType()).isEqualTo(SessionType.FALL);
    }

    @Test
    void givenDateInWinterSession_whenOf_thenWinterSessionReturned() {
        // given
        LocalDate date = LocalDate.of(A_YEAR.getValue(), A_WINTER_MONTH, A_DAY);

        // when
        Session session = Session.of(date);

        assertThat(session.getType()).isEqualTo(SessionType.WINTER);
    }

    @Test
    void givenDateInSummerSession_whenOf_thenSummerSessionReturned() {
        // given
        LocalDate date = LocalDate.of(A_YEAR.getValue(), A_SUMMER_MONTH, A_DAY);

        // when
        Session session = Session.of(date);

        assertThat(session.getType()).isEqualTo(SessionType.SUMMER);
    }
}
