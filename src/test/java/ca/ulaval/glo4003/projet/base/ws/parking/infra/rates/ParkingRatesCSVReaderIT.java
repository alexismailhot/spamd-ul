package ca.ulaval.glo4003.projet.base.ws.parking.infra.rates;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassType;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingRatesCSVReaderIT {

    private static final String PARKING_ZONE_RATES_CSV_PATH = "src/test/java/ca/ulaval/glo4003/projet/base/ws/parking/infra/rates/frais-zone-test.csv";
    private static final String AN_INVALID_PATH = "not/a/valid/file";

    private ParkingRatesCSVReader parkingRatesCSVReader;

    @Test
    void givenFilePathExists_whenRead_thenReadParkingRatesFromCSV() throws FileNotFoundException {
        // given
        parkingRatesCSVReader = new ParkingRatesCSVReader(PARKING_ZONE_RATES_CSV_PATH);
        Map<ParkingZone, Map<ParkingPassType, Price>> expectedParkingRates = createExpectedParkingRates();

        // when
        Map<ParkingZone, Map<ParkingPassType, Price>> parkingZoneRates = parkingRatesCSVReader.read();

        // then
        assertThat(parkingZoneRates).isEqualTo(expectedParkingRates);
    }

    @Test
    void givenFileDoesNotExist_whenRead_thenThrowFileNotFoundException() {
        // given
        parkingRatesCSVReader = new ParkingRatesCSVReader(AN_INVALID_PATH);

        // then
        Assertions.assertThrows(
            FileNotFoundException.class,
            () ->
                // when
                parkingRatesCSVReader.read()
        );
    }

    private Map<ParkingZone, Map<ParkingPassType, Price>> createExpectedParkingRates() {
        Map<ParkingZone, Map<ParkingPassType, Price>> expectedParkingRates = new HashMap<>();
        Map<ParkingPassType, Price> expectedParkingZone1 = createExpectedParkingPassTypeRates(
            Arrays.asList(Price.of(163L), Price.of(183L), Price.of(544L), Price.of(895L), Price.of(1239L))
        );
        Map<ParkingPassType, Price> expectedParkingZone2 = createExpectedParkingPassTypeRates(
            Arrays.asList(Price.of(163L), Price.of(121L), Price.of(366L), Price.of(604L), Price.of(872L))
        );
        Map<ParkingPassType, Price> expectedParkingZone3 = createExpectedParkingPassTypeRates(
            Arrays.asList(Price.of(163L), Price.of(92L), Price.of(272L), Price.of(447L), Price.of(620L))
        );
        Map<ParkingPassType, Price> expectedParkingZoneR = createExpectedParkingPassTypeRates(
            Arrays.asList(Price.of(163L), Price.of(121L), Price.of(366L), Price.of(604L), Price.of(872L))
        );
        expectedParkingRates.put(ParkingZone.ZONE_1, expectedParkingZone1);
        expectedParkingRates.put(ParkingZone.ZONE_2, expectedParkingZone2);
        expectedParkingRates.put(ParkingZone.ZONE_3, expectedParkingZone3);
        expectedParkingRates.put(ParkingZone.ZONE_R, expectedParkingZoneR);
        return expectedParkingRates;
    }

    private Map<ParkingPassType, Price> createExpectedParkingPassTypeRates(List<Price> rates) {
        Map<ParkingPassType, Price> parkingPassTypeRates = new HashMap<>();
        parkingPassTypeRates.put(ParkingPassType.ONE_DAY_A_WEEK_FOR_A_SEMESTER, rates.get(0));
        parkingPassTypeRates.put(ParkingPassType.MONTHLY, rates.get(1));
        parkingPassTypeRates.put(ParkingPassType.ONE_SEMESTER, rates.get(2));
        parkingPassTypeRates.put(ParkingPassType.TWO_SEMESTERS, rates.get(3));
        parkingPassTypeRates.put(ParkingPassType.THREE_SEMESTERS, rates.get(4));
        return parkingPassTypeRates;
    }
}
