package ca.ulaval.glo4003.projet.base.ws.parking.api.request;

import static com.google.common.truth.Truth8.*;

import ca.ulaval.glo4003.projet.base.ws.parking.api.exception.NoDeliveryMethodException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.Year;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SessionParkingPassInformationFactoryTest {

    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final String AN_EMAIL = "antoine.lefrancois@ift.ulaval.ca";
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final String AN_ADDRESS = "123 Mackay";
    private static final String AN_APARTMENT_NUMBER = "4";
    private static final String A_STATE = "Quebec";
    private static final String A_COUNTRY = "Canada";
    private static final String A_CITY = "Quebec";
    private static final String A_POSTAL_CODE = "G1V2X1";
    private static final SessionType A_SESSION_TYPE = SessionType.SUMMER;
    private static final Year A_YEAR = Year.of(4);

    private SessionParkingPassInformationFactory factory;

    @BeforeEach
    void setUp() {
        factory = new SessionParkingPassInformationFactory(new PostalAddressAssembler());
    }

    @Test
    void givenDeliveryMethodIsEmailAndEmailProvided_whenCreate_thenSessionParkingPassInformationHasEmail() {
        // given
        SessionParkingPassRequest request = givenASessionParkingPassRequest(DeliveryMethod.EMAIL, AN_EMAIL);

        // when
        SessionParkingPassInformation sessionParkingPassInformation = factory.create(request);

        // then
        assertThat(sessionParkingPassInformation.getEmail()).isPresent();
    }

    @Test
    void givenDeliveryMethodIsPostalAndPostalAddressProvided_whenCreate_thenSessionParkingPassInformationHasPostalAddress() {
        // given
        SessionParkingPassRequest request = givenASessionParkingPassRequest(DeliveryMethod.POSTAL, givenAPostalAddressRequest());

        // when
        SessionParkingPassInformation sessionParkingPassInformation = factory.create(request);

        // then
        assertThat(sessionParkingPassInformation.getPostalAddress()).isPresent();
    }

    @Test
    void givenDeliveryMethodIsEmailAndEmailNotProvided_whenCreate_thenThrowNoDeliveryMethodException() {
        // given
        SessionParkingPassRequest request = givenASessionParkingPassRequest(DeliveryMethod.EMAIL, (String) null);

        // then
        Assertions.assertThrows(
            NoDeliveryMethodException.class,
            () ->
                // when
                factory.create(request)
        );
    }

    @Test
    void givenDeliveryMethodIsPostalAndPostalAddressNotProvided_whenCreate_thenThrowNoDeliveryMethodException() {
        // given
        SessionParkingPassRequest request = givenASessionParkingPassRequest(DeliveryMethod.POSTAL, (PostalAddressRequest) null);

        // then
        Assertions.assertThrows(
            NoDeliveryMethodException.class,
            () ->
                // when
                factory.create(request)
        );
    }

    @Test
    void givenDeliveryMethodIsSSP_whenCreate_thenSessionParkingPassInformationHasNoEmailOrPostalAddress() {
        // given
        SessionParkingPassRequest request = givenASessionParkingPassRequest(DeliveryMethod.SSP);

        // when
        SessionParkingPassInformation sessionParkingPassInformation = factory.create(request);

        // then
        assertThat(sessionParkingPassInformation.getEmail()).isEmpty();
        assertThat(sessionParkingPassInformation.getPostalAddress()).isEmpty();
    }

    private SessionParkingPassRequest givenASessionParkingPassRequest(DeliveryMethod deliveryMethod, String email) {
        return new SessionParkingPassRequest(
            A_PARKING_ZONE,
            deliveryMethod,
            email,
            givenASessionRequest(),
            givenACreditCardRequest(),
            givenAPostalAddressRequest()
        );
    }

    private SessionParkingPassRequest givenASessionParkingPassRequest(DeliveryMethod deliveryMethod, PostalAddressRequest postalAddressRequest) {
        return new SessionParkingPassRequest(A_PARKING_ZONE, deliveryMethod, AN_EMAIL, givenASessionRequest(), givenACreditCardRequest(), postalAddressRequest);
    }

    private SessionParkingPassRequest givenASessionParkingPassRequest(DeliveryMethod deliveryMethod) {
        return new SessionParkingPassRequest(A_PARKING_ZONE, deliveryMethod, null, givenASessionRequest(), givenACreditCardRequest(), null);
    }

    private PostalAddressRequest givenAPostalAddressRequest() {
        return new PostalAddressRequest(AN_ADDRESS, AN_APARTMENT_NUMBER, A_STATE, A_COUNTRY, A_CITY, A_POSTAL_CODE);
    }

    private CreditCardRequest givenACreditCardRequest() {
        return new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    }

    private SessionRequest givenASessionRequest() {
        return new SessionRequest(A_SESSION_TYPE, A_YEAR);
    }
}
