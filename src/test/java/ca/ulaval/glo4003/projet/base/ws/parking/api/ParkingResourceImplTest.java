package ca.ulaval.glo4003.projet.base.ws.parking.api;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.parking.api.request.PostalAddressRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.SessionParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.SessionParkingPassRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassInformationFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.api.request.WeeklyParkingPassRequest;
import ca.ulaval.glo4003.projet.base.ws.parking.api.response.ParkingPassResponse;
import ca.ulaval.glo4003.projet.base.ws.parking.api.response.ParkingPassResponseAssembler;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.service.ParkingService;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardAssembler;
import ca.ulaval.glo4003.projet.base.ws.payment.api.request.CreditCardRequest;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.universityyear.api.request.SessionRequest;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingResourceImplTest {

    private static final int A_PARKING_CODE_VALUE = 420;
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final DayOfWeek A_PARKING_DAY = DayOfWeek.MONDAY;
    private static final String AN_EMAIL = "antoine.lefrancois@ift.ulaval.ca";
    private static final ParkingCode A_PARKING_CODE = new ParkingCode(A_PARKING_CODE_VALUE);
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final CreditCard A_CREDIT_CARD = new CreditCard(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    private static final String AN_ADDRESS = "123 Mackay";
    private static final String AN_APARTMENT_NUMBER = "4";
    private static final String A_STATE = "Quebec";
    private static final String A_COUNTRY = "Canada";
    private static final String A_CITY = "Quebec";
    private static final String A_POSTAL_CODE = "G1V2X1";
    private static final DeliveryMethod POSTAL_DELIVERY_METHOD = DeliveryMethod.POSTAL;
    private static final SessionType A_SESSION_TYPE = SessionType.WINTER;
    private static final Year A_YEAR = Year.of(5);

    private ParkingResource parkingResource;

    @Mock
    private ParkingService parkingService;

    @Mock
    private WeeklyParkingPassInformationFactory weeklyParkingPassInformationFactory;

    @Mock
    private SessionParkingPassInformationFactory sessionParkingPassInformationFactory;

    @BeforeEach
    void setUp() {
        parkingResource =
            new ParkingResourceImpl(
                parkingService,
                weeklyParkingPassInformationFactory,
                sessionParkingPassInformationFactory,
                new ParkingPassResponseAssembler(),
                new CreditCardAssembler()
            );
    }

    @Test
    void givenAWeeklyParkingPassRequest_whenBuyWeeklyParkingPass_thenParkingPassResponseIsReturned() {
        // given
        WeeklyParkingPassRequest sessionParkingPassRequest = givenAWeeklyParkingPassRequest();
        WeeklyParkingPassInformation sessionParkingPassInformation = givenAWeeklyParkingPassInformation();
        given(parkingService.buyWeeklyParkingPass(sessionParkingPassInformation, A_CREDIT_CARD)).willReturn(A_PARKING_CODE);
        given(weeklyParkingPassInformationFactory.create(sessionParkingPassRequest)).willReturn(sessionParkingPassInformation);

        // when
        Response response = parkingResource.buyWeeklyParkingPass(sessionParkingPassRequest);

        // then
        ParkingPassResponse parkingPassResponse = (ParkingPassResponse) response.getEntity();
        assertThat(parkingPassResponse.getCode()).isEqualTo(A_PARKING_CODE.getCode());
    }

    @Test
    void givenASessionParkingPassRequest_whenBuySessionParkingPass_thenParkingPassResponseIsReturned() {
        // given
        SessionParkingPassRequest sessionParkingPassRequest = givenASessionParkingPassRequest();
        SessionParkingPassInformation sessionParkingPassInformation = givenASessionParkingPassInformation();
        given(parkingService.buySessionParkingPass(sessionParkingPassInformation, A_CREDIT_CARD)).willReturn(A_PARKING_CODE);
        given(sessionParkingPassInformationFactory.create(sessionParkingPassRequest)).willReturn(sessionParkingPassInformation);

        // when
        Response response = parkingResource.buySessionParkingPass(sessionParkingPassRequest);

        // then
        ParkingPassResponse parkingPassResponse = (ParkingPassResponse) response.getEntity();
        assertThat(parkingPassResponse.getCode()).isEqualTo(A_PARKING_CODE.getCode());
    }

    private WeeklyParkingPassRequest givenAWeeklyParkingPassRequest() {
        return new WeeklyParkingPassRequest(
            A_PARKING_ZONE,
            POSTAL_DELIVERY_METHOD,
            AN_EMAIL,
            A_PARKING_DAY,
            givenASessionRequest(),
            givenACreditCardRequest(),
            givenAPostalAddressRequest()
        );
    }

    private WeeklyParkingPassInformation givenAWeeklyParkingPassInformation() {
        return new WeeklyParkingPassInformation(A_PARKING_ZONE, POSTAL_DELIVERY_METHOD, AN_EMAIL, A_PARKING_DAY, new Session(SessionType.WINTER, Year.of(453)));
    }

    private SessionParkingPassRequest givenASessionParkingPassRequest() {
        return new SessionParkingPassRequest(
            A_PARKING_ZONE,
            POSTAL_DELIVERY_METHOD,
            AN_EMAIL,
            givenASessionRequest(),
            givenACreditCardRequest(),
            givenAPostalAddressRequest()
        );
    }

    private SessionParkingPassInformation givenASessionParkingPassInformation() {
        return new SessionParkingPassInformation(A_PARKING_ZONE, POSTAL_DELIVERY_METHOD, new Session(SessionType.WINTER, Year.of(453)));
    }

    private PostalAddressRequest givenAPostalAddressRequest() {
        return new PostalAddressRequest(AN_ADDRESS, AN_APARTMENT_NUMBER, A_STATE, A_COUNTRY, A_CITY, A_POSTAL_CODE);
    }

    private CreditCardRequest givenACreditCardRequest() {
        return new CreditCardRequest(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    }

    private SessionRequest givenASessionRequest() {
        return new SessionRequest(A_SESSION_TYPE, A_YEAR);
    }
}
