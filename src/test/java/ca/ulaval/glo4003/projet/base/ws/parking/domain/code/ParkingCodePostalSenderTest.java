package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import static org.mockito.Mockito.verify;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.PostalAddress;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingCodePostalSenderTest {

    private static final int A_PARKING_CODE_VALUE = 420;
    private static final ParkingCode A_PARKING_CODE = new ParkingCode(A_PARKING_CODE_VALUE);
    private static final String AN_ADDRESS = "123 Mackay";
    private static final String AN_APARTMENT_NUMBER = "4";
    private static final String A_STATE = "Quebec";
    private static final String A_COUNTRY = "Canada";
    private static final String A_CITY = "Quebec";
    private static final String A_POSTAL_CODE = "G1V2X1";
    private static final PostalAddress A_POSTAL_ADDRESS = new PostalAddress(AN_ADDRESS, AN_APARTMENT_NUMBER, A_STATE, A_COUNTRY, A_CITY, A_POSTAL_CODE);
    private static final ParkingCodeMailing A_PARKING_CODE_POSTAL_INFORMATION = new ParkingCodeMailing(A_PARKING_CODE, A_POSTAL_ADDRESS);

    private ParkingCodePostalSender parkingCodePostalSender;

    @Mock
    private ParkingCodeMailingRepository parkingCodeMailingRepository;

    @BeforeEach
    void setUp() {
        parkingCodePostalSender = new ParkingCodePostalSender(parkingCodeMailingRepository, A_PARKING_CODE_POSTAL_INFORMATION);
    }

    @Test
    void whenSend_thenSaveParkingCodeMailingInRepository() {
        // when
        parkingCodePostalSender.send();

        // then
        verify(parkingCodeMailingRepository).save(A_PARKING_CODE_POSTAL_INFORMATION);
    }
}
