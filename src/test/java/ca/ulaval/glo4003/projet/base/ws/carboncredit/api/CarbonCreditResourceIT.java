package ca.ulaval.glo4003.projet.base.ws.carboncredit.api;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.infra.AccessIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.domain.CarbonCreditConfig;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.CarbonCreditULavalConfig;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.infra.QuartzAutomaticJob;
import ca.ulaval.glo4003.projet.base.ws.carboncredit.service.CarbonCreditService;
import ca.ulaval.glo4003.projet.base.ws.income.domain.IncomeSummaryFactory;
import ca.ulaval.glo4003.projet.base.ws.income.service.IncomeService;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.Initiative;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeFactory;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.InitiativeRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingAmountValidator;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundingConfig;
import ca.ulaval.glo4003.projet.base.ws.initiative.domain.funding.InitiativeFundsCalculator;
import ca.ulaval.glo4003.projet.base.ws.initiative.infra.InitiativeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.initiative.infra.funding.InitiativeFundingULavalConfig;
import ca.ulaval.glo4003.projet.base.ws.initiative.service.InitiativeService;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.infra.income.OffenseIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.infra.income.ParkingPassIncomeInMemoryRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYearProvider;
import java.time.Clock;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CarbonCreditResourceIT {

    private static final Price AN_AMOUNT_OF_FUNDED_CARBON_CREDITS = Price.of(174.48);
    private static final CarbonCreditConfig carbonCreditConfig = new CarbonCreditULavalConfig();
    private static final Initiative A_CARBON_CREDIT_INITIATIVE = new Initiative(
        carbonCreditConfig.getInitiativeName(),
        Price.zero(),
        carbonCreditConfig.getCarbonCreditInitiativeCode()
    );

    private CarbonCreditResource carbonCreditResource;

    @Mock
    QuartzAutomaticJob quartzAutomaticJob;

    @Mock
    private Clock clock;

    @BeforeEach
    void setUp() {
        UniversityYearProvider universityYearProvider = new UniversityYearProvider();
        IncomeSummaryFactory incomeSummaryFactory = new IncomeSummaryFactory();
        OffenseIncomeRepository offenseIncomeRepository = new OffenseIncomeInMemoryRepository();
        ParkingPassIncomeRepository parkingPassIncomeRepository = new ParkingPassIncomeInMemoryRepository();
        AccessIncomeRepository accessIncomeRepository = new AccessIncomeInMemoryRepository();
        IncomeService incomeService = new IncomeService(
            accessIncomeRepository,
            parkingPassIncomeRepository,
            offenseIncomeRepository,
            incomeSummaryFactory,
            universityYearProvider,
            clock
        );

        InitiativeFundingConfig initiativeFundingConfig = new InitiativeFundingULavalConfig();
        InitiativeFundsCalculator initiativeFundsCalculator = new InitiativeFundsCalculator(initiativeFundingConfig);
        InitiativeRepository initiativeRepository = new InitiativeInMemoryRepository();
        InitiativeFundingAmountValidator initiativeFundingAmountValidator = new InitiativeFundingAmountValidator();

        InitiativeFactory initiativeFactory = new InitiativeFactory();
        InitiativeService initiativeService = new InitiativeService(
            initiativeFactory,
            initiativeFundingAmountValidator,
            initiativeRepository,
            initiativeFundsCalculator,
            incomeService,
            carbonCreditConfig
        );

        CarbonCreditService carbonCreditService = new CarbonCreditService(quartzAutomaticJob);
        carbonCreditResource = new CarbonCreditResourceImpl(carbonCreditService, initiativeService);

        initiativeRepository.save(A_CARBON_CREDIT_INITIATIVE);
    }

    @Test
    void givenFundedCarbonCredits_whenGetNumberOfCarbonCreditsBought_thenReturnsRightNumberOfCarbonCredit() {
        //given
        givenFundsInCarbonCreditInitiative();

        // when
        Response getNumberOfCarbonCreditsBoughtResponse = carbonCreditResource.getNumberOfCarbonCreditsBought();
        double amountOfFundedCarbonCredit = (double) getNumberOfCarbonCreditsBoughtResponse.getEntity();

        // then
        double expectedNumberOfFundedCarbonCredits = 8;
        assertThat(amountOfFundedCarbonCredit).isEqualTo(expectedNumberOfFundedCarbonCredits);
    }

    private void givenFundsInCarbonCreditInitiative() {
        A_CARBON_CREDIT_INITIATIVE.fund(AN_AMOUNT_OF_FUNDED_CARBON_CREDITS);
    }
}
