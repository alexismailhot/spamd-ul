package ca.ulaval.glo4003.projet.base.ws.offense.api.request.parking;

import static com.google.common.truth.Truth8.*;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParkingPassValidationAssemblerTest {

    private static final int A_PARKING_CODE_VALUE = 8;
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_3;
    private static final LocalDateTime A_TIME_ISSUED = LocalDateTime.of(1, 2, 3, 3, 2);

    private ParkingPassValidationAssembler parkingPassValidationAssembler;

    @BeforeEach
    void setUp() {
        parkingPassValidationAssembler = new ParkingPassValidationAssembler();
    }

    @Test
    void givenRequestHasAParkingCode_whenToDomain_thenParkingPassValidationHasAParkingCode() {
        // given
        ParkingPassValidationRequest request = givenAParkingPassValidationRequest(A_PARKING_CODE_VALUE);

        // when
        ParkingPassValidation parkingPassValidation = parkingPassValidationAssembler.toDomain(request);

        // then
        assertThat(parkingPassValidation.getParkingCode()).isPresent();
    }

    @Test
    void givenRequestHasNoParkingCode_whenToDomain_thenParkingPassValidationHasNoParkingCode() {
        // given
        ParkingPassValidationRequest request = givenAParkingPassValidationRequest(null);

        // when
        ParkingPassValidation parkingPassValidation = parkingPassValidationAssembler.toDomain(request);

        // then
        assertThat(parkingPassValidation.getParkingCode()).isEmpty();
    }

    private ParkingPassValidationRequest givenAParkingPassValidationRequest(Integer parkingCodeValue) {
        return new ParkingPassValidationRequest(parkingCodeValue, A_TIME_ISSUED, A_PARKING_ZONE);
    }
}
