package ca.ulaval.glo4003.projet.base.ws.universityyear.domain;

import static com.google.common.truth.Truth.*;
import static org.mockito.BDDMockito.*;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UniversityYearTest {

    private static final LocalDate A_DATE = LocalDate.of(2, 3, 4);

    @Mock
    private Session fallSession;

    @Mock
    private Session winterSession;

    @Mock
    private Session summerSession;

    private UniversityYear universityYear;

    @BeforeEach
    void setUp() {
        universityYear = new UniversityYear(fallSession, winterSession, summerSession);
    }

    @Test
    void givenFallSessionIncludesDate_whenIncludes_thenReturnTrue() {
        // given
        given(fallSession.includes(A_DATE)).willReturn(true);

        // when
        boolean includes = universityYear.includes(A_DATE);

        // then
        assertThat(includes).isTrue();
    }

    @Test
    void givenWinterSessionIncludesDate_whenIncludes_thenReturnTrue() {
        // given
        given(winterSession.includes(A_DATE)).willReturn(true);

        // when
        boolean includes = universityYear.includes(A_DATE);

        // then
        assertThat(includes).isTrue();
    }

    @Test
    void givenSummerSessionIncludesDate_whenIncludes_thenReturnTrue() {
        // given
        given(summerSession.includes(A_DATE)).willReturn(true);

        // when
        boolean includes = universityYear.includes(A_DATE);

        // then
        assertThat(includes).isTrue();
    }

    @Test
    void givenNoSessionIncludesDate_whenIncludes_thenReturnFalse() {
        // given

        // when
        boolean includes = universityYear.includes(A_DATE);

        // then
        assertThat(includes).isFalse();
    }
}
