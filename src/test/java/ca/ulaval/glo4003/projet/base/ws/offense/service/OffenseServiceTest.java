package ca.ulaval.glo4003.projet.base.ws.offense.service;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth8.assertThat;
import static org.mockito.BDDMockito.*;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.Offense;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseId;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.OffenseRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.exception.OffenseAlreadyPaidException;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.exception.OffenseNotFoundException;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCode;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformation;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.offensecode.OffenseCodeInformationConfig;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.InvalidParkingPassException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotFoundForParkingCodeException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import ca.ulaval.glo4003.projet.base.ws.parking.service.ParkingService;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OffenseServiceTest {

    private static final LocalDateTime A_TIME_ISSUED = LocalDateTime.now();
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_2;
    private static final ParkingCode A_PARKING_CODE = new ParkingCode(5);
    private static final Class<ParkingPassNotFoundForParkingCodeException> AN_INVALID_PARKING_PASS_EXCEPTION_CLASS =
        ParkingPassNotFoundForParkingCodeException.class;
    private static final OffenseCode AN_OFFENSE_CODE = OffenseCode.VIG_03;
    private static final String AN_OFFENSE_DESCRIPTION = "offense";
    private static final Price AN_OFFENSE_AMOUNT = Price.of(3);
    private static final OffenseId AN_OFFENSE_ID = new OffenseId(5);
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";

    private OffenseService offenseService;

    @Mock
    private ParkingService parkingService;

    @Mock
    private PaymentService paymentService;

    @Mock
    private OffenseRepository offenseRepository;

    @Mock
    private OffenseFactory offenseFactory;

    @Mock
    private OffenseCodeInformationConfig offenseCodeInformationConfig;

    @Mock
    private OffenseIncomeFactory offenseIncomeFactory;

    @Mock
    private OffenseIncomeRepository offenseIncomeRepository;

    @BeforeEach
    void setUp() {
        Map<Class<? extends InvalidParkingPassException>, OffenseCode> offenses = new HashMap<>();
        offenses.put(AN_INVALID_PARKING_PASS_EXCEPTION_CLASS, AN_OFFENSE_CODE);
        offenseService =
            new OffenseService(
                offenses,
                offenseCodeInformationConfig,
                parkingService,
                paymentService,
                offenseRepository,
                offenseFactory,
                offenseIncomeFactory,
                offenseIncomeRepository
            );
    }

    @Test
    void givenInvalidParkingPass_whenCreateOffenseIfParkingPassOffenseWasCommitted_thenReturnCreatedOffense() {
        // given
        Offense expectedOffense = givenAnOffense();
        given(offenseFactory.create(AN_OFFENSE_CODE, AN_OFFENSE_DESCRIPTION, AN_OFFENSE_AMOUNT)).willReturn(expectedOffense);
        given(offenseCodeInformationConfig.getOffenseCodeInformation(AN_OFFENSE_CODE)).willReturn(givenAnOffenseCodeInformation());

        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation();
        doThrow(ParkingPassNotFoundForParkingCodeException.class).when(parkingService).validateParkingPass(parkingPassValidation);

        // when
        Optional<Offense> createdOffense = offenseService.createOffenseIfParkingPassOffenseWasCommitted(parkingPassValidation);

        // then
        assertThat(createdOffense.get()).isEqualTo(expectedOffense);
    }

    @Test
    void givenInvalidParkingPass_whenCreateOffenseIfParkingPassOffenseWasCommitted_thenSaveOffenseWithAssociatedOffenseCode() {
        // given
        Offense parkingOffense = givenAnOffense();
        given(offenseFactory.create(AN_OFFENSE_CODE, AN_OFFENSE_DESCRIPTION, AN_OFFENSE_AMOUNT)).willReturn(parkingOffense);
        given(offenseCodeInformationConfig.getOffenseCodeInformation(AN_OFFENSE_CODE)).willReturn(givenAnOffenseCodeInformation());

        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation();
        doThrow(ParkingPassNotFoundForParkingCodeException.class).when(parkingService).validateParkingPass(parkingPassValidation);

        // when
        offenseService.createOffenseIfParkingPassOffenseWasCommitted(parkingPassValidation);

        // then
        verify(offenseRepository).save(parkingOffense);
    }

    @Test
    void givenParkingPassIsValid_whenCreateOffenseIfParkingPassOffenseWasCommitted_thenReturnEmptyOptional() {
        // given
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation();

        // when
        Optional<Offense> offense = offenseService.createOffenseIfParkingPassOffenseWasCommitted(parkingPassValidation);

        // then
        assertThat(offense).isEmpty();
    }

    @Test
    void givenAssociatedOffenseNotFoundInRepo_whenPayOffense_thenThrowOffenseNotFoundException() {
        // given
        given(offenseRepository.findByOffenseId(AN_OFFENSE_ID)).willReturn(Optional.empty());

        // then
        Assertions.assertThrows(
            OffenseNotFoundException.class,
            () ->
                // when
                offenseService.payOffense(givenACreditCard(), AN_OFFENSE_ID)
        );
    }

    @Test
    void givenAssociatedOffenseIsAlreadyPaid_whenPayOffense_thenThrowOffenseAlreadyPaidException() {
        // given
        Offense offense = givenAnOffense();
        offense.pay();
        given(offenseRepository.findByOffenseId(AN_OFFENSE_ID)).willReturn(Optional.of(offense));

        // then
        Assertions.assertThrows(
            OffenseAlreadyPaidException.class,
            () ->
                // when
                offenseService.payOffense(givenACreditCard(), AN_OFFENSE_ID)
        );
    }

    @Test
    void givenAssociatedOffenseIsNotPaid_whenPayOffense_thenPaymentOfOffenseAmountIsMade() {
        // given
        CreditCard creditCard = givenACreditCard();
        Offense offense = givenAnOffense();
        given(offenseRepository.findByOffenseId(AN_OFFENSE_ID)).willReturn(Optional.of(offense));

        // when
        offenseService.payOffense(creditCard, AN_OFFENSE_ID);

        // then
        verify(paymentService).processPayment(creditCard, offense.getAmount());
    }

    private ParkingPassValidation givenAParkingPassValidation() {
        return new ParkingPassValidation(A_PARKING_CODE, A_TIME_ISSUED, A_PARKING_ZONE);
    }

    private Offense givenAnOffense() {
        return new Offense(AN_OFFENSE_ID, AN_OFFENSE_CODE, AN_OFFENSE_DESCRIPTION, AN_OFFENSE_AMOUNT);
    }

    private OffenseCodeInformation givenAnOffenseCodeInformation() {
        return new OffenseCodeInformation(AN_OFFENSE_CODE, AN_OFFENSE_DESCRIPTION, AN_OFFENSE_AMOUNT);
    }

    private CreditCard givenACreditCard() {
        return new CreditCard(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    }
}
