package ca.ulaval.glo4003.projet.base.ws.parking.service;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCode;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSender;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.code.ParkingCodeSenderFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.ParkingPassNotFoundForParkingCodeException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncome;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.income.ParkingPassIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassPriceCalculator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassRepository;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.ParkingPassType;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.SessionParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator.ParkingPassInformationValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session.SessionParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.session.SessionParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.validation.ParkingPassValidator;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPass;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.weekly.WeeklyParkingPassFactory;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingServiceTest {

    private static final int A_PARKING_CODE_VALUE = 420;
    private static final ParkingCode A_PARKING_CODE = new ParkingCode(A_PARKING_CODE_VALUE);
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final DayOfWeek A_PARKING_DAY = DayOfWeek.TUESDAY;
    private static final ParkingPassType WEEKLY_PARKING_PASS_TYPE = ParkingPassType.ONE_DAY_A_WEEK_FOR_A_SEMESTER;
    private static final ParkingPassType ONE_SEMESTER_PARKING_PASS_TYPE = ParkingPassType.ONE_SEMESTER;
    private static final DeliveryMethod A_DELIVERY_METHOD = DeliveryMethod.EMAIL;
    private static final String AN_EMAIL = "antoine.lefrancois@ift.ulaval.ca";
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final CreditCard A_CREDIT_CARD = new CreditCard(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    private static final LocalDateTime A_TIME_ISSUED = LocalDateTime.now();
    private static final Price A_PARKING_PASS_PRICE = Price.of(5);
    private static final ParkingPassIncome A_PARKING_PASS_INCOME = new ParkingPassIncome(A_PARKING_PASS_PRICE, LocalDateTime.now());
    private static final Session A_UNIVERSITY_SESSION = new Session(SessionType.SUMMER, Year.of(5));

    private ParkingService parkingService;

    @Mock
    private WeeklyParkingPassFactory weeklyParkingPassFactory;

    @Mock
    private SessionParkingPassFactory sessionParkingPassFactory;

    @Mock
    private ParkingPassRepository parkingPassRepository;

    @Mock
    private PaymentService paymentService;

    @Mock
    private ParkingCodeSenderFactory parkingCodeSenderFactory;

    @Mock
    private ParkingCodeSender parkingCodeSender;

    @Mock
    private ParkingPassInformationValidator parkingPassInformationValidator;

    @Mock
    private ParkingPassPriceCalculator parkingPassPriceCalculator;

    @Mock
    private ParkingPassIncomeFactory parkingPassIncomeFactory;

    @Mock
    private ParkingPassIncomeRepository parkingPassIncomeRepository;

    @Mock
    private ParkingPassValidator parkingPassValidator;

    @BeforeEach
    void setUp() {
        parkingService =
            new ParkingService(
                parkingPassRepository,
                weeklyParkingPassFactory,
                sessionParkingPassFactory,
                paymentService,
                parkingCodeSenderFactory,
                parkingPassIncomeFactory,
                parkingPassIncomeRepository,
                parkingPassInformationValidator,
                parkingPassPriceCalculator,
                parkingPassValidator
            );
    }

    @Test
    void whenBuyWeeklyParkingPass_thenParkingPassInformationIsValidated() {
        // given
        givenWeeklyParkingPassFactoryReturnsAWeeklyParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, WEEKLY_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        WeeklyParkingPassInformation weeklyParkingPassInformation = givenAWeeklyParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(weeklyParkingPassInformation);

        // when
        parkingService.buyWeeklyParkingPass(weeklyParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingPassInformationValidator).validate(weeklyParkingPassInformation);
    }

    @Test
    void whenBuyWeeklyParkingPass_thenNewlyCreatedWeeklyParkingPassIsSavedInRepository() {
        // given
        WeeklyParkingPass weeklyParkingPass = givenWeeklyParkingPassFactoryReturnsAWeeklyParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, WEEKLY_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        WeeklyParkingPassInformation weeklyParkingPassInformation = givenAWeeklyParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(weeklyParkingPassInformation);

        // when
        parkingService.buyWeeklyParkingPass(weeklyParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingPassRepository).save(weeklyParkingPass);
    }

    @Test
    void whenBuyWeeklyParkingPass_thenParkingPassIncomeRepositoryIsCalledToSaveNewIncome() {
        // given
        givenWeeklyParkingPassFactoryReturnsAWeeklyParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, WEEKLY_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);
        given(parkingPassIncomeFactory.create(A_PARKING_PASS_PRICE)).willReturn(A_PARKING_PASS_INCOME);

        WeeklyParkingPassInformation weeklyParkingPassInformation = givenAWeeklyParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(weeklyParkingPassInformation);

        // when
        parkingService.buyWeeklyParkingPass(weeklyParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingPassIncomeRepository).save(A_PARKING_PASS_INCOME);
    }

    @Test
    void whenBuyWeeklyParkingPass_thenPaymentServiceIsCalledToProcessPayment() {
        // given
        givenWeeklyParkingPassFactoryReturnsAWeeklyParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, WEEKLY_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        WeeklyParkingPassInformation weeklyParkingPassInformation = givenAWeeklyParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(weeklyParkingPassInformation);

        // when
        parkingService.buyWeeklyParkingPass(weeklyParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(paymentService).processPayment(A_CREDIT_CARD, A_PARKING_PASS_PRICE);
    }

    @Test
    void whenBuyWeeklyParkingPass_thenParkingCodeSenderIsCalledToSendParkingCode() {
        // given
        givenWeeklyParkingPassFactoryReturnsAWeeklyParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, WEEKLY_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        WeeklyParkingPassInformation weeklyParkingPassInformation = givenAWeeklyParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(weeklyParkingPassInformation);

        // when
        parkingService.buyWeeklyParkingPass(weeklyParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingCodeSender).send();
    }

    @Test
    void whenBuySessionParkingPass_thenParkingPassInformationIsValidated() {
        // given
        givenSessionParkingPassFactoryReturnsASessionParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, ONE_SEMESTER_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        SessionParkingPassInformation sessionParkingPassInformation = givenASessionParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(sessionParkingPassInformation);

        // when
        parkingService.buySessionParkingPass(sessionParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingPassInformationValidator).validate(sessionParkingPassInformation);
    }

    @Test
    void whenBuySessionParkingPass_thenNewlyCreatedSessionParkingPassIsSavedInRepository() {
        // given
        SessionParkingPass sessionParkingPass = givenSessionParkingPassFactoryReturnsASessionParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, ONE_SEMESTER_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        SessionParkingPassInformation sessionParkingPassInformation = givenASessionParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(sessionParkingPassInformation);

        // when
        parkingService.buySessionParkingPass(sessionParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingPassRepository).save(sessionParkingPass);
    }

    @Test
    void whenBuySessionParkingPass_thenParkingPassIncomeRepositoryIsCalledToSaveNewIncome() {
        // given
        givenSessionParkingPassFactoryReturnsASessionParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, ONE_SEMESTER_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);
        given(parkingPassIncomeFactory.create(A_PARKING_PASS_PRICE)).willReturn(A_PARKING_PASS_INCOME);

        SessionParkingPassInformation sessionParkingPassInformation = givenASessionParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(sessionParkingPassInformation);

        // when
        parkingService.buySessionParkingPass(sessionParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingPassIncomeRepository).save(A_PARKING_PASS_INCOME);
    }

    @Test
    void whenBuySessionParkingPass_thenPaymentServiceIsCalledToProcessPayment() {
        // given
        givenSessionParkingPassFactoryReturnsASessionParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, ONE_SEMESTER_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        SessionParkingPassInformation sessionParkingPassInformation = givenASessionParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(sessionParkingPassInformation);

        // when
        parkingService.buySessionParkingPass(sessionParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(paymentService).processPayment(A_CREDIT_CARD, A_PARKING_PASS_PRICE);
    }

    @Test
    void whenBuySessionParkingPass_thenParkingCodeSenderIsCalledToSendParkingCode() {
        // given
        givenSessionParkingPassFactoryReturnsASessionParkingPass();
        given(parkingPassPriceCalculator.calculatePrice(A_PARKING_ZONE, ONE_SEMESTER_PARKING_PASS_TYPE, A_DELIVERY_METHOD)).willReturn(A_PARKING_PASS_PRICE);

        SessionParkingPassInformation sessionParkingPassInformation = givenASessionParkingPassInformation();
        givenParkingCodeSenderFactoryReturnsAParkingCodeSender(sessionParkingPassInformation);

        // when
        parkingService.buySessionParkingPass(sessionParkingPassInformation, A_CREDIT_CARD);

        // then
        verify(parkingCodeSender).send();
    }

    @Test
    void givenParkingPassIsFound_whenValidateParkingPass_thenCallParkingPassValidator() {
        // given
        ParkingPass parkingPass = givenAParkingPass();
        given(parkingPassRepository.findByParkingCode(A_PARKING_CODE)).willReturn(Optional.of(parkingPass));
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation();

        // when
        parkingService.validateParkingPass(parkingPassValidation);

        // then
        verify(parkingPassValidator).validate(parkingPassValidation, parkingPass);
    }

    @Test
    void givenParkingPassIsNotFound_whenValidateParkingPass_thenReturnParkingPassNotFoundForParkingCodeException() {
        // given
        given(parkingPassRepository.findByParkingCode(A_PARKING_CODE)).willReturn(Optional.empty());
        ParkingPassValidation parkingPassValidation = givenAParkingPassValidation();

        // then
        Assertions.assertThrows(
            ParkingPassNotFoundForParkingCodeException.class,
            () ->
                // when
                parkingService.validateParkingPass(parkingPassValidation)
        );
    }

    private void givenParkingCodeSenderFactoryReturnsAParkingCodeSender(ParkingPassInformation parkingPassInformation) {
        given(parkingCodeSenderFactory.create(parkingPassInformation, A_PARKING_CODE)).willReturn(parkingCodeSender);
    }

    private WeeklyParkingPass givenWeeklyParkingPassFactoryReturnsAWeeklyParkingPass() {
        WeeklyParkingPass weeklyParkingPass = new WeeklyParkingPass(A_PARKING_CODE, A_PARKING_ZONE, A_PARKING_DAY, A_UNIVERSITY_SESSION);
        given(weeklyParkingPassFactory.create(A_PARKING_ZONE, A_PARKING_DAY, A_UNIVERSITY_SESSION)).willReturn(weeklyParkingPass);
        return weeklyParkingPass;
    }

    private SessionParkingPass givenSessionParkingPassFactoryReturnsASessionParkingPass() {
        SessionParkingPass sessionParkingPass = new SessionParkingPass(A_PARKING_CODE, A_PARKING_ZONE, A_UNIVERSITY_SESSION);
        given(sessionParkingPassFactory.create(A_PARKING_ZONE, A_UNIVERSITY_SESSION)).willReturn(sessionParkingPass);
        return sessionParkingPass;
    }

    private WeeklyParkingPass givenAParkingPass() {
        return new WeeklyParkingPass(A_PARKING_CODE, A_PARKING_ZONE, A_PARKING_DAY, A_UNIVERSITY_SESSION);
    }

    private WeeklyParkingPassInformation givenAWeeklyParkingPassInformation() {
        return new WeeklyParkingPassInformation(A_PARKING_ZONE, A_DELIVERY_METHOD, AN_EMAIL, A_PARKING_DAY, A_UNIVERSITY_SESSION);
    }

    private SessionParkingPassInformation givenASessionParkingPassInformation() {
        return new SessionParkingPassInformation(A_PARKING_ZONE, A_DELIVERY_METHOD, AN_EMAIL, A_UNIVERSITY_SESSION);
    }

    private ParkingPassValidation givenAParkingPassValidation() {
        return new ParkingPassValidation(A_PARKING_CODE, A_TIME_ISSUED, A_PARKING_ZONE);
    }
}
