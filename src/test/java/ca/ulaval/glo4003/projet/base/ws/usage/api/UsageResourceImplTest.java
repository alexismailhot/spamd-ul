package ca.ulaval.glo4003.projet.base.ws.usage.api;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.verify;
import static org.mockito.Mockito.doReturn;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequest;
import ca.ulaval.glo4003.projet.base.ws.usage.api.request.AddUsageRequestAssembler;
import ca.ulaval.glo4003.projet.base.ws.usage.api.response.*;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingEntry;
import ca.ulaval.glo4003.projet.base.ws.usage.domain.parking.ParkingZoneUsageSummary;
import ca.ulaval.glo4003.projet.base.ws.usage.service.UsageService;
import java.time.*;
import java.util.Arrays;
import java.util.Collections;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UsageResourceImplTest {

    private static final Month A_MONTH = Month.DECEMBER;
    private static final Year A_YEAR = Year.of(2020);
    private static final LocalDate A_DATE = LocalDate.now();
    private static final LocalDate ANOTHER_DATE = LocalDate.now().plusDays(4);
    private static final LocalDate A_FROM_DATE = LocalDate.of(2020, 6, 1);
    private static final LocalDate A_UNTIL_DATE = LocalDate.of(2020, 7, 1);
    private static final LocalDate FIRST_OF_MONTH = LocalDate.of(A_YEAR.getValue(), A_MONTH, 1);
    private static final LocalDate LAST_OF_MONTH = LocalDate.of(A_YEAR.getValue(), A_MONTH, 31);
    private static final Instant NOW = Instant.now();
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final String A_PARKING_ZONE_STRING = "ZONE_1";
    private static final ParkingZone ANOTHER_PARKING_ZONE = ParkingZone.ZONE_3;
    private static final ParkingEntry A_PARKING_ENTRY = new ParkingEntry(A_DATE, A_PARKING_ZONE);
    private static final ParkingEntry ANOTHER_PARKING_ENTRY = new ParkingEntry(ANOTHER_DATE, ANOTHER_PARKING_ZONE);
    private static final int A_MEAN_DAILY_ACTIVITY = 2;
    private static final int ANOTHER_MEAN_DAILY_ACTIVITY = 4;

    @Mock
    private UsageService usageService;

    @Mock
    private ParkingZoneUsageSummaryResponseAssembler parkingZoneUsageSummaryResponseAssembler;

    @Mock
    private DailyParkingZoneUsageSummaryResponseAssembler dailyParkingZoneUsageSummaryResponseAssembler;

    @Mock
    private Clock clock;

    private UsageResource usageResource;

    @BeforeEach
    void setUp() {
        usageResource =
            new UsageResourceImpl(
                usageService,
                parkingZoneUsageSummaryResponseAssembler,
                clock,
                new AddUsageRequestAssembler(),
                dailyParkingZoneUsageSummaryResponseAssembler
            );
    }

    @Test
    void givenAZoneAndAPeriod_whenGetPeriodUsage_thenReturnParkingZoneSummaryUsageResponse() {
        // given
        ParkingZoneUsageSummary parkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        given(usageService.generateZoneUsageSummary(A_PARKING_ZONE, A_FROM_DATE, A_UNTIL_DATE)).willReturn(parkingZoneUsageSummary);
        DatesActivitySummaryResponse mostActiveDay = givenADailyActivitySummaryResponse(A_DATE);
        DatesActivitySummaryResponse leastActiveDay = givenADailyActivitySummaryResponse(ANOTHER_DATE);
        ParkingZoneUsageSummaryResponse parkingZoneUsageSummaryResponse = givenAParkingZoneUsageSummaryResponse(
            mostActiveDay,
            leastActiveDay,
            A_MEAN_DAILY_ACTIVITY,
            A_PARKING_ZONE
        );
        given(parkingZoneUsageSummaryResponseAssembler.toResponse(parkingZoneUsageSummary)).willReturn(parkingZoneUsageSummaryResponse);

        // when
        Response response = usageResource.getPeriodUsage(A_PARKING_ZONE_STRING, A_FROM_DATE, A_UNTIL_DATE);

        // then
        assertThat(response.getEntity()).isEqualTo(parkingZoneUsageSummaryResponse);
    }

    @Test
    void givenOneDate_whenGetDailyUsage_thenReturnDailyParkingZoneSummaryResponse() {
        // given
        ParkingZoneUsageSummary parkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        given(usageService.generateZoneUsageSummary(A_PARKING_ZONE, A_FROM_DATE, A_FROM_DATE)).willReturn(parkingZoneUsageSummary);
        DailyParkingZoneUsageSummaryResponse dailyParkingZoneUsageSummaryResponse = givenADailyParkingZoneSummaryResponse(
            A_MEAN_DAILY_ACTIVITY,
            A_PARKING_ZONE
        );
        given(dailyParkingZoneUsageSummaryResponseAssembler.toResponse(parkingZoneUsageSummary)).willReturn(dailyParkingZoneUsageSummaryResponse);

        // when
        Response response = usageResource.getDailyUsage(A_PARKING_ZONE_STRING, A_FROM_DATE);

        // then
        assertThat(response.getEntity()).isEqualTo(dailyParkingZoneUsageSummaryResponse);
    }

    @Test
    void givenAZoneAMonthAndAYear_whenGetMonthlyUsage_thenReturnParkingZoneUsageSummaryResponse() {
        // given
        given(clock.instant()).willReturn(NOW);
        ParkingZoneUsageSummary parkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        given(usageService.generateZoneUsageSummary(A_PARKING_ZONE, FIRST_OF_MONTH, LAST_OF_MONTH)).willReturn(parkingZoneUsageSummary);
        DatesActivitySummaryResponse mostActiveDay = givenADailyActivitySummaryResponse(A_DATE);
        DatesActivitySummaryResponse leastActiveDay = givenADailyActivitySummaryResponse(ANOTHER_DATE);
        ParkingZoneUsageSummaryResponse parkingZoneUsageSummaryResponse = givenAParkingZoneUsageSummaryResponse(
            mostActiveDay,
            leastActiveDay,
            A_MEAN_DAILY_ACTIVITY,
            A_PARKING_ZONE
        );
        given(parkingZoneUsageSummaryResponseAssembler.toResponse(parkingZoneUsageSummary)).willReturn(parkingZoneUsageSummaryResponse);

        // when
        Response response = usageResource.getMonthlyUsage(A_PARKING_ZONE_STRING, A_MONTH, A_YEAR);

        // then
        assertThat(response.getEntity()).isEqualTo(parkingZoneUsageSummaryResponse);
    }

    @Test
    void givenTwoDates_whenGetAllZonesPeriodUsage_thenReturnListOfParkingZoneUsageSummaryResponse() {
        // given
        ParkingZoneUsageSummary parkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        ParkingZoneUsageSummary otherParkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        given(usageService.generateAllZonesUsageSummary(A_FROM_DATE, A_UNTIL_DATE))
            .willReturn(Arrays.asList(parkingZoneUsageSummary, otherParkingZoneUsageSummary));

        DatesActivitySummaryResponse mostActiveDay = givenADailyActivitySummaryResponse(A_DATE);
        DatesActivitySummaryResponse leastActiveDay = givenADailyActivitySummaryResponse(ANOTHER_DATE);
        ParkingZoneUsageSummaryResponse aParkingZoneUsageSummaryResponse = givenAParkingZoneUsageSummaryResponse(
            mostActiveDay,
            leastActiveDay,
            A_MEAN_DAILY_ACTIVITY,
            A_PARKING_ZONE
        );
        ParkingZoneUsageSummaryResponse anotherParkingZoneUsageSummaryResponse = givenAParkingZoneUsageSummaryResponse(
            mostActiveDay,
            leastActiveDay,
            ANOTHER_MEAN_DAILY_ACTIVITY,
            ANOTHER_PARKING_ZONE
        );

        doReturn(aParkingZoneUsageSummaryResponse).when(parkingZoneUsageSummaryResponseAssembler).toResponse(parkingZoneUsageSummary);
        doReturn(anotherParkingZoneUsageSummaryResponse).when(parkingZoneUsageSummaryResponseAssembler).toResponse(otherParkingZoneUsageSummary);

        // when
        Response response = usageResource.getAllZonesPeriodUsage(A_FROM_DATE, A_UNTIL_DATE);

        // then
        assertThat(response.getEntity()).isEqualTo(Arrays.asList(aParkingZoneUsageSummaryResponse, anotherParkingZoneUsageSummaryResponse));
    }

    @Test
    void givenOneDate_whenGetAllZonesDailyUsage_thenReturnListOfDailyParkingZoneSummaryResponse() {
        // given
        ParkingZoneUsageSummary parkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        ParkingZoneUsageSummary otherParkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        given(usageService.generateAllZonesUsageSummary(A_FROM_DATE, A_FROM_DATE))
            .willReturn(Arrays.asList(parkingZoneUsageSummary, otherParkingZoneUsageSummary));

        DailyParkingZoneUsageSummaryResponse aDailyParkingZoneUsageSummaryResponse = givenADailyParkingZoneSummaryResponse(
            A_MEAN_DAILY_ACTIVITY,
            A_PARKING_ZONE
        );
        DailyParkingZoneUsageSummaryResponse anotherDailyParkingZoneUsageSummaryResponse = givenADailyParkingZoneSummaryResponse(
            ANOTHER_MEAN_DAILY_ACTIVITY,
            ANOTHER_PARKING_ZONE
        );
        doReturn(aDailyParkingZoneUsageSummaryResponse).when(dailyParkingZoneUsageSummaryResponseAssembler).toResponse(parkingZoneUsageSummary);
        doReturn(anotherDailyParkingZoneUsageSummaryResponse).when(dailyParkingZoneUsageSummaryResponseAssembler).toResponse(otherParkingZoneUsageSummary);

        // when
        Response response = usageResource.getAllZonesDailyUsage(A_FROM_DATE);

        // then
        assertThat(response.getEntity()).isEqualTo(Arrays.asList(aDailyParkingZoneUsageSummaryResponse, anotherDailyParkingZoneUsageSummaryResponse));
    }

    @Test
    void givenAMonthAndAYear_whenGetAllZonesMonthlyUsage_thenReturnListOfParkingZoneUsageSummaryResponse() {
        // given
        given(clock.instant()).willReturn(NOW);

        ParkingZoneUsageSummary parkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        ParkingZoneUsageSummary otherParkingZoneUsageSummary = givenAParkingZoneUsageSummary();
        given(usageService.generateAllZonesUsageSummary(FIRST_OF_MONTH, LAST_OF_MONTH))
            .willReturn(Arrays.asList(parkingZoneUsageSummary, otherParkingZoneUsageSummary));

        DatesActivitySummaryResponse mostActiveDay = givenADailyActivitySummaryResponse(A_DATE);
        DatesActivitySummaryResponse leastActiveDay = givenADailyActivitySummaryResponse(ANOTHER_DATE);
        ParkingZoneUsageSummaryResponse aParkingZoneUsageSummaryResponse = givenAParkingZoneUsageSummaryResponse(
            mostActiveDay,
            leastActiveDay,
            A_MEAN_DAILY_ACTIVITY,
            A_PARKING_ZONE
        );
        ParkingZoneUsageSummaryResponse anotherParkingZoneUsageSummaryResponse = givenAParkingZoneUsageSummaryResponse(
            mostActiveDay,
            leastActiveDay,
            ANOTHER_MEAN_DAILY_ACTIVITY,
            ANOTHER_PARKING_ZONE
        );
        doReturn(aParkingZoneUsageSummaryResponse).when(parkingZoneUsageSummaryResponseAssembler).toResponse(parkingZoneUsageSummary);
        doReturn(anotherParkingZoneUsageSummaryResponse).when(parkingZoneUsageSummaryResponseAssembler).toResponse(otherParkingZoneUsageSummary);

        // when
        Response response = usageResource.getAllZonesMonthlyUsage(A_MONTH, A_YEAR);

        // then
        assertThat(response.getEntity()).isEqualTo(Arrays.asList(aParkingZoneUsageSummaryResponse, anotherParkingZoneUsageSummaryResponse));
    }

    @Test
    void whenAddUsage_thenUsageServiceIsCalledToRecordUsage() {
        // given
        AddUsageRequest addUsageRequest = new AddUsageRequest(A_PARKING_ZONE, A_DATE);

        // when
        usageResource.addUsage(addUsageRequest);

        // then
        ArgumentCaptor<ParkingEntry> parkingEntryArgumentCaptor = ArgumentCaptor.forClass(ParkingEntry.class);
        verify(usageService).recordUsage(parkingEntryArgumentCaptor.capture());
        assertThat(parkingEntryArgumentCaptor.getValue().getEntryDate()).isEqualTo(A_DATE);
        assertTrue(parkingEntryArgumentCaptor.getValue().hasZone(A_PARKING_ZONE));
    }

    private ParkingZoneUsageSummary givenAParkingZoneUsageSummary() {
        return new ParkingZoneUsageSummary(Collections.singletonList(ANOTHER_PARKING_ENTRY), ANOTHER_PARKING_ZONE, A_FROM_DATE, A_UNTIL_DATE);
    }

    private DailyParkingZoneUsageSummaryResponse givenADailyParkingZoneSummaryResponse(int activity, ParkingZone zone) {
        return new DailyParkingZoneUsageSummaryResponse(activity, zone);
    }

    private DatesActivitySummaryResponse givenADailyActivitySummaryResponse(LocalDate date) {
        return new DatesActivitySummaryResponse(Collections.singletonList(date), 2);
    }

    private ParkingZoneUsageSummaryResponse givenAParkingZoneUsageSummaryResponse(
        DatesActivitySummaryResponse mostActiveDay,
        DatesActivitySummaryResponse leastActiveDay,
        double averageDailyActivity,
        ParkingZone zone
    ) {
        return new ParkingZoneUsageSummaryResponse(mostActiveDay, leastActiveDay, (float) averageDailyActivity, zone);
    }
}
