package ca.ulaval.glo4003.projet.base.ws.access.service;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.verify;

import ca.ulaval.glo4003.projet.base.ws.access.domain.*;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.exception.AccessNotFoundForAccessCodeException;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.AccessInformationValidator;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.SessionAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accessinformation.WeeklyAccessInformation;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Driver;
import ca.ulaval.glo4003.projet.base.ws.access.domain.driver.Sex;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncome;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeFactory;
import ca.ulaval.glo4003.projet.base.ws.access.domain.income.AccessIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.Vehicle;
import ca.ulaval.glo4003.projet.base.ws.access.domain.vehicle.VehicleType;
import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.CreditCard;
import ca.ulaval.glo4003.projet.base.ws.payment.domain.PaymentService;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.*;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccessServiceTest {

    private static final String A_NAME = "name";
    private static final Sex A_SEX = Sex.FEMALE;
    private static final String A_VEHICLE_MODEL = "vehicle model";
    private static final String A_VEHICLE_BRAND = "vehicle brand";
    private static final VehicleType A_VEHICLE_TYPE = VehicleType.ECONOMIC;
    private static final LocalDate A_BIRTHDATE = LocalDate.of(5, 3, 4);
    private static final DayOfWeek AN_ACCESS_DAY = DayOfWeek.MONDAY;
    private static final String A_VEHICLE_PLATE_NUMBER = "vehicle plate number";
    private static final int A_VEHICLE_MODEL_YEAR = 7;
    private static final int A_CODE_NUMBER = 7;
    private static final AccessCode AN_ACCESS_CODE = new AccessCode(A_CODE_NUMBER);
    private static final Instant NOW = Instant.now();
    private static final String A_CREDIT_CARD_NUMBER = "1234432543215432";
    private static final String A_CREDIT_CARD_NAME = "Antoine Lefrancois";
    private static final String A_CREDIT_CARD_EXPIRY = "11/23";
    private static final String A_CREDIT_CARD_CCV = "123";
    private static final AccessTimeType WEEKLY_ACCESS_TIME_TYPE = AccessTimeType.ONE_DAY_A_WEEK_FOR_ONE_SEMESTER;
    private static final AccessTimeType SESSION_ACCESS_TIME_TYPE = AccessTimeType.ONE_SEMESTER;
    private static final Price AN_ACCESS_PRICE = Price.of(4);
    private static final AccessIncome AN_ACCESS_INCOME = new AccessIncome(AN_ACCESS_PRICE, A_VEHICLE_TYPE, LocalDateTime.now());
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.now();

    private AccessService accessService;

    @Mock
    private AccessRepository accessRepository;

    @Mock
    private AccessInformationValidator accessInformationValidator;

    @Mock
    private AccessIncomeFactory accessIncomeFactory;

    @Mock
    private AccessIncomeRepository accessIncomeRepository;

    @Mock
    private WeeklyAccessFactory weeklyAccessFactory;

    @Mock
    private SessionAccessFactory sessionAccessFactory;

    @Mock
    private AccessGrantingValidator accessGrantingValidator;

    @Mock
    private PaymentService paymentService;

    @Mock
    private AccessPriceCalculator accessPriceCalculator;

    @Mock
    private Clock clock;

    @BeforeEach
    void setUp() {
        accessService =
            new AccessService(
                weeklyAccessFactory,
                sessionAccessFactory,
                accessIncomeFactory,
                accessIncomeRepository,
                accessInformationValidator,
                accessRepository,
                accessGrantingValidator,
                paymentService,
                accessPriceCalculator,
                clock
            );
    }

    @Test
    void whenCreateWeeklyAccess_thenAccessInformationIsValidated() {
        // given
        WeeklyAccessInformation weeklyAccessInformation = givenAWeeklyAccessInformation();
        WeeklyAccess access = givenAWeeklyAccess();
        given(weeklyAccessFactory.create(weeklyAccessInformation)).willReturn(access);

        // when
        accessService.createWeeklyAccess(weeklyAccessInformation, givenACreditCard());

        // then
        verify(accessInformationValidator).validate(weeklyAccessInformation);
    }

    @Test
    void givenAccessInformationIsValid_whenCreateWeeklyAccess_thenAccessIncomeRepositoryIsCalledToSaveNewlyCreatedAccessIncome() {
        // given
        given(accessPriceCalculator.calculatePrice(A_VEHICLE_TYPE, WEEKLY_ACCESS_TIME_TYPE)).willReturn(AN_ACCESS_PRICE);
        WeeklyAccessInformation weeklyAccessInformation = givenAWeeklyAccessInformation();
        WeeklyAccess access = givenAWeeklyAccess();
        given(weeklyAccessFactory.create(weeklyAccessInformation)).willReturn(access);
        given(accessIncomeFactory.create(AN_ACCESS_PRICE, A_VEHICLE_TYPE)).willReturn(AN_ACCESS_INCOME);

        //when
        accessService.createWeeklyAccess(weeklyAccessInformation, givenACreditCard());

        //then
        verify(accessIncomeRepository).save(AN_ACCESS_INCOME);
    }

    @Test
    void givenAccessInformationIsValid_whenCreateWeeklyAccess_thenAccessCodeIsReturned() {
        // given
        WeeklyAccessInformation weeklyAccessInformation = givenAWeeklyAccessInformation();
        WeeklyAccess access = givenAWeeklyAccess();
        given(weeklyAccessFactory.create(weeklyAccessInformation)).willReturn(access);

        // when
        AccessCode accessCode = accessService.createWeeklyAccess(weeklyAccessInformation, givenACreditCard());

        // then
        assertThat(accessCode.getCodeNumber()).isEqualTo(A_CODE_NUMBER);
    }

    @Test
    void givenAccessInformationIsValid_whenCreateWeeklyAccess_thenProcessPaymentWithCalculatedPrice() {
        // given
        WeeklyAccessInformation weeklyAccessInformation = givenAWeeklyAccessInformation();
        WeeklyAccess weeklyAccess = givenAWeeklyAccess();
        given(weeklyAccessFactory.create(weeklyAccessInformation)).willReturn(weeklyAccess);

        CreditCard creditCard = givenACreditCard();
        given(accessPriceCalculator.calculatePrice(A_VEHICLE_TYPE, WEEKLY_ACCESS_TIME_TYPE)).willReturn(AN_ACCESS_PRICE);

        // when
        accessService.createWeeklyAccess(weeklyAccessInformation, creditCard);

        // then
        verify(paymentService).processPayment(creditCard, AN_ACCESS_PRICE);
    }

    @Test
    void whenCreateSessionAccess_thenAccessInformationIsValidated() {
        // given
        SessionAccessInformation sessionAccessInformation = givenASessionAccessInformation();
        SessionAccess access = givenASessionAccess();
        given(sessionAccessFactory.create(sessionAccessInformation)).willReturn(access);

        // when
        accessService.createSessionAccess(sessionAccessInformation, givenACreditCard());

        // then
        verify(accessInformationValidator).validate(sessionAccessInformation);
    }

    @Test
    void givenAccessInformationIsValid_whenCreateSessionAccess_thenAccessIncomeRepositoryIsCalledToSaveNewlyCreatedAccessIncome() {
        // given
        given(accessPriceCalculator.calculatePrice(A_VEHICLE_TYPE, SESSION_ACCESS_TIME_TYPE)).willReturn(AN_ACCESS_PRICE);
        SessionAccessInformation sessionAccessInformation = givenASessionAccessInformation();
        SessionAccess access = givenASessionAccess();
        given(sessionAccessFactory.create(sessionAccessInformation)).willReturn(access);
        given(accessIncomeFactory.create(AN_ACCESS_PRICE, A_VEHICLE_TYPE)).willReturn(AN_ACCESS_INCOME);

        //when
        accessService.createSessionAccess(sessionAccessInformation, givenACreditCard());

        //then
        verify(accessIncomeRepository).save(AN_ACCESS_INCOME);
    }

    @Test
    void givenAccessInformationIsValid_whenCreateSessionAccess_thenAccessCodeIsReturned() {
        // given
        SessionAccessInformation sessionAccessInformation = givenASessionAccessInformation();
        SessionAccess access = givenASessionAccess();
        given(sessionAccessFactory.create(sessionAccessInformation)).willReturn(access);

        // when
        AccessCode accessCode = accessService.createSessionAccess(sessionAccessInformation, givenACreditCard());

        // then
        assertThat(accessCode.getCodeNumber()).isEqualTo(A_CODE_NUMBER);
    }

    @Test
    void givenAccessInformationIsValid_whenCreateSessionAccess_thenProcessPaymentWithCalculatedPrice() {
        // given
        SessionAccessInformation sessionAccessInformation = givenASessionAccessInformation();
        SessionAccess sessionAccess = givenASessionAccess();
        given(sessionAccessFactory.create(sessionAccessInformation)).willReturn(sessionAccess);

        CreditCard creditCard = givenACreditCard();
        given(accessPriceCalculator.calculatePrice(A_VEHICLE_TYPE, SESSION_ACCESS_TIME_TYPE)).willReturn(AN_ACCESS_PRICE);

        // when
        accessService.createSessionAccess(sessionAccessInformation, creditCard);

        // then
        verify(paymentService).processPayment(creditCard, AN_ACCESS_PRICE);
    }

    @Test
    void givenAnAccessCode_whenGrantAccess_thenValidateAssociatedAccessHasRightParametersToGrantAccess() {
        // given
        given(clock.instant()).willReturn(NOW);
        Access access = givenAWeeklyAccess();
        given(accessRepository.findByAccessCode(AN_ACCESS_CODE)).willReturn(Optional.of(access));

        // when
        accessService.grantAccess(AN_ACCESS_CODE);

        // then
        verify(accessGrantingValidator).validate(access, NOW.atZone(ZoneId.systemDefault()).toLocalDateTime());
    }

    @Test
    void givenNoAccessFoundForAccessCode_whenGrantAccess_thenThrownAccessNotFoundForAccessCodeException() {
        // given
        given(accessRepository.findByAccessCode(AN_ACCESS_CODE)).willReturn(Optional.empty());

        // then
        Assertions.assertThrows(
            AccessNotFoundForAccessCodeException.class,
            () ->
                // when
                accessService.grantAccess(AN_ACCESS_CODE)
        );
    }

    private WeeklyAccess givenAWeeklyAccess() {
        return new WeeklyAccess(new AccessCode(A_CODE_NUMBER), AN_ACCESS_DAY, new Session(A_SESSION_TYPE, A_YEAR));
    }

    private WeeklyAccessInformation givenAWeeklyAccessInformation() {
        return new WeeklyAccessInformation(
            new Driver(A_NAME, A_BIRTHDATE, A_SEX),
            new Vehicle(A_VEHICLE_MODEL, A_VEHICLE_BRAND, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE),
            AN_ACCESS_DAY,
            new Session(A_SESSION_TYPE, A_YEAR)
        );
    }

    private SessionAccess givenASessionAccess() {
        return new SessionAccess(new AccessCode(A_CODE_NUMBER), new Session(A_SESSION_TYPE, A_YEAR));
    }

    private SessionAccessInformation givenASessionAccessInformation() {
        return new SessionAccessInformation(
            new Driver(A_NAME, A_BIRTHDATE, A_SEX),
            new Vehicle(A_VEHICLE_MODEL, A_VEHICLE_BRAND, A_VEHICLE_MODEL_YEAR, A_VEHICLE_PLATE_NUMBER, A_VEHICLE_TYPE),
            new Session(A_SESSION_TYPE, A_YEAR)
        );
    }

    private CreditCard givenACreditCard() {
        return new CreditCard(A_CREDIT_CARD_NUMBER, A_CREDIT_CARD_NAME, A_CREDIT_CARD_EXPIRY, A_CREDIT_CARD_CCV);
    }
}
