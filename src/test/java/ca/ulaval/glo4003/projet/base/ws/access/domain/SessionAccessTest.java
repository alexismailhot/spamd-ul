package ca.ulaval.glo4003.projet.base.ws.access.domain;

import static com.google.common.truth.Truth.assertThat;

import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.LocalDateTime;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SessionAccessTest {

    private static final AccessCode ACCESS_CODE = new AccessCode(4);
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.of(2021);
    private static final Session A_SESSION = new Session(A_SESSION_TYPE, A_YEAR);
    private static final LocalDateTime A_TIME_IN_SESSION = LocalDateTime.of(A_YEAR.getValue(), 10, 4, 2, 2);
    private static final LocalDateTime A_TIME_ANOTHER_SESSION = LocalDateTime.of(A_YEAR.plusYears(1).getValue(), 10, 3, 2, 2);

    private SessionAccess sessionAccess;

    @BeforeEach
    void setUp() {
        sessionAccess = new SessionAccess(ACCESS_CODE, A_SESSION);
    }

    @Test
    void givenTimeIsInSession_whenIsValidOn_thenReturnTrue() {
        // given

        // when
        boolean isValidOn = sessionAccess.isValidOn(A_TIME_IN_SESSION);

        // then
        assertThat(isValidOn).isTrue();
    }

    @Test
    void givenTimeIsNotInSession_whenIsValidOn_thenReturnFalse() {
        // given

        // when
        boolean isValidOn = sessionAccess.isValidOn(A_TIME_ANOTHER_SESSION);

        // then
        assertThat(isValidOn).isFalse();
    }
}
