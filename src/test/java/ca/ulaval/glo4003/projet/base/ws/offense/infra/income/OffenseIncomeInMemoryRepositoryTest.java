package ca.ulaval.glo4003.projet.base.ws.offense.infra.income;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

import ca.ulaval.glo4003.projet.base.ws.money.domain.Price;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncome;
import ca.ulaval.glo4003.projet.base.ws.offense.domain.income.OffenseIncomeRepository;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.UniversityYear;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OffenseIncomeInMemoryRepositoryTest {

    private static final Price A_PRICE = Price.of(3);
    private static final OffenseIncome AN_OFFENSE_INCOME = new OffenseIncome(Price.of(420), LocalDateTime.now());
    private static final OffenseIncome ANOTHER_OFFENSE_INCOME = new OffenseIncome(Price.of(422), LocalDateTime.now());
    private static final LocalDateTime A_LOCAL_DATE_TIME = LocalDateTime.of(5, 5, 5, 5, 5);
    private static final LocalDateTime ANOTHER_LOCAL_DATE_TIME = LocalDateTime.of(100, 5, 5, 5, 5);

    private OffenseIncomeRepository offenseIncomeRepository;

    @Mock
    private UniversityYear universityYear;

    @BeforeEach
    void setUp() {
        offenseIncomeRepository = new OffenseIncomeInMemoryRepository();
    }

    @Test
    void givenOffenseIncomes_whenGetAll_thenReturnAllOffenseIncomes() {
        // given
        offenseIncomeRepository.save(AN_OFFENSE_INCOME);
        offenseIncomeRepository.save(ANOTHER_OFFENSE_INCOME);

        // when
        List<OffenseIncome> offenseIncomes = offenseIncomeRepository.getAll();

        // then
        assertThat(offenseIncomes).containsExactlyElementsIn(Arrays.asList(AN_OFFENSE_INCOME, ANOTHER_OFFENSE_INCOME));
    }

    @Test
    void givenOffenseIncomes_whenFindByUniversityYear_thenReturnOffenseIncomesInUniversityYear() {
        // given
        doReturn(true).when(universityYear).includes(A_LOCAL_DATE_TIME.toLocalDate());
        doReturn(false).when(universityYear).includes(ANOTHER_LOCAL_DATE_TIME.toLocalDate());

        OffenseIncome income = new OffenseIncome(A_PRICE, A_LOCAL_DATE_TIME);
        OffenseIncome anotherIncome = new OffenseIncome(A_PRICE, ANOTHER_LOCAL_DATE_TIME);
        offenseIncomeRepository.save(income);
        offenseIncomeRepository.save(anotherIncome);

        // when
        List<OffenseIncome> offenseIncomes = offenseIncomeRepository.findByUniversityYear(universityYear);

        // then
        assertThat(offenseIncomes).containsExactlyElementsIn(Collections.singletonList(income));
    }
}
