package ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.validator;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import ca.ulaval.glo4003.projet.base.ws.parking.domain.DeliveryMethod;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.ParkingZone;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.exception.pass.passinformation.InvalidEmailException;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.ParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.parking.domain.pass.passinformation.WeeklyParkingPassInformation;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmailValidatorTest {

    private static final String A_VALID_EMAIL = "antoine.lefrancois@gmail.com";
    private static final String AN_INVALID_EMAIL = "antoine.lefrancoisgmail.com";
    private static final ParkingZone A_PARKING_ZONE = ParkingZone.ZONE_1;
    private static final DeliveryMethod EMAIL_DELIVERY_METHOD = DeliveryMethod.EMAIL;
    private static final DayOfWeek A_PARKING_DAY = DayOfWeek.MONDAY;
    private static final Session AN_UNIVERSITY_SESSION = new Session(SessionType.SUMMER, Year.of(32));

    private EmailValidator emailValidator;

    @BeforeEach
    void setUp() {
        emailValidator = new EmailValidator();
    }

    @Test
    void givenAValidEmailAddress_whenValidate_thenDoesNotThrowInvalidEmailException() {
        // given
        ParkingPassInformation parkingPassInformation = givenAParkingPassInformation(A_VALID_EMAIL);

        // then
        assertDoesNotThrow(
            () ->
                // when
                emailValidator.validate(parkingPassInformation)
        );
    }

    @Test
    void givenAnInvalidEmailAddress_whenValidate_thenThrowsInvalidEmailException() {
        // given
        ParkingPassInformation parkingPassInformation = givenAParkingPassInformation(AN_INVALID_EMAIL);

        // then
        assertThrows(
            InvalidEmailException.class,
            () ->
                // when
                emailValidator.validate(parkingPassInformation)
        );
    }

    private ParkingPassInformation givenAParkingPassInformation(String email) {
        return new WeeklyParkingPassInformation(A_PARKING_ZONE, EMAIL_DELIVERY_METHOD, email, A_PARKING_DAY, AN_UNIVERSITY_SESSION);
    }
}
