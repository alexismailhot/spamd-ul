package ca.ulaval.glo4003.projet.base.ws.access.infra;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth8.assertThat;

import ca.ulaval.glo4003.projet.base.ws.access.domain.Access;
import ca.ulaval.glo4003.projet.base.ws.access.domain.WeeklyAccess;
import ca.ulaval.glo4003.projet.base.ws.access.domain.accesscode.AccessCode;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.Session;
import ca.ulaval.glo4003.projet.base.ws.universityyear.domain.SessionType;
import java.time.DayOfWeek;
import java.time.Year;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccessInMemoryRepositoryTest {

    private static final DayOfWeek AN_ACCESS_DAY = DayOfWeek.MONDAY;
    private static final int A_CODE_NUMBER = 7;
    private static final AccessCode AN_ACCESS_CODE = new AccessCode(A_CODE_NUMBER);
    private static final SessionType A_SESSION_TYPE = SessionType.FALL;
    private static final Year A_YEAR = Year.now();
    private static final Session A_SESSION = new Session(A_SESSION_TYPE, A_YEAR);

    private AccessInMemoryRepository accessRepository;

    @BeforeEach
    void setUp() {
        accessRepository = new AccessInMemoryRepository();
    }

    @Test
    void givenAssociatedAccessWasSaved_whenFindByAccessCode_thenReturnAccess() {
        // given
        Access savedAccess = givenAnAccess();
        accessRepository.save(savedAccess);

        // when
        Optional<Access> access = accessRepository.findByAccessCode(AN_ACCESS_CODE);

        // then
        assertThat(access.get()).isEqualTo(savedAccess);
    }

    @Test
    void givenNoAssociatedAccessSaved_whenFindByAccessCode_thenReturnEmptyOptional() {
        // given

        // when
        Optional<Access> access = accessRepository.findByAccessCode(AN_ACCESS_CODE);

        // then
        assertThat(access).isEmpty();
    }

    private Access givenAnAccess() {
        return new WeeklyAccess(AN_ACCESS_CODE, AN_ACCESS_DAY, A_SESSION);
    }
}
