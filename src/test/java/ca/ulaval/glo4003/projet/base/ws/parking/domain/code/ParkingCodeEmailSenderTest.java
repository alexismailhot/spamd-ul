package ca.ulaval.glo4003.projet.base.ws.parking.domain.code;

import static org.mockito.Mockito.verify;

import ca.ulaval.glo4003.projet.base.ws.email.domain.EmailSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingCodeEmailSenderTest {

    private static final int A_PARKING_CODE_VALUE = 420;
    private static final ParkingCode A_PARKING_CODE = new ParkingCode(A_PARKING_CODE_VALUE);
    private static final String AN_EMAIL = "antoine.lefrancois@ift.ulaval.ca";
    private static final String A_PARKING_CODE_EMAIL_SUBJECT = "Votre code de vignette";
    private static final String A_PARKING_CODE_EMAIL_BODY_TEMPLATE = "Votre code de vignette est: %s";

    private ParkingCodeEmailSender parkingCodeEmailSender;

    @Mock
    private EmailSender emailSender;

    @BeforeEach
    void setUp() {
        parkingCodeEmailSender = new ParkingCodeEmailSender(emailSender, A_PARKING_CODE, AN_EMAIL);
    }

    @Test
    void whenSend_thenEmailSenderIsCalledToSendEmail() {
        // when
        parkingCodeEmailSender.send();

        // then
        verify(emailSender).send(A_PARKING_CODE_EMAIL_SUBJECT, String.format(A_PARKING_CODE_EMAIL_BODY_TEMPLATE, A_PARKING_CODE_VALUE), AN_EMAIL);
    }
}
