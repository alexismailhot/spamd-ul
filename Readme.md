# SPAMD-UL

## What is it
* School project carried out as part of the _"Architecture logicielle (Software architecture)"_ course in fall 2020, at Laval University
* **Project definition:** Advanced parking payment system for the sustainable development program of Laval University.
* **Features:** Purchase of parking passes and campus access passes, management of campus access, management of parking infractions, management of parking use and management of parking funds in order to re-invest them in the sustainable development program.

##  How to use
* With Java 8 and Maven installed and the JAVA_HOME variable configured, run `mvn compile` then` mvn exec: java`

## Technologies
* Java
* Maven
* Jetty
* Jersey
* Jackson
* Quartz
* Mockito
* Google Truth
* Swagger
